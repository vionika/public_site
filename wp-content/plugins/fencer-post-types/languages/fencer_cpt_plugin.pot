msgid ""
msgstr ""
"Project-Id-Version: Fencer Custom Post Types 1.0\n"
"POT-Creation-Date: 2014-03-06 20:07+0300\n"
"PO-Revision-Date: 2014-03-06 20:07+0300\n"
"Last-Translator: dedalx <dedalx.rus@gmail.com>\n"
"Language-Team: \n"
"Language: English\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-SearchPath-0: .\n"

#: fencer-post-types.php:50
msgid "Slider"
msgstr ""

#: fencer-post-types.php:51
msgid "Slides"
msgstr ""

#: fencer-post-types.php:53 fencer-post-types.php:56
msgid "Add New Slide"
msgstr ""

#: fencer-post-types.php:54 fencer-post-types.php:75 fencer-post-types.php:97
msgid "Not found"
msgstr ""

#: fencer-post-types.php:55
msgid "No slides found in trash"
msgstr ""

#: fencer-post-types.php:57
msgid "All Slides"
msgstr ""

#: fencer-post-types.php:71 fencer-post-types.php:72
msgid "Portfolio items"
msgstr ""

#: fencer-post-types.php:74 fencer-post-types.php:77
msgid "Add New item"
msgstr ""

#: fencer-post-types.php:76
msgid "No Portfolio items found in trash"
msgstr ""

#: fencer-post-types.php:78
msgid "All Portfolio items"
msgstr ""

#: fencer-post-types.php:93 fencer-post-types.php:94
msgid "Our team"
msgstr ""

#: fencer-post-types.php:96
msgid "New Team member"
msgstr ""

#: fencer-post-types.php:98
msgid "No team members found in trash"
msgstr ""

#: fencer-post-types.php:99
msgid "Add New Team member"
msgstr ""

#: fencer-post-types.php:100
msgid "All Team members"
msgstr ""

#: fencer-post-types.php:133
msgid "Member photo"
msgstr ""

#: fencer-post-types.php:146
msgid "Team member details"
msgstr ""

#: fencer-post-types.php:167
msgid "Member position: "
msgstr ""

#: fencer-post-types.php:170
msgid "Social links"
msgstr ""

#: fencer-post-types.php:246
msgid "Portfolio grid item image"
msgstr ""

#: fencer-post-types.php:259
msgid "Portfolio item page details and settings"
msgstr ""

#: fencer-post-types.php:280
msgid "Project url: "
msgstr ""

#: fencer-post-types.php:287
msgid "Brand title: "
msgstr ""

#: fencer-post-types.php:294
msgid "Brand url: "
msgstr ""

#: fencer-post-types.php:301
msgid "Layout: "
msgstr ""

#: fencer-post-types.php:306
msgid "Normal image size, description at the right"
msgstr ""

#: fencer-post-types.php:310
msgid "Full width image size, description at the bottom"
msgstr ""

#: fencer-post-types.php:318
msgid "Disable slider for this item images (show images in column)"
msgstr ""

#: fencer-post-types.php:326
msgid "Show item images or slider fullwidth (liquid)"
msgstr ""

#: fencer-post-types.php:334
msgid "Use original images sizes for all item images (don't crop images)"
msgstr ""

#: fencer-post-types.php:343
msgid "Don't show 'Portfolio grid item image' on item page"
msgstr ""

#: fencer-post-types.php:352
msgid "Disable social share counters and buttons on this item page"
msgstr ""

#: fencer-post-types.php:374
msgid "Sidebar position: "
msgstr ""

#: fencer-post-types.php:376
msgid "Use theme control panel settings"
msgstr ""

#: fencer-post-types.php:377
msgid "Left"
msgstr ""

#: fencer-post-types.php:378
msgid "Right"
msgstr ""

#: fencer-post-types.php:379
msgid "Disable sidebar"
msgstr ""

#: fencer-post-types.php:497
msgid ""
"Slide background image or video placeholder image (use large image for good "
"results, 1600px+ width recommended)"
msgstr ""

#: fencer-post-types.php:510
msgid "Slide settings"
msgstr ""

#: fencer-post-types.php:538
msgid "Slide button text: "
msgstr ""

#: fencer-post-types.php:542
msgid "Slide button url: "
msgstr ""

#: fencer-post-types.php:547
msgid "Slide button 2 text: "
msgstr ""

#: fencer-post-types.php:551
msgid "Slide button 2 url: "
msgstr ""

#: fencer-post-types.php:558
msgid "Video background source url (.webm): "
msgstr ""

#: fencer-post-types.php:564
msgid "Video background source url (.mp4): "
msgstr ""
