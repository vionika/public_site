<?php
/*
Plugin Name: Fencer v2.0 Custom Post Types
Plugin URI: http://dedalx.com/
Description: Custom Post Types for Fencer WordPress theme
Author: dedalx
Version: 2.0
Author URI: http://dedalx.com/
Text Domain: fencer-cpt
License: General Public License
*/

// Define current version constant
define( 'fencer_CPT_VERSION', '2.0' );

// Define current WordPress version constant
define( 'fencer_CPT_WP_VERSION', get_bloginfo( 'version' ) );

//load translated strings
add_action( 'init', 'fencer_cpt_load_textdomain' );

//process custom taxonomies
add_action( 'init', 'fencer_cpt_create_custom_taxonomies', 0 );

//process custom post types
add_action( 'init', 'fencer_cpt_create_custom_post_types', 0 );

//process custom meta boxes
add_action( 'init', 'fencer_cpt_create_metaboxes', 0 );


//flush rewrite rules on deactivation
register_deactivation_hook( __FILE__, 'fencer_cpt_deactivation' );

function fencer_cpt_deactivation() {
	// Clear the permalinks to remove our post type's rules
	flush_rewrite_rules();
}

function fencer_cpt_load_textdomain() {
	load_plugin_textdomain( 'fencer_cpt_plugin', false, basename( dirname( __FILE__ ) ) . '/languages' );
}

function fencer_cpt_create_custom_post_types() {
	
	function posts_type() {
	register_post_type( 'portfolio',
	array(
	    'labels' => array(
	    'name' => __( 'Portfolio items', 'fencer_cpt_plugin' ),
	    'singular_name' => __( 'Portfolio items', 'fencer_cpt_plugin' ),
	    'has_archive' => true,
	    'add_new' => __( 'Add New item', 'fencer_cpt_plugin' ),
	    'not_found' => __( 'Not found', 'fencer_cpt_plugin' ),
	    'not_found_in_trash' => __( 'No Portfolio items found in trash', 'fencer_cpt_plugin' ),
	    'add_new_item' => __( 'Add New item', 'fencer_cpt_plugin' ),
	    'all_items' => __( 'All Portfolio items', 'fencer_cpt_plugin' ),
	    ),
	    'public' => true,
	    'has_archive' => true,
	    'supports' => array(
	        'title',
	        'thumbnail',
	        'editor',
	        'comments'
	    ),
	    'menu_icon' => plugin_dir_url('fencer-post-types').'fencer-post-types/img/admin/icon-portfolio.png'
	));
	register_post_type( 'team',
	array(
	    'labels' => array(
	    'name' => __( 'Our team', 'fencer_cpt_plugin' ),
	    'singular_name' => __( 'Our team', 'fencer_cpt_plugin' ),
	    'has_archive' => true,
	    'add_new' => __( 'New Team member', 'fencer_cpt_plugin' ),
	    'not_found' => __('Not found', 'fencer_cpt_plugin' ),
	    'not_found_in_trash' => __( 'No team members found in trash', 'fencer_cpt_plugin' ),
	    'add_new_item' => __( 'Add New Team member', 'fencer_cpt_plugin' ),
	    'all_items' => __( 'All Team members', 'fencer_cpt_plugin' ),
	    ),
	    'public' => true,
	    'has_archive' => true,
	    'supports' => array(
	        'title',
	        'thumbnail',
	        'editor'
	    ),
	    'menu_icon' => plugin_dir_url('fencer-post-types').'fencer-post-types/img/admin/icon-team.png'
	));
	}
	add_action( 'init', 'posts_type' );	
}

function fencer_cpt_create_custom_taxonomies() {
	function register_portfolio_taxonomy() {
	    register_taxonomy("portfolio_filter", array("portfolio"), array("hierarchical" => true, "label" => "Project category", "singular_label" => "Project Category", "rewrite" => true, "show_admin_column" => true));
	}
	add_action( 'init', 'register_portfolio_taxonomy');
}

function fencer_cpt_create_metaboxes() {

	/**
	* Custom metabox for Our Team custom post type
	**/

	function team_image_box() {

	    remove_meta_box('postimagediv', 'team', 'side');
	    
	    add_meta_box('postimagediv', __('Member photo', 'fencer_cpt_plugin'), 'post_thumbnail_meta_box', 'team', 'normal', 'high');

	}
	add_action('do_meta_boxes', 'team_image_box');

	function team_settings_box() {

	    $screens = array( 'team' );

	    foreach ( $screens as $screen ) {

	        add_meta_box(
	            'team_settings_box',
	            __( 'Team member details', 'fencer_cpt_plugin' ),
	            'team_settings_inner_box',
	            $screen,
	            'normal'
	        );
	    }
	}
	add_action( 'add_meta_boxes', 'team_settings_box' );

	function team_settings_inner_box( $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'team_settings_inner_box', 'team_settings_inner_box_nonce' );

		/*
		* Use get_post_meta() to retrieve an existing value
		* from the database and use the value for the form.
		*/
		$value_team_subtitle = get_post_meta( $post->ID, '_team_subtitle_value', true );
		
		echo '<label for="team_subtitle" style="width: 200px; display: inline-block;">';
		_e( "Member position: ", 'fencer_cpt_plugin' );
		echo '</label> ';
		echo '<input type="text" id="team_subtitle" name="team_subtitle" value="' . esc_attr( $value_team_subtitle ) . '" style="width: 100%" />';
		echo '<p><strong>'.__( 'Social links', 'fencer_cpt_plugin').':</strong></p>';

		$social_links_arr = array( 'facebook', 'twitter', 'google-plus', 'linkedin', 'tumblr', 'dribbble' );

		foreach ( $social_links_arr as $social_link ) {
			${"value_team_social_" . $social_link} = get_post_meta( $post->ID, '_team_social_'.$social_link.'_value', true );

			echo '<p><label for="team_social_'.$social_link.'" style="width: 100px; display: inline-block;">';
		    echo ucfirst($social_link).': ';
			echo '</label> ';
			echo '<input type="text" id="team_social_'.$social_link.'" name="team_social_'.$social_link.'" value="' . esc_attr( ${"value_team_social_" . $social_link} ) . '" style="width: 100%"/></p>';
		}

	}

	function team_settings_save_postdata( $post_id ) {

		/*
		* We need to verify this came from the our screen and with proper authorization,
		* because save_post can be triggered at other times.
		*/

		// Check if our nonce is set.
		if ( ! isset( $_POST['team_settings_inner_box_nonce'] ) )
		return $post_id;

		$nonce = $_POST['team_settings_inner_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'team_settings_inner_box' ) )
		  return $post_id;

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		  return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) )
		    return $post_id;

		} else {

		if ( ! current_user_can( 'edit_post', $post_id ) )
		    return $post_id;
		}

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['team_subtitle'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_team_subtitle_value', $mydata );

		/* OK, its safe for us to save the data now. */
		$social_links_arr = array( 'facebook', 'twitter', 'google-plus', 'linkedin', 'tumblr', 'dribbble' );

		foreach ( $social_links_arr as $social_link ) {
			// Sanitize user input.
			$mydata = sanitize_text_field( $_POST['team_social_'.$social_link] );

			// Update the meta field in the database.
			update_post_meta( $post_id, '_team_social_'.$social_link.'_value', $mydata );
		}
		
	}
	add_action( 'save_post', 'team_settings_save_postdata' );

	/**
	* Custom metabox for Portfolio custom post type
	**/

	function portfolio_image_box() {

	    remove_meta_box('postimagediv', 'portfolio', 'side');
	    
	    add_meta_box('postimagediv', __('Portfolio grid item image', 'fencer_cpt_plugin'), 'post_thumbnail_meta_box', 'portfolio', 'normal', 'high');

	}
	add_action('do_meta_boxes', 'portfolio_image_box');

	function portfolio_settings_box() {

	    $screens = array( 'portfolio' );

	    foreach ( $screens as $screen ) {

	        add_meta_box(
	            'portfolio_settings_box',
	            __( 'Portfolio item page details and settings', 'fencer_cpt_plugin' ),
	            'portfolio_settings_inner_box',
	            $screen,
	            'normal'
	        );
	    }
	}
	add_action( 'add_meta_boxes', 'portfolio_settings_box' );

	function portfolio_settings_inner_box( $post ) {

		// Add an nonce field so we can check for it later.
		wp_nonce_field( 'portfolio_settings_inner_box', 'portfolio_settings_inner_box_nonce' );

		/*
		* Use get_post_meta() to retrieve an existing value
		* from the database and use the value for the form.
		*/
		$value_portfolio_url = get_post_meta( $post->ID, '_portfolio_url_value', true );
		
		echo '<p><label for="portfolio_url" style="width: 100px; display: inline-block;">';
		_e( "Project url: ", 'fencer_cpt_plugin' );
		echo '</label> ';
		echo '<input type="text" id="portfolio_url" name="portfolio_url" value="' . esc_attr( $value_portfolio_url ) . '" size="30" /></p>';

		$value_portfolio_brand = get_post_meta( $post->ID, '_portfolio_brand_value', true );
		
		echo '<p><label for="portfolio_brand" style="width: 100px; display: inline-block;">';
		_e( "Brand title: ", 'fencer_cpt_plugin' );
		echo '</label> ';
		echo '<input type="text" id="portfolio_brand" name="portfolio_brand" value="' . esc_attr( $value_portfolio_brand ) . '" size="30" /></p>';

		$value_portfolio_brand_url = get_post_meta( $post->ID, '_portfolio_brand_url_value', true );
		
		echo '<p><label for="portfolio_brand_url" style="width: 100px; display: inline-block;">';
		_e( "Brand url: ", 'fencer_cpt_plugin' );
		echo '</label> ';
		echo '<input type="text" id="portfolio_brand_url" name="portfolio_brand_url" value="' . esc_attr( $value_portfolio_brand_url ) . '" size="30" /></p>';

		$value_portfolio_display_type = get_post_meta( $post->ID, '_portfolio_display_type_value', true );
		
		echo '<p><label for="portfolio_display_type" style="width: 100px; display: inline-block;">';
		_e( "Layout: ", 'fencer_cpt_plugin' );
		echo '</label> ';
		echo '<label><input type="radio" name="portfolio_display_type" value="0"'; 
		if($value_portfolio_display_type == 0) { echo 'checked'; }
		echo ' >';
		_e( "Normal image size, description at the right", 'fencer_cpt_plugin' ).'</label>';
		echo ' &nbsp;<label><input type="radio" name="portfolio_display_type" value="1"';
		if($value_portfolio_display_type == 1) { echo 'checked'; }
		echo '>'; 
		_e( "Full width image size, description at the bottom", 'fencer_cpt_plugin' ).'</label>';

		$value_portfolio_disableslider = get_post_meta( $post->ID, '_portfolio_disableslider_value', true );

		$checked = '';
		if( $value_portfolio_disableslider == true ) { 
			$checked = 'checked = "checked"';
		}
		echo '<p><input type="checkbox" id="portfolio_disableslider" name="portfolio_disableslider" '.$checked.' /> <label for="portfolio_disableslider">'.__( "Disable slider for this item images (show images in column)", 'fencer' ).'</label></p>';

		$value_portfolio_fullwidthslider = get_post_meta( $post->ID, '_portfolio_fullwidthslider_value', true );

		$checked = '';
		if( $value_portfolio_fullwidthslider == true ) { 
			$checked = 'checked = "checked"';
		}
		echo '<p><input type="checkbox" id="portfolio_fullwidthslider" name="portfolio_fullwidthslider" '.$checked.' /> <label for="portfolio_fullwidthslider">'.__( "Show item images or slider fullwidth (liquid)", 'fencer' ).'</label></p>';

		$value_portfolio_original_image_sizes = get_post_meta( $post->ID, '_portfolio_original_image_sizes_value', true );

		$checked = '';
		if( $value_portfolio_original_image_sizes == true ) { 
			$checked = 'checked = "checked"';
		}
		echo '<p><input type="checkbox" id="portfolio_original_image_sizes" name="portfolio_original_image_sizes" '.$checked.' /> <label for="portfolio_original_image_sizes">'.__( "Use original images sizes for all item images (don't crop images)", 'fencer' ).'</label></p>';


		$value_portfolio_hide_1st_image_from_slider = get_post_meta( $post->ID, '_portfolio_hide_1st_image_from_slider_value', true );

		$checked = '';
		if( $value_portfolio_hide_1st_image_from_slider == true ) { 
			$checked = 'checked = "checked"';
		}
		echo '<p><input type="checkbox" id="portfolio_hide_1st_image_from_slider" name="portfolio_hide_1st_image_from_slider" '.$checked.' /> <label for="portfolio_hide_1st_image_from_slider">'.__( "Don't show 'Portfolio grid item image' on item page", 'fencer' ).'</label></p>';

		$value_portfolio_socialshare_disable = get_post_meta( $post->ID, '_portfolio_socialshare_disable_value', true );

		$checked = '';
		if( $value_portfolio_socialshare_disable == true ) { 
			$checked = 'checked = "checked"';
		}

		echo '<p><input type="checkbox" id="portfolio_socialshare_disable" name="portfolio_socialshare_disable" '.$checked.' /> <label for="portfolio_socialshare_disable">'.__( "Disable social share counters and buttons on this item page", 'fencer' ).'</label></p>';

		$value_portfolio_sidebarposition = get_post_meta( $post->ID, '_portfolio_sidebarposition_value', true );

		$selected_1 = '';
		$selected_2 = '';
		$selected_3 = '';
		$selected_4 = '';

		if($value_portfolio_sidebarposition == 0) {
			$selected_1 = ' selected';
		}
		if($value_portfolio_sidebarposition == "left") {
			$selected_2 = ' selected';
		}
		if($value_portfolio_sidebarposition == "right") {
			$selected_3 = ' selected';
		}
		if($value_portfolio_sidebarposition == "disable") {
			$selected_4 = ' selected';
		}

		echo '<p><label for="portfolio_sidebarposition" style="display: inline-block; width: 150px;">'.__( "Sidebar position: ", 'fencer_cpt_plugin' ).'</label>';
		echo '<select name="portfolio_sidebarposition" id="portfolio_sidebarposition">
		    <option value="0"'.$selected_1.'>'.__( "Use theme control panel settings", 'fencer_cpt_plugin' ).'</option>
		    <option value="left"'.$selected_2.'>'.__( "Left", 'fencer_cpt_plugin' ).'</option>
		    <option value="right"'.$selected_3.'>'.__( "Right", 'fencer_cpt_plugin' ).'</option>
		    <option value="disable"'.$selected_4.'>'.__( "Disable sidebar", 'fencer_cpt_plugin' ).'</option>
		</select></p>';
	}

	function portfolio_settings_save_postdata( $post_id ) {

		/*
		* We need to verify this came from the our screen and with proper authorization,
		* because save_post can be triggered at other times.
		*/

		// Check if our nonce is set.
		if ( ! isset( $_POST['portfolio_settings_inner_box_nonce'] ) )
		return $post_id;

		$nonce = $_POST['portfolio_settings_inner_box_nonce'];

		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $nonce, 'portfolio_settings_inner_box' ) )
		  return $post_id;

		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) 
		  return $post_id;

		// Check the user's permissions.
		if ( 'page' == $_POST['post_type'] ) {

		if ( ! current_user_can( 'edit_page', $post_id ) )
		    return $post_id;

		} else {

		if ( ! current_user_can( 'edit_post', $post_id ) )
		    return $post_id;
		}

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_url'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_url_value', $mydata );

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_brand'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_brand_value', $mydata );

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_brand_url'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_brand_url_value', $mydata );

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_display_type'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_display_type_value', $mydata );
		
		if(!isset($_POST['portfolio_disableslider'])) $_POST['portfolio_disableslider'] = false;

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_disableslider'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_disableslider_value', $mydata );
		
		if(!isset($_POST['portfolio_fullwidthslider'])) $_POST['portfolio_fullwidthslider'] = false;

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_fullwidthslider'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_fullwidthslider_value', $mydata );

		if(!isset($_POST['portfolio_hide_1st_image_from_slider'])) $_POST['portfolio_hide_1st_image_from_slider'] = false;

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_hide_1st_image_from_slider'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_hide_1st_image_from_slider_value', $mydata );

		if(!isset($_POST['portfolio_socialshare_disable'])) $_POST['portfolio_socialshare_disable'] = false;

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_socialshare_disable'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_socialshare_disable_value', $mydata );

		if(!isset($_POST['portfolio_original_image_sizes'])) $_POST['portfolio_original_image_sizes'] = false;

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_original_image_sizes'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_original_image_sizes_value', $mydata );

		// Sanitize user input.
		$mydata = sanitize_text_field( $_POST['portfolio_sidebarposition'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, '_portfolio_sidebarposition_value', $mydata );
		
	}
	add_action( 'save_post', 'portfolio_settings_save_postdata' );

	/**
	 * Disable VIEW buttons for custom post types that does not have single pages
	 **/
	function fencer_cpt_hide_view_button() {
	 	$hide_view_in_post_types = Array('portfolio', 'team');
		$current_screen = get_current_screen();
		
		foreach ( $hide_view_in_post_types as $post_type ) {
			if( $current_screen->post_type === $post_type ) {
			echo '<style>#edit-slug-box{display: none;}</style>';
			}
		}
		return; 
	}
	add_action( 'admin_head', 'fencer_cpt_hide_view_button' );
	 
	/**
	* Removes the 'view' link in the admin bar
	*
	*/
	function fencer_cpt_remove_view_button_admin_bar() {
	 
		global $wp_admin_bar;
		$hide_view_in_post_types = Array('portfolio', 'team');
		foreach ( $hide_view_in_post_types as $post_type ) {
			if( get_post_type() === $post_type){
				$wp_admin_bar->remove_menu('view');
			}
		}
	}
	add_action( 'wp_before_admin_bar_render', 'fencer_cpt_remove_view_button_admin_bar' );
	 
	function fencer_cpt_remove_view_row_action( $actions ) {
	 	$hide_view_in_post_types = Array('portfolio', 'team');
		foreach ( $hide_view_in_post_types as $post_type ) {
			if( get_post_type() === $post_type ){
				unset( $actions['view'] );
			}
		}
		return $actions;
	 
	}
	add_filter( 'post_row_actions', 'fencer_cpt_remove_view_row_action', 10, 1 );
}
