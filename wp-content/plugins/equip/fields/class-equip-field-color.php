<?php

/**
 * Colorpicker
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Color extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = $settings['key'];

		printf( '<input name="%1$s" id="%2$s" value="%3$s" %4$s>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			$value,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function enqueue() {
		wp_enqueue_script( 'wp-color-picker' );
		wp_enqueue_style( 'wp-color-picker' );
	}

	public function default_attr() {
		return array(
			'type'  => 'text',
			'class' => 'equip-color'
		);
	}

}