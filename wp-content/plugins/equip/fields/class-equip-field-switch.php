<?php

/**
 * Switch
 *
 * @link https://jqueryui.com/button/#radio
 *
 * @author  8guild
 * @package Equip\Field
 */
class Equip_Field_Switch extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = $settings['key'];

		$on  = empty( $settings['on'] ) ? esc_html__( 'On', 'equip' ) : $settings['on'];
		$off = empty( $settings['off'] ) ? esc_html__( 'Off', 'equip' ) : $settings['off'];

		$settings['attr']['data-on'] = $on;
		$settings['attr']['data-off'] = $off;

		printf( '<input type="checkbox" name="%1$s" id="%2$s" value="%3$s" %4$s %5$s>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			(int) $value,
			$this->get_field_attr( $settings['attr'] ),
			checked( $value, true, false )
		);
	}

	public function enqueue() {
		if ( ! wp_script_is( 'jquery-switcher' ) ) {
			wp_enqueue_script( 'jquery-switcher', EQUIP_ASSETS_URI . '/js/switcher.min.js', array( 'jquery' ), null, true );
		}

		if ( ! wp_style_is( 'jquery-switcher' ) ) {
			wp_enqueue_style( 'jquery-switcher', EQUIP_ASSETS_URI . '/css/switcher.min.css', array(), null );
		}
	}

	public function sanitize( $value, $settings, $slug ) {
		return absint( $value );
	}

	public function escape( $value, $settings, $slug ) {
		return filter_var( $value, FILTER_VALIDATE_BOOLEAN );
	}

	public function default_attr() {
		return array(
			'class'         => 'equip-switch',
			'data-style'    => 'default',
			'data-language' => 'en',
		);
	}
}