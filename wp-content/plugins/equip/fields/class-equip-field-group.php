<?php

/**
 * Field "group"
 *
 * A special field for grouping other fields
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Group extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$slug     = sprintf( '%1$s[%2$s]', $slug, $settings['key'] );
		$contents = array();
		if ( array_key_exists( 'fields', $settings ) && ! empty( $settings['fields'] ) ) {
			$contents = $settings['fields'];
		}

		$render = Equip::engine( 'group' );
		$render->render( $slug, $contents, $value );
	}

	/**
	 * Escaping
	 *
	 * Note! No need to escape values of this field, because each field
	 * will be passed to their own escaping function in the next round
	 * or rendering. All fields are passed into the {@see Equip_Render::render}
	 *
	 * @param mixed  $value    Values
	 * @param array  $settings Field settings
	 * @param string $slug     Element name
	 *
	 * @return mixed
	 */
	public function escape( $value, $settings, $slug ) {
		return $value;
	}

	/**
	 * Sanitizing
	 *
	 * Note! Sanitize is required. Fortunately it is enough to pass
	 * values to {@see Equip_Sanitizer::bulk_sanitize}
	 *
	 * @param mixed  $value    Values
	 * @param array  $settings Field settings
	 * @param string $slug     Element name
	 *
	 * @return mixed
	 */
	public function sanitize( $value, $settings, $slug ) {
		// grouped fields
		$contents = ! empty( $settings['fields'] ) ? $settings['fields'] : array();
		$value    = Equip_Sanitizer::bulk_sanitize( $value, $contents, $slug );

		return $value;
	}

}