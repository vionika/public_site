<?php

/**
 * Shows the control for multiple image selection
 *
 * @author 8guild
 */
class Equip_Field_Images extends Equip_Field {

	/**
	 * Constructor
	 */
	public function __construct() {}

	/**
	 * Display the media control for adding single image with preview.
	 *
	 * @param string $slug     Option unique name
	 * @param array  $settings Field arguments
	 * @param mixed  $value    Field value.
	 */
	public function render( $slug, $settings, $value ) {
		$key   = $settings['key'];
		$name  = $this->get_field_name( $slug, $key );

		// For this field $value should be a comma separated list of IDs
		if ( empty( $value ) ) {
			$value       = '';
			$attachments = array();
		} else {
			$attachments = explode( ',', $value );
			$attachments = array_filter( $attachments, 'is_numeric' );
		}

		$media_atts = $this->get_media_attr( $settings );
		$wrap_atts  = $this->get_field_attr( $settings['attr'] );

		// Normally, HTML attributes should be applied to the field itself,
		// but in cases, when we haven't got a field itself, attributes will
		// be used for a wrapper.
		echo '<div ', $wrap_atts, '>';
		?>
		<input type="hidden" class="equip-media-value"
		       name="<?php echo esc_attr( $name ); ?>"
		       value="<?php echo esc_attr( $value ); ?>">

		<ul class="equip-media-items">
			<?php foreach ( $attachments as $attachmentID ) : ?>
				<li class="equip-media-item" data-id="<?php echo absint( $attachmentID ); ?>">
					<a href="#" class="equip-media-remove">&times;</a>
					<?php echo wp_get_attachment_image( $attachmentID, 'thumbnail' ); ?>
				</li>
			<?php endforeach; unset( $attachmentID ); ?>

			<li class="equip-media-control">
				<?php echo '<a ', equip_get_html_attributes( $media_atts ), '>&#43;</a>'; ?>
			</li>
		</ul>
		<?php
		echo '</div>';
	}

	/**
	 * Enqueue WP Media scripts and styles
	 *
	 * @return bool
	 */
	public function enqueue() {
		wp_enqueue_media();
		wp_enqueue_script( 'jquery-ui-sortable' );

		return true;
	}

	public function default_attr() {
		return array(
			'class' => 'equip-media-wrap'
		);
	}

	/**
	 * Get HTML attributes for media button (which opens media frame)
	 *
	 * @param array $field Field settings
	 *
	 * @return array
	 */
	private function get_media_attr( $field ) {
		// default values for Media Frame, based on field
		$attr = array(
			'title'  => $field['title'],
			'button' => 'Select',
			'type'   => 'image',
		);

		// may be merge with user's custom settings
		if ( array_key_exists( 'media', $field ) ) {
			$attr = wp_parse_args( $field['media'], $attr );
		}

		// convert all keys to data-*
		$keys = array_map( function ( $attribute ) {
			return 'data-' . $attribute;
		}, array_keys( $attr ) );

		$attr = array_combine( $keys, array_values( $attr ) );

		// user should not be able to change strict attributes
		$attr = array_merge( $attr, array(
			'href'          => '#',
			'class'         => 'equip-media-add',
			'data-multiple' => 1,
		) );
		krsort( $attr );

		return $attr;
	}
}