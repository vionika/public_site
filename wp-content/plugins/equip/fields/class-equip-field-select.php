<?php

/**
 * <select> field
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Select extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key     = sanitize_key( $settings['key'] );
		$options = '';

		// check if "options" key defined
		if ( ! array_key_exists( 'options', $settings ) ) {
			trigger_error( 'Equip_Field_Select::render(): "options" key not defined in $settings' );

			$options .= sprintf( '<option value="0">%s</option>', __( 'None', 'equip' ) );
		} else {
			foreach ( (array) $settings['options'] as $val => $name ) {
				$options .= sprintf( '<option value="%1$s" %3$s>%2$s</option>',
					$val,
					$name,
					selected( $value, $val, false )
				);
			}
			unset( $val, $name );
		}

		/*
		 * 1 - name
		 * 2 - id
		 * 3 - options
		 * 4 - HTML attributes
		 */
		printf( '<select name="%1$s" id="%2$s" %4$s>%3$s</select>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			$options,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function sanitize( $value, $settings, $slug ) {
		return sanitize_key( $value );
	}

	public function escape( $value, $settings, $slug ) {
		return esc_attr( $value );
	}
}