<?php

/**
 * Field "socials"
 *
 * Displays the control which allow to choose the social network
 * and associate a link with it
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Socials extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$networks = equip_get_socials();
		if ( empty( $networks ) ) {
			return;
		}

		$key  = sanitize_key( $settings['key'] );
		$name = $this->get_field_name( $slug, $key );

		$attr = $settings['attr'];
		if ( array_key_exists( 'class', $attr ) ) {
			$attr['class'] = equip_get_class_set( array( 'equip-socials-wrap', $attr['class'] ) );
		} else {
			$attr['class'] = 'equip-socials-wrap';
		}

		?>
		<div <?php echo $this->get_field_attr( $attr ); ?>>
			<?php

			if ( is_array( $value ) && count( $value ) > 0 ) :
				$this->render_list( $name, $networks, $value );
			else:
				$this->render_empty( $name, $networks );
			endif;

			?>
		</div>
		<br>
		<a href="#"
		   class="equip-socials-add"><?php _e( 'Add one more social network', 'equip' ); ?></a>
		<?php
	}

	/**
	 * Show non-empty list of networks => urls pairs
	 *
	 * @param string $name     Field name
	 * @param array  $networks A list of networks from socials.ini
	 * @param array  $socials  User defined list of socials
	 */
	private function render_list( $name, $networks, $socials ) {
		$select_name = $name . '[networks][]';
		$input_name  = $name . '[urls][]';

		foreach ( $socials as $social => $url ) : ?>
			<div class="equip-socials-group">
			<select name="<?php echo esc_attr( $select_name ); ?>">
				<?php
				foreach ( $networks as $network => $data ) :
					$selected = $network === $social ? 'selected' : '';
					// 1 - network slug, 2 - network name, 3 - selected
					printf(
						'<option value="%1$s" %3$s>%2$s</option>',
						esc_attr( $network ), esc_html( $data['name'] ), $selected
					);
				endforeach;
				unset( $network, $data );
				?>
			</select>
			<input type="text" name="<?php echo esc_attr( $input_name ); ?>"
			       value="<?php echo esc_url( $url ); ?>"
			       placeholder="<?php _e( 'URL', 'equip' ); ?>">
			<a href="#" class="equip-socials-group-remove">&times;</a>
			</div><?php
		endforeach;
		unset( $social, $url );
	}

	/**
	 * Render empty list of social networks (controls for single network)
	 *
	 * @param string $name     Meta box name
	 * @param array  $networks List of networks from social-networks.ini
	 */
	private function render_empty( $name, $networks ) {
		$select_name = $name . '[networks][]';
		$input_name  = $name . '[urls][]';

		?>
		<div class="equip-socials-group">
		<select name="<?php echo esc_attr( $select_name ); ?>">
			<?php foreach ( $networks as $network => $data ) :
				printf(
					'<option value="%1$s">%2$s</option>',
					esc_attr( $network ), esc_html( $data['name'] )
				);
			endforeach;
			unset( $network, $data ); ?>
		</select>
		<input type="text" name="<?php echo esc_attr( $input_name ); ?>"
		       placeholder="<?php _e( 'URL', 'equip' ); ?>">

		<a href="#" class="equip-socials-group-remove">&times;</a>
		</div><?php
	}

	/**
	 * Convert input array of user social networks to more suitable format.
	 *
	 * @param array  $socials  Expected multidimensional array with two keys
	 *                         [networks] and [urls], both contains equal
	 *                         number of elements.
	 *
	 * <code>
	 * [
	 *   networks => array( facebook, twitter, ... ),
	 *   urls     => array( url1, url2, ... ),
	 * ];
	 * </code>
	 *
	 *
	 * @param array  $settings Field settings
	 * @param string $slug     Element slug
	 *
	 * @return array New format of input array
	 *
	 * <code>
	 * [
	 * network  => url,
	 * facebook => url,
	 * twitter  => url
	 * ];
	 * </code>
	 */
	public function sanitize( $socials, $settings, $slug ) {
		if ( 0 === count( (array) $socials ) ) {
			return array();
		}

		// Return empty if networks or url not provided.
		if ( empty( $socials['networks'] ) || empty( $socials['urls'] ) ) {
			return array();
		}

		$result = array();
		// $network is an options group name from socials.ini
		array_map( function ( $network, $url ) use ( &$result ) {

			// Just skip iteration if network or url not set
			if ( '' === $network || '' === $url ) {
				return;
			}

			switch ( $network ) {
				case 'email':
					if ( false !== strpos( $url, 'mailto:' ) ) {
						$url = str_replace( 'mailto:', '', $url );
					}

					$result[ $network ] = 'mailto:' . sanitize_email( $url );
					break;

				default:
					$result[ $network ] = esc_url_raw( $url );
					break;
			}
		}, $socials['networks'], $socials['urls'] );

		return $result;
	}

	/**
	 * Escape social networks urls.
	 *
	 * This function expects that social networks already sanitized.
	 *
	 * @param array  $socials  List of user defined social networks in format
	 *                         [network => url, ... ]
	 *
	 * @param array  $settings Field settings
	 * @param string $slug     Element name
	 *
	 * @return array
	 */
	public function escape( $socials, $settings, $slug ) {
		if ( ! is_array( $socials ) || empty( $socials ) ) {
			return array();
		}

		$result = array();
		foreach ( (array) $socials as $network => $url ) {
			$result[ $network ] = esc_url( $url );
		}

		return $result;
	}
}