<?php

/**
 * Single image field
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Image extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key  = sanitize_key( $settings['key'] );
		$name = $this->get_field_name( $slug, $key );

		$is_value = ! empty( $value );
		$value    = $is_value ? absint( $value ) : '';

		$media_attr = $this->get_media_attr( $settings );
		$wrap_attr  = $this->get_field_attr( $settings['attr'] );

		// Normally, HTML attributes should be applied to the field itself,
		// but in cases, when we haven't got a field itself, attributes will
		// be used for a wrapper.
		echo '<div ', $wrap_attr, '>';
		?>
		<input type="hidden" class="equip-media-value"
		       name="<?php echo esc_attr( $name ); ?>"
		       value="<?php echo $value ?>">

		<ul class="equip-media-items">
			<?php if ( $is_value ) : ?>
				<li class="equip-media-item" data-id="<?php echo $value; ?>">
					<a href="#" class="equip-media-remove">&times;</a>
					<?php echo wp_get_attachment_image( $value, 'thumbnail' ); ?>
				</li>
			<?php endif; ?>

			<li class="equip-media-control">
				<a <?php echo equip_get_html_attributes( $media_attr ); ?>>&#43;</a>
			</li>
		</ul>
		<?php
		echo '</div>';
	}

	/**
	 * Enqueue WP Media scripts and styles
	 *
	 * @return bool
	 */
	public function enqueue() {
		wp_enqueue_media();

		return true;
	}

	public function sanitize( $value, $settings, $slug ) {
		return absint( $value );
	}

	public function escape( $value, $settings, $slug ) {
		return empty( $value ) ? '' : absint( $value );
	}

	public function default_attr() {
		return array(
			'class' => 'equip-media-wrap'
		);
	}

	/**
	 * Return attributes for media button <a>
	 *
	 * @param array $settings Field settings
	 *
	 * @return array
	 */
	private function get_media_attr( $settings ) {
		// default values for Media Frame, based on field
		$attr = array(
			'title'  => empty( $settings['title'] ) ? __( 'Choose the image', 'equip' ) : esc_html( $settings['title'] ),
			'button' => __( 'Select', 'equip' ),
		);

		if ( array_key_exists( 'media', $settings ) ) {
			$attr = wp_parse_args( $settings['media'], $attr );
		}

		// convert all keys to data-*
		$keys = array_map( function ( $attribute ) {
			return 'data-' . $attribute;
		}, array_keys( $attr ) );

		$attr = array_combine( $keys, array_values( $attr ) );

		// user should not be able to change strict attributes
		$attr = array_merge( $attr, array(
			'href'          => '#',
			'class'         => 'equip-media-add',
			'data-multiple' => 0,
		) );

		krsort( $attr );

		return $attr;
	}
}