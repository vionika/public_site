<?php

/**
 * Icon
 *
 * @link http://codeb.it/fonticonpicker/
 *
 * @author  8guild
 * @package Equip\Field
 */
class Equip_Field_Icon extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = $settings['key'];

		if ( empty( $settings['icons'] ) ) {
			$settings['icons'] = array();
		}

		if ( empty( $settings['settings'] ) ) {
			$settings['settings'] = array();
		}

		// picker settings
		$picker = wp_parse_args( $settings['settings'], array(
			'iconsPerPage' => 20,
			'hasSearch'    => true,
			'emptyIcon'    => true,
		) );

		if ( is_array( $settings['icons'] ) ) {
			$picker['source'] = $settings['icons'];
		} elseif ( is_string( $settings['icons'] ) ) {
			$picker['source'] = equip_get_icons( $settings['icons'] );
		}

		$settings['attr']['data-settings'] = $picker;

		printf( '<input name="%1$s" id="%2$s" value="%3$s" %4$s>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			$value,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function enqueue() {
		if ( ! wp_style_is( 'fonticonpicker' ) ) {
			wp_enqueue_style( 'fonticonpicker', EQUIP_ASSETS_URI . '/css/jquery.fonticonpicker.min.css', array(), null );
			wp_enqueue_style( 'fonticonpicker-grey', EQUIP_ASSETS_URI . '/css/jquery.fonticonpicker.grey.min.css', array(), null );
		}

		if ( ! wp_script_is( 'fonticonpicker' ) ) {
			wp_enqueue_script( 'fonticonpicker', EQUIP_ASSETS_URI . '/js/jquery.fonticonpicker.min.js', array( 'jquery' ), null, true );
		}
	}

	public function sanitize( $value, $settings, $slug ) {
		return sanitize_text_field( $value );
	}

	public function escape( $value, $settings, $slug ) {
		return esc_attr( $value );
	}

	public function default_attr() {
		return array(
			'type'  => 'text',
			'class' => 'equip-iconpicker',
		);
	}
}