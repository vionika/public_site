<?php

/**
 * Slider
 *
 * @link https://jqueryui.com/slider/
 *
 * @author  8guild
 * @package Equip\Field
 */
class Equip_Field_Slider extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = $settings['key'];

		$min  = ( ! is_numeric( $settings['min'] ) ) ? 0 : $settings['min'];
		$max  = ( ! is_numeric( $settings['max'] ) || empty( $settings['max'] ) ) ? 10 : $settings['max'];
		$step = ( ! is_numeric( $settings['step'] ) || empty( $settings['step'] ) ) ? 1 : $settings['step'];

		$settings['attr'] = array_merge( $settings['attr'], array(
			'data-min'  => $min,
			'data-max'  => $max,
			'data-step' => $step,
		) );

		$unique = esc_attr( equip_get_unique_id( 'equip-slider-' ) );

		printf( '<input type="text" name="%1$s" id="%2$s" value="%3$s" data-slider="%4$s" readonly>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			$value,
			$unique
		);

		printf( '<div id="%1$s" %2$s></div>',
			$unique,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function enqueue() {
		if ( ! wp_script_is( 'jquery-ui-slider' ) ) {
			wp_enqueue_script( 'jquery-ui-core' );
			wp_enqueue_script( 'jquery-ui-widget' );
			wp_enqueue_script( 'jquery-ui-mouse' );

			wp_enqueue_script( 'jquery-ui-slider' );
		}
	}

	public function sanitize( $value, $settings, $slug ) {
		return $value; // TODO: sanitize
	}

	public function escape( $value, $settings, $slug ) {
		return $value; // TODO: escape
	}

	public function default_attr() {
		return array(
			'class' => 'equip-slider',
		);
	}
}