<?php

/**
 * <textarea> field
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Textarea extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = sanitize_key( $settings['key'] );

		/*
		 * 1 - name
		 * 2 - id
		 * 3 - value
		 * 4 - attributes
		 */
		printf( '<textarea name="%1$s" id="%2$s" %4$s>%3$s</textarea>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			(string) $value,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function sanitize( $value, $settings, $slug ) {
		// Get allowed HTML tags for wp_kses()
		$allowed_html = wp_kses_allowed_html( 'post' );

		$value = stripslashes( $value );
		$value = wp_kses( $value, $allowed_html );

		return $value;
	}

	public function escape( $value, $settings, $slug ) {
		$value = stripslashes( $value );
		$value = esc_textarea( $value );

		return $value;
	}

	public function default_attr() {
		return array(
			'rows' => 10,
			'cols' => 5,
		);
	}
}