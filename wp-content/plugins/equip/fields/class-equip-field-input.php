<?php

/**
 * <input> field
 *
 * @author  8guild
 * @package Equip\Fields
 */
class Equip_Field_Input extends Equip_Field {

	public function render( $slug, $settings, $value ) {
		$key = sanitize_key( $settings['key'] );

		/*
		 * 1 - name
		 * 2 - id
		 * 3 - value
		 * 4 - HTML attributes
		 */
		printf( '<input name="%1$s" id="%2$s" value="%3$s" %4$s>',
			esc_attr( $this->get_field_name( $slug, $key ) ),
			esc_attr( $this->get_field_id( $slug, $key ) ),
			$value,
			$this->get_field_attr( $settings['attr'] )
		);
	}

	public function sanitize( $value, $settings, $slug ) {
		return sanitize_text_field( $value );
	}

	public function escape( $value, $settings, $slug ) {
		return esc_attr( $value );
	}

	public function default_attr() {
		return array( 'type' => 'text' );
	}
}