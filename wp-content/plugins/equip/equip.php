<?php
/**
 * Equip
 *
 * Simple, lightweight and fast fields builder for WordPress themes and plugins.
 *
 * Plugin Name:       Equip
 * Plugin URI:        http://8guild.com
 * Description:       Simple, lightweight and fast fields builder for WordPress themes and plugins.
 * Version:           0.5.1a
 * Author:            8guild
 * Author URI:        http://8guild.com
 * Text Domain:       equip
 * License:           GPL3+
 * License URI:       http://www.gnu.org/licenses/gpl-3.0.txt
 * Domain Path:       languages
 *
 * @package           Equip
 * @author            8guild <info@8guild.com>
 * @license           GNU General Public License, version 3
 * @wordpress-plugin
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
}

/*
 * WP_CLI
 *
 * @todo Read more about WP_CLI
 * @link https://github.com/Automattic/Co-Authors-Plus/blob/master/co-authors-plus.php#L34
 */
if ( defined( 'WP_CLI' ) && WP_CLI ) {
	return;
}

/**#@+
 * Plugin constants
 */
define( 'EQUIP_VERSION', '0.5.1a' );
define( 'EQUIP_REQUIRED_WP_VERSION', '4.0' );
define( 'EQUIP_REQUIRED_PHP_VERSION', '5.3' );
define( 'EQUIP_PLUGIN_DIR', untrailingslashit( plugin_dir_path( __FILE__ ) ) );
define( 'EQUIP_ASSETS_DIR', EQUIP_PLUGIN_DIR . '/assets' );
define( 'EQUIP_ASSETS_URI', plugins_url( '/assets', __FILE__ ) );
define( 'EQUIP_FIELDS_DIR', EQUIP_PLUGIN_DIR . '/fields' );
define( 'EQUIP_ENGINES_DIR', EQUIP_PLUGIN_DIR . '/engines' );
/**#@-*/

/**
 * Auto loader function.
 * Load core classes according to WordPress naming conventions.
 *
 * @link  https://make.wordpress.org/core/handbook/coding-standards/php/#naming-conventions
 *
 * @param string $class Class name
 *
 * @since 1.0.0
 */
function _equip_loader( $class ) {
	$class_lc = strtolower( $class );
	$chunks   = array_filter( explode( '_', $class_lc ) );

	// Load only plugin classes
	if ( ! isset( $chunks[0] ) || 'equip' !== $chunks[0] ) {
		return;
	}

	$root = EQUIP_PLUGIN_DIR . '/core/';
	$file = 'class-' . implode( '-', $chunks ) . '.php';
	$path = wp_normalize_path( $root . $file );

	if ( is_readable( $path ) ) {
		require $path;
	}
}

spl_autoload_register( '_equip_loader' );

/**
 * Init the plugin
 */
function _equip_init() {
	/**
	 * See the Notes
	 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/init
	 */
	load_plugin_textdomain( 'equip', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

add_action( 'init', '_equip_init' );

/**
 * Enqueue scripts and styles
 *
 * @param string $hook Current page hook
 */
function _equip_admin_enqueue_scripts( $hook ) {
	// enqueue fields' JS and CSS
	Equip::get( Equip::ENQUEUE )->enqueue( $hook );

	wp_enqueue_script( 'equip', EQUIP_ASSETS_URI . '/js/equip.js', array( 'jquery' ), null, true );
	wp_enqueue_style( 'equip', EQUIP_ASSETS_URI . '/css/equip.css', array(), null );
}

add_action( 'admin_enqueue_scripts', '_equip_admin_enqueue_scripts', 10, 1 );

/**
 * Define the map of built-in filed types
 *
 * @param array $map A list of fields
 *
 * @return array
 */
function _equip_builtin_fields( $map = array() ) {
	return array_merge( array(
		'input'    => EQUIP_FIELDS_DIR . '/class-equip-field-input.php',
		'select'   => EQUIP_FIELDS_DIR . '/class-equip-field-select.php',
		'textarea' => EQUIP_FIELDS_DIR . '/class-equip-field-textarea.php',
		'image'    => EQUIP_FIELDS_DIR . '/class-equip-field-image.php',
		'images'   => EQUIP_FIELDS_DIR . '/class-equip-field-images.php',
		'socials'  => EQUIP_FIELDS_DIR . '/class-equip-field-socials.php',
		'group'    => EQUIP_FIELDS_DIR . '/class-equip-field-group.php',
		'color'    => EQUIP_FIELDS_DIR . '/class-equip-field-color.php',
		'icon'     => EQUIP_FIELDS_DIR . '/class-equip-field-icon.php',
		'switch'   => EQUIP_FIELDS_DIR . '/class-equip-field-switch.php',
		'slider'   => EQUIP_FIELDS_DIR . '/class-equip-field-slider.php',
	), $map );
}

add_filter( 'equip/factory/field/map', '_equip_builtin_fields', -1 );

/**
 * Default engines
 *
 * @param array $map A list of engines
 *
 * @return array
 */
function _equip_builtin_engines( $map = array() ) {
	return array_merge( array(
		'default' => EQUIP_ENGINES_DIR . '/class-equip-render-default.php',
		'simple'  => EQUIP_ENGINES_DIR . '/class-equip-render-simple.php',
		'group'   => EQUIP_ENGINES_DIR . '/class-equip-render-group.php',
	), $map );
}

add_filter( 'equip/factory/engine/map', '_equip_builtin_engines', -1 );

/*
 * Load helpers functions
 */
require EQUIP_PLUGIN_DIR . '/inc/utils.php';

/*
 * Load the API
 */
require EQUIP_PLUGIN_DIR . '/inc/api.php';
