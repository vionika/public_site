<?php

/**
 * Working with WordPress Settings & Settings API
 *
 * Alpha version! May be dropped in future
 *
 * @author  8guild
 * @package Equip
 * @since   1.0.0
 */
class Equip_Settings {
	/**
	 * @var string Class slug
	 */
	const SETTINGS = 'settings';

	public function __construct() {}

	public function init() {
		add_action( 'admin_init', array( $this, 'register_settings' ) );
	}

	public function register_settings() {
		$storage  = Equip::get( 'storage' );
		$settings = $storage->get( self::SETTINGS );

		foreach ( (array) $settings as $slug => $data ) {
			$args     = $data['args'];
			$page     = $args['page'];
			$fields   = $data['fields'];
			$sections = $data['sections'];

			$this->register_sections( $slug, $sections, $page );
			$this->register_fields( $slug, $fields, $page );

			if ( false === get_option( $slug ) ) {
				add_option( $slug, array() );
			}

			register_setting( $page, $slug, array( $this, 'sanitize' ) );
		}
	}

	private function register_sections( $slug, $sections, $page ) {
		$_sections = array();

		/*
		 * TODO: проверки
		 */

		foreach ( (array) $sections as $section ) {
			$section_id = $this->get_section_id( $slug, $section['id'] );
			add_settings_section(
				$section_id,
				$section['label'],
				//array( $this, 'render_section' ),
				'__return_false',
				$page
			);

			$_sections[] = $section_id;
			unset( $section_id );
		}
		unset( $section );

		$storage = Equip::get('storage');
		$storage->update( self::SETTINGS, $slug, array( '_sections' => $_sections ) );
	}

	private function register_fields( $slug, $fields, $page ) {
		/*
		 * TODO: проверки
		 */
		foreach ( (array) $fields as $field ) {
			$section_id = $this->get_section_id( $slug, $field['section'] );
			/*
			 * TODO: если нету ключа "section", то используем секцию по умолчанию
			 */
			add_settings_field(
				$field['key'],
				$field['label'],
				array( $this, 'render_field' ),
				$page,
				$section_id,
				array(
					'slug'       => $slug,
					'field'      => $field,
					'page'       => $page,
					'section_id' => $section_id,
				)
			);
		}
	}

	public function render_field( $args ) {
		$slug   = (string) $args['slug'];
		$field  = wp_parse_args( $args['field'], Equip_Render::field_defaults() );
		$key    = $field['key'];
		$values = get_option( $slug, array() );

		$value = null;
		if ( is_array( $values ) && array_key_exists( $key, $values ) ) {
			$value = Equip_Escaper::escape( $values[ $key ], $field, $slug );
		}

		$field_obj = Equip::field( $field );
		$field_obj->render( $slug, $field, $value );
		$field_obj->description( $field['desc'], $field['desc_wrap'] );
	}

	public function sanitize( $values ) {
		$storage = Equip::get( 'storage' );
		$all     = $storage->get_list_of( self::SETTINGS );
		$current = array_intersect_key( $_POST, array_flip( $all ) );

		if ( 0 === count( $current ) ) {
			return array();
		}

		if ( false === current_user_can( 'manage_options' ) ) {
			return array();
		}

		$filtered = array();
		foreach ( $current as $slug => $_values ) {
			if ( 0 === count( (array) $_values ) ) {
				continue;
			}

			foreach ( (array) $_values as $key => $value ) {
				if ( array_key_exists( $key, $values ) ) {
					$field = $storage->get_field_by_key( self::SETTINGS, $slug, $key );
					$filtered[ $key ] = Equip_Sanitizer::sanitize( $values[ $key ], $field, $slug );
				}

				continue;
			}
			unset( $key, $value );
		}
		unset( $slug, $_values );

		return $filtered;
	}

	private function get_section_id( $slug, $section_id ) {
		return "{$slug}_{$section_id}_section";
	}
}