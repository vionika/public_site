<?php

/**
 * Working with custom fields in user / profile pages
 *
 * @author  8guild
 * @package Equip
 * @since   1.0.0
 */
class Equip_User {
	/**
	 * @var string Class slug
	 */
	const USER = 'user';

	public function __construct() {}

	public function init() {
		add_action( 'edit_user_profile', array( $this, 'render' ) );
		add_action( 'show_user_profile', array( $this, 'render' ) );
		add_action( 'personal_options_update', array( $this, 'save' ) );
		add_action( 'edit_user_profile_update', array( $this, 'save' ) );
	}

	/**
	 * Render custom fields in user / profile pages
	 *
	 * @param WP_User $user User object
	 *
	 * @since 1.0.0
	 */
	public function render( $user ) {
		$storage      = Equip::get( 'storage' );
		$user_options = $storage->get( self::USER );
		if ( null === $user_options || 0 === count( (array) $user_options ) ) {
			return;
		}

		// user profiles supports only "default" engine
		$render = Equip::engine( 'default' );
		foreach ( $user_options as $slug => $option ) {
			$args     = $option['args'];
			$fields   = $option['fields'];
			$sections = $option['sections'];

			/*
			 * TODO: логика, основываясь на $args
			 */

			$values   = get_user_meta( $user->ID, $slug, true );
			$contents = array( 'sections' => $sections, 'fields' => $fields );
			$render->render( $slug, $contents, $values );

			unset( $args, $fields, $sections );
		}
	}

	/**
	 * Save custom fields from user / profile pages
	 *
	 * @param int $user_id Current user ID
	 *
	 * @since 1.0.0
	 */
	public function save( $user_id ) {
		/*
		 * The same logic, as in meta boxes. Detect if current
		 * $_POST has any fields added through framework
		 */
		$storage = Equip::get( 'storage' );

		$all     = $storage->get_list_of( self::USER );
		$current = array_intersect_key( $_POST, array_flip( $all ) );
		unset( $all );

		if ( 0 === count( $current ) ) {
			return;
		}

		if ( false === current_user_can( 'edit_users', $user_id ) ) {
			return;
		}

		foreach ( $current as $slug => $values ) {
			$fields = (array) $storage->get( self::USER, $slug, 'fields' );
			$values = Equip_Sanitizer::bulk_sanitize( $values, $fields, $slug );

			update_user_meta( $user_id, $slug, $values );

			unset( $fields );
		}
		unset( $slug, $values );
	}
}