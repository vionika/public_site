<?php

/**
 * Class Equip_Debug
 */
class Equip_Debug {

	/**
	 * Log messages
	 *
	 * @param string $message Message
	 *
	 * @return Equip_Debug
	 */
	public static function log( $message ) {
		// Do not do anything, if debug is disabled
		if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
			return;
		}

		if ( defined( 'WP_DEBUG_LOG') && WP_DEBUG_LOG ) {
			error_log( $message );
		}

		if ( defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG_DISPLAY ) {
			trigger_error( $message );
		}
	}

	public static function notice( $message ) {
		// Do not do anything, if debug is disabled
		if ( ! defined( 'WP_DEBUG' ) || false === WP_DEBUG ) {
			return;
		}

		if ( defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG_DISPLAY ) {
			trigger_error( $message );
		}
	}
}