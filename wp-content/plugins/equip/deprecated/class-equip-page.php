<?php

/**
 * Render and process pages
 *
 * @deprecated
 * TODO: нужно все переделать и избавиться от этого Settings API по возможности
 *
 * @since   1.0.0
 * @author  8guild
 * @package Equip
 */
class Equip_Page {

	public function __construct() {}

	/**
	 * Add hooks for rendering pages
	 *
	 * @since 0.5.0a
	 */
	public function init() {
		add_action( 'admin_menu', array( $this, 'add_menu_item' ) );
		add_action( 'admin_init', array( $this, 'register_settings' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue' ) );
	}

	public function add_menu_item() {
		$storage = Equip::get( 'storage' );
		/**
		 * @var array List of all pages, registered through {@see equip_add_page()} or {@see equip_add_submenu_page()}
		 */
		$pages = $storage->get( 'page' );

		/*
		 * Iterate through pages
		 */
		foreach ( (array) $pages as $slug => $data ) {
			if ( array_key_exists( 'parent', $data['args'])) {
				$this->add_submenu_page( $slug, $data['args'] );
			} else {
				$this->add_menu_page( $slug, $data['args'] );
			}
		}

		return true;
	}

	public function register_settings() {
		$storage = Equip::get( 'storage' );
		$pages   = $storage->get( 'page' );

		foreach ( (array) $pages as $slug => $data ) {
			$args      = $data['args'];
			$sections  = $data['sections'];
			$fields    = $data['fields'];
			$menu_slug = $args['menu_slug'];

			$this->register_sections( $slug, $sections, $menu_slug );
			$this->register_fields( $slug, $fields, $menu_slug );

			/*
			 * Be sure, sanitize callback not firing twice!
			 */
			if ( false === get_option( $slug ) ) {
				add_option( $slug );
			}

			register_setting( $menu_slug, $slug, array( $this, 'sanitize' ) );
		}
	}

	public function enqueue( $hook ) {
		$slug = $this->get_page_slug_by_hook( $hook );
		if ( null === $slug ) {
			return;
		}

		$storage = Equip::get( 'storage' );
		$fields  = $storage->get( 'page', $slug, 'fields' );

		$custom_js  = array();
		$custom_css = array();

		foreach( (array) $fields as $field ) {
			$key = $field['key'];
			// collect custom js for current page
			if ( array_key_exists( 'custom_js', $field ) ) {
				$handle = "equip-{$slug}-{$key}-js";
				$custom_js[ $handle ] = $field['custom_js'];
				unset( $handle );
			}

			// collect custom css for current page
			if ( array_key_exists( 'custom_css', $field ) ) {
				$handle = "equip-{$slug}-{$key}-css";
				$custom_css[ $handle ] = $field['custom_css'];
				unset( $handle );
			}

			continue;
		}
		unset( $field );

		foreach ( array_unique( $custom_js ) as $handle => $src ) {
			wp_enqueue_script( $handle, $src, array(), EQUIP_VERSION, true );
		}
		unset( $handle, $src );

		foreach ( array_unique( $custom_css ) as $handle => $src ) {
			wp_enqueue_style( $handle, $src, array(), EQUIP_VERSION );
		}
		unset( $handle, $src );
	}

	/**
	 * Sanitize page values before saving them to database
	 *
	 * Method called in {@see Equip_Page::register_settings}
	 *
	 * @param array $values Values for sanitizing
	 *
	 * @return array
	 */
	public function sanitize( $values ) {
		$storage = Equip::get( 'storage' );
		$render  = Equip::get( 'render' );

		/*
		 * Detect current page for fields sanitizing, check $_POST
		 * $_POST MUST contains only one page, so..
		 */
		$all_pages     = $storage->get_list_of( 'page' );
		$current_pages = array_intersect_key( $_POST, array_flip( $all_pages ) );
		$slug          = key( $current_pages );
		unset( $all_pages, $current_pages );

		$new_values = array();
		$field      = null;
		foreach ( (array) $values as $key => $value ) {
			$field = $storage->get_field_by_key( 'page', $slug, $key );
			if ( null === $field ) {
				continue;
			}

			$new_values[ $key ] = $render->sanitize( $value, $field, $slug );
		}
		unset( $key, $value );

		return $new_values;
	}

	public function render_page() {
		$slug = $this->get_page_slug_by_hook( current_filter() );
		if ( null === $slug ) {
			return;
		}

		$storage = Equip::get( 'storage' );
		$args    = $storage->get( 'page', $slug, 'args' );

		?><div class="wrap"><?php

		settings_errors();

		?><form method="post" action="options.php"><?php

		settings_fields( $args['menu_slug'] );
		do_settings_sections( $args['menu_slug'] );

		submit_button();

		?></form><?php

		?></div><?php
	}

	public function render_section( $section ) {
		var_dump( $section );
	}

	public function render_field( $args ) {
		$slug   = $args['slug'];
		$field  = $args['field'];
		$key    = $field['key'];
		$values = get_option( $slug, array() );
		$value  = null;

		if ( is_array( $values ) && array_key_exists( $key, $values ) ) {
			$value = $values[ $key ];
		}

		$render = Equip::get( 'render' );
		$render->render_field( $slug, $field, $value );
	}

	private function add_menu_page( $slug, $args ) {
		$title = ucfirst( preg_replace( '/[_\\W]+/', ' ', trim( $slug ) ) );

		/**
		 * @var array Equip menu page default params
		 */
		$defaults = array(
			'page_title' => $title,
			'menu_title' => $title,
			'capability' => 'manage_options',
			'menu_slug'  => trim( $slug ),
			'icon'       => '',
			'position'   => null,
		);

		/*
		 * TODO: add menu page default args filter
		 */

		$args = wp_parse_args( $args, $defaults );

		/*
		 * TODO: add menu page args filter
		 */

		$hook = add_menu_page(
			$args['page_title'],
			$args['menu_title'],
			$args['capability'],
			$args['menu_slug'],
			array( $this, 'render_page' ),
			$args['icon'],
			$args['position']
		);

		$storage = Equip::get( 'storage' );
		$storage->update( 'page', $slug, array( 'hook' => $hook ) );
	}

	private function add_submenu_page( $slug, $args ) {
		$title = ucfirst( preg_replace( '/[_\\W]+/', ' ', trim( $slug ) ) );

		/**
		 * @var array Equip menu page default params
		 */
		$defaults = array(
			'parent'     => null,
			'page_title' => $title,
			'menu_title' => $title,
			'capability' => 'manage_options',
			'menu_slug'  => trim( $slug ),
		);

		/*
		 * TODO: add menu page default args filter
		 */

		$args = wp_parse_args( $args, $defaults );

		/**
		 * A little help for DEVs: allow to use aliases for files
		 *
		 * @var array Standard WordPress admin pages
		 */
		$wp_standard = array(
			'dashboard'  => 'index.php',
			'posts'      => 'edit.php',
			'media'      => 'upload.php',
			'links'      => 'link-manager.php',
			'pages'      => 'edit.php?post_type=page',
			'comments'   => 'edit-comments.php',
			'appearance' => 'themes.php',
			'plugins'    => 'plugins.php',
			'users'      => 'users.php',
			'tools'      => 'tools.php',
			'settings'   => 'options-general.php',
		);

		$parent       = $args['parent'];
		$is_invisible = ( null === $parent || 'options.php' === $parent );
		if ( false === $is_invisible && array_key_exists( $parent, $wp_standard ) ) {
			$args['parent'] = $wp_standard[ $parent ];
		}

		// if not invisible or standard pages, all is left is CPT
		if ( false === $is_invisible && false === array_key_exists( $parent, $wp_standard ) ) {
			$args['parent'] = sprintf( 'edit.php?post_type=%s', $parent );
		}
		unset( $parent, $is_invisible );

		/*
		 * TODO: add menu page args filter
		 */

		$hook = add_submenu_page(
			$args['parent'],
			$args['page_title'],
			$args['menu_title'],
			$args['capability'],
			$args['menu_slug'],
			array( $this, 'render_page' )
		);

		$storage = Equip::get( 'storage' );
		$storage->update( 'page', $slug, array( 'hook' => $hook ) );
	}



	private function get_section_id( $slug, $section_id ) {
		/*
		 * TODO: add filter
		 */
		return "{$slug}_{$section_id}_section";
	}

	private function register_sections( $slug, $sections, $menu_slug ) {
		/*
		 * TODO: добавить проверку обязательных полей
		 * TODO: добавить фильтр с возможностью изменить настройки полей
		 * TODO: добавить фильтр с возможностью изменить состав секций до регистрации
		 * TODO: что если кто-то добавит страницу без секций?
		 *
		 * По аналогии с метабоксами - просто массив с полями.
		 * Для этого дела надо будет зарегистрировать "дефолтную" секцию
		 * и использовать ее для add_settings_field()
		 */
		$_sections = array();

		if ( 0 === count( $sections ) ) {
			// TODO: тут регистрируем дефолтную секцию
		}

		foreach ( (array) $sections as $section ) {
			$section_id = $this->get_section_id( $slug, $section['id'] );
			add_settings_section(
				$section_id,
				$section['label'],
				array( $this, 'render_section'),
				$menu_slug
			);

			$_sections[] = $section_id;

			unset( $section_id );
		}
		unset( $section );

		// Save created settings sections in storage
		$storage = Equip::get( 'storage' );
		$storage->update( 'page', $slug, array( '_sections' => $_sections ) );

		return $_sections;
	}

	private function register_fields( $slug, $fields, $menu_slug ) {
		/*
		 * TODO: добавить фильтр для модификации полей
		 */
		foreach ( (array) $fields as $field ) {
			$section_id = $this->get_section_id( $slug, $field['section'] );
			/*
			 * TODO: если нету ключа "section", то используем секцию по умолчанию
			 */
			add_settings_field(
				$field['key'],
				$field['label'],
				array( $this, 'render_field' ),
				$menu_slug,
				$section_id,
				array(
					'slug'       => $slug,
					'field'      => $field,
					'menu_slug'  => $menu_slug,
					'section_id' => $section_id,
				)
			);
		}
	}

	/**
	 * This method returns page slug by its hook, because there is no easy way to
	 * pass arguments to the page rendering callback function or during enqueue scripts and styles.
	 *
	 * @param string $hook Current page hook
	 *
	 * @return null|array
	 */
	private function get_page_slug_by_hook( $hook ) {
		$storage = Equip::get( 'storage' );
		$pages   = $storage->get( 'page' );

		if ( null === $pages || 0 === count( (array) $pages ) ) {
			return null;
		}

		foreach ( (array) $pages as $slug => $page ) {
			if ( $page['hook'] === $hook ) {
				return $slug;
			}
		}

		return null;
	}
}