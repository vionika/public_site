<?php

/**
 * Equip rendering engine "default"
 *
 * @author  8guild
 * @package Equip\Render\Engines
 */
class Equip_Render_Default extends Equip_Render {

	public function before_fields( $slug, $contents ) {
		?><table class="form-table equip-render-default"><tbody><?php
	}

	public function before_field( $slug, $settings, $field ) {
		// Open the <tr> tag
		echo '<tr><th scope="row" class="equip-render-default-title">';
		if ( ! empty( $settings['title'] ) ) {
			printf( '<label for="%1$s">%2$s</label>',
				esc_attr( $field->get_field_id( $slug, $settings['key'] ) ),
				esc_html( $settings['title'] )
			);
		}
		echo '</th>';

		echo '<td class="equip-render-default-field">';
		if ( ! empty( $settings['before_field'] ) ) {
			echo $settings['before_field'];
		}
	}

	public function do_field( $slug, $settings, $value, $field ) {
		$field->render( $slug, $settings, $value );
	}

	public function after_field( $slug, $settings, $field ) {
		if ( ! empty( $settings['after_field'] ) ) {
			echo $settings['after_field'];
		}

		echo '</td>';

		echo '<td class="equip-render-default-description">';
		if ( ! empty( $settings['desc'] ) ) {
			printf( $settings['desc_wrap'], $settings['desc'] );
		}

		// Close the <tr> opened in before_field
		echo '</td></tr>';
	}

	public function after_fields( $slug, $contents ) {
		?></tbody></table><?php
	}
}