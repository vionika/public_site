<?php

/**
 * Equip rendering engine "simple"
 *
 * @author  8guild
 * @package Equip\Render\Engines
 */
class Equip_Render_Simple extends Equip_Render {

	public function before_fields( $slug, $contents ) {
		?>
		<div class="equip-simple-wrapper">
		<?php
	}

	public function before_field( $slug, $settings, $field ) {
		if ( ! empty( $settings['title'] ) ) {
			printf( '<label for="%1$s">%2$s</label>',
				esc_attr( $field->get_field_id( $slug, $settings['key'] ) ),
				esc_html( $settings['title'] )
			);
		}

		if ( ! empty( $settings['before_field'] ) ) {
			echo $settings['before_field'];
		}
	}

	public function do_field( $slug, $settings, $value, $field ) {
		$field->render( $slug, $settings, $value );
	}

	public function after_field( $slug, $settings, $field ) {
		if ( ! empty( $settings['after_field'] ) ) {
			echo $settings['after_field'];
		}

		if ( ! empty( $settings['desc'] ) ) {
			printf( $settings['desc_wrap'], $settings['desc'] );
		}
	}

	public function after_fields( $slug, $contents ) {
		?>
		</div>
		<?php
	}
}