<?php

/**
 * Equip rendering engine "group"
 *
 * A special engine for rendering groups.
 * Should not be used as a standalone engine.
 *
 * @author  8guild
 * @package Equip\Render\Engines
 */
class Equip_Render_Group extends Equip_Render {

	public function before_fields( $slug, $contents ) {
		?><div class="row"><?php
	}

	/**
	 * Override the {@see Equip_Render::_before_field}
	 *
	 * Add support for div.col-* classes
	 *
	 * @param string $slug     Element unique name
	 * @param array  $settings Element field settings
	 */
	protected function _before_field( $slug, $settings ) {
		$attr  = array();
		$class = array(
			'equip-field',
			array_key_exists( 'column', $settings ) ? $settings['column'] : 'col-6'
		);

		$attr['class']      = equip_get_class_set( $class );
		$attr['data-key']   = $settings['key'];
		$attr['data-field'] = $settings['field'];

		echo '<div ', equip_get_html_attributes( $attr ), '>';
	}

	public function before_field( $slug, $settings, $field ) {
		if ( ! empty( $settings['title'] ) ) {
			printf( '<label for="%1$s">%2$s</label>',
				esc_attr( $field->get_field_id( $slug, $settings['key'] ) ),
				esc_html( $settings['title'] )
			);
		}
	}

	public function do_field( $slug, $settings, $value, $field ) {
		if ( array_key_exists( 'before_field', $settings ) ) {
			echo $settings['before_field'];
		}

		$field->render( $slug, $settings, $value );

		if ( array_key_exists( 'after_field', $settings ) ) {
			echo $settings['after_field'];
		}
	}

	public function after_field( $slug, $settings, $field ) {
		if ( ! empty( $settings['desc'] ) ) {
			printf( $settings['desc_wrap'], $settings['desc'] );
		}
	}

	public function after_fields( $slug, $contents ) {
		?></div><?php
	}
}