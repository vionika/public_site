<?php
/**
 * Equip API
 *
 * @author  8guild
 * @package Equip
 */

/**
 * Ask Equip to create a meta box.
 *
 * @param string         $slug     Meta box unique name. Will be used to save and retrieve data from
 *                                 database as a second parameter of {@see get_post_meta()}.
 * @param array|callable $contents Meta box contents. May be a list of fields or callback.
 * @param array          $args     Meta box arguments. See {@see add_meta_box()} for more info.
 *
 * @uses Equip
 */
function equip_meta_box_add( $slug, $contents, $args = array() ) {
	// No need to add meta boxes during ajax calls?
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

	// Also, do not execute if not in admin screens
	if ( ! is_admin() ) {
		return;
	}

	/**
	 * @var Equip_Storage $storage
	 */
	$storage = Equip::get( Equip::STORAGE );
	$storage->add( Equip::META_BOX, $slug, $contents, $args );

	Equip::init( Equip::META_BOX );
}

/**
 * Returns the values of meta box.
 *
 * If $field is specified will return the field's value.
 *
 * @param int         $post_id Post ID
 * @param string      $slug    Meta box unique name
 * @param null|string $field   Key of the field
 * @param mixed       $default Default value
 *
 * @return mixed Array with key-value, mixed data if field is specified and the value
 *               of $default field if nothing found.
 */
function equip_meta_box_get( $post_id, $slug, $field = null, $default = array() ) {
	/**
	 * @var Equip_Meta_Box $meta_box
	 */
	$meta_box = Equip::get( Equip::META_BOX );

	return $meta_box->get( $post_id, $slug, $field, $default );
}

/**
 * Add custom fields to user / profile pages
 *
 * @param string $slug     Fields name. This name will be used for storing fields' values in the DB
 *                         and as a second parameter for {@see get_user_meta()}.
 * @param array  $contents List of sections and(or) fields
 * @param array  $args     Arguments
 *
 * @since 1.0.0
 */
function equip_add_user_fields( $slug, $contents, $args = array() ) {
	// skip during ajax calls
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

	// do not execute if not in admin screens
	if ( ! is_admin() ) {
		return;
	}


	Equip::get( Equip::STORAGE )->add( 'user', $slug, $contents, $args );
	Equip::get( 'user' )->init();
}

function equip_add_page( $slug, $content, $args = array() ) {
	// skip during ajax calls
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

	// do not execute if not in admin screens
	if ( ! is_admin() ) {
		return;
	}

	Equip::get( 'storage' )->add( 'page', $slug, $content, $args );
	Equip::get( 'page' )->init();
}

/**
 * Add custom fields to Settings pages
 *
 * TODO: comment
 *
 * @param string $slug
 * @param array  $contents
 * @param array  $args
 *
 * @since 1.0.0
 */
function equip_add_settings_fields( $slug, $contents, $args ) {
	// skip during ajax calls
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return;
	}

	// do not execute if not in admin screens
	if ( ! is_admin() ) {
		return;
	}

	Equip::get( 'storage' )->add( 'settings', $slug, $contents, $args );
	Equip::get( 'settings' )->init();
}
