<?php
/**
 * Utilities and helper functions
 *
 * @author 8guild
 * @package Equip\Utils
 */

if ( ! function_exists( 'equip_get_class_set' ) ) :
	/**
	 * Prepare and sanitize the class set.
	 *
	 * Caution! This function sanitize each class,
	 * but don't escape the returned result.
	 *
	 * E.g. [ 'my', 'cool', 'class' ] or 'my cool class'
	 * will be sanitized and converted to "my cool class".
	 *
	 * @param array|string $classes
	 *
	 * @return string
	 */
	function equip_get_class_set( $classes ) {
		if ( empty( $classes ) ) {
			return '';
		}

		if ( is_string( $classes ) ) {
			$classes = (array) $classes;
		}

		// remove empty elements before loop, if exists
		// and explode array into the flat list
		$classes   = array_filter( $classes );
		$class_set = array();
		foreach ( $classes as $class ) {
			$class = trim( $class );
			if ( false === strpos( $class, ' ' ) ) {
				$class_set[] = $class;

				continue;
			}

			// replace possible multiple whitespaces with single one
			$class = preg_replace( '/\\s\\s+/', ' ', $class );
			foreach ( explode( ' ', $class ) as $subclass ) {
				$class_set[] = trim( $subclass );
			}
			unset( $subclass );
		}
		unset( $class );

		// do not duplicate
		$class_set = array_unique( $class_set );
		$class_set = array_map( 'sanitize_html_class', $class_set );
		$class_set = array_filter( $class_set );

		$set = implode( ' ', $class_set );

		return $set;
	}
endif;

if ( ! function_exists( 'equip_get_html_attributes' ) ) :
	/**
	 * Return HTML attributes list for given attributes pairs
	 *
	 * <strong>Caution!</strong> This function does not escape attribute value,
	 * only attribute name, for more flexibility. You should do this manually
	 * for each attribute before calling this function.
	 *
	 * Also you can pass a multidimensional array with one level depth,
	 * this array will be encoded to json format.
	 *
	 * <pre>
	 * penny_get_html_attributes(array(
	 *   'class' => 'super-class',
	 *   'title' => 'My cool title',
	 *   'data-settings' => array( 'first' => '', 'second' => '' ),
	 *   'hidden' => true,
	 * ));
	 * </pre>
	 *
	 * Sometimes some attributes are required and should be present in attributes
	 * list. For example, when you build attributes for a link "href" is mandatory.
	 * So if user do not fill this field default values will be used. Should
	 * be an array with the same keys as in $atts.
	 *
	 * @example <pre>
	 * penny_get_html_attributes([href => ''], [href => #]); // returns href="#"
	 * </pre>
	 *
	 * @param array $atts     Key and value pairs of HTML attributes
	 * @param array $defaults Default values, that should be present in attributes list
	 *
	 * @return string
	 */
	function equip_get_html_attributes( $atts, $defaults = array() ) {
		$attributes = array();

		foreach ( $atts as $attribute => $value ) {
			$template = '%1$s="%2$s"';

			// if user pass empty value, use one from defaults if same key exists
			// allowed only for scalar types
			if ( is_scalar( $value )
			     && '' === (string) $value
			     && array_key_exists( $attribute, $defaults )
			) {
				$value = $defaults[ $attribute ];
			}

			// convert array to json
			if ( is_array( $value ) ) {
				$template = '%1$s=\'%2$s\'';
				$value    = json_encode( $value );
			}

			if ( is_bool( $value ) ) {
				$template = '%1$s';
			}

			// $value should not be empty, except numeric types
			if ( ! is_numeric( $value ) && empty( $value ) ) {
				continue;
			}

			$attribute    = sanitize_key( $attribute );
			$attributes[] = sprintf( $template, $attribute, $value );
		}

		return implode( ' ', $attributes );
	}
endif;

if ( ! function_exists( 'equip_get_socials' ) ) :
	/**
	 * Get social networks list
	 *
	 * @see assets/misc/socials.ini
	 *
	 * @return array
	 */
	function equip_get_socials() {
		$ini      = wp_normalize_path( EQUIP_ASSETS_DIR . '/misc/socials.ini' );
		$networks = parse_ini_file( $ini, true );

		ksort( $networks );

		return apply_filters( 'equip_get_socials', $networks );
	}
endif;

if ( ! function_exists( 'equip_get_icons' ) ) :
	/**
	 * Return icons from current theme pack
	 *
	 * @param string $pack Name of the required pack
	 *
	 * @return array
	 */
	function equip_get_icons( $pack ) {
		/**
		 * Filter the icons pack
		 *
		 * See the given link for more information about the format of an array
		 * @link http://codeb.it/fonticonpicker/
		 *
		 * @param array $icons A list of icons
		 */
		return apply_filters( "equip_get_icons_{$pack}", array() );
	}
endif;

if ( ! function_exists( 'equip_cache_key' ) ) :
	/**
	 * Get the cache key
	 *
	 * @param string     $key  Unique name of the element
	 * @param string|int $salt Some unique information, e.g. post ID
	 *
	 * @return string
	 */
	function equip_cache_key( $key, $salt = '' ) {
		$hash = substr( md5( $salt . $key ), 0, 8 );

		$key = preg_replace( '/[^a-z0-9_-]+/i', '-', $key );
		$key = str_replace( array( '-', '_' ), '-', $key );
		$key = trim( $key, '-' );
		$key = str_replace( '-', '_', $key );

		$hash = "equip_{$key}_{$hash}";

		return $hash;
	}
endif;

if ( ! function_exists( 'equip_get_unique_id' ) ):
	/**
	 * Return the unique ID for general purposes
	 *
	 * @param string $prepend Will be prepended to generated string
	 * @param int $limit      Limit the number of unique symbols!
	 *                        How many unique symbols should be in a string,
	 *                        maximum is 32 symbols. $prepend not included.
	 *
	 * @return string
	 */
	function equip_get_unique_id( $prepend = '', $limit = 8 ) {
		$unique = substr( md5( uniqid() ), 0, $limit );

		return $prepend . $unique;
	}
endif;
