/**
 * Equip
 *
 * @author 8guild
 */

(function ( $ ) {
	'use strict';

	/**
	 * Equip
	 *
	 * @constructor
	 */
	function Equip() {
		this.media.init();
		this.socials.init();
		this.icons.init();
        this.color.init();
		this.icons.init();
		this.switch.init();
		this.slider.init();
		this.dependencies.init();
	}

	/**
	 * Guild Utils Prototype
	 */
	Equip.prototype = {
		/**
		 * Work with WordPress Media Frame
		 */
		media: {
			/**
			 * Selector for "Add" button, which open the media frame
			 */
			addSelector: '.equip-media-add',

			/**
			 * Remove selector, for removing images from the set
			 */
			removeSelector: '.equip-media-remove',

			/**
			 * The <li> wrapper around the control button
			 */
			$control: null,

			/**
			 * Selector for the wrapper around "Add" button
			 */
			controlSelector: '.equip-media-control',

			/**
			 * Value input field, keeps attachment ID(s)
			 */
			$value: null,

			/**
			 * Selector for hidden value input
			 */
			valueSelector: '.equip-media-value',

			/**
			 * Main wrapper of current control
			 */
			$wrapper: null,

			/**
			 * Selector for main wrapper of current control
			 */
			wrapperSelector: '.equip-media-wrap',

			/**
			 * Preview ul list element
			 */
			$items: null,

			/**
			 * Selector for preview ul list
			 */
			itemsSelector: '.equip-media-items',

			/**
			 * Selector for each <li> inside the items previw list
			 */
			itemSelector: '.equip-media-item',

			/**
			 * Media frame itself
			 */
			mediaFrame: null,

			/**
			 * A collection of all sortable items
			 */
			$sortable: null,

			init: function () {
				var self = this;

				$( document ).on( 'click', self.addSelector, function ( e ) {
					var $this = $( this );
					self.openMediaFrame( e, $this );
				} );

				$( document ).on( 'click', self.removeSelector, function ( e ) {
					var $this = $( this );
					self.removeMedia( e, $this );
				} );

				self.$sortable = $( '[data-sortable=true] ' + self.itemsSelector );
				self.$sortable.sortable( {
					cursor: 'move',
					items: '> ' + self.itemSelector,
					placeholder: 'equip-media-item-highlight'
				} ).disableSelection();

				self.$sortable.on( 'sortupdate', function ( e, ui ) {
					var $this = $( this );
					self.sortUpdate( e, ui, $this );
				} );
			},

			/**
			 * Add new media
			 *
			 * @param e
			 * @param $this
			 */
			openMediaFrame: function ( e, $this ) {
				var self = this;

				e.preventDefault();

				self.$control = $this.parent( self.controlSelector );
				self.$wrapper = self.$control.parents( self.wrapperSelector );
				self.$value = self.$wrapper.find( self.valueSelector );
				self.$items = self.$wrapper.find( self.itemsSelector );

				// Media Library Configuration
				// Control for adding images is inside preview
				var params = $this.data(),
					values = self.$value.val(),
					is_multiple = Boolean( params.multiple || 0 );

				// Store globally
				self.mediaFrame = wp.media( {
					title: params.title,
					multiple: is_multiple,
					button: { text: params.button },
				} );

				self.mediaFrame.on( 'select', function () {
					var IDs;
					var attachments = self.mediaFrame.state().get( 'selection' ).toJSON();

					if ( is_multiple ) {
						IDs = self.handleMultipleImages( values, attachments );
					} else {
						IDs = self.handleSingleImage( attachments );
					}

					// Add IDs to hidden field
					self.$value.val( IDs );
				} );

				self.mediaFrame.open();
			},

			/**
			 * Remove selected media.
			 * Remove <li> element and remove attachment ID from hidden input.
			 *
			 * @param e
			 * @param $this
			 */
			removeMedia: function ( e, $this ) {
				var self = this;
				e.preventDefault();

				var attachmentID = $this.parents( self.itemSelector ).data( 'id' ).toString(),
					$li = $this.parent( 'li' ),
					$wrapper = $li.parents( self.wrapperSelector ),
					$value = $wrapper.find( self.valueSelector );

				// Delete <li>
				$li.remove();

				// Remove ID from hidden field
				// This should work normally both for single and multiple images
				var IDs = $value.val().split( ',' );
				var position = IDs.indexOf( attachmentID );
				if ( ~ position ) {
					IDs.splice( position, 1 );
				}

				// Save new value
				$value.val( IDs.join( ',' ) );
			},

			/**
			 * Update the values when user sort the images
			 *
			 * @param {Event} e
			 * @param {Object} ui
			 * @param {jQuery} $this
			 */
			sortUpdate: function ( e, ui, $this ) {
				var self = this;

				var $value = $this.siblings( self.valueSelector ),
					IDs = self.$sortable.sortable( 'toArray', { attribute: 'data-id' } );

				$value.val( IDs.join( ',' ) )
			},

			/**
			 * Select a single image
			 *
			 * @param attachments
			 * @returns {string}
			 */
			handleSingleImage: function ( attachments ) {
				var self = this;

				var IDs = [],
					previewHTML = '';

				// Prepare preview
				$.each( attachments, function ( key, attachment ) {
					IDs.push( attachment.id );
					previewHTML += self.preparePreview( attachment );
				} );

				// Prepend new li before "add" controller or
				// replace the first <li> under the preview ul with a new one
				var LIs = self.$items.find( 'li' );
				if ( LIs.length > 1 ) {
					LIs.first().replaceWith( previewHTML );
				} else {
					self.$items.prepend( previewHTML );
				}

				return IDs.join( '' );
			},

			/**
			 * Showing up multiple images
			 *
			 * @param {string} values List of previously selected IDs
			 * @param {object} attachments List of selected attachments by user as object.
			 *
			 * @returns {string} List of attachment IDs, separated by comma.
			 */
			handleMultipleImages: function ( values, attachments ) {
				var self = this;
				var IDs = values || [],
					previewHTML = '';

				if ( 'string' === typeof IDs ) {
					IDs = IDs.split( ',' );
				}

				// Prepare preview
				$.each( attachments, function ( key, attachment ) {
					IDs.push( attachment.id );
					previewHTML += self.preparePreview( attachment );
				} );

				// Display added images right before the add control,
				// because add control is a <li>, too
				self.$control.before( previewHTML );

				return IDs.join( ',' );
			},

			/**
			 * Prepare the single item preview HTML
			 *
			 * @param attachment
			 * @returns {string} Single item preview HTML
			 */
			preparePreview: function ( attachment ) {
				var tpl = '<li class="equip-media-item" data-id="{{id}}">'
					+ '<a href="#" class="equip-media-remove">&times;</a>'
					+ '<img src="{{src}}" alt="{{alt}}" width="150" height="150">'
					+ '</li>';

				return tpl
					.replace( '{{id}}', attachment.id )
					.replace( '{{src}}', attachment.url )
					.replace( '{{alt}}', attachment.name );
			}

		},

		/**
		 * Socials control
		 */
		socials: {
			/**
			 * Selector for "Add" button
			 */
			addSelector: '.equip-socials-add',

			/**
			 * Wrapper for all groups
			 */
			wrapperSelector: '.equip-socials-wrap',

			/**
			 * Wrapper around the single social network group of controls:
			 * select with list of networks, input for URL and remove button.
			 */
			groupSelector: '.equip-socials-group',

			/**
			 * Selector for removing the single group of fields
			 */
			removeSelector: '.equip-socials-group-remove',

			init: function () {
				var self = this;

				$( document ).on( 'click', self.addSelector, function ( e ) {
					var $this = $( this );
					self.addField( e, $this );
				} );

				$( document ).on( 'click', self.removeSelector, function ( e ) {
					var $this = $( this );
					self.removeField( e, $this );
				} );

			},

			/**
			 * Handler for adding new field for a new social network.
			 * Fires when user want to add one more field and click "Add" link.
			 *
			 * @param e
			 * @param $this
			 */
			addField: function ( e, $this ) {
				var self = this;

				e.preventDefault();

				// Detect the wrapper, based on clicked button
				// May be some buttons per one page
				var $wrapper = $this.siblings( self.wrapperSelector );
				// Clone first element
				var $item = $wrapper.find( self.groupSelector ).first().clone();

				// Append this item to container and clear the <input> field
				$item.appendTo( $wrapper ).children( 'input:text' ).val( '' );
			},

			/**
			 * Remove the single control
			 *
			 * Before removing do not forget to check if we have more than one group.
			 * Do not allow to remove the last element, because user won't be able to
			 * add new controls.
			 *
			 * @param e
			 * @param $this
			 */
			removeField: function ( e, $this ) {
				var self = this;

				e.preventDefault();

				// Find wrapper and groups to check if it possible to remove element
				var $wrapper = $this.parents( self.wrapperSelector );
				var $groups = $wrapper.find( self.groupSelector );
				// Find group which user want to remove..
				var $group = $this.parent( self.groupSelector );
				if ( $groups.length > 1 ) {
					// ..and remove it
					$group.remove();
				} else {
					// Do not remove last element, just reset values
					var selectField = $group.find( 'select' );
					var firstOptionsValue = $group.find( 'select option:first' ).val();

					selectField.val( firstOptionsValue );
					$group.find( 'input' ).val( '' );
				}
			}
		},

		/**
		 * Select an icon
		 */
		icons: {
			/**
			 * IconPicker selector.
			 * The fontIconPicker will be initialized on this element.
			 */
			iconPickerSelector: '.equip-iconpicker',

			init: function () {
				var self = this;
				var $pickers = $( self.iconPickerSelector );

				$.each( $pickers, function ( index, picker ) {
					// convert to jQuery object
					var $picker = $( picker );
					var settings = $picker.data( 'settings' ) || {};

					$picker.fontIconPicker( settings );
				} );
			}
		},

		/**
		 * Color picker
		 */
		color: {
			selector: '.equip-color',

			init: function () {
				var self = this;

				if ( typeof $.fn.wpColorPicker === 'function' ) {
					$( self.selector ).wpColorPicker();
				}
			}
		},

		/**
		 * Switch
		 */
		switch: {
			selector: '.equip-switch',

			init: function() {
				var self = this;

				var $switchers = $( self.selector );
				if ( $switchers.length > 0 && typeof $.fn.switcher === 'function' ) {
					$.each( $switchers, function( index, switcher ) {
						var $switcher = $( switcher );
						var $instance = $switcher.switcher();
						$instance.switcher( 'importLanguage', {
							en: { yes: $switcher.data( 'on' ), no: $switcher.data( 'off' ) }
						} );
						$instance.switcher( 'setLanguage', 'en' );
					} );
				}

				$( document ).on( 'change', self.selector, function ( e ) {
					var $this = $( this );
					$this.val( $this.is( ':checked' ) ? 1 : 0 );
				} );
			}
		},

		/**
		 * Slider
		 */
		slider: {
			selector: '.equip-slider',

			init: function() {
				var self = this;

				var $sliders = $( self.selector );
				if ( $sliders.length > 0 && typeof $.fn.slider === 'function' ) {
					$.each( $sliders, function ( index, slider ) {
						var $slider = $( slider ),
							sliderID = $slider.attr('id' );

						// connected input, stores real value
						var $input = $slider.siblings( '[data-slider= ' + sliderID + ']' );

						// slider options, see data- attributes on div.equip-slider
						var options = $slider.data();

						// extra options for slider
						options.value = $input.val();
						options.slide = function( event, ui ) {
							$input.val( $slider.slider( 'value' ) );
						};

						$slider.slider( options );
					} );
				}
			}
		},

		/**
		 * Fields dependencies
		 */
		dependencies: {

			/**
			 * List of all master fields by their keys
			 *
			 * [key => $master]
			 *
			 * Required for hook up these fields
			 */
			$masters: {},

			/**
			 * List of all dependent fields
			 *
			 * [jQuery object, ...]
			 */
			$dependent: [],

			/**
			 * List of all dependent field in format [key => [dependent, ...]]
			 *
			 * Where
			 * "key" - is a master's field key, @see data-key attribute value
			 * "dependent" is an object of [field: jQuery object, operator: string, value: string]
			 *
			 *   field - dependent field object to manipulate with
			 *   operator - dependent operator @see testDependent
			 *   value - dependent field value for compare with master value by given operator
			 */
			$dependents: {},

			init: function () {
				var self = this;

				self.$dependent = $( '.equip-field[data-dependent=true]' );
				self.hideAllDependent();

				$.each( self.$dependent, function( index, field ) {
					var $field = $( field ),
						required = $field.data( 'required' ),
						key = required[ 0 ];

					self.$masters[ key ] = $( '.equip-field[data-key=' + key + ']' );
					if ( ! $.isArray( self.$dependents[ key ] ) ) {
						self.$dependents[ key ] = [];
					}

					self.$dependents[ key ].push( {
						field: $field,
						operator: required[ 1 ],
						value: required[ 2 ]
					} );
				} );

				// hook up $master
				// and trigger to show suitable dependent fields
				// @see hideAllDependent
				$.each( self.$masters, function ( index, master ) {
					$( master ).on( 'keyup change', function ( e ) {
						var $this = $( this );
						self.hookUp( $this, self );
					} ).trigger( 'change' );
				} );
			},

			/**
			 * Hide all dependent fields on document.ready
			 */
			hideAllDependent: function() {
				var self = this;

				$.each( self.$dependent, function ( index, field ) {
					$( field ).hide();
				} );
			},

			/**
			 * Hide only dependents, assigned to a specified $master
			 *
			 * @param dependents
			 */
			hideMasterDependents: function( dependents ) {
				$.each( dependents, function( index, slave ) {
					slave.field.hide();
				} );
			},

			/**
			 * This function will be called when master field is changed
			 *
			 * @param $this
			 * @param self
			 */
			hookUp: function ( $this, self ) {
				// TODO: reorder and may be delete
				setTimeout( function () {
					var key = $this.data( 'key' ),
						$control = $this.find( ':input:enabled' ),
						value = $control.val(),
						dependents = self.$dependents[ key ];

					self.hideMasterDependents( dependents );
					setTimeout( function () {
						$.each( dependents, function ( index, slave ) {
							var isSuitable = self.testDependent( value, slave.operator, slave.value );
							if ( isSuitable ) {
								slave.field.show();
							}
						} );
					}, 50 );
				}, 50 );
			},

			/**
			 * Test the dependent with master value by given operator
			 *
			 * @param masterValue
			 * @param operator
			 * @param slaveValue
			 *
			 * @returns {boolean}
			 */
			testDependent: function ( masterValue, operator, slaveValue ) {
				var result;

				switch ( operator ) {
					case '!=':
						result = ( masterValue != slaveValue );
						break;

					case '=':
					default:
						result = ( masterValue == slaveValue );
						break;
				}

				return result;
			}
		}
	};

	/**
	 * Initialize the Equip
	 */
	$( document ).ready( function () {
		var equip = new Equip();
	} );

})( jQuery );
