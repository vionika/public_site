<?php

/**
 * Sanitize values
 *
 * @author  8guild
 * @package Equip
 */
final class Equip_Sanitizer {

	/**
	 * Sanitize all values at once.
	 *
	 * @param array  $values   Values
	 * @param array  $contents List of fields arrays
	 * @param string $slug     Element unique name
	 *
	 * @return array
	 */
	public static function bulk_sanitize( $values, $contents, $slug ) {
		// if $slug or $contents not given returns $values AS IS
		if ( empty( $slug ) || empty( $contents ) ) {
			return $values;
		}

		// if $contents has a callable type
		if ( is_callable( $contents ) ) {
			return static::sanitize_callback( $values, $slug );
		}

		// convert $contents into format [key => $settings],
		// where key is a possible value in $values array

		$_fields   = array();
		$_contents = array();
		array_walk( $contents, function ( $settings, $k ) use ( &$_contents, &$_fields ) {
			$key   = sanitize_key( $settings['key'] );
			$field = Equip::field( $settings );

			// Here $settings is a single array, and $_contents
			// is a converted list of arrays with $settings
			$_contents[ $key ] = $settings;
			$_fields[ $key ]   = $field;
		} );

		$_values = array();
		foreach( (array) $values as $key => $value ) {
			$_values[ $key ] = static::sanitize( $value, $_contents[ $key ], $_fields[ $key ], $slug );
		}
		unset( $key, $value );

		return $_values;
	}

	/**
	 * Sanitize the single field before it will be saved into database.
	 *
	 * Custom sanitizing callback has a highest priority. If custom
	 * callback not defined, use built-in field-specific sanitizing,
	 * if exists.
	 *
	 * Also, filter is available before returning the value.
	 *
	 * @param mixed       $value    Field value
	 * @param array       $settings Field settings
	 * @param Equip_Field $field    Field object
	 * @param string      $slug     Current element unique name
	 *
	 * @return mixed
	 */
	public static function sanitize( $value, $settings, $field, $slug ) {
		if ( array_key_exists( 'sanitize', $settings ) && is_callable( $settings['sanitize'] ) ) {
			$value = call_user_func( $settings['sanitize'], $value );
		} elseif ( is_callable( array( $field, 'sanitize' ) ) ) {
			$value = $field->sanitize( $value, $settings, $slug );
		}

		/**
		 * Filter the value of field before it will be saved into the database.
		 *
		 * This hook fires only for unique element by its slug.
		 *
		 * @param mixed $value    Value of current field
		 * @param array $settings Field settings
		 */
		$value = apply_filters( "equip/{$slug}/sanitize", $value, $settings );

		/**
		 * Filter the value of field before it will be saved into the database.
		 *
		 * This hook fires for each element.
		 *
		 * @param mixed  $value    Value of current field.
		 * @param array  $settings Field settings
		 * @param string $slug     Unique element name, like meta box or page slug.
		 */
		$value = apply_filters( 'equip/sanitize', $value, $settings, $slug );

		return $value;
	}

	/**
	 * Sanitize the content of callbacks
	 *
	 * @param mixed  $values Values of current element
	 * @param string $slug   Element unique name
	 *
	 * @return mixed
	 */
	public static function sanitize_callback( $values, $slug ) {
		/**
		 * Sanitize the defined callback's values
		 *
		 * @param mixed $values Values of current element
		 */
		$values = apply_filters( "equip/{$slug}/callback/sanitize", $values );

		/**
		 * Sanitize the each callback's values
		 *
		 * @param mixed  $values Values of current element
		 * @param string $slug   Element unique name
		 */
		$values = apply_filters( 'equip/callback/sanitize', $values, $slug );

		return $values;
	}
}