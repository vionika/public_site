<?php

/**
 * Abstract field
 */
abstract class Equip_Field {

	/**
	 * Render the field itself
	 *
	 * @param string $slug     Element unique name
	 * @param array  $settings Field settings
	 * @param mixed  $value    Field value. Real value, default one or null. Already escaped!
	 */
	abstract public function render( $slug, $settings, $value );

	/**
	 * Sanitize value before saving to database
	 *
	 * This method does not sanitize anything! It returns value AS IS.
	 * If you want to use proper sanitizing you MUST to overload this
	 * method in your custom field class.
	 *
	 * @param mixed  $value    Field value
	 * @param array  $settings Field settings
	 * @param string $slug     Current element slug
	 *
	 * @return mixed Sanitized value
	 */
	public function sanitize( $value, $settings, $slug ) {
		return $value;
	}

	/**
	 * Escape value before rendering
	 *
	 * This method does not escape anything! It returns value AS IS.
	 * If you want to use proper escaping you MUST to overload this
	 * method in your custom field class.
	 *
	 * @param mixed  $value    Field value
	 * @param array  $settings Field settings
	 * @param string $slug     Current element slug
	 *
	 * @return mixed Escaped value
	 */
	public function escape( $value, $settings, $slug ) {
		return $value;
	}

	/**
	 * Enqueue JS and/or CSS for this field.
	 *
	 * This is a best place to enqueue scripts and styles.
	 * This method called during the "admin_enqueue_scripts" hook
	 *
	 * @see Equip_Enqueue::enqueue
	 *
	 * @return bool
	 */
	public function enqueue() {
		return false;
	}

	/**
	 * Get field ID, based on field $slug and $key
	 *
	 * @param string $slug Unique name for element, like meta box or page, etc.
	 * @param string $key  Current field unique name
	 *
	 * @return string
	 */
	public function get_field_id( $slug, $key ) {
		$slug = ltrim( $slug, '_' );
		$slug = str_replace( '_', '-', $slug );

		return sprintf( 'equip-%1$s-%2$s', $slug, $key );
	}

	/**
	 * Get field "name" attribute for the field based on $slug and $key
	 *
	 * @param string $slug Unique name for element
	 * @param string $key  Current field unique name
	 *
	 * @return string
	 */
	public function get_field_name( $slug, $key ) {
		return sprintf( '%1$s[%2$s]', $slug, $key );
	}

	/**
	 * Returns ready-to-use string of HTML attributes
	 *
	 * @param array $attr Raw HTML attributes
	 *
	 * @return string
	 */
	public function get_field_attr( $attr ) {
		$attr = Equip_Helpers::combine_attr( $this->default_attr(), $attr );

		$restricted = Equip_Helpers::get_restricted_attr();
		$attributes = array_diff_key( (array) $attr, $restricted );

		// TODO: standalone attributes? like hidden, disabled, etc
		// TODO: arrays
		// $attributes = array_map( 'esc_attr', $attributes );

		$attributes = equip_get_html_attributes( $attributes );

		return $attributes;
	}

	/**
	 * This method should return an array of default HTML attributes for current field.
	 *
	 * Yes, this method should be implemented in each field, e.g. for "input"
	 * these attributes should be type=text, etc.
	 *
	 * @return array A list of default attributes
	 */
	public function default_attr() {
		return array();
	}

}