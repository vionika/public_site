<?php

/**
 * Render and process meta boxes
 *
 * TODO: how to add meta box above the content? See ACF
 *
 * @author  8guild
 * @package Equip
 */
class Equip_Meta_Box {

	/**
	 * Constructor
	 */
	public function __construct() {
		add_action( 'add_meta_boxes', array( $this, 'add' ), 10, 2 );
		add_action( 'save_post', array( $this, 'save' ), 10, 2 );
		add_action( 'equip/meta_box/saved', array( $this, 'flush' ), 10, 2 );
	}

	/**
	 * Add meta box
	 *
	 * @see  add_meta_box
	 * @link https://codex.wordpress.org/Function_Reference/add_meta_box
	 *
	 * @param string  $post_type Post type
	 * @param WP_Post $post      Post object
	 *
	 * @return bool
	 */
	public function add( $post_type, $post ) {
		$storage    = Equip::get( Equip::STORAGE );
		$meta_boxes = $storage->get( Equip::META_BOX );

		// Add all meta boxes from storage at once
		foreach ( (array) $meta_boxes as $slug => $data ) {
			// Make sure all required args present
			$args = wp_parse_args(
				$data['args'],
				Equip_Helpers::get_meta_box_defaults( $slug )
			);

			/**
			 * Filter current meta box arguments.
			 *
			 * Fires only for unique meta box.
			 *
			 * @param array $args Meta box arguments
			 */
			$args = apply_filters( "equip/meta_box/{$slug}/args", $args );

			/**
			 * Filter meta box arguments.
			 *
			 * Fires for each meta box.
			 *
			 * @param array  $args Meta box arguments
			 * @param string $slug Meta box unique key
			 */
			$args = apply_filters( 'equip/meta_box/args', $args, $slug );

			// Finally, add the meta box (with multiple screens hack!)
			if ( is_array( $args['screen'] ) ) {
				foreach( $args['screen'] as $screen ) {
					add_meta_box(
						$args['id'],
						$args['title'],
						array( $this, 'render' ),
						$screen,
						$args['context'],
						$args['priority'],
						array( 'slug' => $slug, 'contents' => $data['contents'], 'args' => $args )
					);
				}
			} else {
				add_meta_box(
					$args['id'],
					$args['title'],
					array( $this, 'render' ),
					$args['screen'],
					$args['context'],
					$args['priority'],
					array( 'slug' => $slug, 'contents' => $data['contents'], 'args' => $args )
				);
			}

			unset( $defaults, $args );
		}
		unset( $slug, $data );

		return true;
	}

	/**
	 * Render meta box
	 *
	 * @param WP_Post $post     Post object
	 * @param array   $meta_box Is an array with meta box slug, contents and args.
	 */
	public function render( $post, $meta_box ) {
		// Get meta box data
		$slug     = $meta_box['args']['slug'];
		$args     = $meta_box['args']['args'];
		$contents = $meta_box['args']['contents'];

		if ( empty( $slug ) ) {
			return;
		}

		// create a nonce
		$action = Equip_Helpers::get_nonce_action( Equip::META_BOX, $slug );
		$nonce  = Equip_Helpers::get_nonce_name( Equip::META_BOX, $slug );
		wp_nonce_field( $action, $nonce );
		unset( $nonce, $action );

		// get values from DB
		$values = get_post_meta( $post->ID, $slug, true );

		// render
		$render = Equip::engine( $args['engine'] );
		$render->render( $slug, $contents, $values );
	}

	/**
	 * Save post metadata when the post is saved.
	 *
	 * @param int     $post_id The ID of the post.
	 * @param WP_Post $post    Post object
	 *
	 * @return void
	 */
	public function save( $post_id, $post ) {
		// No auto-drafts, please
		if ( isset( $post->post_status ) && 'auto-draft' === $post->post_status ) {
			return;
		}

		/*
		 * How to detect that current post has meta boxes, added through Equip Framework?
		 *
		 * 1. Get list of all meta boxes from Equip_Storage
		 * 2. Check, if $_POST has any of them
		 * 3. ...
		 * 4. PROFIT!
		 *
		 * Also, this check costs less.
		 */

		/**
		 * @var Equip_Storage $storage
		 */
		$storage = Equip::get( Equip::STORAGE );
		$all     = $storage->get_list_of( Equip::META_BOX );
		$current = array_intersect_key( $_POST, array_flip( $all ) );
		unset( $all );

		if ( 0 === count( $current ) ) {
			// if there is no Equip's meta boxes
			return;
		}

		// Check the permissions
		$post_type = 'page' === $post->post_type ? 'pages' : 'posts';
		if ( false === current_user_can( "edit_{$post_type}" ) ) {
			return;
		}

		// Check the auto-save and revisions
		if ( wp_is_post_autosave( $post_id ) || wp_is_post_revision( $post_id ) ) {
			return;
		}

		foreach ( $current as $slug => $values ) {
			/**
			 * Fires right before the meta box would be saved
			 *
			 * @param string  $slug    Meta box unique name
			 * @param int     $post_id Post ID
			 * @param WP_Post $post    Post object
			 */
			do_action( 'equip/meta_box/save', $slug, $post_id, $post );

			// Check the nonce. Nonce is individual for each meta box.
			$action = Equip_Helpers::get_nonce_action( Equip::META_BOX, $slug );
			$nonce  = Equip_Helpers::get_nonce_name( Equip::META_BOX, $slug );

			if ( false === array_key_exists( $nonce, $_POST ) ) {
				continue;
			}

			if ( false === wp_verify_nonce( $_POST[ $nonce ], $action ) ) {
				continue;
			}

			$contents = $storage->get_contents( Equip::META_BOX, $slug );
			$values   = Equip_Sanitizer::bulk_sanitize( $values, $contents, $slug );

			update_post_meta( $post_id, $slug, $values );

			unset( $nonce, $action, $contents );

			/**
			 * Fires when meta box was already saved
			 *
			 * @param string  $slug    Meta box unique name
			 * @param int     $post_id Post ID
			 * @param WP_Post $post    Post object
			 */
			do_action( 'equip/meta_box/saved', $slug, $post_id, $post );
		}
		unset( $slug, $values );
	}

	/**
	 * Returns the values of meta box.
	 *
	 * If $field is specified will return the field's value.
	 *
	 * @param int         $post_id Post ID
	 * @param string      $slug    Meta box unique name
	 * @param null|string $field   Key of the field
	 * @param mixed       $default Default value
	 *
	 * @return mixed Array with key-values, mixed data if field is specified
	 *               and the value of $default field if nothing found.
	 */
	public function get( $post_id, $slug, $field = null, $default = array() ) {
		$cache_key   = equip_cache_key( $slug, $post_id );
		$cache_group = Equip::META_BOX;

		// Cached value should always be an array
		$values = wp_cache_get( $cache_key, $cache_group );
		if ( false === $values ) {
			$values = get_post_meta( $post_id, $slug, true );
			if ( empty( $values ) ) {
				// possible cases: meta box not saved yet
				// or mistake in $post_id or $slug
				return $default;
			}

			// cache for 1 day
			wp_cache_set( $cache_key, $values, $cache_group, 86400 );
		}

		if ( ! is_array( $values ) ) {
			// return AS IS for non-array values
			$result = $values;
		} elseif ( null === $field ) {
			// return whole array if $field not specified
			$result = $values;
		} elseif ( array_key_exists( $field, $values ) ) {
			// if specified $field present
			$result = $values[ $field ];
		} else {
			// nothing matched, return default value
			$result = $default;
		}

		return $result;
	}

	/**
	 * Flush the cached meta box value when new instance is saved
	 *
	 * Note! This methods removes only current cached meta box,
	 * by its $slug and $post_id
	 *
	 * @param string $slug    Meta box unique name
	 * @param int    $post_id Post ID
	 */
	public function flush( $slug, $post_id ) {
		$cache_key   = equip_cache_key( $slug, $post_id );
		$cache_group = Equip::META_BOX;

		wp_cache_delete( $cache_key, $cache_group );
	}
}