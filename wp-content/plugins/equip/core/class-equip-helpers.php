<?php

/**
 * Helpers functions and utilities, which used only
 * inside the Equip and not intended for using outside.
 *
 * @author  8guild
 * @package Equip\Utils
 */
class Equip_Helpers {

	/**
	 * Returns the default settings for fields.
	 *
	 * This fields should be present in every field to avoid bad indexes.
	 *
	 * @return array
	 */
	public static function get_field_defaults() {
		$defaults = array(
			'weight'    => 10,
			'title'     => '',
			'desc'      => '',
			'desc_wrap' => '%s',
			'default'   => null,
			'attr'      => array(),
			'sanitize'  => '',
			'escape'    => '',
		);

		/**
		 * Filter the default field settings.
		 *
		 * @param array $defaults Default keys and values
		 */
		return apply_filters( 'equip/field/defaults', $defaults );
	}

	/**
	 * Returns the meta box default args
	 *
	 * @param string $slug Meta box unique name
	 *
	 * @return array
	 */
	public static function get_meta_box_defaults( $slug ) {
		$defaults = array(
			'id'       => str_replace( '_', '-', ltrim( $slug, '_' ) ),
			'title'    => ucfirst( preg_replace( '/[_\\W]+/', ' ', ltrim( $slug, '_' ) ) ),
			'screen'   => 'post',
			'context'  => 'normal',
			'priority' => 'default',
			'engine'   => 'default',
		);

		/**
		 * Filter the default args of current meta box.
		 *
		 * Fires only for unique meta box. This filter expects the array with keys
		 * described in {@see add_meta_box} and some extra equip keys.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_meta_box
		 *
		 * @param array $defaults Meta box default arguments
		 */
		$defaults = apply_filters( "equip/meta_box/{$slug}/defaults", $defaults );
		/**
		 * Filter the meta box default args.
		 *
		 * Fires for each meta box. This filter expects the array with keys
		 * described in {@see add_meta_box} and some extra equip keys.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/add_meta_box
		 *
		 * @param array  $defaults Meta box default arguments
		 * @param string $slug     Meta box unique key
		 */
		$defaults = apply_filters( 'equip/meta_box/defaults', $defaults, $slug );

		return $defaults;
	}

	/**
	 * Returns meta box nonce name.
	 *
	 * Used as first parameter for {@see wp_nonce_filed}
	 * and second for {@see wp_verify_nonce}
	 *
	 * @link https://codex.wordpress.org/Function_Reference/wp_nonce_field
	 * @link https://codex.wordpress.org/Function_Reference/wp_verify_nonce
	 *
	 * @param string $module Element type: meta box, etc.
	 * @param string $slug   Element unique name
	 *
	 * @return string
	 */
	public static function get_nonce_name( $module, $slug ) {
		$_slug = ltrim( $slug, '_' );
		$nonce = "equip_{$module}_{$_slug}_nonce";

		/**
		 * Filter the nonce name by its $slug
		 *
		 * Fires only for unique element
		 *
		 * @param string $nonce Nonce name
		 */
		$nonce = apply_filters( "equip/{$slug}/nonce/name", $nonce );

		/**
		 * Filter the nonce name.
		 *
		 * Fires for all elements.
		 *
		 * @param string $nonce Nonce name
		 * @param string $slug  Element unique name
		 */
		$nonce = apply_filters( "equip/nonce/name", $nonce, $slug );

		return $nonce;
	}

	/**
	 * Returns meta box nonce action.
	 *
	 * Used as second parameter for {@see wp_nonce_field} and as a key
	 * for receiving the nonce value from $_POST for {@see wp_verify_nonce}.
	 *
	 * @link https://codex.wordpress.org/Function_Reference/wp_nonce_field
	 * @link https://codex.wordpress.org/Function_Reference/wp_verify_nonce
	 *
	 * @param string $module Element type: meta box, etc.
	 * @param string $slug   Element unique name
	 *
	 * @return string
	 */
	public static function get_nonce_action( $module, $slug ) {
		$_slug = ltrim( $slug, '_' );
		$action = "equip_{$module}_{$_slug}_nonce_action";

		/**
		 * Filter the nonce action by $slug.
		 *
		 * Fires only for unique element.
		 *
		 * @param string $action Action name
		 */
		$action = apply_filters( "equip/{$slug}/nonce/action", $action );

		/**
		 * Filter the meta box nonce action. Fires for all meta boxes.
		 *
		 * @link https://codex.wordpress.org/Function_Reference/wp_nonce_field
		 * @link https://codex.wordpress.org/Function_Reference/wp_verify_nonce
		 *
		 * @param string $action Action name
		 * @param string $slug   Meta box unique name
		 */
		$action = apply_filters( 'equip/nonce/action', $action, $slug );

		return $action;
	}

	/**
	 * Get list of restricted HTML attributes, which are not allowed to use
	 * in every field settings.
	 *
	 * @link  https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes
	 *
	 * @return array
	 */
	public static function get_restricted_attr() {
		$restricted = array(
			'id'    => '',
			'name'  => '',
			'value' => ''
		);

		/**
		 * Filter the restricted attributes
		 *
		 * @param array $attr List of restricted HTML attributes
		 */
		return apply_filters( 'equip/attr/restricted', $restricted );
	}

	/**
	 * Combine possible duplicated attributes
	 *
	 * This helper is designed for situations, when particular field MUST have some
	 * attributes, like some specific class. But user have an option to add a custom
	 * attributes by passing them into "attr" key. E.g. class.
	 *
	 * Note! Combined values removes from the original arrays.
	 *
	 * @param array $left  Required attribute for a field, defined in {@see Equip_Field::default_attr}
	 * @param array $right A list of user-defined attributes, passed into "attr" key.
	 *
	 * @return array
	 */
	public static function combine_attr( $left = array(), $right = array() ) {
		$intersected = array_intersect_key( $left, $right );
		if ( empty( $intersected ) ) {
			return array_merge( $left, $right );
		}

		$combined = array();
		foreach ( array_keys( $intersected ) as $key ) {
			switch ( $key ) {
				case 'class':
					$combined[ $key ] = equip_get_class_set( array( $left[ $key ], $right[ $key ] ) );
					break;

				default:
					/**
					 * Apply the custom combination strategy for unknown attributes
					 *
					 * $key is html attribute name, like "class", "id", etc.
					 *
					 * @param string $left  Required attribute for a field, defined in {@see Equip_Field::default_attr}
					 * @param string $right User-defined attribute, passed into "attr" key.
					 */
					$combined[ $key ] = apply_filters( "equip/attr/combine/{$key}", $left[ $key ], $right[ $key ] );
					break;
			}

			unset( $left[ $key ], $right[ $key ] );
		}

		return array_merge( $left, $right, $combined );
	}
}