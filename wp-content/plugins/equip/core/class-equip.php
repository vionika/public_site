<?php

/**
 * Create and save objects for multiple usage to save the performance
 *
 * @author  8guild
 * @package Equip
 */
class Equip {
	/**#@+
	 * Modules identifiers.
	 * Uses for creating an instance of class.
	 */
	const STORAGE  = 'storage';
	const META_BOX = 'meta_box';
	const ENQUEUE  = 'enqueue';
	const PAGE     = 'page';
	const USER     = 'user';
	/**#@-*/

	/**
	 * @var array Static variable for stored all instances
	 */
	private static $instances = array();

	/**
	 * @var array A list of engine instances
	 */
	private static $engines = array();

	/**
	 * @var array A list of field instances
	 */
	private static $fields = array();

	/**
	 * Return the instance of requested class
	 *
	 * @param string $slug Class slug
	 *
	 * @return mixed
	 */
	public static function get( $slug ) {

		// TODO: Allow to use custom core class. Add a filter.
		// Например, кто-то захочет реализовать плагин с более продвинутым
		// хранилищем. Нужно дать возможность переопределять базовые классы.
		// И использовать кастомные вместо них.

		if ( false === array_key_exists( $slug, self::$instances ) ) {

			$instance = null;

			switch ( $slug ) {
				case self::META_BOX:
					$instance = new Equip_Meta_Box();
					break;

				case self::STORAGE:
					$instance = new Equip_Storage();
					break;

				case self::PAGE:
					$instance = new Equip_Page();
					break;

				case self::USER:
					$instance = new Equip_User();
					break;

				case 'settings':
					$instance = new Equip_Settings();
					break;

				case self::ENQUEUE:
					$instance = new Equip_Enqueue();
					break;

				default:
					/*
					 * TODO: добавить сюда фильтр или экшн, что делать с "расширением" функционала
					 */
					break;
			}

			self::$instances[ $slug ] = $instance;

			unset( $instance );
		}

		return self::$instances[ $slug ];
	}

	/**
	 * Initialize a module: meta box, user settings, etc.
	 *
	 * Formally this method is a wrapper for Equip::get()
	 * and will be used for better code readability.
	 *
	 * TODO: maybe add more logic in future
	 *
	 * @param string $module
	 *
	 * @return mixed
	 */
	public static function init( $module ) {
		return self::get( $module );
	}

	/**
	 * Return the instance of field, depending on field type
	 *
	 * @param array|Equip_Field $settings Field settings
	 *
	 * @return Equip_Field Instance of class or {@see exit}
	 */
	public static function field( $settings ) {
		if ( null === $settings ) {
			trigger_error( 'Equip::field(): can\'t create field object from nothing.' );
			exit( 'Equip::field(): can\'t create field object from nothing.' );
		}

		if ( ! array_key_exists( 'field', $settings ) ) {
			trigger_error( 'Equip::field(): can\'t create field object. Key "field" is absent in field settings.' );
			exit( 'Equip::field(): can\'t create field object. Key "field" is absent in field settings.' );
		}

		// If Equip_Field object passed return AS IS
		if ( $settings['field'] instanceof Equip_Field ) {
			return $settings['field'];
		}

		$field = strtolower( trim( $settings['field'] ) );

		/*
		 * In my opinion it is a great idea to store field objects
		 * by types, not by their class names. In this case objects
		 * won't be affected by class names, because class name can
		 * be changed.
		 */
		if ( array_key_exists( $field, self::$fields ) ) {
			return self::$fields[ $field ];
		}

		/**
		 * Filter the class name of given field by its type.
		 *
		 * Field type is the string, passed as "field" in field settings.
		 *
		 * @param string $class Class name. The default is: "Equip_Field_{$field}".
		 */
		$class = apply_filters( "equip/factory/field/{$field}/class", "Equip_Field_{$field}" );

		/**
		 * Filter the class name for given field by it's type.
		 *
		 * Fires for all fields
		 *
		 * @param string $class Class name. Default is "Equip_Field_{$field}.
		 * @param string $field Field type. This is the string, passed as "field" key in field settings.
		 */
		$class = apply_filters( 'equip/factory/field/class', $class, $field );

		/**
		 * Filter the fields map.
		 *
		 * This filter expects the array where key is a field type,
		 * and value is an absolute path to field class.
		 *
		 * @param array $map Fields map
		 */
		$map = apply_filters( 'equip/factory/field/map', array() );
		if ( empty( $map ) ) {
			trigger_error( 'Equip::field(): can\'t create field object. Fields $map is empty.' );
			exit( 'Equip::field(): can\'t create field object. Fields $map is empty.' );
		}

		if ( ! array_key_exists( $field, $map ) ) {
			trigger_error( "Equip::field(): field {$field} not found in \$map." );
			exit( "Field \"{$field}\" not found in \$map." );
		}

		$path = $map[ $field ];
		if ( ! is_readable( $path ) ) {
			trigger_error( "Equip::field(): path {$path} is not readable or file not exists." );
			exit( "Equip::field(): path {$path} is not readable or file not exists." );
		}

		require $path;

		self::$fields[ $field ] = new $class();

		return self::$fields[ $field ];
	}

	/**
	 * Return the instance of engine, based on engine type.
	 * Also it is possible to pass an object of Equip_Render.
	 *
	 * TODO: implement overload all engines
	 *
	 * @param string|Equip_Render $engine Engine
	 *
	 * @return Equip_Render Instance of class
	 */
	public static function engine( $engine ) {
		// return as is
		if ( $engine instanceof Equip_Render ) {
			return $engine;
		}

		if ( empty( $engine ) ) {
			/**
			 * Filter the default rendering engine. By default is "default" engine :)
			 *
			 * @param string $engine Default engine
			 */
			$engine = (string) apply_filters( 'equip/factory/engine/default', 'default' );
		} else {
			$engine = strtolower( trim( $engine ) );
		}

		/*
		 * Engine instances are storing by type, not class names.
		 */
		if ( array_key_exists( $engine, self::$engines ) ) {
			return self::$engines[ $engine ];
		}

		/**
		 * Filter the engine class name. Fires only for given engine.
		 *
		 * @param string $class Class name
		 */
		$class = apply_filters( "equip/factory/engine/{$engine}/class", "Equip_Render_{$engine}" );

		/**
		 * Filter the engine class name. Fires for every engine.
		 *
		 * @param string $class  Class name
		 * @param string $engine Current engine type
		 */
		$class = apply_filters( 'equip/factory/engine/class', $class, $engine );

		/**
		 * Filter the engines map.
		 *
		 * This filters expects the array where key is engine type
		 * and value is an absolute path to engine class.
		 *
		 * @param array $map Engines map
		 */
		$map = apply_filters( 'equip/factory/engine/map', array() );
		if ( empty( $map ) ) {
			trigger_error( 'Equip::engine(): engine maps should not be empty.' );
		}

		if ( ! array_key_exists( $engine, $map ) ) {
			trigger_error( "Equip::engine(): engine {$engine} not found in map." );
		}

		$path = $map[ $engine ];
		if ( ! is_readable( $path ) ) {
			trigger_error( "Equip::engine(): path {$path} is not readable or file not exists." );
		}

		// finally load a file
		require $path;

		// and create an instance
		self::$engines[ $engine ] = new $class();

		return self::$engines[ $engine ];
	}
}