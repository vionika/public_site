<?php

/**
 * Repository class for storing fields, sections, args and use them anywhere.
 * No global variables, please.
 *
 * @author  8guild
 * @package Equip\Core
 */
class Equip_Storage {
	/**
	 * @var array Storage
	 */
	private static $storage = array();

	public function __construct() {}

	/**
	 * Add elements to storage
	 *
	 * @param string $module Type of stored element for
	 *                       further usage {@see Equip_Storage::get()}
	 * @param string $slug   Element unique name
	 * @param mixed  $data   Data to be stored
	 */
	protected function _add( $module, $slug, $data ) {
		self::$storage[ $module ][ $slug ] = $data;
	}

	/**
	 * Store elements to storage. Wrapper for {@see Equip_Storage::_add}.
	 *
	 * This method filters and organize data before storing for more easy work.
	 *
	 * @param string $module   Module: meta box, user, page, etc.
	 * @param string $slug     Element unique name
	 * @param array  $contents Element contents
	 * @param array  $args     Element args
	 *
	 * @return bool
	 */
	public function add( $module, $slug, $contents, $args = array() ) {
		if ( empty( $slug ) ) {
			return false;
		}

		if ( is_array( $contents ) ) {
			/**
			 * Filter fields of current element by $slug.
			 *
			 * This filter may be useful for dynamically add or remove fields before
			 * they will be added to the storage.
			 *
			 * @param array $contents List of fields
			 */
			$contents = apply_filters( "equip/storage/{$slug}/contents", $contents );
			/**
			 * Filter fields of each elements.
			 *
			 * Use this filter to dynamically add or remove fields.
			 *
			 * @param array  $contents List of fields
			 * @param string $slug     Current element slug
			 */
			$contents = apply_filters( 'equip/storage/contents', $contents, $slug );
		}

		// Prepare data
		$data = array(
			'contents' => $contents,
			'args'     => $args,
		);

		$this->_add( $module, $slug, $data );

		return true;
	}

	/**
	 * Get data from storage
	 *
	 * @param string      $module Type of stored element, like "meta_box", etc
	 * @param string      $slug   Element unique name
	 * @param string|null $filter If user require some specific information
	 *                            from storage, not the whole bunch of data, he could
	 *                            specify the key and data will be filtered. E.g. get
	 *                            only args or only fields, etc.
	 *
	 * @return mixed NULL if nothing found or data, typically an array
	 */
	public function get( $module, $slug = '', $filter = null ) {
		// check, if requested "type" exists in storage
		if ( false === array_key_exists( $module, self::$storage ) ) {
			return null;
		} elseif ( ! empty( $slug ) && array_key_exists( $slug, self::$storage[ $module ] ) ) {
			$data = self::$storage[ $module ][ $slug ];
		} else {
			$data = self::$storage[ $module ];
		}

		// if user require something special from storage, not whole bunch of data
		if ( null !== $filter && array_key_exists( $filter, $data ) ) {
			$data = $data[ $filter ];
		}

		return $data;
	}

	/**
	 * Update element in storage
	 *
	 * Note: this method does not replace but complements existing data
	 *
	 * @deprecated
	 *
	 * @param string $type Type of stored element for further usage, {@see Equip_Storage::get()}
	 * @param string $slug Element unique name
	 * @param mixed  $data Data to be updated
	 *
	 * @return bool
	 */
	public function update( $type, $slug, $data ) {
		// Get current item from storage
		$_data = $this->get( $type, $slug );
		if ( null === $_data ) {
			return false;
		}

		$data = array_merge( $_data, $data );
		$this->_add( $type, $slug, $data );

		return true;
	}

	/**
	 * Return the elements keys, registers through framework.
	 * Key is a "slug" parameter, with this name values stored in the database.
	 *
	 * @param string $type What elements [meta boxes, pages, etc] to return
	 *
	 * @return array
	 *
	 * @since 1.0.0
	 */
	public function get_list_of( $type ) {
		if ( false === array_key_exists( $type, self::$storage ) ) {
			return array();
		}

		return array_keys( self::$storage[ $type ] );
	}

	/**
	 * Returns the contents of the element by given $slug
	 *
	 * @param string $module Module type: meta box, etc
	 * @param string $slug   Element unique name
	 *
	 * @return mixed
	 */
	public function get_contents( $module, $slug ) {
		return $this->get( $module, $slug, 'contents' );
	}

	/**
	 * Returns the element arguments by given $slug
	 *
	 * @param string $module Module type: meta box, etc
	 * @param string $slug   Element unique name
	 *
	 * @return mixed
	 */
	public function get_args( $module, $slug ) {
		return $this->get( $module, $slug, 'args' );
	}
}