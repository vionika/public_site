<?php

/**
 * Render
 *
 * @author  8guild
 * @package Equip
 */
abstract class Equip_Render {

	/**
	 * Render contents
	 *
	 * @param string $slug     Unique element name, also value of HTML "name" parameter
	 * @param array  $contents Expects, that both keys "sections" and "fields" will be present in array.
	 * @param mixed  $values   Values from DB. Not defaults!
	 *
	 * @return bool
	 */
	public function render( $slug, $contents, $values = null ) {
		if ( empty( $slug ) ) {
			trigger_error( 'Equip_Render::render(): Empty $slug not allowed.' );

			return false;
		}

		// Callbacks support
		if ( is_callable( $contents ) ) {
			call_user_func( $contents, $slug, $values );

			return true;
		}

		if ( ! is_array( $contents ) ) {
			trigger_error(
				'Equip_Render::render(): $contents should be an array or callable, '
				. gettype( $contents )
				. ' given'
			);

			return false;
		}

		/**
		 * Defaults key-values for every field
		 *
		 * @var array $defaults
		 */
		$defaults = Equip_Helpers::get_field_defaults();

		// Filter fields for avoiding bad indexes
		// Remove non-valid fields (missing indexes "key" or "field")
		$_contents = array();
		$fields    = array();
		foreach ( $contents as $k => $settings ) {
			if ( ! array_key_exists( 'key', $settings ) ) {
				continue;
			}

			if ( ! array_key_exists( 'field', $settings ) ) {
				continue;
			}

			// skip hidden fields
			if ( array_key_exists( 'hidden', $settings ) && true === $settings['hidden'] ) {
				continue;
			}

			// merge with defaults and add to list of fields
			$key             = sanitize_key( $settings['key'] );
			$settings['key'] = $key;
			$_contents[ $k ] = wp_parse_args( $settings, $defaults );

			// create the field object
			$fields[ $key ] = Equip::field( $settings );
			unset( $key );
		}
		unset( $k, $settings );

		$contents = $_contents;
		unset( $_contents );

		// Escape values
		$values = Equip_Escaper::bulk_escape( $values, $contents, $slug );

		/**
		 * Fires before the fields starting to render
		 *
		 * @param string $slug     Element unique name
		 * @param array  $contents Element fields
		 */
		do_action( 'equip/render/fields/before', $slug, $contents );

		$this->before_fields( $slug, $contents );

		foreach ( $contents as $settings ) {
			$key = $settings['key'];

			$this->_before_field( $slug, $settings );

			/**
			 * Fires before the each field
			 *
			 * @param string      $slug     Element unique name
			 * @param array       $settings Element field settings
			 * @param Equip_Field $field    Element field object
			 */
			do_action( 'equip/render/field/before', $slug, $settings, $fields[ $key ] );

			$this->before_field( $slug, $settings, $fields[ $key ] );
			$this->do_field( $slug, $settings, $values[ $key ], $fields[ $key ] );
			$this->after_field( $slug, $settings, $fields[ $key ] );

			/**
			 * Fires at the end of each field
			 *
			 * @param string      $slug     Element unique name
			 * @param array       $settings Element field declaration array
			 * @param Equip_Field $field    Element field object
			 */
			do_action( 'equip/render/field/after', $slug, $settings, $fields[ $key ] );

			$this->_after_field( $slug, $settings );

			unset( $key );
		}
		unset( $settings );

		$this->after_fields( $slug, $contents );

		/**
		 * Fires at the end of fields rendering
		 *
		 * @param string $slug     Element unique name
		 * @param array  $contents Element fields
		 */
		do_action( 'equip/render/fields/after', $slug, $contents );

		return true;
	}

	/**
	 * Start rendering the list of fields.
	 *
	 * @param string $slug     Element unique name
	 * @param array  $contents Element contents
	 *
	 * @return void
	 */
	abstract public function before_fields( $slug, $contents );

	/**
	 * This is a service method which should wrap the field "row"
	 * Contains data-* attributes required for for JS.
	 *
	 * It is not recommended to override this method. Use at your own risk.
	 *
	 * @param string $slug     Element unique name
	 * @param array  $settings Element field settings
	 *
	 * @return void
	 */
	protected function _before_field( $slug, $settings ) {
		$attr = array();

		$class = equip_get_class_set( array(
			'equip-field',
			( 'group' === $settings['field'] ) ? 'equip-group' : '',
		) );

		$attr['class']      = esc_attr( $class );
		$attr['data-key']   = $settings['key'];
		$attr['data-field'] = $settings['field'];

		if ( ! empty( $settings['required'] ) ) {
			$attr['data-dependent'] = 'true';
			$attr['data-required']  = $settings['required'];
		}

		if ( ! empty( $settings['sortable'] ) ) {
			$attr['data-sortable'] = 'true';
		}

		echo '<div ', equip_get_html_attributes( $attr ), '>';
	}

	/**
	 * Start rendering the field.
	 *
	 * @param string      $slug     Element unique name
	 * @param array       $settings Element field settings
	 * @param Equip_Field $field    Element field object
	 *
	 * @return void
	 */
	abstract public function before_field( $slug, $settings, $field );

	/**
	 * Render the field itself
	 *
	 * @param string      $slug     Element unique name
	 * @param array       $settings Element field settings
	 * @param mixed       $value    Element value. Already escaped.
	 * @param Equip_Field $field    Element field object
	 *
	 * @return void
	 */
	abstract public function do_field( $slug, $settings, $value, $field );

	/**
	 * End rendering the field.
	 *
	 * @param string      $slug     Element unique name
	 * @param array       $settings Element field declaration array
	 * @param Equip_Field $field    Element field object
	 *
	 * @return void
	 */
	abstract public function after_field( $slug, $settings, $field );

	/**
	 * End of the service block
	 *
	 * @see Equip_Render::_before_field
	 *
	 * @param string $slug     Element unique name
	 * @param array  $settings Element field settings
	 *
	 * @return void
	 */
	protected function _after_field( $slug, $settings ) {
		echo '</div>';
	}

	/**
	 * Stop rendering the list of fields.
	 *
	 * @param string $slug     Element unique name
	 * @param array  $contents Element contents
	 *
	 * @return void
	 */
	abstract public function after_fields( $slug, $contents );
}