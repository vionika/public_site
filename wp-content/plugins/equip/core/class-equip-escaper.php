<?php

/**
 * Escaping values
 *
 * @author  8guild
 * @package Equip
 */
final class Equip_Escaper {

	/**
	 * Escape all values at once
	 *
	 * Unlike sanitizer, escaper should return values for all
	 * given fields. So if some values are missing the settings[default] key
	 * will be used instead, if it is present. If everything fails NULL will
	 * be used as a field value.
	 *
	 * Note that default value does not escape. Escaping should be done during
	 * the declaring the field.
	 *
	 * @param array  $values   Values
	 * @param array  $contents List of fields
	 * @param string $slug     Element unique name
	 *
	 * @return array
	 */
	public static function bulk_escape( $values, $contents, $slug ) {
		// if $slug or $contents not given returns $values AS IS
		if ( empty( $slug ) || null === $contents ) {
			return $values;
		}

		$_values = array();
		foreach ( $contents as $settings ) {
			$key   = $settings['key'];
			$field = Equip::field( $settings );

			if ( is_array( $values ) && array_key_exists( $key, $values ) ) {
				$value = $values[ $key ];
			} elseif ( array_key_exists( 'default', $settings ) ) {
				$value = $settings['default'];
			} else {
				$value = null;
			}

			$_values[ $key ] = static::escape( $value, $settings, $field, $slug );

			unset( $key, $field, $value );
		}
		unset( $settings );

		return $_values;
	}

	/**
	 * Escape field before it will be rendered.
	 *
	 * Custom escaping callback has a highest priority. If custom
	 * callback not defined, use built-in field-specific escaping,
	 * if exists.
	 *
	 * Also, filter is available before returning the value.
	 *
	 * @param mixed       $value    Field value
	 * @param array       $settings Field declaration array
	 * @param Equip_Field $field    Field object
	 * @param string      $slug     Current element unique name
	 *
	 * @return mixed
	 */
	public static function escape( $value, $settings, $field, $slug ) {
		if ( array_key_exists( 'escape', $settings ) && is_callable( $settings['escape'] ) ) {
			$value = call_user_func( $settings['escape'], $value );
		} elseif ( is_callable( array( $field, 'escape' ) ) ) {
			$value = $field->escape( $value, $settings, $slug );
		}

		/**
		 * Filter the value of field before it will be rendered.
		 *
		 * Fires only for unique element by its $slug.
		 *
		 * @param mixed $value    Value of current field
		 * @param array $settings Field settings
		 */
		$value = apply_filters( "equip/{$slug}/escape", $value, $settings );

		/**
		 * Filter the value of each field before it will be rendered
		 *
		 * @param mixed  $value    Value of current field.
		 * @param array  $settings Field settings
		 * @param string $slug     Unique element name, like meta box or page slug.
		 */
		$value = apply_filters( 'equip/escape', $value, $settings, $slug );

		return $value;
	}
}