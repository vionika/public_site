<?php

/**
 * CPT "Team"
 *
 * @since      1.0.0
 * @since      2.0.0 New loader
 *
 * @author     8guild
 * @package    Appica\Core\CPT
 */
class Appica_Cpt_Team extends Appica_Cpt {
	/**
	 * Custom Post Type slug
	 *
	 * @var string
	 */
	private $post_type = 'appica_team';

	/**
	 * Post Type's taxonomy slug
	 *
	 * @var string
	 */
	private $taxonomy = 'appica_team_category';

	/**
	 * Teammate meta box slug
	 *
	 * @var string
	 */
	private $meta_box_teammate = '_appica_team_mate';

	/**#@+
	 * Cache variables
	 *
	 * @see flush_cats_cache
	 * @see flush_posts_cache
	 */
	private $cache_key_for_posts = 'appica_team_posts';
	private $cache_key_for_cats = 'appica_team_cats';
	private $cache_group = 'appica_cpt';
	/**#@-*/

	public function __construct() {}

	/**
	 * Initialization
	 *
	 * @return Appica_Cpt_Team
	 */
	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );

		// clear cache on adding or deleting posts
		add_action( "save_post_{$this->post_type}", array( $this, 'flush_posts_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_posts_cache' ) );

		// clear cache on modifying categories
		add_action( "create_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		add_action( "delete_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		// fires for both situations when term is edited and term post count changes
		// @see taxonomy.php :: 3440 wp_update_term
		// @see taxonomy.php :: 4152 _update_post_term_count
		add_action( 'edit_term_taxonomy', array( $this, 'flush_cats_cache' ), 10, 2 );

		// featured image
		add_action( 'do_meta_boxes', array( $this, 'change_featured_image_context' ) );
		add_filter( "manage_{$this->post_type}_posts_columns", array( $this, 'additional_posts_screen_columns' ) );
		add_action( "manage_{$this->post_type}_posts_custom_column", array( $this, 'additional_posts_screen_content' ), 10, 2 );

		// meta boxes
		if ( function_exists( 'equip_meta_box_add' ) ) {
			add_action( 'current_screen', array( $this, 'add_meta_boxes' ) );
		}
	}

	public function register() {
		$this->register_post_type();
		$this->register_taxonomy();
	}

	private function register_post_type() {
		$labels = array(
			'name'                => _x( 'Team', 'post type general name', 'appica' ),
			'singular_name'       => _x( 'Team', 'post type singular name', 'appica' ),
			'menu_name'           => __( 'Team', 'appica' ),
			'all_items'           => __( 'All Items', 'appica' ),
			'view_item'           => __( 'View', 'appica' ),
			'add_new_item'        => __( 'Add New', 'appica' ),
			'add_new'             => __( 'Add New', 'appica' ),
			'edit_item'           => __( 'Edit', 'appica' ),
			'update_item'         => __( 'Update', 'appica' ),
			'search_items'        => __( 'Search', 'appica' ),
			'not_found'           => __( 'Not found', 'appica' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'appica' )
		);

		$args = array(
			'label'               => __( 'Team', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'My cool team', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 47,
			'menu_icon'           => 'dashicons-groups',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title', 'thumbnail' ),
			'taxonomies'          => array( $this->taxonomy ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}

	private function register_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Categories', 'taxonomy general name', 'appica' ),
			'singular_name'              => _x( 'Category', 'taxonomy singular name', 'appica' ),
			'menu_name'                  => __( 'Categories', 'appica' ),
			'all_items'                  => __( 'All Items', 'appica' ),
			'parent_item'                => __( 'Parent Item', 'appica' ),
			'parent_item_colon'          => __( 'Parent Item:', 'appica' ),
			'new_item_name'              => __( 'New Item Name', 'appica' ),
			'add_new_item'               => __( 'Add New', 'appica' ),
			'edit_item'                  => __( 'Edit', 'appica' ),
			'update_item'                => __( 'Update', 'appica' ),
			'separate_items_with_commas' => __( 'Separate with commas', 'appica' ),
			'search_items'               => __( 'Search', 'appica' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'appica' ),
			'choose_from_most_used'      => __( 'Choose from the most used items', 'appica' ),
			'not_found'                  => __( 'Not Found', 'appica' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Useful for selective displaying the content.', 'appica' ),
			'public'             => false,
			'show_ui'            => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,
			'hierarchical'       => true,
			'query_var'          => false,
			'rewrite'            => false,
		);

		register_taxonomy( $this->taxonomy, array( $this->post_type ), $args );
	}

	/**
	 * Flush object cache for posts
	 *
	 * Fires when posts creating, updating or deleting.
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_team_posts
	 *
	 * @param int $post_id Post ID
	 */
	public function flush_posts_cache( $post_id ) {
		$type = get_post_type( $post_id );
		if ( $this->post_type !== $type ) {
			return;
		}

		wp_cache_delete( $this->cache_key_for_posts, $this->cache_group );
	}

	/**
	 * Flush object cache for categories
	 *
	 * Fires when created, deleted, modified or assigned a category to a post
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_team_categories
	 *
	 * @param int    $term_id  Term ID or Term Taxonomy ID
	 * @param string $taxonomy Taxonomy name, exists only for "edit_term_taxonomy"
	 */
	public function flush_cats_cache( $term_id, $taxonomy = null ) {
		if ( null === $taxonomy || $this->taxonomy === $taxonomy ) {
			wp_cache_delete( $this->cache_key_for_cats, $this->cache_group );
		}
	}

	public function add_meta_boxes( $screen ) {
		if ( $this->post_type !== $screen->post_type ) {
			return;
		}

		// legacy meta box values as defaults
		$post_id = isset( $_GET['post'] ) ? (int) $_GET['post'] : null;
		if ( null !== $post_id ) {
			$position = get_post_meta( $post_id, '_appica_team_subtitle', true );
			$socials  = get_post_meta( $post_id, '_appica_team_social', true );
		} else {
			$position = '';
			$socials  = '';
		}

		$contents = array(
			array(
				'key'       => 'position',
				'field'     => 'input',
				'title'     => __( 'Position', 'appica' ),
				'desc'      => __( 'Enter teammate position / second name / any additional information you want. This information will be displayed under the title.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'attr'      => array( 'class' => 'widefat' ),
				'default'   => $position,
			),
			array(
				'key'     => 'socials',
				'field'   => 'socials',
				'title'   => __( 'Social Networks', 'appica' ),
				'default' => $socials,
			),
		);

		equip_meta_box_add( $this->meta_box_teammate, $contents, array(
			'id'       => 'appica-team-mate',
			'title'    => __( 'Teammate', 'appica' ),
			'screen'   => $this->post_type,
			'priority' => 'core',
			'engine'   => 'simple',
		) );
	}

	/**
	 * Change Featured Image context on "appica_gallery" post type
	 *
	 * @since  1.0.0
	 *
	 * @author 8guild, Bill Erickson
	 * @link   http://www.billerickson.net/code/move-featured-image-metabox
	 *
	 * @param string $post_type Current post type
	 */
	public function change_featured_image_context( $post_type ) {
		if ( $this->post_type !== $post_type ) {
			return;
		}

		remove_meta_box( 'postimagediv', $this->post_type, 'side' );
		add_meta_box(
			'postimagediv',
			esc_html__( 'Teammate Avatar', 'appica' ),
			'post_thumbnail_meta_box',
			$this->post_type,
			'normal',
			'high'
		);
	}

	/**
	 * Add extra columns to a post type screen
	 *
	 * @param array $columns Current Posts Screen columns
	 *
	 * @return array New Posts Screen columns.
	 */
	public function additional_posts_screen_columns( $columns ) {
		return array_merge( array(
			'cb'       => '<input type="checkbox" />',
			'avatar'   => __( 'Teammate Avatar', 'appica' ),
			'title'    => __( 'Title', 'appica' ),
			'position' => __( 'Position', 'appica' ),
		), $columns );
	}

	/**
	 * Display extra columns content
	 *
	 * @param string $column  Column slug
	 * @param int    $post_id Post ID
	 */
	public function additional_posts_screen_content( $column, $post_id ) {
		switch ( $column ) {
			case 'avatar':
				echo get_the_post_thumbnail( $post_id, array( 75, 75 ) );
				break;

			case 'position':
				$position = appica_meta_box_get( $post_id, $this->meta_box_teammate, 'position', '' );
				echo esc_html( $position );
				unset( $position );
				break;
		}
	}
}