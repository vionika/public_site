<?php

/**
 * CPT App Gallery
 *
 * @since   1.0.0
 * @since   2.0.0 New CPT loader, taxonomy
 *
 * @author  8guild
 * @package Appica\Core\CPT
 */
class Appica_Cpt_App_Gallery extends Appica_Cpt {
	/**
	 * Custom Post Type slug
	 *
	 * @var string
	 */
	private $post_type = 'appica_app_gallery';

	/**
	 * Post Type's taxonomy slug
	 *
	 * @var string
	 */
	private $taxonomy = 'appica_app_gallery_category';

	/**#@+
	 * Cache variables
	 *
	 * @see flush_cats_cache
	 * @see flush_posts_cache
	 */
	private $cache_key_for_posts = 'appica_app_gallery_posts';
	private $cache_key_for_cats = 'appica_app_gallery_cats';
	private $cache_group = 'appica_cpt';
	/**#@-*/

	public function __construct() {}

	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );
		add_action( 'do_meta_boxes', array( $this, 'change_featured_image_context' ) );

		// Add image sizes: normal and large
		add_image_size( 'appica-app-gallery', 378, 223, true );
		add_image_size( 'appica-app-gallery-large', 378, 451, true );

		// clear cache on adding or deleting posts
		add_action( "save_post_{$this->post_type}", array( $this, 'flush_posts_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_posts_cache' ) );

		// clear cache on modifying categories
		add_action( "create_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		add_action( "delete_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		// fires for both situations when term is edited and term post count changes
		// @see taxonomy.php :: 3440 wp_update_term
		// @see taxonomy.php :: 4152 _update_post_term_count
		add_action( 'edit_term_taxonomy', array( $this, 'flush_cats_cache' ), 10, 2 );

		// Display Featured Image in entries list
		add_filter( "manage_{$this->post_type}_posts_columns", array( $this, 'additional_posts_screen_columns' ) );
		add_action( "manage_{$this->post_type}_posts_custom_column", array( $this, 'additional_posts_screen_content' ), 10, 2 );
	}

	public function register() {
		$this->register_post_type();
		$this->register_taxonomy();
	}

	private function register_post_type() {
		$labels = array(
			'name'                => _x( 'App Gallery', 'post type general name', 'appica' ),
			'singular_name'       => _x( 'App Gallery', 'post type singular name', 'appica' ),
			'menu_name'           => __( 'App Gallery', 'appica' ),
			'all_items'           => __( 'All Items', 'appica' ),
			'view_item'           => __( 'View', 'appica' ),
			'add_new_item'        => __( 'Add New', 'appica' ),
			'add_new'             => __( 'Add New', 'appica' ),
			'edit_item'           => __( 'Edit', 'appica' ),
			'update_item'         => __( 'Update', 'appica' ),
			'search_items'        => __( 'Search', 'appica' ),
			'not_found'           => __( 'Not found', 'appica' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'appica' )
		);
		$args = array(
			'label'               => __( 'App Gallery', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'Fancy app gallery', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 49,
			'menu_icon'           => 'dashicons-format-gallery',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title', 'thumbnail' ),
			'taxonomies'          => array( $this->taxonomy ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}

	public function register_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Categories', 'taxonomy general name', 'appica' ),
			'singular_name'              => _x( 'Category', 'taxonomy singular name', 'appica' ),
			'menu_name'                  => __( 'Categories', 'appica' ),
			'all_items'                  => __( 'All Items', 'appica' ),
			'parent_item'                => __( 'Parent Item', 'appica' ),
			'parent_item_colon'          => __( 'Parent Item:', 'appica' ),
			'new_item_name'              => __( 'New Item Name', 'appica' ),
			'add_new_item'               => __( 'Add New', 'appica' ),
			'edit_item'                  => __( 'Edit', 'appica' ),
			'update_item'                => __( 'Update', 'appica' ),
			'separate_items_with_commas' => __( 'Separate with commas', 'appica' ),
			'search_items'               => __( 'Search', 'appica' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'appica' ),
			'choose_from_most_used'      => __( 'Choose from the most used items', 'appica' ),
			'not_found'                  => __( 'Not Found', 'appica' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Useful for selective displaying the content.', 'appica' ),
			'public'             => false,
			'show_ui'            => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,
			'hierarchical'       => true,
			'query_var'          => false,
			'rewrite'            => false,
		);

		register_taxonomy( $this->taxonomy, array( $this->post_type ), $args );
	}

	/**
	 * Flush object cache for posts
	 *
	 * Fires when posts creating, updating or deleting.
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_app_gallery_posts
	 *
	 * @param int $post_id Post ID
	 */
	public function flush_posts_cache( $post_id ) {
		$type = get_post_type( $post_id );
		if ( $this->post_type !== $type ) {
			return;
		}

		wp_cache_delete( $this->cache_key_for_posts, $this->cache_group );
	}

	/**
	 * Flush object cache for categories
	 *
	 * Fires when created, deleted, modified or assigned a category to a post
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_app_gallery_categories
	 *
	 * @param int    $term_id  Term ID or Term Taxonomy ID
	 * @param string $taxonomy Taxonomy name, exists only for "edit_term_taxonomy"
	 */
	public function flush_cats_cache( $term_id, $taxonomy = null ) {
		if ( null === $taxonomy || $this->taxonomy === $taxonomy ) {
			wp_cache_delete( $this->cache_key_for_cats, $this->cache_group );
		}
	}


	/**
	 * Change Featured Image context on "appica_app_gallery" post type
	 *
	 * @since 1.0.0
	 * @author 8guild, Bill Erickson
	 * @link http://www.billerickson.net/code/move-featured-image-metabox
	 */
	public function change_featured_image_context( $post_type ) {
		if ( $this->post_type !== $post_type ) {
			return;
		}

		remove_meta_box( 'postimagediv', $this->post_type, 'side' );
		add_meta_box( 'postimagediv', __( 'App Image', 'appica' ), 'post_thumbnail_meta_box', $this->post_type, 'normal', 'high' );
	}

	/**
	 * Add extra columns to a post type screen
	 *
	 * @param array $columns Current Posts Screen columns
	 *
	 * @return array New Posts Screen columns.
	 */
	public function additional_posts_screen_columns( $columns ) {
		$_columns = array(
			'cb'    => '<input type="checkbox" />',
			'image' => __( 'Featured Image', 'appica' )
		);

		$columns = array_merge( $_columns, $columns );

		return $columns;
	}

	/**
	 * Show extra columns content
	 *
	 * @param string $column  Column slug
	 * @param int    $post_id Post ID
	 */
	public function additional_posts_screen_content( $column, $post_id ) {
		switch ( $column ) {
			case 'image':
				echo get_the_post_thumbnail( $post_id, array( 75, 75 ) );
				break;
		}
	}
}