<?php

/**
 * Intro
 *
 * Post type for Intro Sections
 *
 * @author  8guild
 * @package Appica\Core\CPT
 */
class Appica_Cpt_Intro extends Appica_Cpt {
	/**
	 * Custom Post Type slug
	 *
	 * @var string
	 */
	private $post_type = 'appica_intro';

	/**
	 * Meta box slug for intro section settings
	 *
	 * @var string
	 */
	private $meta_box_settings = '_appica_intro_settings';

	/**
	 * Appica_Cpt_Intro constructor
	 */
	public function __construct() {}

	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );
		add_action( "save_post_{$this->post_type}", array( $this, 'flush' ), 10, 2 );

		// meta boxes
		if ( function_exists( 'equip_meta_box_add' ) ) {
			add_action( 'current_screen', array( $this, 'add_meta_boxes' ) );
		}
	}

	public function register() {
		$labels = array(
			'name'               => _x( 'Intro', 'post type general name', 'appica' ),
			'singular_name'      => _x( 'Intro', 'post type singular name', 'appica' ),
			'menu_name'          => __( 'Intro', 'appica' ),
			'all_items'          => __( 'All Items', 'appica' ),
			'view_item'          => __( 'View', 'appica' ),
			'add_new_item'       => __( 'Add New Slider', 'appica' ),
			'add_new'            => __( 'Add New', 'appica' ),
			'edit_item'          => __( 'Edit', 'appica' ),
			'update_item'        => __( 'Update', 'appica' ),
			'search_items'       => __( 'Search', 'appica' ),
			'not_found'          => __( 'Nothing found', 'appica' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'appica' )
		);

		$args = array(
			'label'               => __( 'Intro', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'An Intro Section in corporate style', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 42,
			'menu_icon'           => 'dashicons-desktop',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title' ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}

	/**
	 * Add meta boxes on "current_screen" hook
	 *
	 * @param WP_Screen $screen Current WP_Screen object.
	 */
	public function add_meta_boxes( $screen ) {
		if ( $this->post_type !== $screen->post_type ) {
			return;
		}

		$enable  = __( 'Enable', 'appica' );
		$disable = __( 'Disable', 'appica' );

		$contents = array(
			array(
				'key'       => 'bg',
				'field'     => 'image',
				'weight'    => 1,
				'title'     => __( 'Background Image', 'appica' ),
				'desc'      => __( 'Optimal size is 1920x1200px', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'media'     => array(
					'title' => __( 'Choose the Background Image', 'appica' ),
				),
			),
			array(
				'key'       => 'logo',
				'field'     => 'image',
				'title'     => __( 'Logo', 'appica' ),
				'desc'      => __( 'Logo optimal size is 360x360px', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'media'     => array(
					'title' => __( 'Choose the Logo for Intro Section', 'appica' ),
				),
			),
			array(
				'key'   => 'title',
				'field' => 'input',
				'title' => __( 'Title', 'appica' ),
				'attr'  => array(
					'class'       => 'widefat',
					'placeholder' => __( 'Appica 2', 'appica' ),
				),
			),
			array(
				'key'   => 'subtitle',
				'field' => 'input',
				'title' => __( 'Subtitle', 'appica' ),
				'attr'  => array(
					'class'       => 'widefat',
					'placeholder' => __( 'Flatter, lighter, appler, stronger', 'appica' ),
				),
			),
			array(
				'key'     => 'device_color',
				'field'   => 'select',
				'title'   => __( 'Device Color', 'appica' ),
				'attr'    => array( 'class' => 'widefat' ),
				'hidden'  => apply_filters( 'appica_intro_hide_device_color', false ),
				'default' => 'gold',
				'options' => array(
					'gold'       => __( 'Gold', 'appica' ),
					'silver'     => __( 'Silver', 'appica' ),
					'space-gray' => __( 'Space Gray', 'appica' ),
				),
			),
			array(
				'key'       => 'device_screen',
				'field'     => 'image',
				'title'     => __( 'Device Screen', 'appica' ),
				'desc'      => __( 'The image inside the device. Optimal size is 652x1186px', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'media'     => array(
					'title' => __( 'Choose the image for Device Screen', 'appica' ),
				),
			),
			array(
				'key'     => 'is_scroll',
				'field'   => 'select',
				'title'   => __( 'Enable/Disable Scroll for More Button', 'appica' ),
				'attr'    => array( 'class' => 'widefat' ),
				'default' => 0,
				'options' => array(
					1 => $enable,
					0 => $disable,
				),
			),
			array(
				'key'      => 'more_text',
				'field'    => 'input',
				'title'    => __( 'Scroll for More Button Text', 'appica' ),
				'default'  => __( 'Scroll for More', 'appica' ),
				'required' => array( 'is_scroll', '=', 1 ),
				'attr'     => array(
					'class' => 'widefat',
				),
			),
			array(
				'key'       => 'more_anchor',
				'field'     => 'input',
				'title'     => __( 'Scroll for More Anchor', 'appica' ),
				'desc'      => __( 'Set the ID of element, e.g. #my-first-block, where page will be scrolled to.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'required'  => array( 'is_scroll', '=', 1 ),
				'attr'      => array(
					'class'       => 'widefat',
					'placeholder' => '#id-here',
				),
			),
			array(
				'key'     => 'is_download',
				'field'   => 'select',
				'title'   => __( 'Enable/Disable Download Button', 'appica' ),
				'attr'    => array( 'class' => 'widefat' ),
				'default' => 0,
				'options' => array(
					1 => $enable,
					0 => $disable,
				),
			),
			array(
				'key'       => 'download_helper',
				'field'     => 'input',
				'title'     => __( 'Download Helper', 'appica' ),
				'desc'      => __( 'This line will be displayed above the download buttons. Applicable for both App Store and Google Play.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'required'  => array( 'is_download', '=', 1 ),
				'attr'      => array(
					'class'       => 'widefat',
					'placeholder' => __( 'e.g. iPhone and iPad versions', 'appica' ),
				),
			),
			array(
				'key'       => 'app_store',
				'field'     => 'group',
				'title'     => __( 'App Store', 'appica' ),
				'desc'      => __( 'Leave URL or Text field blank to hide the button', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'required'  => array( 'is_download', '=', 1 ),
				'fields'    => array(
					array(
						'key'       => 'text',
						'field'     => 'input',
						'title'     => __( 'Text', 'appica' ),
						'desc'      => __( '* This field is required', 'appica' ),
						'desc_wrap' => '<p class="description">%s</p>',
						'attr'      => array(
							'class'       => 'widefat',
							'placeholder' => __( 'e.g. Download on the', 'appica' ),
						),
					),
					array(
						'key'       => 'url',
						'field'     => 'input',
						'title'     => __( 'URL', 'appica' ),
						'desc'      => __( '* This field is required', 'appica' ),
						'desc_wrap' => '<p class="description">%s</p>',
						'sanitize'  => 'esc_url_raw',
						'escape'    => 'esc_url',
						'attr'      => array( 'class' => 'widefat' ),
					),
					array(
						'key'     => 'target',
						'field'   => 'select',
						'title'   => __( 'Open in new tab/window?', 'appica' ),
						'attr'    => array( 'class' => 'widefat' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'No', 'appica' ),
							'_blank' => __( 'Yes', 'appica' ),
						),
					),
				),
			),
			array(
				'key'       => 'google_play',
				'field'     => 'group',
				'title'     => __( 'Google Play', 'appica' ),
				'desc'      => __( 'Leave URL or Text field blank to hide the button', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'required'  => array( 'is_download', '=', 1 ),
				'fields'    => array(
					array(
						'key'       => 'text',
						'field'     => 'input',
						'title'     => __( 'Text', 'appica' ),
						'desc'      => __( '* This field is required', 'appica' ),
						'desc_wrap' => '<p class="description">%s</p>',
						'attr'      => array(
							'class'       => 'widefat',
							'placeholder' => __( 'e.g. Get it on', 'appica' ),
						),
					),
					array(
						'key'       => 'url',
						'field'     => 'input',
						'title'     => __( 'URL', 'appica' ),
						'desc'      => __( '* This field is required', 'appica' ),
						'desc_wrap' => '<p class="description">%s</p>',
						'sanitize'  => 'esc_url_raw',
						'escape'    => 'esc_url',
						'attr'      => array( 'class' => 'widefat' ),
					),
					array(
						'key'     => 'target',
						'field'   => 'select',
						'title'   => __( 'Open in new tab/window?', 'appica' ),
						'attr'    => array( 'class' => 'widefat' ),
						'default' => '_self',
						'options' => array(
							'_self'  => __( 'No', 'appica' ),
							'_blank' => __( 'Yes', 'appica' ),
						),
					),
				),
			),
			array(
				'key'       => 'is_features',
				'field'     => 'select',
				'title'     => __( 'Enable/Disable Features', 'appica' ),
				'desc'      => __( 'List of features to the right side of device', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'attr'    => array( 'class' => 'widefat' ),
				'default' => 0,
				'options' => array(
					1 => $enable,
					0 => $disable,
				),
			),
			array(
				'key'      => 'feature_first_icon',
				'field'    => 'icon',
				'title'    => __( 'First Feature Icon', 'appica' ),
				'icons'    => 'flaticons',
				'required' => array( 'is_features', '=', 1 ),
				'settings' => array(
					'iconsPerPage' => 30,
					'emptyIcon'    => true,
				),
			),
			array(
				'key'      => 'feature_first_title',
				'field'    => 'input',
				'title'    => __( 'First Feature Title', 'appica' ),
				'attr'     => array( 'class' => 'widefat' ),
				'required' => array( 'is_features', '=', 1 ),
			),
			array(
				'key'       => 'feature_first_description',
				'field'     => 'textarea',
				'title'     => __( 'First Feature Description', 'appica' ),
				'desc'      => esc_html__( 'Tags a, br, em, strong are allowed.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'attr'      => array( 'class' => 'widefat' ),
				'required'  => array( 'is_features', '=', 1 ),
			),
			array(
				'key'      => 'feature_second_icon',
				'field'    => 'icon',
				'title'    => __( 'Second Feature Icon', 'appica' ),
				'icons'    => 'flaticons',
				'required' => array( 'is_features', '=', 1 ),
				'settings' => array(
					'iconsPerPage' => 30,
					'emptyIcon'    => true,
				),
			),
			array(
				'key'      => 'feature_second_title',
				'field'    => 'input',
				'title'    => __( 'Second Feature Title', 'appica' ),
				'attr'     => array( 'class' => 'widefat' ),
				'required' => array( 'is_features', '=', 1 ),
			),
			array(
				'key'       => 'feature_second_description',
				'field'     => 'textarea',
				'title'     => __( 'Second Feature Description', 'appica' ),
				'desc'      => esc_html__( 'Tags a, br, em, strong are allowed.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'attr'      => array( 'class' => 'widefat' ),
				'required'  => array( 'is_features', '=', 1 ),
			),
			array(
				'key'      => 'feature_third_icon',
				'field'    => 'icon',
				'title'    => __( 'Third Feature Icon', 'appica' ),
				'icons'    => 'flaticons',
				'required' => array( 'is_features', '=', 1 ),
				'settings' => array(
					'iconsPerPage' => 30,
					'emptyIcon'    => true,
				),
			),
			array(
				'key'      => 'feature_third_title',
				'field'    => 'input',
				'title'    => __( 'Third Feature Title', 'appica' ),
				'attr'     => array( 'class' => 'widefat' ),
				'required' => array( 'is_features', '=', 1 ),
			),
			array(
				'key'       => 'feature_third_description',
				'field'     => 'textarea',
				'title'     => __( 'Third Feature Description', 'appica' ),
				'desc'      => esc_html__( 'Tags a, br, em, strong are allowed.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'attr'      => array( 'class' => 'widefat' ),
				'required'  => array( 'is_features', '=', 1 ),
			),
		);

		equip_meta_box_add( $this->meta_box_settings, $contents, array(
			'id'       => 'appica-slideshow-settings',
			'title'    => __( 'Slideshow Settings', 'appica' ),
			'screen'   => $this->post_type,
			'priority' => 'high',
			'engine'   => 'simple',
		) );
	}

	/**
	 * Flush cached data
	 *
	 * @see appica_the_intro_section
	 * @see template-parts/page-with-intro.php
	 *
	 * @see appica_get_intro_sections
	 * @see inc/meta-boxes.php
	 *
	 * @param int     $post_id Post ID
	 * @param WP_Post $post    Post object
	 */
	public function flush( $post_id, $post ) {
		// @see appica_the_intro_section
		$cache_key = appica_cache_key( $this->post_type, $post_id );
		delete_transient( $cache_key );
		unset( $cache_key );

		// @see appica_get_intro_sections
		$cache_key = appica_cache_key( 'appica_intro_sections' );
		wp_cache_delete( $cache_key );
		unset( $cache_key );
	}
}