<?php

/**
 * CPT "Testimonials"
 *
 * @since      1.0.0
 * @since      2.0.0 New loader
 *
 * @author     8guild
 * @package    Appica\Core\CPT
 */
class Appica_Cpt_Testimonials extends Appica_Cpt {
	/**
	 * Custom Post Type slug.
	 * @var string
	 */
	private $post_type = 'appica_testimonials';

	public function __construct() {}

	/**
	 * Initialization
	 *
	 * @return Appica_Cpt_Testimonials
	 */
	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );
	}

	public function register() {
		$labels = array(
			'name'                => _x( 'Testimonials', 'Post Type General Name', 'appica' ),
			'singular_name'       => _x( 'Testimonial', 'Post Type Singular Name', 'appica' ),
			'menu_name'           => __( 'Testimonials', 'appica' ),
			'all_items'           => __( 'All Items', 'appica' ),
			'view_item'           => __( 'View', 'appica' ),
			'add_new_item'        => __( 'Add New', 'appica' ),
			'add_new'             => __( 'Add New', 'appica' ),
			'edit_item'           => __( 'Edit', 'appica' ),
			'update_item'         => __( 'Update', 'appica' ),
			'search_items'        => __( 'Search', 'appica' ),
			'not_found'           => __( 'Not found', 'appica' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'appica' )
		);

		$args = array(
			'label'               => __( 'Testimonials', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'Testimonials', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 50,
			'menu_icon'           => 'dashicons-testimonial',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title', 'editor', 'thumbnail' ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}
}