<?php

/**
 * CPT "Gadget Slideshow"
 *
 * @since   1.0.0
 * @since   2.0.0 New CPT loader, taxonomy
 *
 * @author  8guild
 * @package Appica\Core\CPT
 */
class Appica_Cpt_Gadget_Slideshow extends Appica_Cpt {
	/**
	 * Custom Post Type slug
	 *
	 * @var string
	 */
	private $post_type = 'appica_slideshow';

	/**
	 * Custom taxonomy for current post type
	 *
	 * @var string $taxonomy
	 */
	private $taxonomy = 'appica_slideshow_category';

	/**#@+
	 * Cache variables
	 *
	 * @see flush_cats_cache
	 * @see flush_posts_cache
	 */
	private $cache_key_for_posts = 'appica_slideshow_posts';
	private $cache_key_for_cats = 'appica_slideshow_cats';
	private $cache_group = 'appica_cpt';
	/**#@-*/

	public function __construct() {}

	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );

		// clear cache on adding or deleting posts
		add_action( "save_post_{$this->post_type}", array( $this, 'flush_posts_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_posts_cache' ) );

		// clear cache on modifying categories
		add_action( "create_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		add_action( "delete_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		// fires for both situations when term is edited and term post count changes
		// @see taxonomy.php :: 3440 wp_update_term
		// @see taxonomy.php :: 4152 _update_post_term_count
		add_action( 'edit_term_taxonomy', array( $this, 'flush_cats_cache' ), 10, 2 );

		// meta boxes
		if ( function_exists( 'equip_meta_box_add' ) ) {
			add_action( 'current_screen', array( $this, 'add_meta_boxes' ) );
		}
	}

	public function register() {
		$this->register_post_type();
		$this->register_taxonomy();
	}

	public function register_post_type() {
		$labels = array(
			'name'               => _x( 'Gadget Slideshow', 'Post Type General Name', 'appica' ),
			'singular_name'      => _x( 'Gadget Slideshow', 'Post Type Singular Name', 'appica' ),
			'menu_name'          => __( 'Gadget Slideshow', 'appica' ),
			'all_items'          => __( 'All Items', 'appica' ),
			'view_item'          => __( 'View', 'appica' ),
			'add_new_item'       => __( 'Add New', 'appica' ),
			'add_new'            => __( 'Add New', 'appica' ),
			'edit_item'          => __( 'Edit', 'appica' ),
			'update_item'        => __( 'Update', 'appica' ),
			'search_items'       => __( 'Search', 'appica' ),
			'not_found'          => __( 'Not found', 'appica' ),
			'not_found_in_trash' => __( 'Not found in Trash', 'appica' )
		);
		$args   = array(
			'label'               => __( 'Gadget Slideshow', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'Gadget Slideshow', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 51,
			'menu_icon'           => 'dashicons-slides',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title', 'excerpt' ),
			'taxonomies'          => array( $this->taxonomy ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}

	public function register_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Categories', 'taxonomy general name', 'appica' ),
			'singular_name'              => _x( 'Category', 'taxonomy singular name', 'appica' ),
			'menu_name'                  => __( 'Categories', 'appica' ),
			'all_items'                  => __( 'All Items', 'appica' ),
			'parent_item'                => __( 'Parent Item', 'appica' ),
			'parent_item_colon'          => __( 'Parent Item:', 'appica' ),
			'new_item_name'              => __( 'New Item Name', 'appica' ),
			'add_new_item'               => __( 'Add New', 'appica' ),
			'edit_item'                  => __( 'Edit', 'appica' ),
			'update_item'                => __( 'Update', 'appica' ),
			'separate_items_with_commas' => __( 'Separate with commas', 'appica' ),
			'search_items'               => __( 'Search', 'appica' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'appica' ),
			'choose_from_most_used'      => __( 'Choose from the most used items', 'appica' ),
			'not_found'                  => __( 'Not Found', 'appica' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'For filtration and building queries', 'appica' ),
			'public'             => false,
			'show_ui'            => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,
			'hierarchical'       => true,
			'query_var'          => false,
			'rewrite'            => false,
		);

		register_taxonomy( $this->taxonomy, array( $this->post_type ), $args );
	}

	/**
	 * Flush object cache for posts
	 *
	 * Fires when portfolio posts creating, updating or deleting.
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_slideshow_posts
	 *
	 * @param int $post_id Post ID
	 */
	public function flush_posts_cache( $post_id ) {
		$type = get_post_type( $post_id );
		if ( $this->post_type !== $type ) {
			return;
		}

		wp_cache_delete( $this->cache_key_for_posts, $this->cache_group );
	}

	/**
	 * Flush object cache for categories
	 *
	 * Fires when created, deleted, modified or assigned a category to a post
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_slideshow_categories
	 *
	 * @param int    $term_id  Term ID or Term Taxonomy ID
	 * @param string $taxonomy Taxonomy name, exists only for "edit_term_taxonomy"
	 */
	public function flush_cats_cache( $term_id, $taxonomy = null ) {
		if ( null === $taxonomy || $this->taxonomy === $taxonomy ) {
			wp_cache_delete( $this->cache_key_for_cats, $this->cache_group );
		}
	}

	/**
	 * Add meta boxes on "current_screen" hook
	 *
	 * @param WP_Screen $screen Current WP_Screen object.
	 */
	public function add_meta_boxes( $screen ) {
		if ( $this->post_type !== $screen->post_type ) {
			return;
		}

		// legacy meta box values as defaults
		$post_id = isset( $_GET['post'] ) ? (int) $_GET['post'] : null;
		if ( null !== $post_id ) {
			$icon       = get_post_meta( $post_id, '_appica_slideshow_icon', true );
			$transition = get_post_meta( $post_id, '_appica_slideshow_transition', true );
			$media      = get_post_meta( $post_id, '_appica_slideshow_media', true );
			$media      = wp_parse_args( $media, array(
				'phone'  => 0,
				'tablet' => 0,
			) );
		} else {
			$icon       = '';
			$transition = '';
			$media      = array();
		}

		$contents = array(
			array(
				'key'       => 'phone',
				'field'     => 'image',
				'title'     => __( 'Select "Phone" image', 'appica' ),
				'desc'      => __( 'Min image size: 233х423px. Recommended size: 473x858px for better smoothing.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'default'   => empty( $media ) ? '' : (int) $media['phone'],
				'media'     => array(
					'title' => __( 'Choose the image for Phone', 'appica' ),
				),
			),
			array(
				'key'       => 'tablet',
				'field'     => 'image',
				'title'     => __( 'Select "Tablet" image', 'appica' ),
				'desc'      => __( 'Min image size: 838х629px. Recommended size: 1223x917px for better smoothing.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'default'   => empty( $media ) ? '' : (int) $media['tablet'],
				'media'     => array(
					'title' => __( 'Choose the image for Tablet', 'appica' ),
				),
			),
			array(
				'key'      => 'icon',
				'field'    => 'icon',
				'title'    => __( 'Choose an icon', 'appica' ),
				'icons'    => 'flaticons',
				'default'  => empty( $icon ) ? '' : $icon,
				'settings' => array(
					'iconsPerPage' => 30,
					'emptyIcon'    => false,
				),
			),
			array(
				'key'     => 'transition',
				'field'   => 'select',
				'title'   => __( 'Transition', 'appica' ),
				'attr'    => array( 'class' => 'widefat' ),
				'default' => empty( $transition ) ? 'fade' : $transition,
				'options' => array(
					'fade'    => 'Fade',
					'scale'   => 'Scale',
					'scaleup' => 'ScaleUp',
					'top'     => 'Top',
					'bottom'  => 'Bottom',
					'left'    => 'Left',
					'right'   => 'Right',
					'flip'    => 'Flip'
				),
			),
		);

		equip_meta_box_add( '_appica_slideshow_settings', $contents, array(
			'id'       => 'appica-slideshow-settings',
			'title'    => __( 'Slideshow Settings', 'appica' ),
			'screen'   => $this->post_type,
			'priority' => 'high',
			'engine'   => 'simple',
		) );
	}
}