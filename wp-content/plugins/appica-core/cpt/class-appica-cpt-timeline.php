<?php

/**
 * CPT "Timeline"
 *
 * @since      1.0.0
 * @since      2.0.0 New loader, added taxonomy
 *
 * @author     8guild
 * @package    Appica\Core\CPT
 */
class Appica_CPT_Timeline extends Appica_Cpt {
	/**
	 * Custom Post Type slug
	 *
	 * @var string
	 */
	private $post_type = 'appica_timeline';

	/**
	 * Post Type's taxonomy name
	 *
	 * @var string
	 */
	private $taxonomy = 'appica_timeline_category';

	/**
	 * Meta box "Date" slug
	 * @var string
	 */
	private $meta_box_date = '_appica_timeline_date';

	/**#@+
	 * Cache variables
	 *
	 * @see flush_cats_cache
	 * @see flush_posts_cache
	 */
	private $cache_key_for_posts = 'appica_timeline_posts';
	private $cache_key_for_cats = 'appica_timeline_cats';
	private $cache_group = 'appica_cpt';
	/**#@-*/

	public function __construct() {}

	/**
	 * Initialization
	 *
	 * @return Appica_CPT_Timeline
	 */
	public function init() {
		add_action( 'init', array( $this, 'register' ), 0 );

		// clear cache on adding or deleting posts
		add_action( "save_post_{$this->post_type}", array( $this, 'flush_posts_cache' ) );
		add_action( 'deleted_post', array( $this, 'flush_posts_cache' ) );

		// clear cache on modifying categories
		add_action( "create_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		add_action( "delete_{$this->taxonomy}", array( $this, 'flush_cats_cache' ) );
		// fires for both situations when term is edited and term post count changes
		// @see taxonomy.php :: 3440 wp_update_term
		// @see taxonomy.php :: 4152 _update_post_term_count
		add_action( 'edit_term_taxonomy', array( $this, 'flush_cats_cache' ), 10, 2 );

		// meta boxes
		if ( function_exists( 'equip_meta_box_add' ) ) {
			add_action( 'current_screen', array( $this, 'add_meta_boxes' ) );
		}
	}

	public function register() {
		$this->register_post_type();
		$this->register_taxonomy();
	}

	private function register_post_type() {
		$labels = array(
			'name'                => _x( 'Timeline', 'Post Type General Name', 'appica' ),
			'singular_name'       => _x( 'Timeline', 'Post Type Singular Name', 'appica' ),
			'menu_name'           => __( 'Timeline', 'appica' ),
			'all_items'           => __( 'All Items', 'appica' ),
			'view_item'           => __( 'View', 'appica' ),
			'add_new_item'        => __( 'Add New', 'appica' ),
			'add_new'             => __( 'Add New', 'appica' ),
			'edit_item'           => __( 'Edit', 'appica' ),
			'update_item'         => __( 'Update', 'appica' ),
			'search_items'        => __( 'Search', 'appica' ),
			'not_found'           => __( 'Not found', 'appica' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'appica' )
		);

		$args = array(
			'label'               => __( 'Timeline', 'appica' ),
			'labels'              => $labels,
			'description'         => __( 'Custom post type for Timeline', 'appica' ),
			'public'              => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => '48',
			'menu_icon'           => 'dashicons-flag',
			'capability_type'     => 'post',
			'hierarchical'        => false,
			'supports'            => array( 'title', 'excerpt' ),
			'taxonomies'          => array( $this->taxonomy ),
			'has_archive'         => false,
			'rewrite'             => false,
			'query_var'           => false,
			'can_export'          => true,
		);

		register_post_type( $this->post_type, $args );
	}

	private function register_taxonomy() {
		$labels = array(
			'name'                       => _x( 'Categories', 'Taxonomy General Name', 'appica' ),
			'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'appica' ),
			'menu_name'                  => __( 'Categories', 'appica' ),
			'all_items'                  => __( 'All Items', 'appica' ),
			'parent_item'                => __( 'Parent Item', 'appica' ),
			'parent_item_colon'          => __( 'Parent Item:', 'appica' ),
			'new_item_name'              => __( 'New Item Name', 'appica' ),
			'add_new_item'               => __( 'Add New', 'appica' ),
			'edit_item'                  => __( 'Edit', 'appica' ),
			'update_item'                => __( 'Update', 'appica' ),
			'separate_items_with_commas' => __( 'Separate with commas', 'appica' ),
			'search_items'               => __( 'Search', 'appica' ),
			'add_or_remove_items'        => __( 'Add or remove items', 'appica' ),
			'choose_from_most_used'      => __( 'Choose from the most used items', 'appica' ),
			'not_found'                  => __( 'Not Found', 'appica' ),
		);

		$args = array(
			'labels'             => $labels,
			'description'        => __( 'Useful for selective displaying the content.', 'appica' ),
			'public'             => false,
			'show_ui'            => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,
			'hierarchical'       => true,
			'query_var'          => false,
			'rewrite'            => false,
		);

		register_taxonomy( $this->taxonomy, array( $this->post_type ), $args );
	}

	/**
	 * Flush object cache for posts
	 *
	 * Fires when posts creating, updating or deleting.
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_timeline_posts
	 *
	 * @param int $post_id Post ID
	 */
	public function flush_posts_cache( $post_id ) {
		$type = get_post_type( $post_id );
		if ( $this->post_type !== $type ) {
			return;
		}

		wp_cache_delete( $this->cache_key_for_posts, $this->cache_group );
	}

	/**
	 * Flush object cache for categories
	 *
	 * Fires when created, deleted, modified or assigned a category to a post
	 *
	 * @see inc/vc-map.php
	 * @see appica_core_timeline_categories
	 *
	 * @param int    $term_id  Term ID or Term Taxonomy ID
	 * @param string $taxonomy Taxonomy name, exists only for "edit_term_taxonomy"
	 */
	public function flush_cats_cache( $term_id, $taxonomy = null ) {
		if ( null === $taxonomy || $this->taxonomy === $taxonomy ) {
			wp_cache_delete( $this->cache_key_for_cats, $this->cache_group );
		}
	}

	/**
	 * Add meta boxes on "current_screen" hook
	 *
	 * @param WP_Screen $screen Current WP_Screen object.
	 */
	public function add_meta_boxes( $screen ) {
		if ( $this->post_type !== $screen->post_type ) {
			return;
		}

		// legacy meta box value as default
		$post_id = isset( $_GET['post'] ) ? (int) $_GET['post'] : null;
		if ( null !== $post_id ) {
			$date = get_post_meta( $post_id, $this->meta_box_date, true );
		} else {
			$date = '';
		}

		$contents = array(
			array(
				'key'       => 'date',
				'field'     => 'input',
				'desc'      => __( 'Set date here. Any format acceptable.', 'appica' ),
				'desc_wrap' => '<p class="description">%s</p>',
				'default'   => $date,
				'attr'      => array( 'class' => 'widefat' ),
			),
		);

		equip_meta_box_add( $this->meta_box_date, $contents, array(
			'id'       => 'appica-timeline-date',
			'title'    => __( 'Date', 'appica' ),
			'screen'   => $this->post_type,
			'priority' => 'high',
			'engine'   => 'simple',
		) );
	}
}