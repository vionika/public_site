<?php
/**
 * Plugin Name: Appica Core
 * Plugin URI: http://the8guild.com/themes/wordpress/appica2/
 * Description: Core functionality for 8guild's Appica 2 Theme
 * Version: 2.0.1
 * Author: 8guild
 * Author URI: http://8guild.com
 * License: GPLv2 or later
 *
 * @author  8guild
 * @package Appica\Core
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // exit if accessed directly
}

/**#@+
 * Plugin constants
 *
 * @since 2.0.0 Added APPICA_CORE_FILE
 * @since 1.0.0
 */
define( 'APPICA_CORE_FILE', __FILE__ );
define( 'APPICA_CORE_ROOT', untrailingslashit( plugin_dir_path( APPICA_CORE_FILE ) ) );
/**#@-*/

/**
 * Load core classes according to WordPress naming conventions.
 *
 * @link  https://make.wordpress.org/core/handbook/coding-standards/php/#naming-conventions
 *
 * @param string $class Class name
 *
 * @since 1.0.0
 * @since 2.0.0 Renamed and updated code
 */
function appica_core_loader( $class ) {
	$class_lc = strtolower( $class );
	$chunks   = array_filter( explode( '_', strtolower( $class_lc ) ) );

	// Load only plugins classes
	$fchunk = reset( $chunks );
	if ( false === $fchunk || ! in_array( $fchunk, array( 'appica', 'guild' ) ) ) {
		return;
	}

	$root   = APPICA_CORE_ROOT;
	$subdir = '/core/';

	$file = 'class-' . implode( '-', $chunks ) . '.php';
	$path = wp_normalize_path( $root . $subdir . $file );
	if ( is_readable( $path ) ) {
		require $path;
	}
}

spl_autoload_register( 'appica_core_loader' );

function appica_core_textdomain() {
	load_plugin_textdomain( 'appica', false, dirname( plugin_basename( __FILE__ ) ) . '/languages' );
}

add_action( 'plugins_loaded', 'appica_core_textdomain' );

/**
 * Load custom post types
 */
function appica_core_cpt() {
	$loader = Appica_Cpt_Loader::instance();

	$path  = wp_normalize_path( APPICA_CORE_ROOT . '/cpt' );
	$files = appica_get_directory_contents( $path );

	$loader->init( $files );
}

add_action( 'plugins_loaded', 'appica_core_cpt' );

/**
 * Callback for {@see register_activation_hook}
 *
 * Flushing the rewrite rules on plugin activation.
 * Required for preventing 404's errors for public custom post types.
 */
function appica_core_activation() {
	$loader = Appica_Cpt_Loader::instance();

	$path  = wp_normalize_path( APPICA_CORE_ROOT . '/cpt' );
	$files = appica_get_directory_contents( $path );

	$loader->register( $files );

	flush_rewrite_rules();
}

register_activation_hook( APPICA_CORE_FILE, 'appica_core_activation' );

/**
 * Init the shortcodes
 */
function appica_core_shortcodes() {
	// collect all shortcodes
	$path  = wp_normalize_path( APPICA_CORE_ROOT . '/shortcodes' );
	$files = appica_get_directory_contents( $path );

	Appica_Shortcodes::init( $files );
}

add_action( 'init', 'appica_core_shortcodes' );

/**
 * Plugin initialization
 *
 * @since 1.0.0
 */
function appica_core_init() {
    Appica_Filters::init();
    Appica_AJAX::init();
}

add_action( 'init', 'appica_core_init' );

/**
 * Enqueue scripts and styles on front-end
 *
 * @since 1.0.0
 */
function appica_core_front_scripts() {
	// TODO: maybe get custom google maps api key?
	wp_register_script( 'appica-google-maps', '//maps.googleapis.com/maps/api/js?key=AIzaSyA5DLwPPVAz88_k0yO2nmFe7T9k1urQs84', array(), null );
	wp_register_script( 'magnific-popup', plugins_url( 'assets/js/jquery.magnific-popup.min.js', __FILE__ ), array( 'jquery' ), null, true );
	wp_enqueue_script( 'appica-core', plugins_url( 'assets/js/appica-core.js', __FILE__ ), array(
		'jquery',
		'magnific-popup'
	), null, true );
}

add_action( 'wp_enqueue_scripts', 'appica_core_front_scripts' );

/**
 * Enqueue scripts and styles on admin screens
 *
 * @since 1.0.0
 */
function appica_core_admin_scripts() {
    wp_enqueue_style( 'appica-core', plugins_url( 'assets/css/appica-core.css', __FILE__ ), array(), null );
	wp_enqueue_script( 'appica-core', plugins_url( 'assets/js/appica-core-admin.js', __FILE__ ), array( 'jquery' ), null, true );

    wp_enqueue_media();
    wp_localize_script( 'appica-core', 'appicaCore', array(
        'nonce' => wp_create_nonce( 'appica-ajax' ),
        'icon'  => array(
            'preview' => '',
            'value'   => ''
        )
    ) );

    $head_css = Appica_Helpers::get_head_css();
    wp_add_inline_style( 'appica-core', $head_css );
}

add_action( 'admin_enqueue_scripts', 'appica_core_admin_scripts' );

/**
 * Plugin auto-updater
 *
 * @since 1.0.0
 */
function zurapp_core_load_updater() {
	$plugin = get_plugin_data( APPICA_CORE_FILE, false, false );
	$plugin = array_change_key_case( $plugin, CASE_LOWER );

	// extra data
	$plugin['slug']     = strtolower( trim( $plugin['textdomain'] ) );
	$plugin['file']     = APPICA_CORE_FILE;
	$plugin['basename'] = plugin_basename( APPICA_CORE_FILE );

	$settings = array(
		'cron_hook'      => 'appica_check_for_updates',
		'cron_period'    => 'daily',
		'transient_name' => 'appica_update_info',
		'plugin'         => $plugin,
	);

	$updater = new Guild_Updater( $settings );
}

//add_action( 'admin_init', 'zurapp_core_load_updater' );

/**
 * One-click-demo-import feature
 *
 * @since 1.0.0
 */
function zurapp_core_load_importer() {
	$template_dir = get_template_directory();

	$settings = array(
		'page_title'         => __( 'ZurApp Import', 'zurapp' ),
		'menu_title'         => __( 'Demo Import', 'zurapp' ),
		'menu_slug'          => 'zurapp-import',
		'nonce'              => 'zurapp_import_nonce',
		'nonce_field'        => 'zurapp_import_nonce_field',
		'import_id'          => 0, // do not change
		'import_attachments' => true, // bool
		'variants'           => array(
			'zurapp'     => array(
				'preview' => plugins_url( 'assets/img/zurapp.png', APPICA_CORE_FILE ),
				'title'   => __( 'ZurApp', 'zurapp' ),
				'xml'     => $template_dir . '/demo/zurapp/demo.xml',
				'extra'   => $template_dir . '/demo/zurapp/extra.json',
			),
			'zurapp-alt' => array(
				'preview' => plugins_url( 'assets/img/zurapp-alt.png', APPICA_CORE_FILE ),
				'title'   => __( 'ZurApp Alt', 'zurapp' ),
				'xml'     => $template_dir . '/demo/zurapp/demo.xml',
				'extra'   => $template_dir . '/demo/zurapp/extra.json',
			),
			'tron'       => array(
				'preview' => plugins_url( 'assets/img/tron.png', APPICA_CORE_FILE ),
				'title'   => __( 'Tron', 'zurapp' ),
				'xml'     => $template_dir . '/demo/zurapp/demo.xml',
				'extra'   => $template_dir . '/demo/zurapp/extra.json',
			),
		),
	);

	$importer = new Guild_Importer( $settings ) ;
}

//add_action( 'plugins_loaded', 'zurapp_core_load_importer' );

/*
 * Load helpers functions
 */
require APPICA_CORE_ROOT . '/inc/helpers.php';

/*
 * Visual Composer custom shortcodes mapping
 */
require APPICA_CORE_ROOT . '/inc/vc-map.php';