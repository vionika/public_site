<?php
/**
 * Shortcode "Team" output
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Core
 */

$a = shortcode_atts( array(
	'type'                 => 'grid',
	'source'               => 'categories', // categories | ids
	'query_post__in'       => '',
	'query_categories'     => '',
	'query_post__not_in'   => '',
	'query_posts_per_page' => 'all',
	'query_orderby'        => 'date',
	'query_order'          => 'DESC', // DESC | ASC
	'extra_class'          => '',
), $atts );

$team_type = 'appica_team';
$team_tax  = 'appica_team_category';

$type           = sanitize_key( $a['type'] );
$is_grid        = ( 'grid' === $type );
$is_transparent = ( 'transparent' === $type );
$source         = sanitize_key( $a['source'] );
$query_defaults = array(
	'post_type'     => $team_type,
	'post_status'   => 'publish',
	'no_found_rows' => true,
);

$query_args = appica_parse_array( $a, 'query_' );
$query_args = array_merge( $query_defaults, $query_args );
$query_args = appica_query_build( $query_args, function( $query ) use ( $team_tax, $source ) {
	// tax query, @see WP_Query
	if ( 'categories' === $source && array_key_exists( 'categories', $query ) ) {
		$categories = $query['categories'];
		unset( $query['categories'] );

		$query['tax_query'] = appica_query_single_tax( $categories, $team_tax );
	}

	// fetch all posts if getting by IDs
	if ( 'ids' === $source ) {
		$query['posts_per_page'] = -1;
	}

	return $query;
} );

$query = new WP_Query( $query_args );
if ( $query->have_posts() ) {

	$classes = appica_get_class_set( array(
		$is_transparent ? 'row' : '',
		$is_grid ? 'team-grid' : '',
		$is_grid ? 'space-top-2x' : '',
		$a['extra_class'],
	) );

	// .row for transparent or .team-grid for grid
	echo '<div class="', esc_attr( $classes ), '">';

	$i = 0;
	while ( $query->have_posts() ) {
		$query->the_post();
		if ( $is_transparent ) {
			// Each 3 entries wrap to .row, but except last
			if ( 0 === $i % 3 && 0 !== $i % $query->post_count ) {
				echo '</div><div class="row">';
			}

			$i ++;
			get_template_part( 'template-parts/tile', 'team-transparent' );
		} else {
			get_template_part( 'template-parts/tile', 'team-grid' );
		}
	}

	echo '</div>'; // close .row for transparent or .team-grid for grid
}
wp_reset_postdata();
