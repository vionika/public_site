<?php
/**
 * Shortcode "Posts" output
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Core
 */

if ( ! function_exists( 'vc_path_dir' ) || ! function_exists( 'vc_build_loop_query' ) ) {
	return;
}

$a = shortcode_atts( array(
	'loop'        => '',
	'extra_class' => ''
), $atts );

$extra = appica_get_class_set( $a['extra_class'] );

if ( '' === $a['loop'] ) {
	return;
}

require_once vc_path_dir( 'PARAMS_DIR', 'loop/loop.php' );
list( $loop_args, $query ) = vc_build_loop_query( $a['loop'] );

if ( $query->have_posts() ) : ?>

	<div class="masonry-grid">
		<div class="grid-sizer"></div>
		<div class="gutter-sizer"></div>

		<?php
		while ( $query->have_posts() ) :
			$query->the_post();
			get_template_part( 'template-parts/content', 'posts' );
		endwhile;
		?>

	</div>

<?php endif;
wp_reset_postdata();
