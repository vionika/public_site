<?php
/**
 * Shortcode "Feature" output
 *
 * Mapped params are in {@path appica-core/inc/vc-map.php} {@see $appica_feature}
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Core
 */

$a = shortcode_atts( array(
	'title'            => '',
	'description'      => '',
	'link'             => '',
	'link_text'        => esc_html__( 'See more', 'appica' ),
	'align'            => 'left', // left | center | right
	'is_icon'          => 'yes',
	'icon_lib'         => 'fontawesome',
	'icon_fontawesome' => '',
	'icon_openiconic'  => '',
	'icon_typicons'    => '',
	'icon_entypo'      => '',
	'icon_linecons'    => '',
	'icon_flaticons'   => '',
	'icon_size'        => 'default', // default | large
	'icon_pos'         => 'left', // left | top | right
	'icon_va'          => 'top', // top | middle
	'icon_ha'          => 'left',
	'extra_class'      => '',
), $atts );

$title    = esc_html( $a['title'] );
$desc     = stripslashes( $a['description'] );
$align    = esc_attr( $a['align'] );
$is_icon  = ( 'yes' === $a['is_icon'] );
$icon_pos = $a['icon_pos'];

$icon = '';
if ( $is_icon && function_exists( 'vc_icon_element_fonts_enqueue' ) ) {
	$icon_size = $a['icon_size'];
	$icon_va   = $a['icon_va'];
	$icon_ha   = $a['icon_ha'];

	$library = sanitize_key( $a['icon_lib'] );
	vc_icon_element_fonts_enqueue( $library );
	$library = 'icon_' . $library;

	$icon_classes = appica_get_class_set( array(
		'icon',
		( 'large' === $icon_size ) ? 'icon-bigger' : '',
		( 'middle' === $icon_va ) ? 'va-middle' : '',
		( 'top' === $icon_va ) ? 'text-' . $icon_ha : '',
	) );

	$icon = sprintf( '<div class="%1$s"><i class="%2$s"></i></div>',
		esc_attr( $icon_classes ),
		esc_attr( $a[ $library ] )
	);

	unset( $icon_classes, $icon_size, $icon_va, $library );
}

// link
$link     = appica_build_link( $a['link'] );
$is_link  = ( ! empty( $link['url'] ) && ! empty( $a['link_text'] ) );
$link_tag = '';

if ( $is_link ) {
	$link_attr = appica_get_html_attributes( array(
		'href'   => esc_url( $link['url'] ),
		'class'  => 'link',
		'target' => esc_attr( $link['target'] ),
		'title'  => esc_html( $link['title'] ),
	) );

	$link_tag = sprintf( '<a %1$s>%2$s</a>', $link_attr, esc_html( $a['link_text'] ) );
}

// text
$text = sprintf( '<div class="text text-%4$s">%1$s%2$s%3$s</div>',
	appica_get_text( $title, '<h3>', '</h3>' ),
	appica_get_text( $desc, '<p>', '</p>' ),
	$link_tag,
	$align
);

/*
 * If icon position is "left" or "top", icon have to be before .text block
 * But, if icon position is "right", icon have to be after .text block
 */
$classes = appica_get_class_set( array(
	'icon-block',
	in_array( $icon_pos, array( 'left', 'right' ) ) ? 'icon-block-horizontal' : '',
	$a['extra_class'],
) );

// 1 - wrapper class, 2 - text block, 3 - icon block
$tpl = in_array( $icon_pos, array( 'left', 'top' ) )
	? '<div class="%1$s">%3$s%2$s</div>'
	: '<div class="%1$s">%2$s%3$s</div>';

printf( $tpl, $classes, $text, $icon );