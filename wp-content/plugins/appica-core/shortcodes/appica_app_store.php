<?php
/**
 * App Store Button | appica_app_store
 *
 * @var array $atts    Shortcode attributes
 * @var mixed $content Shortcode content
 *
 * @since 2.0.1
 *
 * @author  8guild
 * @package Appica\Core\Shortcodes
 */

$a = shortcode_atts( array(
	'link'        => '',
	'text'        => __( 'Download on the', 'appica' ),
	'extra_class' => '',
), $atts );

$link = appica_build_link( $a['link'] );
if ( empty( $link['url'] ) ) {
	return;
}

$classes = appica_get_class_set( array(
	'btn',
	'btn-default',
	'btn-app-store',
	$a['extra_class'],
) );

$attributes = array(
	'href'   => esc_url( $link['url'] ),
	'class'  => esc_attr( $classes ),
	'target' => esc_attr( trim( $link['target'] ) ),
);

?>
<a <?php echo appica_get_html_attributes( $attributes ); ?>>
	<i class="bi-apple"></i>
	<div>
		<?php appica_the_text( esc_html( $a['text'] ), '<span>', '</span>' ); ?>
		<?php esc_html_e( 'App Store', 'appica' ); ?>
	</div>
</a>
