<?php
/**
 * Google Play Button | appica_google_play
 *
 * @var array $atts    Shortcode attributes
 * @var mixed $content Shortcode content
 *
 * @since 2.0.1
 *
 * @author  8guild
 * @package Appica\Core\Shortcodes
 */

$a = shortcode_atts( array(
	'link'        => '',
	'text'        => __( 'Get it on', 'appica' ),
	'extra_class' => '',
), $atts );

$link = appica_build_link( $a['link'] );
if ( empty( $link['url'] ) ) {
	return;
}

$classes = appica_get_class_set( array(
	'btn',
	'btn-default',
	'btn-float',
	'btn-google-play',
	'waves-effect',
	$a['extra_class'],
) );

$attributes = array(
	'href'   => esc_url( $link['url'] ),
	'class'  => esc_attr( $classes ),
	'target' => esc_attr( trim( $link['target'] ) ),
);

?>
<a <?php echo appica_get_html_attributes( $attributes ); ?>>
	<img src="<?php appica_image_uri( 'img/google-play.png' ); ?>"
	     alt="<?php esc_html_e( 'Google Play', 'appica' ); ?>">
	<?php appica_the_text( esc_html( $a['text'] ), '<span>', '</span>' ); ?>
</a>
