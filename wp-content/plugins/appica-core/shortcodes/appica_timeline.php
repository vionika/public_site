<?php
/**
 * Timeline | appica_timeline
 *
 * @author  8guild
 * @package Appica\Core\Shortcode
 */

$a = shortcode_atts( array(
	'title'                => '',
	'description'          => '',
	'source'               => 'categories', // categories | ids
	'query_post__in'       => '',
	'query_categories'     => '',
	'query_post__not_in'   => '',
	'query_posts_per_page' => 'all',
	'query_orderby'        => 'date',
	'query_order'          => 'DESC', // DESC | ASC
	'extra_class'          => '',
), $atts );

$timeline_type = 'appica_timeline';
$timeline_tax  = 'appica_timeline_category';

$source         = sanitize_key( $a['source'] );
$query_defaults = array(
	'post_type'     => $timeline_type,
	'post_status'   => 'publish',
	'no_found_rows' => true,
);

$query_args = appica_parse_array( $a, 'query_' );
$query_args = array_merge( $query_defaults, $query_args );
$query_args = appica_query_build( $query_args, function( $query ) use ( $timeline_tax, $source ) {
	// tax query, @see WP_Query
	if ( 'categories' === $source && array_key_exists( 'categories', $query ) ) {
		$categories = $query['categories'];
		unset( $query['categories'] );

		$query['tax_query'] = appica_query_single_tax( $categories, $timeline_tax );
	}

	// fetch all posts if getting by IDs
	if ( 'ids' === $source ) {
		$query['posts_per_page'] = -1;
	}

	return $query;
} );

appica_the_text( esc_html( $a['title'] ), '<h1>', '</h1>' );
appica_the_text( nl2br( esc_textarea( stripslashes( $a['description'] ) ) ),
	'<div class="text-light"><p>', '</p></div>'
);

$query = new WP_Query( $query_args );
if ( $query->have_posts() ) {
	$classes = appica_get_class_set( array(
		'timeline',
		'space-top-2x',
		'space-bottom-3x',
		$a['extra_class'],
	) );

	// start .timeline
	echo '<div class="', $classes, '">';
	while ( $query->have_posts() ) {
		$query->the_post();

		$post_id = get_the_ID();
		$date    = appica_meta_box_get( $post_id, '_appica_timeline_date', 'date', '' );
		echo '<div class="timeline-row">';
		printf( '<div class="date">%1$s</div><div class="event"><p>%2$s</p></div>',
			esc_html( $date ),
			esc_html( get_the_title() )
		);
		echo '</div>';
	}

	// end .timeline
	echo '</div>';

	unset( $tpl, $date );
}
wp_reset_postdata();