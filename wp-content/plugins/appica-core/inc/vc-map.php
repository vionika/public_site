<?php
/**
 * Mapping all custom shortcodes in Visual Composer interface
 *
 * @author  8guild
 * @package Appica\Core\Shortcodes
 */

if ( ! defined( 'ABSPATH' ) ) {
	die;
}

// Do not load if VC is disabled
if ( ! is_admin() ) {
	return;
}

/**
 * Fetch all posts by Gadgets Slideshow post type for autocomplete field.
 *
 * It is safe to use IDs for import, because it is a post
 * and WP Importer do not change IDs for posts.
 *
 * @return array
 */
function appica_core_slideshow_posts() {
	$cache_key   = 'appica_slideshow_posts';
	$cache_group = 'appica_cpt';

	$posts = wp_cache_get( $cache_key, $cache_group );
	if ( false === $posts ) {
		$posts = array();
		$query = new WP_Query( array(
			'post_type'      => 'appica_slideshow',
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'posts_per_page' => - 1,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$posts[] = array(
					'value' => get_the_ID(),
					'label' => get_the_title(),
				);
			endwhile;

			wp_cache_set( $cache_key, $posts, $cache_group, DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $posts;
}

/**
 * Fetch the categories of Gadgets Slideshow CPT for autocomplete field
 *
 * The taxonomy slug used as autocomplete value because
 * of export/import issues. WP Importer creates new
 * categories, tags, taxonomies based on import information
 * with NEW IDs!
 *
 * @return array
 */
function appica_core_slideshow_categories() {
	$cache_key   = 'appica_slideshow_cats';
	$cache_group = 'appica_cpt';

	$data = wp_cache_get( $cache_key, $cache_group );
	if ( false === $data ) {
		$categories = get_terms( 'appica_slideshow_category', array( 'hierarchical' => false ) );
		if ( is_wp_error( $categories ) || empty( $categories ) ) {
			return array();
		}

		$data = array();
		foreach ( $categories as $category ) {
			$data[] = array(
				'value' => $category->slug,
				'label' => $category->name,
			);
		}

		wp_cache_set( $cache_key, $data, $cache_group, DAY_IN_SECONDS );
	}

	return $data;
}

/**
 * Fetch all posts by Timeline post type for autocomplete field.
 *
 * It is safe to use IDs for import, because it is a post
 * and WP Importer do not change IDs for posts.
 *
 * @return array
 */
function appica_core_timeline_posts() {
	$cache_key   = 'appica_timeline_posts';
	$cache_group = 'appica_cpt';

	$posts = wp_cache_get( $cache_key, $cache_group );
	if ( false === $posts ) {
		$posts = array();
		$query = new WP_Query( array(
			'post_type'      => 'appica_timeline',
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'posts_per_page' => - 1,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$posts[] = array(
					'value' => get_the_ID(),
					'label' => get_the_title(),
				);
			endwhile;

			wp_cache_set( $cache_key, $posts, $cache_group, DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $posts;
}

/**
 * Fetch the categories of Gadgets Slideshow CPT for autocomplete field
 *
 * The taxonomy slug used as autocomplete value because
 * of export/import issues. WP Importer creates new
 * categories, tags, taxonomies based on import information
 * with NEW IDs!
 *
 * @return array
 */
function appica_core_timeline_categories() {
	$cache_key   = 'appica_timeline_cats';
	$cache_group = 'appica_cpt';

	$data = wp_cache_get( $cache_key, $cache_group );
	if ( false === $data ) {
		$categories = get_terms( 'appica_timeline_category', array( 'hierarchical' => false ) );
		if ( is_wp_error( $categories ) || empty( $categories ) ) {
			return array();
		}

		$data = array();
		foreach ( $categories as $category ) {
			$data[] = array(
				'value' => $category->slug,
				'label' => $category->name,
			);
		}

		wp_cache_set( $cache_key, $data, $cache_group, DAY_IN_SECONDS );
	}

	return $data;
}

/**
 * Fetch all posts by Gallery post type for autocomplete field.
 *
 * It is safe to use IDs for import, because it is a post
 * and WP Importer do not change IDs for posts.
 *
 * @return array
 */
function appica_core_gallery_posts() {
	$cache_key   = 'appica_gallery_posts';
	$cache_group = 'appica_cpt';

	$posts = wp_cache_get( $cache_key, $cache_group );
	if ( false === $posts ) {
		$posts = array();
		$query = new WP_Query( array(
			'post_type'      => 'appica_gallery',
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'posts_per_page' => - 1,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$posts[] = array(
					'value' => get_the_ID(),
					'label' => get_the_title(),
				);
			endwhile;

			wp_cache_set( $cache_key, $posts, $cache_group, DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $posts;
}

/**
 * Fetch the categories of Gallery CPT for autocomplete field
 *
 * The taxonomy slug used as autocomplete value because
 * of export/import issues. WP Importer creates new
 * categories, tags, taxonomies based on import information
 * with NEW IDs!
 *
 * @return array
 */
function appica_core_gallery_categories() {
	$cache_key   = 'appica_gallery_cats';
	$cache_group = 'appica_cpt';

	$data = wp_cache_get( $cache_key, $cache_group );
	if ( false === $data ) {
		$categories = get_terms( 'appica_gallery_category', array( 'hierarchical' => false ) );
		if ( is_wp_error( $categories ) || empty( $categories ) ) {
			return array();
		}

		$data = array();
		foreach ( $categories as $category ) {
			$data[] = array(
				'value' => $category->slug,
				'label' => $category->name,
			);
		}

		wp_cache_set( $cache_key, $data, $cache_group, DAY_IN_SECONDS );
	}

	return $data;
}

/**
 * Fetch all posts by App Gallery post type for autocomplete field.
 *
 * It is safe to use IDs for import, because it is a post
 * and WP Importer do not change IDs for posts.
 *
 * @return array
 */
function appica_core_app_gallery_posts() {
	$cache_key   = 'appica_app_gallery_posts';
	$cache_group = 'appica_cpt';

	$posts = wp_cache_get( $cache_key, $cache_group );
	if ( false === $posts ) {
		$posts = array();
		$query = new WP_Query( array(
			'post_type'      => 'appica_app_gallery',
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'posts_per_page' => - 1,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$posts[] = array(
					'value' => get_the_ID(),
					'label' => get_the_title(),
				);
			endwhile;

			wp_cache_set( $cache_key, $posts, $cache_group, DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $posts;
}

/**
 * Fetch the categories of App Gallery CPT for autocomplete field
 *
 * The taxonomy slug used as autocomplete value because
 * of export/import issues. WP Importer creates new
 * categories, tags, taxonomies based on import information
 * with NEW IDs!
 *
 * @return array
 */
function appica_core_app_gallery_categories() {
	$cache_key   = 'appica_app_gallery_cats';
	$cache_group = 'appica_cpt';

	$data = wp_cache_get( $cache_key, $cache_group );
	if ( false === $data ) {
		$categories = get_terms( 'appica_app_gallery_category', array( 'hierarchical' => false ) );
		if ( is_wp_error( $categories ) || empty( $categories ) ) {
			return array();
		}

		$data = array();
		foreach ( $categories as $category ) {
			$data[] = array(
				'value' => $category->slug,
				'label' => $category->name,
			);
		}

		wp_cache_set( $cache_key, $data, $cache_group, DAY_IN_SECONDS );
	}

	return $data;
}

/**
 * Fetch all posts by Team post type for autocomplete field.
 *
 * It is safe to use IDs for import, because it is a post
 * and WP Importer do not change IDs for posts.
 *
 * @return array
 */
function appica_core_team_posts() {
	$cache_key   = 'appica_team_posts';
	$cache_group = 'appica_cpt';

	$posts = wp_cache_get( $cache_key, $cache_group );
	if ( false === $posts ) {
		$posts = array();
		$query = new WP_Query( array(
			'post_type'      => 'appica_team',
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'posts_per_page' => - 1,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$posts[] = array(
					'value' => get_the_ID(),
					'label' => get_the_title(),
				);
			endwhile;

			wp_cache_set( $cache_key, $posts, $cache_group, DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $posts;
}

/**
 * Fetch the categories of Team CPT for autocomplete field
 *
 * The taxonomy slug used as autocomplete value because
 * of export/import issues. WP Importer creates new
 * categories, tags, taxonomies based on import information
 * with NEW IDs!
 *
 * @return array
 */
function appica_core_team_categories() {
	$cache_key   = 'appica_team_cats';
	$cache_group = 'appica_cpt';

	$data = wp_cache_get( $cache_key, $cache_group );
	if ( false === $data ) {
		$categories = get_terms( 'appica_team_category', array( 'hierarchical' => false ) );
		if ( is_wp_error( $categories ) || empty( $categories ) ) {
			return array();
		}

		$data = array();
		foreach ( $categories as $category ) {
			$data[] = array(
				'value' => $category->slug,
				'label' => $category->name,
			);
		}

		wp_cache_set( $cache_key, $data, $cache_group, DAY_IN_SECONDS );
	}

	return $data;
}

/**
 * Visual Composer Mapping
 *
 * @since 2.0.0 Move action to this file
 *
 * @uses  vc_map
 */
function appica_core_vc_init() {
	$icon     = plugins_url( 'assets/img/appica-small.png', __DIR__ );
	$category = __( 'Appica', 'appica' );

	/**#@+
	 * Translated strings for "heading" parameters, which used more than once
	 */
	$heading_title    = __( 'Title', 'appica' );
	$heading_subtitle = __( 'Subtitle', 'appica' );
	$heading_icon     = __( 'Icon', 'appica' );
	$heading_color    = __( 'Color', 'appica' );
	/**#@-*/

	/**#@+
	 * Translated strings for "group" parameters, which used more than once
	 */
	$group_general    = __( 'General', 'appica' );
	$group_map        = __( 'Map', 'appica' );
	$group_first      = __( 'First', 'appica' );
	$group_second     = __( 'Second', 'appica' );
	$group_third      = __( 'Third', 'appica' );
	$group_percent    = __( 'Percent', 'appica' );
	$group_filtration = __( 'Filtration', 'appica' );
	$group_icon       = __( 'Icon', 'appica' );
	/**#@-*/

	/**#@+
	 * Translated strings for "description" parameters, which used more than once
	 */
	$desc_percent = __( 'Column height in percents from 0 to 100', 'appica' );
	/**#@-*/

	/**#@+
	 * Shortcode attribute values mostly for "dropdown" type which used more than once
	 */
	$value_yes_no = array(
		__( 'Yes', 'appica' ) => 'yes',
		__( 'No', 'appica' )  => 'no',
	);

	$value_left_right = array(
		__( 'Left', 'appica' )  => 'left',
		__( 'Right', 'appica' ) => 'right',
	);

	$value_enable_disable = array(
		__( 'Enable', 'appica' )  => 'enable',
		__( 'Disable', 'appica' ) => 'disable',
	);
	/**#@-*/

	/**#@+
	 * Shortcode attributes which used more than once
	 */
	$field_extra_class = array(
		'param_name'  => 'extra_class',
		'type'        => 'textfield',
		'weight'      => 1,
		'heading'     => __( 'Extra class name', 'appica' ),
		'description' => __( 'Add extra classes, divided by whitespace, if you wish to style particular content element differently.', 'appica' ),
	);

	$field_image_size = array(
		'param_name'  => 'img_size',
		'type'        => 'textfield',
		'weight'      => 10,
		'heading'     => __( 'Image Size', 'appica' ),
		'description' => __( 'Choose one of WordPress built-in sizes: "thumbnail", "medium", "large" or "full". Or any custom width and height in pixels, separated by "x", e.g. "300x200". Or single "300" which means width and height will be 300px.', 'appica' ),
		'value'       => 'medium',
	);

	$field_title = array(
		'param_name' => 'title',
		'type'       => 'textfield',
		'weight'     => 10,
		'heading'    => $heading_title,
	);

	$field_subtitle = array(
		'type'       => 'textfield',
		'param_name' => 'subtitle',
		'weight'     => 10,
		'heading'    => $heading_subtitle,
	);

	$field_icon_library = array(
		'param_name' => 'icon_lib',
		'type'       => 'dropdown',
		'weight'     => 10,
		'heading'    => __( 'Icon library', 'appica' ),
		'value'      => array(
			'Font Awesome' => 'fontawesome',
			'Open Iconic'  => 'openiconic',
			'Typicons'     => 'typicons',
			'Entypo'       => 'entypo',
			'Linecons'     => 'linecons',
			'Flaticons'    => 'flaticons',
		),
	);

	$field_icon_fontawesome = array(
		'param_name' => 'icon_fontawesome',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'emptyIcon' => true, 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'fontawesome' ),
	);

	$field_icon_openiconic = array(
		'param_name' => 'icon_openiconic',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'type' => 'openiconic', 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'openiconic' ),
	);

	$field_icon_typicons = array(
		'param_name' => 'icon_typicons',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'type' => 'typicons', 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'typicons' ),
	);

	$field_icon_entypo = array(
		'param_name' => 'icon_entypo',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'type' => 'entypo', 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'entypo' ),
	);

	$field_icon_linecons = array(
		'param_name' => 'icon_linecons',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'type' => 'linecons', 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'linecons' ),
	);

	$field_icon_flaticons = array(
		'param_name' => 'icon_flaticons',
		'type'       => 'iconpicker',
		'weight'     => 10,
		'heading'    => $heading_icon,
		'settings'   => array( 'type' => 'flaticons', 'iconsPerPage' => 500 ),
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'flaticons' ),
	);

	$field_text_align = array(
		'param_name' => 'align',
		'type'       => 'dropdown',
		'weight'     => 10,
		'heading'    => __( 'Alignment', 'appica' ),
		'std'        => 'left',
		'value'      => array(
			__( 'Left', 'appica' )   => 'left',
			__( 'Center', 'appica' ) => 'center',
			__( 'Right', 'appica' )  => 'right',
		),
	);

	$field_source = array(
		'param_name' => 'source',
		'type'       => 'dropdown',
		'weight'     => 10,
		'heading'    => __( 'Data source', 'appica' ),
		'std'        => 'categories',
		'value'      => array(
			__( 'Categories', 'appica' ) => 'categories',
			__( 'IDs', 'appica' )        => 'ids',
		),
	);

	$field_posts_per_page = array(
		'param_name'  => 'query_posts_per_page',
		'type'        => 'textfield',
		'weight'      => 10,
		'heading'     => __( 'Number of posts', 'appica' ),
		'description' => __( 'Any number or "all" for displaying all posts.', 'appica' ),
		'value'       => 'all',
		'dependency'  => array( 'element' => 'source', 'value_not_equal_to' => 'ids' ),
	);

	$field_order_by = array(
		'param_name'       => 'query_orderby',
		'type'             => 'dropdown',
		'weight'           => 10,
		'heading'          => __( 'Order by', 'appica' ),
		'edit_field_class' => 'vc_column vc_col-sm-6',
		'std'              => 'date',
		'value'            => array(
			__( 'Post ID', 'appica' )            => 'ID',
			__( 'Author', 'appica' )             => 'author',
			__( 'Post name (slug)', 'appica' )   => 'name',
			__( 'Date', 'appica' )               => 'date',
			__( 'Last Modified Date', 'appica' ) => 'modified',
			__( 'Number of comments', 'appica' ) => 'comment_count',
			__( 'Random', 'appica' )             => 'rand',
		),
	);

	$field_order = array(
		'param_name'       => 'query_order',
		'type'             => 'dropdown',
		'weight'           => 10,
		'heading'          => __( 'Sorting', 'appica' ),
		'edit_field_class' => 'vc_column vc_col-sm-6',
		'std'              => 'DESC',
		'value'            => array(
			__( 'Ascending', 'appica' )  => 'ASC',
			__( 'Descending', 'appica' ) => 'DESC',
		),
	);

	/**
	 * Custom Title | appica_custom_title
	 */
	vc_map( array(
		'name'        => __( 'Appica Custom Title', 'appica' ),
		'base'        => 'appica_custom_title',
		'description' => __( 'A fancy title', 'appica' ),
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'title',
				'heading'     => $heading_title,
				'admin_label' => true,
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'subtitle',
				'heading'     => $heading_subtitle,
				'admin_label' => true,
			),
			$field_text_align,
			$field_extra_class,
		),
	) );

	/**
	 * Testimonials | appica_testimonials
	 */
	vc_map( array(
		'name'                    => __( 'Appica Testimonials', 'appica' ),
		'description'             => __( 'For display the "Testimonials" CPT', 'appica' ),
		'base'                    => 'appica_testimonials',
		'icon'                    => $icon,
		'category'                => $category,
		'show_settings_on_create' => false,
		'params'                  => array(
			$field_extra_class,
		),
	) );

	/**
	 * Team | appica_team
	 */
	vc_map( array(
		'name'        => __( 'Appica Team', 'appica' ),
		'description' => __( 'For display the "Team" CPT', 'appica' ),
		'base'        => 'appica_team',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'param_name'  => 'type',
				'type'        => 'dropdown',
				'weight'      => 10,
				'heading'     => __( 'Tile', 'appica' ),
				'description' => __( 'Choose the tile type', 'appica' ),
				'value'       => array(
					__( 'Grid', 'appica' )        => 'grid',
					__( 'Transparent', 'appica' ) => 'transparent',
				),
			),
			$field_source,
			array(
				'param_name'  => 'query_post__in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Specify posts to retrieve', 'appica' ),
				'description' => __( 'Specify slideshow items you want to retrieve, by title', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'ids' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_team_posts' ),
				),
			),
			array(
				'param_name' => 'query_categories',
				'type'       => 'autocomplete',
				'weight'     => 10,
				'heading'    => __( 'Categories', 'appica' ),
				'dependency' => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'   => array(
					'multiple'       => true,
					'min_length'     => 2,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_team_categories' ),
				),
			),
			array(
				'param_name'  => 'query_post__not_in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude posts', 'appica' ),
				'description' => __( 'Specify posts, by title, which will be excluded from results', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_team_posts' ),
				),
			),
			$field_posts_per_page,
			$field_order_by,
			$field_order,
			$field_extra_class,
		),
	) );

	/**
	 * Gadgets Slideshow | appica_gadgets_slideshow
	 */
	vc_map( array(
		'name'        => __( 'Appica Gadgets Slideshow', 'appica' ),
		'description' => __( 'For displaying the Gadgets Slideshow', 'appica' ),
		'base'        => 'appica_gadgets_slideshow',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			$field_title,
			$field_subtitle,
			$field_source,
			array(
				'param_name'  => 'query_post__in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Specify posts to retrieve', 'appica' ),
				'description' => __( 'Specify slideshow items you want to retrieve, by title', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'ids' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_slideshow_posts' ),
				),
			),
			array(
				'param_name' => 'query_categories',
				'type'       => 'autocomplete',
				'weight'     => 10,
				'heading'    => __( 'Categories', 'appica' ),
				'dependency' => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'   => array(
					'multiple'       => true,
					'min_length'     => 2,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_slideshow_categories' ),
				),
			),
			array(
				'param_name'  => 'query_post__not_in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude posts', 'appica' ),
				'description' => __( 'Specify posts, by title, which will be excluded from results', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_slideshow_posts' ),
				),
			),
			$field_posts_per_page,
			$field_order_by,
			$field_order,
			array(
				'param_name' => 'is_sl',
				'type'       => 'dropdown',
				'weight'     => 10,
				'heading'    => __( 'Enable/Disable slideshow', 'appica' ),
				'std'        => 'disable',
				'value'      => $value_enable_disable,
			),
			array(
				'param_name'  => 'sl_interval',
				'type'        => 'textfield',
				'weight'      => 10,
				'heading'     => __( 'Slideshow interval', 'appica' ),
				'description' => __( 'Interval between slide switching, in ms', 'appica' ),
				'value'       => 3000,
				'dependency'  => array( 'element' => 'is_sl', 'value' => 'enable' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Video Popup | appica_video_popup
	 */
	vc_map( array(
		'name'        => __( 'Appica Video Popup', 'appica' ),
		'description' => __( 'For display embed video in popup', 'appica' ),
		'base'        => 'appica_video_popup',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'textarea',
				'param_name'  => 'text',
				'heading'     => $heading_title,
				'description' => __( 'Short description of video', 'appica' ),
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'video',
				'heading'    => __( 'Video URL', 'appica' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Half Block Image | appica_half_block_image
	 */
	vc_map( array(
		'name'     => __( 'Appica Half Block Image', 'appica' ),
		'base'     => 'appica_half_block_image',
		'icon'     => $icon,
		'category' => $category,
		'params'   => array(
			$field_title,
			$field_subtitle,
			array(
				'type'       => 'attach_image',
				'param_name' => 'image',
				'heading'    => __( 'Image', 'appica' ),
			),
			array(
				'type'       => 'textarea_html',
				'param_name' => 'content',
				'heading'    => __( 'Description', 'appica' ),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'align',
				'heading'    => __( 'Image position', 'appica' ),
				'std'        => 'left',
				'value'      => $value_left_right,
			),
			$field_extra_class,
		),
	) );

	/**
	 * Download Button | appica_download_btn
	 */
	vc_map( array(
		'name'     => __( 'Appica Download Button', 'appica' ),
		'base'     => 'appica_download_btn',
		'icon'     => $icon,
		'category' => $category,
		'params'   => array(
			array(
				'type'       => 'textfield',
				'param_name' => 'text',
				'heading'    => __( 'Button text', 'appica' ),
				'value'      => 'Download on the',
			),
			array(
				'type'       => 'vc_link',
				'param_name' => 'link',
				'heading'    => __( 'Link', 'appica' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Recent Posts | appica_recent_posts
	 */
	vc_map( array(
		'name'        => __( 'Appica Recent Posts', 'appica' ),
		'description' => __( 'Your latest posts inside slider', 'appica' ),
		'base'        => 'appica_recent_posts',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'per_page',
				'heading'     => __( 'Number of entries to show', 'appica' ),
				'description' => __( 'Specify the number or "all" for all posts', 'appica' ),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'is_excerpt',
				'heading'    => __( 'Show excerpt?', 'appica' ),
				'std'        => 'yes',
				'value'      => $value_yes_no,
			),
			$field_image_size,
			$field_extra_class,
		),
	) );

	/**
	 * Fancy Text | appica_fancy_text
	 */
	vc_map( array(
		'name'        => __( 'Appica Fancy Text', 'appica' ),
		'description' => __( 'A fancy string', 'appica' ),
		'base'        => 'appica_fancy_text',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'text',
				'weight'      => 999,
				'heading'     => __( 'Text', 'appica' ),
				'admin_label' => true,
			),
			$field_extra_class,
		),
	) );

	/**
	 * Timeline | appica_timeline
	 */
	vc_map( array(
		'name'        => __( 'Appica Timeline', 'appica' ),
		'description' => __( 'For display the Timeline', 'appica' ),
		'base'        => 'appica_timeline',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			$field_title,
			array(
				'param_name' => 'description',
				'type'       => 'textarea',
				'weight'     => 10,
				'heading'    => __( 'Description', 'appica' ),
			),
			$field_source,
			array(
				'param_name'  => 'query_post__in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Specify posts to retrieve', 'appica' ),
				'description' => __( 'Specify slideshow items you want to retrieve, by title', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'ids' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_timeline_posts' ),
				),
			),
			array(
				'param_name' => 'query_categories',
				'type'       => 'autocomplete',
				'weight'     => 10,
				'heading'    => __( 'Categories', 'appica' ),
				'dependency' => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'   => array(
					'multiple'       => true,
					'min_length'     => 2,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_timeline_categories' ),
				),
			),
			array(
				'param_name'  => 'query_post__not_in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude posts', 'appica' ),
				'description' => __( 'Specify posts, by title, which will be excluded from results', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_timeline_posts' ),
				),
			),
			$field_posts_per_page,
			$field_order_by,
			$field_order,
			$field_extra_class,
		),
	) );

	/**
	 * Bar Charts | appica_bar_charts
	 */
	vc_map( array(
		'name'     => __( 'Appica Bar Charts', 'appica' ),
		'base'     => 'appica_bar_charts',
		'icon'     => $icon,
		'category' => $category,
		'params'   => array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'extra_class',
				'group'       => __( 'General', 'appica' ),
				'heading'     => __( 'Extra class name', 'appica' ),
				'description' => __( 'Add extra classes, divided by whitespace, if you wish to style particular content element differently.', 'appica' ),
			),
			array(
				'param_name' => 'first_title',
				'type'       => 'textfield',
				'heading'    => $heading_title,
				'group'      => $group_first,
			),
			array(
				'param_name' => 'first_subtitle',
				'type'       => 'textfield',
				'heading'    => $heading_subtitle,
				'group'      => $group_first,
			),
			array(
				'param_name'  => 'first_percent',
				'type'        => 'textfield',
				'heading'     => $group_percent,
				'description' => $desc_percent,
				'group'       => $group_first,
			),
			array(
				'param_name' => 'first_color',
				'type'       => 'colorpicker',
				'heading'    => $heading_color,
				'group'      => $group_first,
			),
			array(
				'param_name' => 'second_title',
				'type'       => 'textfield',
				'heading'    => $heading_title,
				'group'      => $group_second,
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'second_subtitle',
				'heading'    => $heading_subtitle,
				'group'      => $group_second,
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'second_percent',
				'heading'     => $group_percent,
				'description' => $desc_percent,
				'group'       => $group_second,
			),
			array(
				'type'       => 'colorpicker',
				'param_name' => 'second_color',
				'heading'    => $heading_color,
				'group'      => $group_second,
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'third_title',
				'heading'    => $heading_title,
				'group'      => $group_third,
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'third_subtitle',
				'heading'    => $heading_subtitle,
				'group'      => $group_third,
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'third_percent',
				'heading'     => $group_percent,
				'description' => $desc_percent,
				'group'       => $group_third,
			),
			array(
				'type'       => 'colorpicker',
				'param_name' => 'third_color',
				'heading'    => $heading_color,
				'group'      => $group_third,
			),
		),
	) );

	/**
	 * Button | appica_button
	 */
	vc_map( array(
		'name'        => __( 'Appica Button', 'appica' ),
		'description' => __( 'A stylish button for universal purposes', 'appica' ),
		'base'        => 'appica_button',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'       => 'textfield',
				'weight'     => 999,
				'param_name' => 'text',
				'heading'    => __( 'Button text', 'appica' ),
				'value'      => __( 'Click Me!', 'appica' ),
			),
			array(
				'type'       => 'vc_link',
				'weight'     => 999,
				'param_name' => 'link',
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'type',
				'heading'    => __( 'Button type', 'appica' ),
				'std'        => 'default',
				'value'      => array(
					__( 'Default', 'appica' ) => 'default',
					__( 'Round', 'appica' )   => 'round',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'style',
				'heading'    => __( 'Button style', 'appica' ),
				'value'      => array(
					__( 'Standard', 'appica' ) => 'standard',
					__( 'Outlined', 'appica' ) => 'outlined',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'size',
				'heading'    => __( 'Button size', 'appica' ),
				'std'        => 'nl',
				'value'      => array(
					__( 'Small', 'appica' )  => 'sm',
					__( 'Normal', 'appica' ) => 'nl',
					__( 'Large', 'appica' )  => 'lg',
				),
			),
			array(
				'type'               => 'dropdown',
				'param_name'         => 'color',
				'param_holder_class' => 'appica-button-color',
				'heading'            => __( 'Button color', 'appica' ),
				'std'                => 'default',
				'value'              => array(
					__( 'Default', 'appica' ) => 'default',
					__( 'Primary', 'appica' ) => 'primary',
					__( 'Success', 'appica' ) => 'success',
					__( 'Info', 'appica' )    => 'info',
					__( 'Warning', 'appica' ) => 'warning',
					__( 'Danger', 'appica' )  => 'danger',
				),
			),
			$field_icon_library,
			$field_icon_fontawesome,
			$field_icon_openiconic,
			$field_icon_typicons,
			$field_icon_entypo,
			$field_icon_linecons,
			$field_icon_flaticons,
			array(
				'type'       => 'dropdown',
				'param_name' => 'icon_pos',
				'heading'    => __( 'Icon position', 'appica' ),
				'std'        => 'left',
				'value'      => $value_left_right,
				'dependency' => array(
					'element' => 'type',
					'value'   => 'default',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'is_full',
				'heading'    => __( 'Make button full-width?', 'appica' ),
				'std'        => 'no',
				'value'      => $value_yes_no,
				'dependency' => array(
					'element' => 'type',
					'value'   => 'default',
				),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Feature | appica_feature
	 */
	vc_map( array(
		'name'        => __( 'Appica Feature', 'appica' ),
		'description' => __( 'Universal feature presentation + icon', 'appica' ),
		'base'        => 'appica_feature',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			$field_title,
			array(
				'type'       => 'textarea',
				'param_name' => 'description',
				'heading'    => __( 'Description', 'appica' ),
			),
			array(
				'type'       => 'vc_link',
				'param_name' => 'link',
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'link_text',
				'heading'    => __( 'Link Text', 'appica' ),
			),
			$field_text_align,
			array(
				'param_name' => 'is_icon',
				'type'       => 'dropdown',
				'weight'     => 10,
				'heading'    => __( 'Use icon?', 'zurapp' ),
				'group'      => $group_icon,
				'std'        => 'yes',
				'value'      => array(
					__( 'Yes', 'zurapp' ) => 'yes',
					__( 'No', 'zurapp' )  => 'no',
				),
			),
			array_merge( $field_icon_library, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_fontawesome, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_openiconic, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_typicons, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_entypo, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_linecons, array( 'group' => $group_icon ) ),
			array_merge( $field_icon_flaticons, array( 'group' => $group_icon ) ),
			array(
				'type'       => 'dropdown',
				'param_name' => 'icon_size',
				'heading'    => __( 'Icon size', 'appica' ),
				'group'      => $group_icon,
				'value'      => array(
					__( 'Default', 'appica' ) => 'default',
					__( 'Large', 'appica' )   => 'large',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'icon_pos',
				'heading'    => __( 'Icon position', 'appica' ),
				'group'      => $group_icon,
				'value'      => array(
					__( 'Left', 'appica' )  => 'left',
					__( 'Top', 'appica' )   => 'top',
					__( 'Right', 'appica' ) => 'right',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'icon_ha',
				'heading'    => __( 'Icon HORIZONTAL Align', 'appica' ),
				'group'      => $group_icon,
				'dependency' => array( 'element' => 'icon_pos', 'value' => 'top' ),
				'value'      => array(
					__( 'Left', 'appica' )   => 'left',
					__( 'Center', 'appica' ) => 'center',
					__( 'Right', 'appica' )  => 'right',
				),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'icon_va',
				'heading'    => __( 'Icon VERTICAL Align', 'appica' ),
				'group'      => $group_icon,
				'value'      => array(
					__( 'Top', 'appica' )    => 'top',
					__( 'Middle', 'appica' ) => 'middle',
				),
				'dependency' => array(
					'element' => 'icon_pos',
					'value'   => array( 'left', 'right' ),
				),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Gallery | appica_gallery
	 */
	vc_map( array(
		'name'        => __( 'Appica Gallery', 'appica' ),
		'description' => __( 'Show your gallery to the world', 'appica' ),
		'base'        => 'appica_gallery',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array_merge( $field_title, array( 'group' => $group_general ) ),
			array_merge( $field_subtitle, array( 'group' => $group_general ) ),
			array_merge( $field_source, array( 'group' => $group_general ) ),
			array(
				'param_name'  => 'query_post__in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Specify posts to retrieve', 'appica' ),
				'description' => __( 'Specify slideshow items you want to retrieve, by title', 'appica' ),
				'group'       => $group_general,
				'dependency'  => array( 'element' => 'source', 'value' => 'ids' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_gallery_posts' ),
				),
			),
			array(
				'param_name' => 'query_categories',
				'type'       => 'autocomplete',
				'weight'     => 10,
				'heading'    => __( 'Categories', 'appica' ),
				'group'      => $group_general,
				'dependency' => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'   => array(
					'multiple'       => true,
					'min_length'     => 2,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_gallery_categories' ),
				),
			),
			array(
				'param_name'  => 'query_post__not_in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude posts', 'appica' ),
				'description' => __( 'Specify posts, by title, which will be excluded from results', 'appica' ),
				'group'       => $group_general,
				'dependency'  => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_gallery_posts' ),
				),
			),
			array_merge( $field_posts_per_page, array( 'group' => $group_general ) ),
			array_merge( $field_order_by, array( 'group' => $group_general ) ),
			array_merge( $field_order, array( 'group' => $group_general ) ),
			array(
				'param_name' => 'is_filters',
				'type'       => 'dropdown',
				'weight'     => 10,
				'heading'    => __( 'Enable filters?', 'appica' ),
				'group'      => $group_filtration,
				'std'        => 'yes',
				'value'      => $value_yes_no,
			),
			array(
				'param_name'  => 'filters_exclude',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude from filter list', 'zurapp' ),
				'description' => __( 'Enter categories won\'t be shown in the filters list. This option is useful if you specify some categories in General tab.', 'zurapp' ),
				'group'       => $group_filtration,
				'dependency'  => array( 'element' => 'is_filters', 'value' => 'yes' ),
				'settings'    => array(
					'multiple'       => true,
					'min_length'     => 2,
					'unique_values'  => true,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_gallery_categories' ),
				),
			),
			array_merge( $field_extra_class, array( 'group' => $group_general ) ),
		),
	) );

	/**
	 * Pricing Plans | appica_pricing_plans
	 */
	vc_map( array(
		'name'        => __( 'Appica Pricing Plans', 'appica' ),
		'description' => __( 'For displaying the Pricings CPT', 'appica' ),
		'base'        => 'appica_pricing_plans',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'param_name' => 'is_switcher',
				'type'       => 'dropdown',
				'weight'     => 10,
				'heading'    => __( 'Show plan switcher?', 'appica' ),
				'value'      => $value_yes_no,
			),
			array(
				'param_name' => 'title',
				'type'       => 'textfield',
				'weight'     => 10,
				'heading'    => $heading_title,
				'value'      => __( 'Choose a plan you need', 'appica' ),
				'dependency' => array(
					'element' => 'is_switcher',
					'value'   => 'yes',
				),
			),
			array(
				'param_name' => 'subtitle',
				'type'       => 'textfield',
				'weight'     => 10,
				'heading'    => $heading_subtitle,
				'value'      => __( 'Save 20%', 'appica' ),
				'dependency' => array(
					'element' => 'is_switcher',
					'value'   => 'yes',
				),
			),
			$field_extra_class,
		),
	) );

	/**
	 * App Gallery | appica_app_gallery
	 */
	vc_map( array(
		'name'        => __( 'Appica App Gallery', 'appica' ),
		'description' => __( 'For displaying the App Gallery CPT', 'appica' ),
		'base'        => 'appica_app_gallery',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			$field_source,
			array(
				'param_name'  => 'query_post__in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Specify posts to retrieve', 'appica' ),
				'description' => __( 'Specify slideshow items you want to retrieve, by title', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'ids' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_app_gallery_posts' ),
				),
			),
			array(
				'param_name' => 'query_categories',
				'type'       => 'autocomplete',
				'weight'     => 10,
				'heading'    => __( 'Categories', 'appica' ),
				'dependency' => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'   => array(
					'multiple'       => true,
					'min_length'     => 2,
					'display_inline' => true,
					'values'         => call_user_func( 'appica_core_app_gallery_categories' ),
				),
			),
			array(
				'param_name'  => 'query_post__not_in',
				'type'        => 'autocomplete',
				'weight'      => 10,
				'heading'     => __( 'Exclude posts', 'appica' ),
				'description' => __( 'Specify posts, by title, which will be excluded from results', 'appica' ),
				'dependency'  => array( 'element' => 'source', 'value' => 'categories' ),
				'settings'    => array(
					'multiple'   => true,
					'min_length' => 2,
					'values'     => call_user_func( 'appica_core_app_gallery_posts' ),
				),
			),
			$field_posts_per_page,
			$field_order_by,
			$field_order,
			$field_extra_class,
		),
	) );

	/**
	 * News | appica_news
	 */
	vc_map( array(
		'name'        => __( 'Appica News', 'appica' ),
		'description' => __( 'For displaying the News CPT', 'appica' ),
		'base'        => 'appica_news',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'       => 'textfield',
				'param_name' => 'more',
				'heading'    => __( '"Read More" text', 'appica' ),
				'value'      => __( 'More...', 'appica' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Posts | appica_posts
	 */
	vc_map( array(
		'name'        => __( 'Appica Posts', 'appica' ),
		'description' => __( 'Your posts inside simple, but fancy grid', 'appica' ),
		'base'        => 'appica_posts',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'loop',
				'param_name'  => 'loop',
				'settings'    => array(
					'size'     => array( 'value' => 10 ),
					'order_by' => array( 'value' => 'date' ),
				),
				'heading'     => __( 'Grids content', 'appica' ),
				'description' => __( 'Create WordPress loop, to populate content from your site.', 'appica' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Contacts | appica_contacts
	 */
	vc_map( array(
		'name'        => __( 'Appica Contacts', 'appica' ),
		'description' => __( 'Contacts information + Google Map', 'appica' ),
		'base'        => 'appica_contacts',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array_merge( $field_title, array( 'group' => $group_general ) ),
			array_merge( $field_subtitle, array( 'group' => $group_general ) ),
			array(
				'type'       => 'textarea_html',
				'param_name' => 'content',
				'heading'    => __( 'Contact Info', 'appica' ),
				'group'      => $group_general,
			),
			array_merge( $field_extra_class, array( 'group' => $group_general ) ),
			array(
				'type'        => 'textfield',
				'param_name'  => 'location',
				'group'       => $group_map,
				'heading'     => __( 'Location', 'appica' ),
				'description' => __( 'Enter any search query, which you can find on Google Maps, e.g. "New York, USA" or "Odessa, Ukraine".', 'appica' ),
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'zoom',
				'group'       => $group_map,
				'heading'     => __( 'Zoom', 'appica' ),
				'description' => __( 'The initial Map zoom level', 'appica' ),
				'value'       => 14,
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'is_zoom',
				'group'      => $group_map,
				'heading'    => __( 'Zoom Controls', 'appica' ),
				'std'        => 'disable',
				'value'      => $value_enable_disable,
			),
			array(
				'type'        => 'dropdown',
				'param_name'  => 'is_scroll',
				'group'       => $group_map,
				'heading'     => __( 'ScrollWheel', 'appica' ),
				'description' => __( 'Enable or disable scrollwheel zooming on the map.', 'appica' ),
				'std'         => 'disable',
				'value'       => $value_enable_disable,
			),
			array(
				'type'       => 'attach_image',
				'param_name' => 'marker',
				'group'      => $group_map,
				'heading'    => __( 'Custom marker', 'appica' ),
			),
			array(
				'type'        => 'textarea_raw_html',
				'param_name'  => 'gm_custom',
				'group'       => __( 'Styling', 'appica' ),
				'heading'     => __( 'Custom styling', 'appica' ),
				'description' => __( 'Generate your styles in <a href="https://snazzymaps.com/editor" target="_blank">Snazzymaps Editor</a> and paste JavaScript Style Array in field above', 'appica' ),
			),
		),
	) );

	/**
	 * MailChimp Form | appica_mailchimp_form
	 */
	vc_map( array(
		'name'        => __( 'Appica MailChimp', 'appica' ),
		'description' => __( 'MailChimp subscribe form', 'appica' ),
		'base'        => 'appica_mailchimp_form',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			$field_title,
			array(
				'type'        => 'textfield',
				'param_name'  => 'action',
				'heading'     => __( 'MailChimp URL', 'appica' ),
				'description' => __( 'You can use custom URL here. If blank, url from Appica > <a href="admin.php?page=appica&tab=4" target="_blank">Socials</a> will be used instead.', 'appica' ),
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'label_name',
				'heading'     => __( 'Label: Name', 'appica' ),
				'description' => __( 'Label for "name" field', 'appica' ),
				'value'       => __( 'Name', 'appica' ),
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'label_email',
				'heading'     => __( 'Label: Email', 'appica' ),
				'description' => __( 'Label for "email" field', 'appica' ),
				'value'       => __( 'Email', 'appica' ),
			),
			array(
				'type'        => 'textfield',
				'param_name'  => 'label_btn',
				'heading'     => __( 'Button text', 'appica' ),
				'description' => __( 'Text on the button', 'appica' ),
				'value'       => __( 'Subscribe', 'appica' ),
			),
			array(
				'type'        => 'dropdown',
				'param_name'  => 'orientation',
				'heading'     => __( 'Orientation', 'appica' ),
				'description' => __( 'How form fields will be aligned', 'appica' ),
				'std'         => 'horizontal',
				'value'       => array(
					__( 'Horizontal', 'appica' ) => 'horizontal',
					__( 'Vertical', 'appica' )   => 'vertical',
				),
			),
		),
	) );

	/**
	 * Portfolio | appica_portfolio
	 */
	vc_map( array(
		'name'        => __( 'Appica Portfolio', 'appica' ),
		'description' => __( 'Display Portfolio CPT', 'appica' ),
		'base'        => 'appica_portfolio',
		'icon'        => $icon,
		'category'    => $category,
		'params'      => array(
			array(
				'type'        => 'textfield',
				'param_name'  => 'num',
				'heading'     => __( 'Number of posts', 'appica' ),
				'description' => __( 'Set "all" for displaying all posts.', 'appica' ),
			),
			array(
				'type'       => 'textfield',
				'param_name' => 'lm_text',
				'heading'    => __( 'Load More text', 'appica' ),
				'value'      => __( 'Load More Portfolio', 'appica' ),
			),
			array(
				'type'       => 'dropdown',
				'param_name' => 'is_filters',
				'heading'    => __( 'Enable / Disable Filters', 'appica' ),
				'std'        => 'enable',
				'value'      => $value_enable_disable,
			),
			$field_extra_class,
		),
	) );

	/**
	 * App Store Button | appica_app_store
	 *
	 * @since 2.0.1
	 */
	vc_map( array(
		'base'     => 'appica_app_store',
		'name'     => __( 'App Store Button', 'appica' ),
		'icon'     => $icon,
		'category' => $category,
		'params'   => array(
			array(
				'param_name' => 'link',
				'type'       => 'vc_link',
				'weight'     => 10,
				'heading'    => __( 'Link', 'appica' ),
			),
			array(
				'param_name' => 'text',
				'type'       => 'textfield',
				'weight'     => 10,
				'heading'    => __( 'Upper line', 'appica' ),
				'value'      => __( 'Download on the', 'appica' ),
			),
			$field_extra_class,
		),
	) );

	/**
	 * Google Play Button | appica_google_play
	 *
	 * @since 2.0.1
	 */
	vc_map( array(
		'base'     => 'appica_google_play',
		'name'     => __( 'Google Play Button', 'appica' ),
		'icon'     => $icon,
		'category' => $category,
		'params'   => array(
			array(
				'param_name' => 'link',
				'type'       => 'vc_link',
				'weight'     => 10,
				'heading'    => __( 'Link', 'appica' ),
			),
			array(
				'param_name' => 'text',
				'type'       => 'textfield',
				'weight'     => 10,
				'heading'    => __( 'Upper line', 'appica' ),
				'value'      => __( 'Get it on', 'appica' ),
			),
			$field_extra_class,
		),
	) );
}

add_action( 'vc_mapper_init_after', 'appica_core_vc_init' );