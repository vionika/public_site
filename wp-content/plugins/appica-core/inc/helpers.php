<?php
/**
 * Utility & helpers functions
 *
 * @author 8guild
 */

/*
 * Misc
 */

if ( ! function_exists( 'appica_get_class_set' ) ) :
	/**
	 * Prepare and sanitize the class set.
	 *
	 * Caution! This function sanitize each class,
	 * but don't escape the returned result.
	 *
	 * E.g. [ 'my', 'cool', 'class' ] or 'my cool class'
	 * will be sanitized and converted to "my cool class".
	 *
	 * @param array|string $classes
	 *
	 * @return string
	 */
	function appica_get_class_set( $classes ) {
		if ( empty( $classes ) ) {
			return '';
		}

		if ( is_string( $classes ) ) {
			$classes = (array) $classes;
		}

		// remove empty elements before loop, if exists
		// and explode array into the flat list
		$classes   = array_filter( $classes );
		$class_set = array();
		foreach ( $classes as $class ) {
			$class = trim( $class );
			if ( false === strpos( $class, ' ' ) ) {
				$class_set[] = $class;

				continue;
			}

			// replace possible multiple whitespaces with single one
			$class = preg_replace( '/\\s\\s+/', ' ', $class );
			foreach ( explode( ' ', $class ) as $subclass ) {
				$class_set[] = trim( $subclass );
			}
			unset( $subclass );
		}
		unset( $class );

		// do not duplicate
		$class_set = array_unique( $class_set );
		$class_set = array_map( 'sanitize_html_class', $class_set );
		$class_set = array_filter( $class_set );

		$set = implode( ' ', $class_set );

		return $set;
	}
endif;

if ( ! function_exists( 'appica_get_html_attributes' ) ) :
	/**
	 * Return HTML attributes list for given attributes pairs
	 *
	 * <strong>Caution!</strong> This function does not escape attribute value,
	 * only attribute name, for more flexibility. You should do this manually
	 * for each attribute before calling this function.
	 *
	 * Also you can pass a multidimensional array with one level depth,
	 * this array will be encoded to json format.
	 *
	 * <pre>
	 * appica_get_html_attributes(array(
	 *   'class' => 'super-class',
	 *   'title' => 'My cool title',
	 *   'data-settings' => array( 'first' => '', 'second' => '' ),
	 * ));
	 * </pre>
	 *
	 * Sometimes some attributes are required and should be present in attributes
	 * list. For example, when you build attributes for a link "href" is mandatory.
	 * So if user do not fill this field default values will be used. Should
	 * be an array with the same keys as in $atts.
	 *
	 * @example <pre>
	 * appica_get_html_attributes([href => ''], [href => #]); // returns href="#"
	 * </pre>
	 *
	 * @param array $atts     Key and value pairs of HTML attributes
	 * @param array $defaults Default values, that should be present in attributes list
	 *
	 * @return string
	 */
	function appica_get_html_attributes( $atts, $defaults = array() ) {
		$attributes = array();

		// hack for "extra_attr"
		// not safety, very limited usage!
		if ( ! empty( $atts['extra'] ) ) {
			$extra_raw = $atts['extra']; // should be in format key:value,key:value
			unset( $atts['extra'] );

			$extra     = array();
			$extra_raw = explode( ',', $extra_raw ); // returns key:value pairs
			array_walk( $extra_raw, function ( $pair ) use ( &$extra ) {
				if ( false === strpos( $pair, ':' ) ) {
					return;
				}

				$chunks = explode( ':', $pair );
				$extra[ $chunks[0] ] = $chunks[1];
			} );

			$atts = array_merge( $atts, $extra );
			unset( $extra_raw, $extra );
		}

		foreach ( $atts as $attribute => $value ) {
			$template = '%1$s="%2$s"';

			// if user pass empty value, use one from defaults if same key exists
			// allowed only for scalar types
			if ( is_scalar( $value )
			     && '' === (string) $value
			     && array_key_exists( $attribute, $defaults )
			) {
				$value = $defaults[ $attribute ];
			}

			// convert array to json
			if ( is_array( $value ) ) {
				$template = '%1$s=\'%2$s\'';
				$value    = json_encode( $value );
			}

			if ( is_bool( $value ) ) {
				$template = '%1$s';
			}

			// $value should not be empty
			if ( empty( $value ) ) {
				continue;
			}

			$attribute    = sanitize_key( $attribute );
			$attributes[] = sprintf( $template, $attribute, $value );
		}

		return implode( ' ', $attributes );
	}
endif;

if ( ! function_exists( 'appica_get_css_declarations' ) ) :
	/**
	 * Generate CSS declarations like "width: auto;", "background-color: red;", etc.
	 *
	 * May be used either standalone function or in pair with {@see appica_get_css_rules}
	 *
	 * @param array $props Array of properties where key is property name
	 *                     and value is a property value
	 *
	 * @return string
	 */
	function appica_get_css_declarations( $props ) {
		$declarations = array();

		foreach ( $props as $name => $value ) {
			if ( is_scalar( $value ) ) {
				$declarations[] = "{$name}: {$value};";
				continue;
			}

			/*
			 * $value may be an array, not only scalar, in case of multiple declarations,
			 * like background gradients, etc.
			 *
			 * background: white;
			 * background: -moz-linear-gradient....
			 *
			 * $sub (sub value) should be a string!
			 */
			foreach ( (array) $value as $sub ) {
				$declarations[] = "{$name}: {$sub};";
			}
			unset( $sub );
		}
		unset( $name, $value );

		return implode( ' ', $declarations );
	}
endif;

if ( ! function_exists( 'appica_get_css_rules' ) ) :
	/**
	 * Generate CSS rules
	 *
	 * @uses appica_get_css_declarations
	 *
	 * @param string|array $selectors Classes or tags where properties will be applied to.
	 * @param string|array $props     Array of css rules where key is property name itself
	 *                                and value is a property value. Example: [font-size => 14px].
	 *                                Or string with CSS rules declarations in format: "font-size: 14px;"
	 *
	 * @return string CSS rules in format .selector {property: value;}
	 */
	function appica_get_css_rules( $selectors, $props ) {
		// Convert to string
		if ( is_array( $selectors ) ) {
			$selectors = implode( ', ', $selectors );
		}

		// convert to string, too
		if ( is_array( $props ) ) {
			$props = appica_get_css_declarations( $props );
		}

		return sprintf( '%1$s {%2$s}', $selectors, $props );
	}
endif;

if ( ! function_exists( 'appica_get_unique_id' ) ):
	/**
	 * Return the unique ID for general purposes
	 *
	 * @param string $prepend Will be prepended to generated string
	 * @param int $limit      Limit the number of unique symbols!
	 *                        How many unique symbols should be in a string,
	 *                        maximum is 32 symbols. $prepend not included.
	 *
	 * @return string
	 */
	function appica_get_unique_id( $prepend = '', $limit = 8 ) {
		$unique = substr( md5( uniqid() ), 0, $limit );

		return $prepend . $unique;
	}
endif;

if ( ! function_exists( 'appica_get_image_src' ) ):
	/**
	 * Return non-escaped URL of the attachment by given ID.
	 * Perfect for background images or img src attribute.
	 *
	 * @param int    $attachment_id Attachment ID
	 * @param string $size          Image size, can be "full", "large", etc..
	 *
	 * @uses wp_get_attachment_image_src
	 *
	 * @return string String with url on success or empty string on fail.
	 */
	function appica_get_image_src( $attachment_id, $size = 'full' ) {
		if ( empty( $attachment_id ) ) {
			return '';
		}

		$attachment = wp_get_attachment_image_src( $attachment_id, $size );
		if ( false === $attachment ) {
			return '';
		}

		if ( ! empty( $attachment[0] ) ) {
			return $attachment[0];
		}

		return '';
	}
endif;

if ( ! function_exists( 'appica_get_image_size' ) ):
	/**
	 * Return prepared image size
	 *
	 * @param string $size User specified image size. Default is "full"
	 *
	 * @return array|string Built-in size keyword or array of width and height
	 */
	function appica_get_image_size( $size = 'full' ) {
		/**
		 * @var array Allowed image sizes and aliases
		 */
		$allowed = array_merge( get_intermediate_image_sizes(), array(
			'thumb',
			'post-thumbnail',
			'full'
		) );

		$out = 'full';
		if ( is_numeric( $size ) ) {
			// user specify single integer
			$size = (int) $size;
			$out  = array( $size, $size );
		} elseif ( false !== strpos( $size, 'x' ) ) {
			// user specify pair of width and height
			$out = array_map( 'absint', explode( 'x', $size ) );
		} elseif ( in_array( $size, $allowed, true ) ) {
			// user specify one of the built-in sizes
			$out = $size;
		}

		return $out;
	}
endif;

if ( ! function_exists( 'appica_get_image_bg' ) ) :
	/**
	 * Returns ready-to-use and escaped "background-image: %".
	 * Useful for situations when you only need a background image.
	 * Specify the fallback if you do not want see element without
	 * the background.
	 *
	 * Optimized for {@see appica_get_html_attributes}
	 *
	 * @example
	 * <pre>
	 * $attr = appica_get_html_attributes(array(
	 *   'class' => 'some-class',
	 *   'style' => appica_get_image_bg( 123, 'medium', 'placeholder.jpg' )
	 * ));
	 * </pre>
	 *
	 * @param int    $attachment_id Attachment ID
	 * @param string $size          Image size, like "full", "medium", etc..
	 * @param string $fallback      Full URI to fallback image, good for placeholders.
	 *
	 * @return string
	 */
	function appica_get_image_bg( $attachment_id, $size = 'full', $fallback = '' ) {
		$src = appica_get_image_src( $attachment_id, $size );
		if ( empty( $src ) && empty( $fallback ) ) {
			return '';
		}

		if ( empty( $src ) && ! empty( $fallback ) ) {
			$src = $fallback;
		}

		return appica_get_css_declarations( array(
			'background-image' => sprintf( 'url(%s)', esc_url( $src ) ),
		) );
	}
endif;

if ( ! function_exists( 'appica_get_image_tag' ) ) :
	/**
	 * Return the ready-to-use <img> tag
	 *
	 * @param int    $attachment_id Attachment ID
	 * @param string $size          Image size, like "full", "medium", etc..
	 * @param array  $attr          HTML attributes, like "alt"
	 *
	 * @example
	 * <pre>
	 * echo appica_get_image_tag( 123, 'large', [ 'alt' => 'Logo' ] );
	 * </pre>
	 *
	 * @return string
	 */
	function appica_get_image_tag( $attachment_id, $size = 'full', $attr = array() ) {
		$src = appica_get_image_src( $attachment_id, $size );
		if ( empty( $src ) ) {
			return '';
		}

		$attr = wp_parse_args( $attr, array(
			'src' => $src,
			'alt' => '',
		) );

		return sprintf( '<img %s>', appica_get_html_attributes( $attr ) );
	}
endif;

if ( ! function_exists( 'appica_get_directory_contents' ) ) :
	/**
	 * Returns the contents of directory
	 *
	 * Designed for CPTs and shortcodes directories for auto loading
	 * files.
	 *
	 * @param string      $path Absolute path to directory
	 * @param string|null $ext  Suffix for {@see DirectoryIterator::getBasename}
	 *
	 * @return array A list of [filename => path]
	 */
	function appica_get_directory_contents( $path, $ext = '.php' ) {
		$files = array();
		try {
			$dir = new DirectoryIterator( $path );
			foreach( $dir as $file ) {
				if ( $file->isDot() || ! $file->isReadable() ) {
					continue;
				}

				$filename = $file->getBasename( $ext );

				// Do not load files if name starts with underscores
				if ( '_' === substr( $filename, 0, 1 ) ) {
					continue;
				}

				$files[ $filename ] = $file->getPathname();
				unset( $filename );
			}
			unset( $file );
		} catch ( Exception $e ) {
			trigger_error( 'appica_get_directory_contents(): ' . $e->getMessage() );
		}

		return $files;
	}
endif;

if ( ! function_exists( 'appica_get_post_terms' ) ) :
	/**
	 * Return terms, assigned for given Post ID,
	 * depending on {@see $context} param: "slug" or "name".
	 *
	 * @param integer $post_id  Post ID.
	 * @param string  $taxonomy The taxonomy for which to retrieve terms.
	 * @param string  $context  Term slug or name, depending on what is required.
	 *                          Default is "slug".
	 *
	 * @return array [ term, term, ... ]
	 */
	function appica_get_post_terms( $post_id, $taxonomy, $context = 'slug' ) {
		$post_terms = wp_get_post_terms( $post_id, $taxonomy );
		// Catch the WP_Error or if any terms was not assigned to post
		if ( is_wp_error( $post_terms ) || empty( $post_terms ) ) {
			return array();
		}

		$terms = array();
		foreach ( $post_terms as $term ) {
			$terms[] = $term->$context;
		}
		unset( $term, $post_terms );

		return $terms;
	}
endif;

if ( ! function_exists( 'appica_get_text' ) ) :
	/**
	 * Maybe returns some text.
	 *
	 * HTML allowed
	 *
	 * @param string $text   A piece of text
	 * @param string $before Before the text
	 * @param string $after  After the text
	 *
	 * @return string
	 */
	function appica_get_text( $text, $before = '', $after = '' ) {
		if ( empty( $text ) ) {
			return '';
		}

		return $before . $text . $after;
	}
endif;

if ( ! function_exists( 'appica_the_text' ) ) :
	/**
	 * Maybe echoes some text
	 *
	 * HTML allowed
	 *
	 * @param string $text   A piece of text
	 * @param string $before Before the text
	 * @param string $after  After the text
	 */
	function appica_the_text( $text, $before = '', $after = '' ) {
		if ( empty( $text ) ) {
			return;
		}

		echo $before . $text . $after;
	}
endif;

if ( ! function_exists( 'appica_google_font_url' ) ) :
	/**
	 * Prepare the link for Google Fonts the right way
	 *
	 * @param string $url A url to a Google Font
	 *
	 * @return string
	 */
	function appica_google_font_url( $url ) {
		$query = parse_url( $url, PHP_URL_QUERY );
		if ( null === $query ) {
			return '';
		}

		parse_str( $query, $out );
		if ( ! array_key_exists( 'family', $out ) || empty( $out['family'] ) ) {
			return '';
		}

		$url = add_query_arg( 'family', urlencode( $out['family'] ), '//fonts.googleapis.com/css' );

		return esc_url( $url );
	}
endif;

if ( ! function_exists( 'appica_parse_array' ) ):
	/**
	 * Extract key-value pairs from array by given prefix
	 *
	 * Very useful for integrated shortcode params.
	 *
	 * Found undocumented function in Visual Composer and now I can integrate
	 * shortcode params from one shortcode into another and then execute them
	 * by passing into do_shortcode() function.
	 *
	 * However as I receive shortcode attributes with prefix I should remove
	 * that prefix and pass valid attribute names into the master shortcode.
	 *
	 * Example:
	 * <pre>
	 *  carousel_images => images
	 *  carousel_speed  => speed
	 * </pre>
	 *
	 * Example usage:
	 * <pre>
	 * $a = [
	 *   title => '',
	 *   subtitle => '',
	 *   query_orderby => '',
	 *   query_order => '',
	 * ];
	 *
	 * $query_args = appica_parse_array( $a, 'query_' );
	 * // returns [ orderby => '', order => '' ]
	 * </pre>
	 *
	 * @param array  $pairs  Key-value pairs
	 * @param string $prefix Prefix in key
	 *
	 * @return array New pairs without prefix
	 */
	function appica_parse_array( $pairs, $prefix ) {
		// Prefix should always be appended with underscores
		// e.g. "prefix_"
		$prefix = rtrim( $prefix, '_' );
		$prefix .= '_';

		$attributes = array();
		foreach ( $pairs as $param => $value ) {
			if ( false !== strpos( $param, $prefix ) ) {
				$clean_param                = str_replace( $prefix, '', $param );
				$attributes[ $clean_param ] = $value;
			}

			continue;
		}

		return $attributes;
	}
endif;

if ( ! function_exists( 'appica_parse_slug_list' ) ) :
	/**
	 * Clean up an array, comma- or space-separated list of slugs.
	 *
	 * @param array|string $list List of slugs
	 *
	 * @return array Sanitized array of slugs
	 */
	function appica_parse_slug_list( $list ) {
		if ( ! is_array( $list ) ) {
			$list = preg_split( '/[\s,]+/', $list );
		}

		$list = array_map( 'sanitize_key', $list );
		$list = array_unique( $list );

		return $list;
	}
endif;

/*
 * Plugin. Functions designer for using in plugins
 */

if ( ! function_exists( 'appica_plugin_asset_uri' ) ) :
	/**
	 * Echoes or just return the URI to specified asset, like image,
	 * from the "assets" directory from plugin.
	 *
	 * Note! $path should not contain "assets" substring
	 *
	 * @param string $path A relative path to asset in plugins dir
	 * @param bool   $echo Whether echo or not
	 *
	 * @return string
	 */
	function appica_plugin_asset_uri( $path, $echo = true ) {
		$path = str_replace( 'assets', '', $path );
		$path = ltrim( $path, '/' );
		$path = 'assets' . DIRECTORY_SEPARATOR . $path;

		$asset_uri = esc_url( plugins_url( $path, APPICA_CORE_FILE ) );
		if ( $echo ) {
			echo $asset_uri;
		}

		return $asset_uri;
	}
endif;

/*
 * Theme. Functions designed for using in themes
 */

if ( ! function_exists( 'appica_theme_asset' ) ) :
	/**
	 * Return unescaped and maybe echoes escaped full uri to asset.
	 * See examples.
	 *
	 * <pre>
	 * // Echoes the full uri to bg.png
	 * appica_theme_asset( 'img/bg.png' );
	 * </pre>
	 *
	 * <pre>
	 * // Get the uri to bg.png without echoing
	 * $uri = esc_url( appica_theme_asset( 'img/bg.png', false ) );
	 * </pre>
	 *
	 * <pre>
	 * <!-- Echoes the inline background-image -->
	 * <div class="page-title light-skin"
	 *      style="background-image: url(<?php appica_theme_asset( 'img/page-title/bg01.jpg' ); ?>);">
	 *</pre>
	 *
	 * <pre>
	 * // Build the attributes with style, contains background-image
	 * $style = appica_get_css_declarations(array(
	 *   'background-image' => sprintf( 'url(%s)', esc_url( appica_theme_asset( 'img/bg.png', false ) ) ),
	 * ));
	 * $attr = appica_get_html_attributes(array(
	 *   'style' => $style
	 * ));
	 * </pre>
	 *
	 * @param string $path Relative path to asset (img, css, js, etc)
	 * @param bool   $echo
	 *
	 * @return string
	 */
	function appica_theme_asset( $path, $echo = true ) {
		$theme_uri = get_template_directory_uri();

		$path = ltrim( $path, '/' );
		$uri  = $theme_uri . '/' . $path;

		if ( $echo ) {
			echo esc_url( $uri );
		}

		return $uri;
	}
endif;

/*
 * Options. Functions designed for working with theme options.
 */

if ( ! function_exists( 'appica_option_get' ) ) :
	/**
	 * Get option by it name
	 *
	 * This option designed for use with Redux,
	 * but everything should work without Redux plugin.
	 *
	 * @see inc/options.php Cache flushing should be here
	 *
	 * @param string     $key     Option name or "all" for whole bunch of options.
	 * @param bool|mixed $default Option default value
	 * @param string     $option  Option name in _options table
	 *
	 * @return mixed
	 */
	function appica_option_get( $key, $default = false, $option = 'appica_options' ) {
		$options = wp_cache_get( $option );
		if ( empty( $options ) ) {
			$options = get_option( $option );
			wp_cache_set( $option, $options, '', DAY_IN_SECONDS );
		}

		// if something wrong with options
		if ( empty( $options ) ) {
			return $default;
		}

		if ( 'all' === $key ) {
			return $options;
		}

		$value = $default;
		if ( is_array( $options )
		     && array_key_exists( $key, $options )
		) {
			$value = $options[ $key ];
		}

		return $value;
	}
endif;

if ( ! function_exists( 'appica_option_slice' ) ) :
	/**
	 * Returns only those options which names started from given prefix.
	 *
	 * Based on naming convention when each option should has a global
	 * prefix based on section where this option is added.
	 *
	 * E.g. for typography options prefix should be "typography_",
	 * for color options prefix should be "color_", etc.
	 *
	 * @uses appica_option_get
	 *
	 * @param string $prefix Part of option name
	 *
	 * @return array Options names without prefix and its values
	 */
	function appica_option_slice( $prefix ) {
		$options = appica_option_get( 'all' );
		if ( false === $options ) {
			return array();
		}

		$prefix = rtrim( $prefix, '_' );
		$prefix .= '_';

		$sliced = array();
		foreach ( (array) $options as $option => $value ) {
			if ( false === strpos( $option, $prefix ) ) {
				continue;
			}

			$option            = str_replace( $prefix, '', $option );
			$sliced[ $option ] = $value;
		}

		return $sliced;
	}
endif;

if ( ! function_exists( 'appica_option_image' ) ) :
	/**
	 * Wrapper for {@see appica_option_get} for image field types.
	 *
	 * Based on Redux native behaviour (always returns array for images).
	 * Typically, I expected image URL as a result.
	 *
	 * @uses appica_option_get()
	 *
	 * @param string     $option  Option name
	 * @param string     $key     What key I wanna get? Default is "url".
	 * @param bool|false $default Option default value
	 *
	 * @return mixed
	 */
	function appica_option_image( $option, $key = 'url', $default = false ) {
		$data  = $default;
		$image = appica_option_get( $option );

		// In Redux image field types always returns arrays
		// Also, check for empty field
		if ( ! empty( $image )
		     && is_array( $image )
		     && array_key_exists( $key, $image )
		     && ! empty( $image[ $key ] )
		) {
			$data = $image[ $key ];
		}

		return $data;
	}
endif;

/*
 * Equip. Fallback functions
 */

if ( ! function_exists( 'appica_meta_box_get' ) ) :
	/**
	 * Returns the values of meta box.
	 *
	 * If $field is specified will return the field's value.
	 *
	 * @param int         $post_id Post ID
	 * @param string      $slug    Meta box unique name
	 * @param null|string $field   Key of the field
	 * @param mixed       $default Default value
	 *
	 * @return mixed Array with key-value, mixed data if field is specified and the value
	 *               of $default field if nothing found.
	 */
	function appica_meta_box_get( $post_id, $slug, $field = null, $default = array() ) {
		// pass to equip if exists
		if ( function_exists( 'equip_meta_box_get' ) ) {
			return equip_meta_box_get( $post_id, $slug, $field, $default );
		}

		$cache_key   = appica_cache_key( $slug, $post_id );
		$cache_group = 'meta_box';

		// Cached value should always be an array
		$values = wp_cache_get( $cache_key, $cache_group );
		if ( false === $values ) {
			$values = get_post_meta( $post_id, $slug, true );
			if ( empty( $values ) ) {
				// possible cases: meta box not saved yet
				// or mistake in $post_id or $slug
				return $default;
			}

			// cache for 1 day
			wp_cache_set( $cache_key, $values, $cache_group, 86400 );
		}

		$result = null;
		if ( ! is_array( $values ) ) {
			// return AS IS for non-array values
			$result = $values;
		} elseif ( null === $field ) {
			// return whole array if $field not specified
			$result = $values;
		} elseif ( array_key_exists( $field, $values ) ) {
			// if specified $field present
			$result = $values[ $field ];
		} else {
			// nothing matched, return default value
			$result = $default;
		}

		return $result;
	}
endif;

if ( ! function_exists( 'appica_get_networks' ) ) :
	/**
	 * Get social networks list
	 *
	 * If Equip missed or inactive fallback of socials.ini
	 * in core plugin or theme will be used, based on $ini path
	 *
	 * @see equip/assets/misc/socials.ini
	 * @see assets/misc/socials.ini
	 * @see misc/socials.ini
	 *
	 * @param string $path_to_ini Optional path to .ini file. Will be used if specified.
	 *
	 * @return array
	 */
	function appica_get_networks( $path_to_ini = '' ) {
		if ( empty( $path_to_ini ) && function_exists( 'equip_get_socials' ) ) {
			return equip_get_socials();
		}

		$path_to_ini = wp_normalize_path( $path_to_ini );
		$networks    = parse_ini_file( $path_to_ini, true );

		ksort( $networks );

		return apply_filters( 'appica_get_networks', $networks );
	}
endif;

/**
 * Cache
 */

if ( ! function_exists( 'appica_cache_key' ) ) :
	/**
	 * Return the cache key based on some $slug and $salt
	 *
	 * Should be less than 45 symbols!
	 *
	 * @param string $slug Name of the element which required hashing
	 * @param string $salt Some unique information, e.g. post ID
	 *
	 * @return string Example $slug_8
	 */
	function appica_cache_key( $slug, $salt = '' ) {
		$hash = substr( md5( $salt . $slug ), 0, 8 );

		$slug = preg_replace( '/[^a-z0-9_-]+/i', '-', $slug );
		$slug = str_replace( array( '-', '_' ), '-', $slug );
		$slug = trim( $slug, '-' );
		$slug = str_replace( '-', '_', $slug );

		return "{$slug}_{$hash}";
	}
endif;

if ( ! function_exists( 'appica_cache_encode' ) ) :
	/**
	 * Encode the content before caching.
	 *
	 * @param string $content Some content, usually HTML string.
	 *
	 * @return string
	 */
	function appica_cache_encode( $content ) {
		return str_replace( array( "\r\n", "\r", "\n", "\t" ), '', $content );
	}
endif;

if ( ! function_exists( 'appica_cache_decode' ) ) :
	/**
	 * Decode the previously encoded content
	 *
	 * @see appica_cache_encode
	 *
	 * @param string $cache Encoded and cached value
	 *
	 * @return string
	 */
	function appica_cache_decode( $cache ) {
		return $cache; // TODO: may be sanitize in some way?
	}
endif;

/*
 * Query. Working with WP_Query
 */

if ( ! function_exists( 'appica_query_encode' ) ) :
	/**
	 * Encoding the query args for passing into the html
	 *
	 * @see appica_query_decode()
	 *
	 * @param array $query Query args for WP_Query
	 *
	 * @return string
	 */
	function appica_query_encode( $query ) {
		return base64_encode( json_encode( $query ) );
	}
endif;

if ( ! function_exists( 'appica_query_decode' ) ):
	/**
	 * Decoding the encoded string with query args for WP_Query
	 *
	 * @see appica_query_encode()
	 *
	 * @param string $query Encoded string with query args
	 *
	 * @return array|null
	 */
	function appica_query_decode( $query ) {
		return json_decode( base64_decode( $query ), true );
	}
endif;

if ( ! function_exists( 'appica_query_per_page' ) ) :
	/**
	 * Handle the "posts_per_page" option for WP_Query.
	 *
	 * Return -1 for "all" posts, absolute number or if valid value not given
	 * returns the value from Settings > Reading option.
	 *
	 * @param mixed $per_page
	 *
	 * @return mixed
	 */
	function appica_query_per_page( $per_page ) {
		if ( 'all' === strtolower( $per_page ) ) {
			return - 1;
		} elseif ( is_numeric( $per_page ) ) {
			return absint( $per_page );
		} else {
			return get_option( 'posts_per_page' );
		}
	}
endif;

if ( ! function_exists( 'appica_query_single_tax' ) ) :
	/**
	 * Build a tax_query with a single taxonomy for WP_Query.
	 * Multiple terms are allowed.
	 *
	 * Use the taxonomy slug because of export/import issues.
	 * During the import process WordPress creates new taxonomy
	 * (with new ID) based on import information.
	 *
	 * @param string $terms    A comma-separated list of slugs, directly from a shortcode atts
	 * @param string $taxonomy Taxonomy name
	 *
	 * @return array A read-to-use tax_query
	 */
	function appica_query_single_tax( $terms, $taxonomy ) {
		$tax_queries = array();

		$tax_queries[] = array(
			'taxonomy' => sanitize_key( $taxonomy ),
			'field'    => 'slug',
			'terms'    => appica_parse_slug_list( $terms ),
		);

		return $tax_queries;
	}
endif;

if ( ! function_exists( 'appica_query_multiple_tax' ) ) :
	/**
	 * Build a tax_query for WP_Query with multiple number of taxonomies
	 *
	 * @param string $term_ids   A comma-separated list of IDs, directly from a shortcode atts.
	 * @param array  $taxonomies A list of taxonomies, like "category", "post_tag", custom tax.
	 *
	 * @return array
	 */
	function appica_query_multiple_tax( $term_ids, $taxonomies ) {
		$terms = get_terms( $taxonomies, array(
			'hierarchical' => false,
			'include'      => wp_parse_id_list( $term_ids ),
		) );

		if ( ! is_array( $terms ) || empty( $terms ) ) {
			return array();
		}

		/*
		 * Build the taxonomies array for use in tax_query
		 *
		 * If taxonomy already exists in list, just add value to terms array.
		 * Otherwise add a new taxonomy to $tax_queries array.
		 */
		$tax_queries = array();
		foreach ( $terms as $t ) {
			if ( array_key_exists( $t->taxonomy, $tax_queries ) ) {
				$tax_queries[ $t->taxonomy ]['terms'][] = $t->term_id;
			} else {
				$tax_queries[ $t->taxonomy ] = array(
					'taxonomy' => $t->taxonomy,
					'field'    => 'term_id',
					'terms'    => array( (int) $t->term_id ),
				);
			}
		}
		unset( $t );

		return array_values( $tax_queries );
	}
endif;

if ( ! function_exists( 'appica_query_build' ) ) :
	/**
	 * Building an query for using in WP_Query
	 *
	 * If callback is specified the parsed query should be passed into it,
	 * so you can use some extra logic to process the query args before it
	 * will be returned. A good place for passing an anonymous function.
	 *
	 * @param array         $args     Key-value pairs for using inside WP_Query
	 * @param null|callable $callback Process query args with extra logic before returning
	 *
	 * @return array|mixed
	 */
	function appica_query_build( $args, $callback = null ) {
		$query = array();

		$args = array_filter( $args );
		foreach ( $args as $param => $value ) {
			switch ( $param ) {
				case 'post__in':
				case 'post__not_in':
					$query[ $param ] = wp_parse_id_list( $value );
					break;

				case 'orderby':
				case 'order':
					$query[ $param ] = sanitize_text_field( $value );
					break;

				case 'posts_per_page':
					$query[ $param ] = appica_query_per_page( $value );
					break;

				case 'categories':
				case 'taxonomies':
					// these are service keys, that are used for
					// building the tax_query, they should be processing in a callback,
					// so pass as is.
					$query[ $param ] = $value;
					break;

				default:
					$query[ $param ] = $value;
					break;
			}
		}
		unset( $param, $value );

		if ( is_callable( $callback ) ) {
			$query = call_user_func( $callback, $query );
		}

		// remove empty values
		$query = array_filter( $query );

		return $query;
	}
endif;

/*
 * Shortcodes
 */

if ( ! function_exists( 'appica_shortcode_cache_key' ) ) :
	/**
	 * This function is designed for creating a cache keys
	 * for shortcodes and based on shortcodes params.
	 *
	 * @param array  $parsed The result of {@see shortcode_atts}
	 * @param string $prefix Prefix, shortcode name is a good choice
	 *
	 * @return string
	 */
	function appica_shortcode_cache_key( $parsed, $prefix = '' ) {
		$params = wp_array_slice_assoc( $parsed, array_keys( $parsed ) );
		$params = serialize( $params );
		$key    = substr( md5( $params ), 0, 8 );

		if ( ! empty( $prefix ) ) {
			$prefix = rtrim( $prefix, '-_' );

			return "{$prefix}_{$key}";
		}

		return $key;
	}
endif;

if ( ! function_exists( 'appica_shortcode_build' ) ) :
	/**
	 * Build a shortcode
	 *
	 * @param       $tag
	 * @param array $atts
	 * @param null  $content
	 *
	 * @return string
	 */
	function appica_shortcode_build( $tag, $atts = array(), $content = null ) {
		$attributes = array();
		$enclosed   = '';
		if ( count( $atts ) > 0 ) {
			foreach ( $atts as $param => $value ) {
				$attributes[] = sprintf( '%1$s="%2$s"', $param, $value );
			}

			$attributes = implode( ' ', $attributes );
		}

		if ( null !== $content ) {
			$enclosed .= $content;
			$enclosed .= "[/{$tag}]";
		}

		return sprintf( '[%1$s %2$s]%3$s', $tag, $attributes, $enclosed );
	}
endif;

/*
 * VC Compatibility
 */

if ( ! function_exists( 'appica_build_link' ) ) :
	/**
	 * Parse string like "title:Hello world|weekday:Monday"
	 * to [title => 'Hello World', weekday => 'Monday']
	 *
	 * This function is a fallback to Visual Composer's {@see vc_build_link()} function.
	 * Necessary for situations, when user disable VC but do not remove the shortcode.
	 *
	 * @param string $link Encoded link from TinyMCE link builder
	 *
	 * @return array
	 */
	function appica_build_link( $link ) {
		if ( function_exists( 'vc_build_link' ) ) {
			return vc_build_link( $link );
		}

		$pairs = explode( '|', $link );
		if ( 0 === count( $pairs ) ) {
			return array( 'url' => '', 'title' => '', 'target' => '' );
		}

		$result = array();
		foreach ( $pairs as $pair ) {
			$param = preg_split( '/\:/', $pair );
			if ( ! empty( $param[0] ) && isset( $param[1] ) ) {
				$result[ $param[0] ] = rawurldecode( $param[1] );
			}
		}

		return $result;
	}
endif;

if ( ! function_exists( 'appica_convert_link' ) ) :
	/**
	 * Convert a link in VC compatible format
	 *
	 * [url, title, target] into url|title|target with
	 * URL encoding
	 *
	 * @param array $attr Attributes
	 *
	 * @return string
	 */
	function appica_convert_link( $attr ) {
		$link = array();

		foreach( $attr as $a => $v ) {
			switch( $a ) {
				case 'url':
					$link[] = 'url:' . rawurlencode( $v );
					break;

				case 'title':
					$link[] = 'title:' . rawurlencode( $v );
					break;

				case 'target':
					$link[] = 'target:' . rawurlencode( $v );
					break;

				default:
					$link[] = '';
					break;
			}
		}
		unset( $a, $v );

		return implode( '|', array_filter( $link ) );
	}
endif;

if ( ! function_exists( 'appica_get_css_animation_class' ) ):
	/**
	 * Returns CSS Animation classes, defined in Visual Composer
	 *
	 * This function is a fallback for getCSSAnimation() method
	 *
	 * @param string $animation Animation class or "no" if no animation used
	 *
	 * @return string Empty string if css animation not used
	 */
	function appica_get_css_animation_class( $animation ) {
		if ( 'no' === (string) $animation ) {
			return '';
		}

		if ( wp_script_is( 'waypoints', 'registered' ) ) {
			wp_enqueue_script( 'waypoints' );
		}

		return "wpb_animate_when_almost_visible wpb_{$animation}";
	}
endif;

if ( ! function_exists( 'appica_do_shortcode' ) ) :
	/**
	 * Parse TinyMCE content. Maybe do_shortcode().
	 *
	 * This function is a fallback for Visual Composer's
	 * wpb_js_remove_wpautop() function.
	 *
	 * @param string     $content Shortcode content
	 * @param bool|false $autop   Use {@see wpautop()} or not
	 *
	 * @return string
	 */
	function appica_do_shortcode( $content, $autop = false ) {
		if ( function_exists( 'wpb_js_remove_wpautop' ) ) {
			return wpb_js_remove_wpautop( $content, $autop );
		}

		if ( $autop ) {
			$content = wpautop( preg_replace( '/<\/?p\>/', "\n", $content ) . "\n" );
		}

		return do_shortcode( shortcode_unautop( $content ) );
	}
endif;

if ( ! function_exists( 'appica_hex2rgba' ) ) :
	/**
	 * Convert HEX to RGB(A)
	 *
	 * @param string      $hex   Hexadecimal string
	 * @param string|bool $alpha Alpha
	 *
	 * @return string rgba()
	 */
	function appica_hex2rgba( $hex, $alpha = '0.3' ) {
		$hex = ltrim( $hex, '#' );
		$rgba = array();

		// If hex has only three octets - split string and repeat each symbol twice
		if ( 3 === strlen( $hex ) ) {
			foreach ( str_split( $hex ) as $_hex ) {
				$rgba[] = hexdec( str_repeat( $_hex, 1 ) );
			}
		} else {
			$rgba = array_map( 'hexdec', str_split( $hex, 2 ) );
		}

		if ( false !== $alpha ) {
			$rgba[] = (float) $alpha;
		}

		$rgba = implode( ', ', $rgba );

		return "rgba({$rgba})";
	}
endif;

if ( ! function_exists( 'appica_image_uri' ) ) :
	/**
	 * Returns the URI for provided image. This function is designed primarily for images,
	 * but it is allowable for any other assets type you need: images, fonts, js, css etc.
	 *
	 * @param string $asset Relative path to image, e.g. "image/logo.png"
	 * @param bool   $echo  Echoed or not the value. Default is TRUE.
	 *
	 * @return string
	 */
	function appica_image_uri( $asset, $echo = true ) {
		$asset = ltrim( $asset, '/' );
		$uri   = trailingslashit( get_template_directory_uri() ) . $asset;

		if ( $echo ) {
			echo esc_url( $uri );
		}

		return $uri;
	}
endif;
