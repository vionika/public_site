<?php

/**
 * Appica shortcode
 *
 * @since      2.0.0 New shortcodes loader
 *
 * @author     8guild
 * @package    Appica\Core\Shortcodes
 */
class Appica_Shortcodes {
	/**
	 * Instance of class
	 *
	 * @var null|Appica_Shortcodes
	 */
	private static $instance;

	/**
	 * List of shortcodes
	 *
	 * @var array
	 */
	private $shortcodes = array();

	/**
	 * Initialization
	 *
	 * @param array $files A list of file names and path
	 *                     to shortcode output template
	 *
	 * @return Appica_Shortcodes
	 */
	public static function init( $files ) {
		if ( null === self::$instance ) {
			self::$instance = new self( $files );
		}

		return self::$instance;
	}

	private function __construct( $files ) {
		$this->shortcodes = array_keys( $files );

		/**
		 * Filter the shortcodes list. The best place to add or remove shortcode(s).
		 *
		 * @param array $shortcodes Shortcodes list
		 */
		$this->shortcodes = apply_filters( 'appica_shortcodes', $this->shortcodes );

		// register shortcodes
		foreach( $this->shortcodes as $shortcode ) {
			add_shortcode( $shortcode, array( $this, 'render' ) );
		}
	}

	/**
	 * Get shortcode output
	 *
	 * @param array       $atts      Shortcode attributes
	 * @param string|null $content   Shortcode content
	 * @param string      $shortcode Shortcode tag
	 *
	 * @return string Shortcode HTML
	 */
	public function render( $atts = array(), $content = null, $shortcode = '' ) {
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			$time_start = microtime( true );
		}

		/**
		 * Shortcode output HTML
		 *
		 * @var string $html
		 */
		$html = '';

		/**
		 * Absolute path to default shortcodes templates directory
		 *
		 * @var string $default
		 */
		$default = wp_normalize_path( APPICA_CORE_ROOT . '/shortcodes' );

		/**
		 * Filter the absolute path to shortcodes folder.
		 *
		 * Allow to override any shortcode templates.
		 *
		 * @param string $default Default absolute path to shortcode templates folder
		 */
		$path = apply_filters( 'appica_shortcodes_templates_path', $default );
		$path = wp_normalize_path( $path );

		// finally, path to shortcode template
		$template = $path . DIRECTORY_SEPARATOR . $shortcode . '.php';

		// if user template doesn't exists - use default one
		if ( ! is_readable( $template ) ) {
			$template = $default . DIRECTORY_SEPARATOR . $shortcode . '.php';
		}

		if ( is_readable( $template ) ) {
			ob_start();
			require $template;
			$html = ob_get_contents();
			ob_end_clean();
		}

		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			$time = sprintf( '%.4f', microtime( true ) - $time_start );
			$exec = "<!-- Render shortcode \"{$shortcode}\" in: {$time} seconds -->";

			return $html . $exec;
		}

		return $html;
	}
}