<?php

/**
 * Private and self-hosted plugins updater
 *
 * @author  8guild
 * @package Guild\Core\Updater
 */
class Guild_Updater {
	/**
	 * API endpoint.
	 *
	 * @see request
	 *
	 * @var string
	 */
	private $endpoint = 'http://api.8guild.com/v1/plugins/';

	/**
	 * Action name for {@see wp_schedule_event}
	 *
	 * @see __construct
	 *
	 * @var string
	 */
	private $cron_hook;

	/**
	 * Period name for {@see wp_schedule_event}, determines how often
	 * API will be checked for updates. Default is "daily".
	 *
	 * @see __construct
	 * @see custom_period
	 *
	 * @var string
	 */
	private $cron_period = 'daily';

	/**
	 * Transient name
	 *
	 * @see get_plugin_info()
	 * @see set_plugin_info()
	 * @see delete_plugin_info()
	 *
	 * @var string
	 */
	private $transient_name;

	/**
	 * Plugin metadata, also contains plugin_basename, path to file, etc
	 * All lowercase.
	 *
	 * @var array
	 */
	private $plugin;

	/**
	 * Constructor
	 *
	 * @param array $settings Updater settings
	 */
	public function __construct( $settings ) {
		if ( empty( $settings ) ) {
			return;
		}

		foreach ( $settings as $option => $value ) {
			$this->$option = $value;
		}

		// Only fo local development
		// add_filter( 'cron_schedules', array( $this, 'custom_period' ) );

		// Add cron job
		if ( ! defined( 'WP_INSTALLING' ) && false === wp_next_scheduled( $this->cron_hook ) ) {
			wp_schedule_event( time(), $this->cron_period, $this->cron_hook );
		}
		add_action( $this->cron_hook, array( $this, 'request' ) );

		// at once register the deactivation hook
		register_deactivation_hook( $this->plugin['file'], array( $this, 'deactivation' ) );

		// Modify plugin info popup, inject plugin info into the popup
		add_filter( 'plugins_api', array( $this, 'inject_info' ), 20, 3 );

		// Inject update info to update list, maintained by WP
		add_filter( 'site_transient_update_plugins', array( $this, 'inject_update' ) );

		// Remove cached plugin info when WP clears the update cache.
		add_action( 'delete_site_transient_update_plugins', array( $this, 'delete_cache' ) );
	}

	/**
	 * Make a request to API {@see $endpoint} and save update info, if update is ready.
	 *
	 * @return mixed
	 */
	public function request() {
		$slug     = ltrim( $this->plugin['slug'], '/' );
		$endpoint = untrailingslashit( $this->endpoint );
		$url      = "{$endpoint}/{$slug}";
		$args     = array(
			'httpversion' => '1.1',
			'decompress'  => false,
			'headers'     => array(
				'Accept' => 'application/updater.1.0.0+json'
			)
		);

		$response = wp_remote_get( $url, $args );
		if ( is_wp_error( $response ) ) {
			return null;
		}

		$code = wp_remote_retrieve_response_code( $response );
		if ( 200 !== (int) $code ) {
			return null;
		}

		$body = json_decode( wp_remote_retrieve_body( $response ), true );
		// check, if update required
		if ( array_key_exists( 'version', $body )
		     && version_compare( $body['version'], $this->plugin['version'], '>' )
		) {
			$this->set_plugin_info( $body );
		}

		return $body;
	}

	/**
	 * Callback for "plugins_api" filter.
	 *
	 * Fires, when open the popup with new plugin version details.
	 *
	 * @param bool|object   $result The result object. Default false.
	 * @param null|string   $action The type of information being requested from the Plugin Install API.
	 * @param null|stdClass $args   Plugin API arguments.
	 *
	 * @return stdClass
	 */
	public function inject_info( $result, $action = null, $args = null ) {
		if ( 'plugin_information' !== $action
		     || ! isset( $args->slug )
		     || $this->plugin['slug'] !== $args->slug
		) {
			return $result;
		}

		$info = $this->get_plugin_info( 'info' );
		if ( null === $info ) {
			return $result;
		}

		return $info;
	}

	/**
	 * Callback for "site_transient_update_plugins".
	 *
	 * Show the info about available updates and used for downloading updates.
	 *
	 * @see wp-admin/update.php :: 64
	 * @see wp-admin/include/class-wp-upgrader.php :: 781
	 *
	 * @param stdClass $updates List of updates
	 *
	 * @return stdClass
	 */
	public function inject_update( $updates ) {
		$plugin_info = $this->get_plugin_info( 'update' );
		if ( null === $plugin_info ) {
			return $updates;
		}

		$updates->response[ $this->plugin['basename'] ] = $plugin_info;

		return $updates;
	}

	/**
	 * Remove cached plugin info when WP clears the update cache.
	 *
	 * @return bool
	 */
	public function delete_cache() {
		$this->delete_plugin_info();

		return true;
	}

	/**
	 * Returns the plugin info, if exists
	 *
	 * @param string $for Available params: update | info
	 *
	 * @uses Transient API
	 *
	 * @return null|stdClass
	 */
	private function get_plugin_info( $for = 'update' ) {
		// Plugin info from API response
		$plugin_info = get_site_transient( $this->transient_name );
		if ( false === (bool) $plugin_info ) {
			return null;
		}

		switch ( $for ) {
			case 'update':
				$plugin_info = $this->parse_for_update( $plugin_info );
				break;

			case 'info':
				$plugin_info = $this->parse_for_info( $plugin_info );
				break;
		}

		return $plugin_info;
	}

	/**
	 * Save plugin info into cache
	 *
	 * @param array $info Plugin info, received from API
	 *
	 * @uses Transient API
	 */
	private function set_plugin_info( $info ) {
		$this->delete_plugin_info();
		set_site_transient( $this->transient_name, $info, DAY_IN_SECONDS );
	}

	/**
	 * Remove cached plugin info
	 *
	 * @uses Transient API
	 */
	private function delete_plugin_info() {
		delete_site_transient( $this->transient_name );
	}

	/**
	 * Parse response and return info for
	 * "site_transient_update_plugins" hook callback
	 *
	 * @param array $info Response array
	 *
	 * @return object|null
	 */
	private function parse_for_update( $info ) {
		if ( false === array_key_exists( 'version', $info )
		     || false === array_key_exists( 'download_link', $info )
		) {
			return null;
		}

		$defaults = array(
			'id'          => 0,
			'slug'        => $this->plugin['slug'],
			'plugin'      => $this->plugin['basename'],
			'new_version' => '', // from "version"
			'url'         => '', // from "homepage"
			'package'     => '' // from "download_link"
		);

		$parsed = array(
			'new_version' => $info['version'],
			'url'         => $info['homepage'],
			'package'     => $info['download_link'],
		);

		$parsed = wp_parse_args( $parsed, $defaults );

		return (object) $parsed;
	}

	/**
	 * Parse response and return info for "plugins_api"
	 * filter callback
	 *
	 * TODO: add defaults arguments
	 *
	 * @param array $info Response array
	 *
	 * @return stdClass
	 */
	private function parse_for_info( $info ) {
		$defaults = array();

		return (object) $info;
	}

	/**
	 * Clear the scheduled event
	 */
	public function deactivation() {
		wp_clear_scheduled_hook( $this->cron_hook );
	}

	/**
	 * Add custom scheduled period. Only for local development.
	 *
	 * @param array $schedules Schedules
	 *
	 * @return array
	 */
	public function custom_period( $schedules ) {
		$schedules[ $this->cron_period ] = array(
			'interval' => 60,
			'display'  => 'Once Minutely'
		);

		return $schedules;
	}
}