<?php

/**
 * Abstract class each CPT should inherit
 *
 * @author  8guild
 * @package Appica\Core\CPT
 */
abstract class Appica_Cpt {
	/**
	 * Add actions and filter for new CPT.
	 *
	 * You should add actions and filters in this method.
	 *
	 * This method called during the "plugins_loaded" hook.
	 *
	 * @see appica_core_cpt
	 */
	abstract public function init();

	/**
	 * Register post type (and/or) custom taxonomy
	 *
	 * You should call register_post_type() or register_taxonomy() in this method.
	 *
	 * This method called during the "init" hook to register the post type or/and
	 * taxonomy, which should be added inside init() method and during the plugin
	 * activation to flush the rewrite rules.
	 *
	 * @see  register_post_type
	 * @see  register_taxonomy
	 * @see  Appica_CPT::init
	 * @see  appica_activation
	 *
	 * @link https://codex.wordpress.org/Function_Reference/flush_rewrite_rules
	 */
	abstract public function register();
}