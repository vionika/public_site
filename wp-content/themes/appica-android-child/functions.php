<?php
/**
 * Appica 2 Android Child
 *
 * @since   1.0.0
 * @author  8guild
 * @package Appica
 */

/**
 * Enqueue parent and child styles
 */
function appica_child_enqueue_styles() {
	wp_enqueue_style( 'appica-parent', get_template_directory_uri() . '/style.css' );
	wp_enqueue_style( 'appica-child', get_stylesheet_directory_uri() . '/style.css', array( 'appica-parent' ) );
}

add_action( 'wp_enqueue_scripts', 'appica_child_enqueue_styles' );