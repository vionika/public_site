// Custom Contact Forms Script //
jQuery(window).load( function() {

	// Contact Forms 7 extra data
	jQuery('.contact-form-7-data').each( function() {

		var $div = jQuery(this);
		var token = $div.data('token');
		var settings = window['vsc_custom_contact_form_7_' + token];
		/*
		settings = {
			_wpcf7_vsc_use_mailchimp: [0|1],
			_wpcf7_vsc_mailchimp_list_id: ...,
			_wpcf7_vsc_double_opt: [0|1],
			_wpcf7_vsc_redirect_after_send: [0|1],
			_wpcf7_vsc_redirect_url: ...,
		}
		*/

		var $form = $div.find('form');
		if (settings._wpcf7_vsc_use_mailchimp) {
			$form.append( '<input type="hidden" name="_wpcf7_vsc_use_mailchimp" value="true" />' );
			$form.append( '<input type="hidden" name="_wpcf7_vsc_mailchimp_list_id" value="'+ settings._wpcf7_vsc_mailchimp_list_id + '" />' );
			$form.append( '<input type="hidden" name="_wpcf7_vsc_double_opt" value="'+ settings._wpcf7_vsc_double_opt + '" />' );
		}

		if (settings._wpcf7_vsc_redirect_after_send) {
			$form.append( '<input type="hidden" name="_wpcf7_vsc_redirect_after_send" value="true" />' );
			$form.append( '<input type="hidden" name="_wpcf7_vsc_redirect_url" value="'+ settings._wpcf7_vsc_redirect_url + '" />' );
		}		
		
		if (settings._wpcf7_vsc_hide_after_send) {
			$form.append( '<input type="hidden" name="_wpcf7_vsc_hide_after_send" value="true" />' );
		}
	});
});
