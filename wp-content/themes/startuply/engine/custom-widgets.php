<?php

/**
 *
 * Startuply WP vustom widgets
 *
 * @author Vivaco
 * @license Commercial License
 * @link http://startuplywp.com
 * @copyright 2014 Vivaco
 * @package Startuply
 * @version 2.2.0
 *
 */

/** Recent posts widget **/
class VSC_Widget_Recent_Posts extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_entries', 'description' => __( "Your site&#8217;s most recent Posts.", "vivaco") );
		parent::__construct('recent-posts_sply_widget', __('Recent Posts', 'vivaco'), $widget_ops);
		$this->alt_option_name = 'widget_recent_entries';

		add_action( 'save_post', array($this, 'flush_widget_cache') );
		add_action( 'deleted_post', array($this, 'flush_widget_cache') );
		add_action( 'switch_theme', array($this, 'flush_widget_cache') );
	}

	function widget($args, $instance) {
		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get( 'widget_recent_posts', 'widget' );
		}

		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		ob_start();
		extract($args);

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Posts' , 'vivaco' );

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;
		$show_date = isset( $instance['show_date'] ) ? $instance['show_date'] : false;

		/**
		 * Filter the arguments for the Recent Posts widget.
		 *
		 * @since 3.4.0
		 *
		 * @see WP_Query::get_posts()
		 *
		 * @param array $args An array of arguments used to retrieve the recent posts.
		 */
		$r = new WP_Query( apply_filters( 'widget_posts_args', array(
			'posts_per_page'      => $number,
			'no_found_rows'       => true,
			'post_status'         => 'publish',
			'ignore_sticky_posts' => true
		) ) );

		if ($r->have_posts()) :
?>
		<?php echo $before_widget; ?>
		<?php if ( $title ) echo $before_title . $title . $after_title; ?>
		<ul>
		<?php while ( $r->have_posts() ) : $r->the_post(); ?>
		<?php

		$comments_count = wp_count_comments(get_the_ID());
		$total_comments = $comments_count->approved;

		$post_title = get_the_title() ? get_the_title() : get_the_ID();
		if (strlen($post_title) > 25) {$post_title = substr($post_title, 0, 35)."...";}

		?>
			<li>
				<div class="info">

					<span class="wrap avatar rounded">
						 <?php echo get_avatar( get_the_author_meta('email') , 90 ); ?>
					</span>

					<span class="wrap title">
						<a href="<?php the_permalink(); ?>"><?php echo $post_title ?></a>
					</span>

					<span class="wrap date">
						<?php //if ( $show_date ) : ?>
						<span class="post-date"><?php echo get_the_date(); ?></span>
						<?php //endif; ?>
					</span>


					<!-- <span class="wrap comments">
						<i class="fa fa-comment-o"></i>
						<?php if ( $total_comments > 0 ) : ?>
						<a href="<?php  the_permalink(); ?>#comments">
							<span class="postid"><?php echo $comments_count->approved; ?></span>
						</a>
						<?php else : ?>
						<a><span class="postid"> 0 </span></a>
						<?php endif; // end have_comments() ?>
					</span> -->
				</div>
			</li>
		<?php endwhile; ?>
		</ul>
		<?php echo $after_widget; ?>
<?php
		// Reset the global $the_post as this query will have stomped on it
		wp_reset_postdata();

		endif;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = ob_get_flush();
			wp_cache_set( 'widget_recent_posts', $cache, 'widget' );
		} else {
			ob_end_flush();
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = (int) $new_instance['number'];
		$instance['show_date'] = isset( $new_instance['show_date'] ) ? (bool) $new_instance['show_date'] : false;
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_entries']) )
			delete_option('widget_recent_entries');

		return $instance;
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_posts', 'widget');
	}

	function form( $instance ) {
		$title     = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number    = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
		$show_date = isset( $instance['show_date'] ) ? (bool) $instance['show_date'] : false;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vivaco' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of posts to show:', 'vivaco' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>

		<p><input class="checkbox" type="checkbox" <?php checked( $show_date ); ?> id="<?php echo $this->get_field_id( 'show_date' ); ?>" name="<?php echo $this->get_field_name( 'show_date' ); ?>" />
		<label for="<?php echo $this->get_field_id( 'show_date' ); ?>"><?php _e( 'Display post date?', 'vivaco' ); ?></label></p>
<?php
	}
}
/** end **/


/** Recent comments widget **/
class VSC_Widget_Recent_Comments extends WP_Widget {

	function __construct() {
		$widget_ops = array('classname' => 'widget_recent_comments', 'description' => __( 'Your site&#8217;s most recent comments.' , 'vivaco' ) );
		parent::__construct('recent-comments_sply_widget', __('Recent Comments' , 'vivaco'), $widget_ops);
		$this->alt_option_name = 'widget_recent_comments';

		if ( is_active_widget(false, false, $this->id_base) )
			add_action( 'wp_head', array($this, 'recent_comments_style') );

		add_action( 'comment_post', array($this, 'flush_widget_cache') );
		add_action( 'edit_comment', array($this, 'flush_widget_cache') );
		add_action( 'transition_comment_status', array($this, 'flush_widget_cache') );
	}

	function recent_comments_style() {

		/**
		 * Filter the Recent Comments default widget styles.
		 *
		 * @since 3.1.0
		 *
		 * @param bool   $active  Whether the widget is active. Default true.
		 * @param string $id_base The widget ID.
		 */
		if ( ! current_theme_supports( 'widgets' ) // Temp hack #14876
			|| ! apply_filters( 'show_recent_comments_widget_style', true, $this->id_base ) )
			return;
		?>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
<?php
	}

	function flush_widget_cache() {
		wp_cache_delete('widget_recent_comments', 'widget');
	}

	function widget( $args, $instance ) {
		global $comments, $comment;

		$cache = array();
		if ( ! $this->is_preview() ) {
			$cache = wp_cache_get('widget_recent_comments', 'widget');
		}
		if ( ! is_array( $cache ) ) {
			$cache = array();
		}

		if ( ! isset( $args['widget_id'] ) )
			$args['widget_id'] = $this->id;

		if ( isset( $cache[ $args['widget_id'] ] ) ) {
			echo $cache[ $args['widget_id'] ];
			return;
		}

		extract($args, EXTR_SKIP);
		$output = '';

		$title = ( ! empty( $instance['title'] ) ) ? $instance['title'] : __( 'Recent Comments' , 'vivaco');

		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		$number = ( ! empty( $instance['number'] ) ) ? absint( $instance['number'] ) : 5;
		if ( ! $number )
			$number = 5;

		/**
		 * Filter the arguments for the Recent Comments widget.
		 *
		 * @since 3.4.0
		 *
		 * @see get_comments()
		 *
		 * @param array $comment_args An array of arguments used to retrieve the recent comments.
		 */
		$comments = get_comments( apply_filters( 'widget_comments_args', array(
			'number'      => $number,
			'status'      => 'approve',
			'post_status' => 'publish'
		) ) );

		$output .= $before_widget;
		if ( $title )
			$output .= $before_title . $title . $after_title;

		$output .= '<ul id="recentcomments">';
		if ( $comments ) {
			// var_dump($comments[0]);
			// Prime cache for associated posts. (Prime post term cache if we need it for permalinks.)
			$post_ids = array_unique( wp_list_pluck( $comments, 'comment_post_ID' ) );
			_prime_post_caches( $post_ids, strpos( get_option( 'permalink_structure' ), '%category%' ), false );

			foreach ( (array) $comments as $comment) {
				//$output .=  '<li class="recentcomments">' . /* translators: comments widget: 1: comment author, 2: post link */ sprintf(_x('%1$s on %2$s', 'widgets'), get_comment_author_link(), '<a href="' . esc_url( get_comment_link($comment->comment_ID) ) . '">' . get_the_title($comment->comment_post_ID) . '</a>') . '</li>';

					$output .='
						<li>
							<div class="info">

								<i class="fa fa-comment-o"></i><span class="author">' . get_comment_author_link() . '</span> on

								<span class="comment-body"><a href=" ' . esc_url( get_comment_link($comment->comment_ID) ) . ' "> ' . substr(($comment->post_title), 0, 30) . '...</a></span>

							</div>
						</li>
					';

			}
		}
		$output .= '</ul>';
		$output .= $after_widget;

		echo $output;

		if ( ! $this->is_preview() ) {
			$cache[ $args['widget_id'] ] = $output;
			wp_cache_set( 'widget_recent_comments', $cache, 'widget' );
		}
	}

	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['number'] = absint( $new_instance['number'] );
		$this->flush_widget_cache();

		$alloptions = wp_cache_get( 'alloptions', 'options' );
		if ( isset($alloptions['widget_recent_comments']) )
			delete_option('widget_recent_comments');

		return $instance;
	}

	function form( $instance ) {
		$title  = isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$number = isset( $instance['number'] ) ? absint( $instance['number'] ) : 5;
?>
		<p><label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vivaco' ); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo $title; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id( 'number' ); ?>"><?php _e( 'Number of comments to show:', 'vivaco' ); ?></label>
		<input id="<?php echo $this->get_field_id( 'number' ); ?>" name="<?php echo $this->get_field_name( 'number' ); ?>" type="text" value="<?php echo $number; ?>" size="3" /></p>
<?php
	}
}

/* end */

class VSC_Widget_Contacts extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'widget_contacts', 'description' => __( 'Your site&#8217;s contacts info.' , 'vivaco' ) );

		parent::__construct(
			'contacts_sply_widget', // Base ID
			__('Contacts' , 'vivaco'),
			$widget_ops // Args
		);

		$this->alt_option_name = 'widget_contacts';
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		$title = $instance['title'];
		$our_email = $instance['our_email'];
		$our_address = $instance['our_address'];
		$our_telephone = $instance['our_telephone'];

		# Output
		//WMPL
		/**
		 * retreive translations
		 */
		if (function_exists ( 'icl_translate' )){
			$title = icl_translate('Widgets', 'Startuply Contacts Widget - Title', $instance['title']);
			$our_email = icl_translate('Widgets', 'Startuply Contacts Widget - Email', $instance['our_email']);
			$our_address = icl_translate('Widgets', 'Startuply Contacts Widget - Address', $instance['our_address']);
			$our_telephone = icl_translate('Widgets', 'Startuply Contacts Widget - Telephone', $instance['our_telephone']);
		}
		//\WMPL
		$title = apply_filters( 'widget_title', $title );

		echo $before_widget;
		if ( ! empty( $title ) )
			echo '<div class="footer-title">'.$before_title . $title . $after_title.'</div>';
		?>
		<ul class="list-unstyled">
			<?php if ($our_email && strlen(trim($our_email)) > 0 ) { ?>
			<li>
				<span class="icon icon-chat-messages-14"></span>
				<a href="mailto:<?php echo $our_email; ?>"><?php echo $our_email; ?></a>
			</li>
			<?php } ?>
			<?php if ($our_address && strlen(trim($our_address)) > 0 ) { ?>
			<li>
				<span class="icon icon-seo-icons-34"></span>
				<?php echo $our_address; ?>
			</li>
			<?php } ?>
			<?php if ($our_telephone && strlen(trim($our_telephone)) > 0 ) { ?>
			<li>
				<span class="icon icon-seo-icons-17"></span>
				<?php echo $our_telephone; ?>
			</li>
			<?php } ?>
		</ul>
		<?php
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		$instance['our_email'] = strip_tags( $new_instance['our_email'] );
		if ( current_user_can('unfiltered_html') ) {
			$instance['our_address'] = $new_instance['our_address'];
		}
		else {
			$instance['our_address'] = strip_tags($new_instance['our_address']);
		}

		$instance['our_telephone'] = strip_tags( $new_instance['our_telephone'] );


		//WMPL
		/**
		 * register strings for translation
		 */
		if (function_exists ( 'icl_register_string' )){
			icl_register_string('Widgets', 'Startuply Contacts Widget - Title', $instance['title']);
			icl_register_string('Widgets', 'Startuply Contacts Widget - Email', $instance['our_email']);
			icl_register_string('Widgets', 'Startuply Contacts Widget - Address', $instance['our_address']);
			icl_register_string('Widgets', 'Startuply Contacts Widget - Telephone', $instance['our_telephone']);
		}
		//\WMPL

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args(
			(array)$instance,
			array(
				'title' => __( 'Our Contacts', 'vivaco' ),
				'our_email' => 'office@vivaco.com',
				'our_address' => '2901 Marmora road, Glassgow,<br> Seattle, WA 98122-1090',
				'our_telephone' => '+9 500 750',
			)
		);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $instance['title'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'our_email' ); ?>"><?php _e( 'Email:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'our_email' ); ?>" name="<?php echo $this->get_field_name( 'our_email' ); ?>" type="text" value="<?php echo esc_attr( $instance['our_email'] ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'our_address' ); ?>"><?php _e( 'Address:', 'vivaco' ); ?></label>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id( 'our_address' ); ?>" name="<?php echo $this->get_field_name( 'our_address' ); ?>"><?php echo esc_textarea( $instance['our_address'] ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'our_telephone' ); ?>"><?php _e( 'Telephone:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'our_telephone' ); ?>" name="<?php echo $this->get_field_name( 'our_telephone' ); ?>" type="text" value="<?php echo esc_attr( $instance['our_telephone'] ); ?>" />
		</p>

		<?php
	}

} // class VSC_Widget_Contacts

class VSC_Widget_Socials extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'widget_socials', 'description' => __( 'Your site&#8217;s socials info.' , 'vivaco' ) );

		parent::__construct(
			'socials_sply_widget', // Base ID
			__('Socials' , 'vivaco'),
			$widget_ops // Args
		);

		$this->alt_option_name = 'widget_socials';
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );
		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;
		if ( ! empty( $title ) )
			echo '<div class="footer-title">'.$before_title . $title . $after_title.'</div>';
		?>

		<ul class="list-inline socials">
			<?php
				$socials['fb'] = array('val' => $instance['fb_opt'], 'ico' => 'icon-socialmedia-08');
				$socials['twitter'] = array('val' => $instance['twitter_opt'], 'ico' => 'icon-socialmedia-07');
				$socials['gplus'] = array('val' => $instance['google_opt'], 'ico' => 'icon-socialmedia-16');
				$socials['linkedin'] = array('val' => $instance['linkedin_opt'], 'ico' => 'icon-socialmedia-05');
				$socials['instagram'] = array('val' => $instance['instagram_opt'], 'ico' => 'icon-socialmedia-26');
				$socials['skype'] = array('val' => $instance['skype_opt'], 'ico' => 'icon-socialmedia-09');
				$socials['pinterest'] = array('val' => $instance['pinterest_opt'], 'ico' => 'icon-socialmedia-04');
				$socials['youtube'] = array('val' => $instance['youtube_opt'], 'ico' => 'icon-socialmedia-29');
				$socials['soundcloud'] = array('val' => $instance['soundcloud_opt'], 'ico' => 'icon-socialmedia-10');
				$socials['rss'] = array('val' => $instance['rss_opt'], 'ico' => 'icon-socialmedia-20');

				foreach ($socials as $k => $v) {
					if ($v['val'] != '') {
						if ( $k === 'skype')
							$v['val'] = 'skype:'.$v['val'];
						?>
						<li><a href="<?php echo $v['val']; ?>" target="_blank"><span class="icon <?php echo $v['ico']; ?>"></span></a></li>
				<?php }
				} ?>
		</ul>
		<?php
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );

		if ( current_user_can('unfiltered_html') ) {
			$instance['fb_opt'] = $new_instance['fb_opt'];
			$instance['twitter_opt'] = $new_instance['twitter_opt'];
			$instance['google_opt'] = $new_instance['google_opt'];
			$instance['linkedin_opt'] = $new_instance['linkedin_opt'];
			$instance['instagram_opt'] = $new_instance['instagram_opt'];
			$instance['skype_opt'] = $new_instance['skype_opt'];
			$instance['pinterest_opt'] = $new_instance['pinterest_opt'];
			$instance['youtube_opt'] = $new_instance['youtube_opt'];
			$instance['soundcloud_opt'] = $new_instance['soundcloud_opt'];
			$instance['rss_opt'] = $new_instance['rss_opt'];
		} else {
			$instance['fb_opt'] = strip_tags( $new_instance['fb_opt'] );
			$instance['twitter_opt'] = strip_tags( $new_instance['twitter_opt'] );
			$instance['google_opt'] = strip_tags( $new_instance['google_opt'] );
			$instance['linkedin_opt'] = strip_tags( $new_instance['linkedin_opt'] );
			$instance['instagram_opt'] = strip_tags( $new_instance['instagram_opt'] );
			$instance['skype_opt'] = strip_tags( $new_instance['skype_opt'] );
			$instance['pinterest_opt'] = strip_tags( $new_instance['pinterest_opt'] );
			$instance['youtube_opt'] = strip_tags( $new_instance['youtube_opt'] );
			$instance['soundcloud_opt'] = strip_tags( $new_instance['soundcloud_opt'] );
			$instance['rss_opt'] = strip_tags( $new_instance['rss_opt'] );
		}

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args(
			(array)$instance,
			array(
				'title' => __( 'Social Networks', 'vivaco' ),
				'fb_opt' => 'http://your-social-link-here.com',
				'twitter_opt' => 'http://your-social-link-here.com',
				'google_opt' => 'http://your-social-link-here.com',
				'linkedin_opt' => 'http://your-social-link-here.com',
				'instagram_opt' => 'http://your-social-link-here.com',
				'skype_opt' => 'your-skype-here',
				'pinterest_opt' => 'http://your-social-link-here.com',
				'youtube_opt' => 'http://your-social-link-here.com',
				'soundcloud_opt' => '',
				'rss_opt' => '',
			)
		);

		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'Social Networks', 'vivaco' );
		}
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'fb_opt' ); ?>"><?php _e( 'Facebook:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'fb_opt' ); ?>" name="<?php echo $this->get_field_name( 'fb_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['fb_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'twitter_opt' ); ?>"><?php _e( 'Twitter:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'twitter_opt' ); ?>" name="<?php echo $this->get_field_name( 'twitter_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['twitter_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'google_opt' ); ?>"><?php _e( 'Google+:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'google_opt' ); ?>" name="<?php echo $this->get_field_name( 'google_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['google_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'linkedin_opt' ); ?>"><?php _e( 'LinkedIn:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'linkedin_opt' ); ?>" name="<?php echo $this->get_field_name( 'linkedin_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['linkedin_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'instagram_opt' ); ?>"><?php _e( 'Instagram:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'instagram_opt' ); ?>" name="<?php echo $this->get_field_name( 'instagram_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['instagram_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'skype_opt' ); ?>"><?php _e( 'Skype:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'skype_opt' ); ?>" name="<?php echo $this->get_field_name( 'skype_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['skype_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'pinterest_opt' ); ?>"><?php _e( 'Pinterest:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'pinterest_opt' ); ?>" name="<?php echo $this->get_field_name( 'pinterest_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['pinterest_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'youtube_opt' ); ?>"><?php _e( 'YouTube:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'youtube_opt' ); ?>" name="<?php echo $this->get_field_name( 'youtube_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['youtube_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'soundcloud_opt' ); ?>"><?php _e( 'SoundCloud:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'soundcloud_opt' ); ?>" name="<?php echo $this->get_field_name( 'soundcloud_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['soundcloud_opt'] ); ?>" />
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'rss_opt' ); ?>"><?php _e( 'RSS:', 'vivaco'); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'rss_opt' ); ?>" name="<?php echo $this->get_field_name( 'rss_opt' ); ?>" type="text" value="<?php echo esc_attr( $instance['rss_opt'] ); ?>" />
		</p>


		<?php
	}

} // class VSC_Widget_Socials

class VSC_Widget_About extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'widget_about', 'description' => __( 'Your site&#8217;s about info.' , 'vivaco' ) );

		parent::__construct(
			'about_sply_widget', // Base ID
			__('About' , 'vivaco'),
			$widget_ops // Args
		);

		$this->alt_option_name = 'widget_about';

		add_action('admin_enqueue_scripts', array($this, 'upload_scripts'));
		add_action('admin_enqueue_styles', array($this, 'upload_styles'));
	}

	/**
	 * Upload the Javascripts for the media uploader
	 */
	public function upload_scripts() {
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_enqueue_script('upload_media_widget', get_template_directory_uri().'/js/upload-media.js', array('jquery'));
	}

	/**
	 * Add the styles for the upload media box
	 */
	public function upload_styles() {
		wp_enqueue_style('thickbox');
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		extract( $args );

		$title = $instance['title'];
		$description = $instance['description'];
		$author = $instance['author'];

		# Output
		//WMPL
		/**
		 * retreive translations
		 */
		if (function_exists ( 'icl_translate' )){
			$title = icl_translate('Widgets', 'Startuply About Widget - Title', $instance['title']);
			$description = icl_translate('Widgets', 'Startuply About Widget - Description', $instance['description']);
			$author = icl_translate('Widgets', 'Startuply About Widget - Author', $instance['author']);
		}

		$title = apply_filters( 'widget_title', $instance['title'] );

		echo $before_widget;

		if ( !empty( $title ) ) {
			echo '<div class="footer-title">'.$before_title . $title . $after_title.'</div>';
		}

		$logo_img = $logo_retina_img = '';
		$options = startuply_get_all_option();

		if ( !empty( $instance['bg_image'] ) ) {
			$logo_img = '<img src="' . $instance['bg_image'] . '" alt="">';

			if ( !empty( $instance['retina_bg_image'] ) ) {
				$logo_retina_img = '<img class="retina" src="' . $instance['retina_bg_image'] . '" alt="">';
			} else {
				$logo_retina_img = '<img class="retina" src="' . $instance['bg_image'] . '" alt="">';
			}
		} else if (!empty($options['site_logo'])) {
			$logo_img = '<img src="' . $options['site_logo'] . '" alt="">';

			// Startuply retina logo
			if (!empty($options['retina_site_logo'])) {
				$logo_retina_img = '<img class="retina" src="' . $options['retina_site_logo'] . '" alt="">';
			} else {
				$logo_retina_img = '<img class="retina" src="' . $options['site_logo'] . '" alt="">';
			}

		} else {
			$logo_img = '<img src="' . get_template_directory_uri() . '/images/logo.png" alt="">';
			$logo_retina_img = '<img class="retina" src="' . get_template_directory_uri() . '/images/logo-retina.png" alt="">';

		}

		?>

			<div class="logo-wrapper">
				<div class="brand-logo">
					<a href="/" class="logo">
						<?php echo $logo_img; ?>
						<?php echo $logo_retina_img; ?>
					</a>
				</div>
			</div>
			<p>
				<?php echo $description; ?>
			<br>
				<strong><?php echo $author; ?></strong>
			</p>

		<?php
		echo $after_widget;
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = strip_tags( $new_instance['title'] );
		if ( current_user_can('unfiltered_html') ) {
			$instance['description'] = $new_instance['description'];
		} else {
			$instance['description'] = strip_tags( $new_instance['description'] );
		}

		$instance['author'] = strip_tags( $new_instance['author'] );
		$instance['bg_image'] = strip_tags( $new_instance['bg_image'] );
		$instance['retina_bg_image'] = strip_tags( $new_instance['retina_bg_image'] );

        //WMPL
        /**
         * retreive translations
         */
        if (function_exists ( 'icl_translate' )){
			icl_register_string('Widgets', 'Startuply About Widget - Title', $instance['title']);
			icl_register_string('Widgets', 'Startuply About Widget - Description', $instance['description']);
			icl_register_string('Widgets', 'Startuply About Widget - Author', $instance['author']);
        }
        //\WMPL

		return $instance;
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$instance = wp_parse_args(
			(array)$instance,
			array(
				'title' => '',
				'description' => 'Ut enim ad minim veniam, quis nostrud exercitation ullamco. Qui officia deserunt mollit anim id est laborum. Ut enim ad minim veniam, quis nostrud exercitation ullamco.',
				'author' => 'John Doeson, Founder.',
				'bg_image' => '',
				'retina_bg_image' => ''
			)
		);

		$title = $instance[ 'title' ];
		$description = $instance[ 'description' ];
		$author = $instance[ 'author' ];
		$bg_image = $instance[ 'bg_image' ];
		$retina_bg_image = $instance[ 'retina_bg_image' ];

		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_name( 'bg_image' ); ?>"><?php _e( 'Footer logo:', 'vivaco' ); ?></label>
			<input name="<?php echo $this->get_field_name( 'bg_image' ); ?>" id="<?php echo $this->get_field_id( 'bg_image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $bg_image ); ?>" />
			<input class="upload_image_button" type="button" value="Upload Image" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_name( 'retina_bg_image' ); ?>"><?php _e( 'Retina @2x footer logo:', 'vivaco' ); ?></label>
			<input name="<?php echo $this->get_field_name( 'retina_bg_image' ); ?>" id="<?php echo $this->get_field_id( 'retina_bg_image' ); ?>" class="widefat" type="text" size="36"  value="<?php echo esc_url( $retina_bg_image ); ?>" />
			<input class="upload_image_button" type="button" value="Upload Image" />
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'description' ); ?>"><?php _e( 'Description:', 'vivaco' ); ?></label>
			<textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id( 'description' ); ?>" name="<?php echo $this->get_field_name( 'description' ); ?>"><?php echo esc_textarea( $description ); ?></textarea>
		</p>

		<p>
			<label for="<?php echo $this->get_field_id( 'author' ); ?>"><?php _e( 'Author:', 'vivaco' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'author' ); ?>" name="<?php echo $this->get_field_name( 'author' ); ?>" type="text" value="<?php echo esc_attr( $author ); ?>" />
		</p>
		<?php
	}

} // class VSC_Widget_About

class VSC_Widget_CF7 extends WP_Widget {
	/**
	 * Register widget with WordPress.
	 */
	public function __construct() {
		$widget_ops = array('classname' => 'widget_cf7', 'description' => __( "Widget for Contact Form 7", 'vivaco' ) );

		parent::__construct(
			'cf7_sply_widget', // Base ID
			__('Contact Form 7 Widget' , 'vivaco'),
			$widget_ops // Args
		);

		$this->alt_option_name = 'widget_cf7';
	}

	function widget( $args, $instance ) {
		extract($args);
		$title = $instance['title'];

		echo $before_widget;
		if ( $title )
			echo $before_title . $title . $after_title;

		?>
		<div class="container-cf7">
			<?php
				$widget_text = empty($instance['form']) ? '' : stripslashes($instance['form']);
				echo apply_filters('widget_text','[contact-form-7 id="' . $widget_text . '"]');
			?>
		</div>
		<?php

		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		$instance = array();

		$instance['title'] = strip_tags( $new_instance['title'] );
		$instance['form'] = strip_tags( $new_instance['form'] );

		return $instance;
	}

	function form( $instance ) {
		//Defaults
		$instance = wp_parse_args(
			(array) $instance,
			array(
				'title' => false,
				'form' => ''
			)
		);
?>
	<p>
		<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'vivaco') ?></label>
		<input type="text" class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo esc_attr( $instance['title'] ); ?>" />
	</p>

	<p>
		<label for="<?php echo $this->get_field_id('form'); ?>"><?php _e('Form:', 'vivaco') ?></label>
	<?php $cf7posts = new WP_Query( array( 'post_type' => 'wpcf7_contact_form' ));
		if ( $cf7posts->have_posts() ) { ?>
			<select class="widefat" name="<?php echo $this->get_field_name('form'); ?>" id="<?php echo $this->get_field_id('form'); ?>">

		<?php while( $cf7posts->have_posts() ) : $cf7posts->the_post(); ?>
			<option value="<?php the_id(); ?>"<?php selected(get_the_id(), $instance['form']); ?>><?php the_title(); ?></option>

		<?php endwhile;

		} else {?>
			<select class="widefat" disabled>
			<option disabled="disabled">No Forms Found</option> <?php
		} ?>
			</select>
	</p>

	<?php }
} // class VSC_Widget_CF7
