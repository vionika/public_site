<?php

function startuply_get_theme_option_css() {
	$options = startuply_get_all_option();

	ob_start(); // Capture all output (output buffering)

	/* all default avalaible options at this time

	[vivaco_favicon] =>
	[site_logo] =>
	[site_sticky_logo] =>
	[loading_gif_on] =>
	[loading_gif] =>
	[vivaco_base_color] =>
	[responsive_on] =>
	[fullscreen_on] =>
	[smooth_scroll_on] =>
	[smooth_scroll_speed] =>
	[google_analytics] =>
	[menu_bg_light] =>
	[menu_text_color] =>
	[menu_active_color] =>
	[inner_menu_bg_light] =>
	[inner_menu_text_color] =>
	[inner_menu_active_color] =>
	[sticky_menu_display] =>
	[sticky_menu_position] => 600
	[sticky_menu_bg_light] =>
	[sticky_menu_text_color] =>
	[sticky_menu_active_color] =>
	[custom_fonts_h16] =>
	[custom_fonts_content] =>
	[custom_fonts_menu_item] =>
	[custom_css] =>
	[custom_js] =>
	[footer_bg_image] =>
	[footer_bg_color] =>
	[footer_color_opacity] =>
	[footer_bg_position] => center center
	[footer_bg_repeat] => no-repeat
	[footer_bg_size] => auto
	[footer_on] => 1
	[footer_widgets] => 3_widget
	[footer_copy] => StartuplyWP.com © All rights reserved.
	*/

	/* you can set $default value for all options!!! */

	$default = null;

	echo " "; //echo whitespace

	/*
	*
	* START LOADING GIF
	*
	*/
	$options_loading_gif = '';
	$loading_gif_on = isset($options['loading_gif_on']) ? $options['loading_gif_on'] : $default;
	//$loading_gif = isset($options['loading_gif']) ? $options['loading_gif'] : $default;
	if ($loading_gif_on) { $options_loading_gif = '#custom_loader {background-image: none !important;} #mask, #custom_loader { display:none; }'."\r\n"; }
	//if ($loading_gif) { $options_loading_gif = '#custom_loader {background-image: url("' . esc_attr($loading_gif) . '") !important;}'."\r\n"; }
	echo $options_loading_gif;

	/* END LOADING GIF */


	/*
	*
	* START BOXED LAYOUT WIDTH
	*
	*/

	$boxed_on = isset($options['fullscreen_on']) ? $options['fullscreen_on'] : $default;
	$boxed_layout_width = isset($options['boxed_width']) ? $options['boxed_width'] : $default;
	$options_layout = '';


	if ( $boxed_layout_width ) {
		$options_layout = '@media (min-width: 1200px) {'."\r\n".' body .container { max-width: 960px; }'."\r\n".'}'."\r\n";
	}

	if ( $boxed_on && $boxed_layout_width ) {
		$options_layout .= 'html body, body .navigation-header { max-width: 960px; }'."\r\n";
	}

	echo $options_layout;

	/* END BOXED LAYOUT WIDTH  */

	/*
	*
	* START BOXED LAYOUT BACKGROUND
	*
	*/

	$boxed_layout_bg_image = isset($options['boxed_background']) ? $options['boxed_background'] : $default;
	$boxed_layout_bg_position = isset($options['boxed_background_position']) ? $options['boxed_background_position'] : $default;
	$boxed_layout_bg_repeat = isset($options['boxed_background_repeat']) ? $options['boxed_background_repeat'] : $default;
	$boxed_layout_bg_size = isset($options['boxed_background_size']) ? $options['boxed_background_size'] : $default;
	$options_layout_bg = '';

	if ( $boxed_layout_bg_image ) {
		$options_layout_bg = 'html { background-image: url("' . remove_base_url(esc_attr($boxed_layout_bg_image)) . '"); }'."\r\n";

		if ( $boxed_layout_bg_position ) $options_layout_bg .= 'html { background-position: ' . esc_attr($boxed_layout_bg_position) . '; }'."\r\n";
		if ( $boxed_layout_bg_repeat ) $options_layout_bg .= 'html { background-repeat: ' . esc_attr($boxed_layout_bg_repeat) . '; }'."\r\n";
		if ( $boxed_layout_bg_size ) $options_layout_bg .= 'html { background-size: ' . esc_attr($boxed_layout_bg_size) . '; -moz-background-size: ' . esc_attr($boxed_layout_bg_size) . '; -webkit-background-size: ' . esc_attr($boxed_layout_bg_size) . '; }'."\r\n";
	}

	echo $options_layout_bg;

	/* END BOXED LAYOUT BACKGROUND  */


	/*
	*
	* START DISABLE BASE COLOR BORDER ON FIELDS
	*
	*/

	$disable_border_color = isset($options['disable_border_color']) ? $options['disable_border_color'] : false;
	$options_border_color = '';

	if ( $disable_border_color ) {
		$options_border_color = 'select, .gform_wrapper .gform_body select, textarea, input[type=text], input[type=url], input[type=tel], input[type=number], input[type=color], input[type=email], input[type=email], input[type=password],
			textarea:focus, input[type=text]:focus, input[type=url]:focus, input[type=tel]:focus, input[type=number]:focus, input[type=color]:focus, input[type=email]:focus, input[type=email]:focus, input[type=password]:focus,
			textarea:hover, input[type=text]:hover, input[type=url]:hover, input[type=tel]:hover, input[type=number]:hover, input[type=color]:hover, input[type=email]:hover, input[type=password]:hover { border-left-width: 1px; border-left-color: #e7e7e7; }'."\r\n";
	}

	echo $options_border_color;

	/* END DISABLE BASE COLOR BORDER ON FIELDS */


	/*
	*
	* START HOMEPAGE MENU
	*
	*/

	$main_menu_height = isset($options['main_menu_height']) ? $options['main_menu_height'] : $default;
	$main_menu_bg_color = isset($options['main_menu_bg_color']) ? $options['main_menu_bg_color'] : $default;
	$main_menu_bg_image = isset($options['main_menu_bg_image']) ? $options['main_menu_bg_image'] : $default;
	$main_menu_bg_color_rgba = hex2rgb($main_menu_bg_color);
	$main_menu_color_opacity = isset($options['main_menu_color_opacity']) ? $options['main_menu_color_opacity'] : $default;
	$main_menu_color_opacity = $main_menu_color_opacity / 100;
	$main_menu_bg_position = isset($options['main_menu_bg_position']) ? $options['main_menu_bg_position'] : $default;
	$main_menu_bg_repeat = isset($options['main_menu_bg_repeat']) ? $options['main_menu_bg_repeat'] : $default;
	$main_menu_bg_size = isset($options['main_menu_bg_size']) ? $options['main_menu_bg_size'] : $default;
	$main_menu_text_color = isset($options['main_menu_text_color']) ? $options['main_menu_text_color'] : $default;
	$main_menu_active_color = isset($options['main_menu_active_color']) ? $options['main_menu_active_color'] : $default;

	$header_style = '';
	$options_custom_bg_img = '';
	$options_custom_menu_bg = '';
	$options_custom_menu_bg_clr = '';
	$options_custom_menu_clr = '';
	$options_responsive = '@media (max-width: 1024px) {'."\r\n";

	/* Menu background overlay color*/

	if ( $main_menu_height ) {
		$header_style = '.navigation-header:not(.inner-menu):not(.fixmenu-clone) { height: ' . intval($main_menu_height) . 'px; }'."\r\n";
	}

	if( !$main_menu_color_opacity ) $main_menu_color_opacity = 1;

	if( $main_menu_bg_color ) {
		$options_custom_menu_bg_clr = '.navigation-header.main-menu:not(.fixmenu-clone) { background-color: rgba(' . esc_attr($main_menu_bg_color_rgba) . ',' . esc_attr($main_menu_color_opacity) .'); }'."\r\n";
		$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-color: rgba(' . esc_attr($main_menu_bg_color_rgba) . ',' . esc_attr($main_menu_color_opacity) .'); }'."\r\n";
	}

	/* Menu background*/

	if ( $main_menu_bg_image ) {
		$options_custom_bg_img = '.navigation-header.main-menu:not(.fixmenu-clone) { background-image: url("' . remove_base_url(esc_attr($main_menu_bg_image)) . '");}'."\r\n";
		$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-image: url("' . remove_base_url(esc_attr($main_menu_bg_image)) . '");}'."\r\n";

		if ( $main_menu_bg_position ) {
			$options_custom_menu_bg .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-position: ' . esc_attr($main_menu_bg_position) . '; }'."\r\n";
			$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-position: ' . esc_attr($main_menu_bg_position) . '; }'."\r\n";
		}

		if ( $main_menu_bg_repeat ) {
			$options_custom_menu_bg .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-repeat: ' . esc_attr($main_menu_bg_repeat) . '; }'."\r\n";
			$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-repeat: ' . esc_attr($main_menu_bg_repeat) . '; }'."\r\n";
		}

		if ( $main_menu_bg_size ) {
			$options_custom_menu_bg .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-size: ' . esc_attr($main_menu_bg_size) . '; -moz-background-size: ' . esc_attr($main_menu_bg_size) . '; -webkit-background-size: ' . esc_attr($main_menu_bg_size) . '; }'."\r\n";
			$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone) { background-size: ' . esc_attr($main_menu_bg_size) . '; -moz-background-size: ' . esc_attr($main_menu_bg_size) . '; -webkit-background-size: ' . esc_attr($main_menu_bg_size) . '; }'."\r\n";
		}

		if ( $main_menu_bg_color ) {
			$options_custom_bg_img .= '.navigation-header.main-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($main_menu_bg_color_rgba) .','. esc_attr($main_menu_color_opacity) .'); }'."\r\n";
			$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($main_menu_bg_color_rgba) .','. esc_attr($main_menu_color_opacity) .'); z-index: 1; }'."\r\n";
		}

	} else {
		$opions_custom_bg_img = '.navigation-header.main-menu:not(.fixmenu-clone) { background:url("'.remove_base_url(get_template_directory_uri().'/images/low_poly_background.jpg').'") top center no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover; background-attachment: fixed;}'."\r\n";
		if ( $main_menu_bg_color ) {
			$options_custom_bg_img .= '.navigation-header.main-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($main_menu_bg_color_rgba) .','. esc_attr($main_menu_color_opacity) .'); }'."\r\n";
			$options_responsive .= '.navigation-header.main-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($main_menu_bg_color_rgba) .','. esc_attr($main_menu_color_opacity) .'); z-index: 1; }'."\r\n";

		}
	}

	/* Menu text color*/

	if ( $main_menu_text_color ) {
		$options_custom_menu_clr .= '.navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($main_menu_text_color) . '; }'."\r\n";
	}

	if ( $main_menu_active_color ) {
		$options_custom_menu_clr .= '.navigation-header a, .navigation-header a:hover, .navigation-header a:active, .navigation-header a:focus,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header i, .navigation-header i:before, .navigation-header i:hover, .navigation-header i:hover:before,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header i, .navigation-header i:before, .navigation-header i:hover, .navigation-header i:hover:before,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .featured > a, .navigation-header .current > a:not(.dropdown-toggle), .navigation-header .dropdown-menu > .menu-item > a, .navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .dropdown-menu > .menu-item > a:active, .navigation-header .dropdown-menu > .menu-item > a:focus, .navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .dropdown-menu > .menu-item.active > a:hover, .navigation-header .dropdown-menu > .menu-item.active > a:focus, .navigation-header .dropdown-menu > .menu-item.active > a:active, .navigation-header .dropdown:hover:after,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .base_clr_txt, .navigation-header .btn-default:hover, .navigation-header .btn-default:focus { color: ' . esc_attr($main_menu_active_color) . '; }'."\r\n";

		$options_custom_menu_clr .= '.navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .dropdown-toggle:before, .navigation-header .featured > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .featured > a:active, .navigation-header .base_clr_bg, .navigation-header .base_clr_bg:hover, .navigation-header .base_clr_bg:active'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($main_menu_active_color) . '; }'."\r\n";

		$options_custom_menu_clr .= '.navigation-header .featured > a,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .featured > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .featured > a:active,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .featured > a:focus,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header .base_clr_brd:hover, .navigation-header .base_clr_brd:active, .navigation-header .base_clr_brd { border-color: ' . esc_attr($main_menu_active_color) . '; }'."\r\n";
	}

	$options_responsive .= '}'."\r\n";

	echo $header_style;
	echo $options_custom_menu_clr;
	echo $options_custom_menu_bg;
	echo $options_custom_menu_bg_clr;
	echo $options_custom_bg_img;
	echo $options_responsive;

	/*** END HOMEPAGE MENU ***/

	/*
	*
	* START INNER PAGES MENU
	*
	*/
	$menu_height = isset($options['menu_height']) ? $options['menu_height'] : $default;
	$menu_bg_color = isset($options['menu_bg_color']) ? $options['menu_bg_color'] : $default;
	$menu_bg_image = isset($options['menu_bg_image']) ? $options['menu_bg_image'] : $default;
	$menu_bg_color_rgba = hex2rgb($menu_bg_color);
	$menu_color_opacity = isset($options['menu_color_opacity']) ? $options['menu_color_opacity'] : $default;
	$menu_color_opacity = $menu_color_opacity / 100;
	$menu_bg_position = isset($options['menu_bg_position']) ? $options['menu_bg_position'] : $default;
	$menu_bg_repeat = isset($options['menu_bg_repeat']) ? $options['menu_bg_repeat'] : $default;
	$menu_bg_size = isset($options['menu_bg_size']) ? $options['menu_bg_size'] : $default;
	$menu_text_color = isset($options['menu_text_color']) ? $options['menu_text_color'] : $default;
	$menu_active_color = isset($options['menu_active_color']) ? $options['menu_active_color'] : $default;

	$inner_header_style = '';
	$options_custom_bg_img = '';
	$options_custom_menu_bg = '';
	$options_custom_menu_bg_clr = '';
	$options_custom_menu_clr = '';
	$options_responsive = '@media (max-width: 1024px) {'."\r\n";

	/* Menu background overlay color*/

	if ( $menu_height ) {
		$inner_header_style = '.navigation-header.inner-menu:not(.fixmenu-clone) { height: ' . intval($menu_height) . 'px; }'."\r\n";
	}

	if( !$menu_color_opacity ) $menu_color_opacity = 1;

	if( $menu_bg_color ) {
		$options_custom_menu_bg_clr = '.navigation-header.inner-menu:not(.fixmenu-clone) { background-color: rgba(' . esc_attr($menu_bg_color_rgba) . ',' . esc_attr($menu_color_opacity) .'); }'."\r\n";
		$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-color: rgba(' . esc_attr($menu_bg_color_rgba) . ',' . esc_attr($menu_color_opacity) .'); }'."\r\n";
	}

	/* Menu background*/

	if ( $menu_bg_image ) {
		$options_custom_bg_img = '.navigation-header.inner-menu:not(.fixmenu-clone) { background-image: url("' . remove_base_url(esc_attr($menu_bg_image)) . '");}'."\r\n";

		if ( $menu_bg_position ) {
			$options_custom_menu_bg .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-position: ' . esc_attr($menu_bg_position) . ' }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-position: ' . esc_attr($menu_bg_position) . '; }'."\r\n";
		}

		if ( $menu_bg_repeat ) {
			$options_custom_menu_bg .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-repeat: ' . esc_attr($menu_bg_repeat) . ' }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-repeat: ' . esc_attr($menu_bg_repeat) . '; }'."\r\n";
		}

		if ( $menu_bg_size ) {
			$options_custom_menu_bg .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-size: ' . esc_attr($menu_bg_size) . '; -moz-background-size: ' . esc_attr($menu_bg_size) . '; -webkit-background-size: ' . esc_attr($menu_bg_size) . '; }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-size: ' . esc_attr($menu_bg_size) . '; -moz-background-size: ' . esc_attr($menu_bg_size) . '; -webkit-background-size: ' . esc_attr($menu_bg_size) . '; }'."\r\n";
		}

		if ( $menu_bg_color ) {
			$options_custom_bg_img .= '.navigation-header.inner-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($menu_bg_color_rgba) .','. esc_attr($menu_color_opacity) .'); }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone):after { display: none; }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($menu_bg_color_rgba) .','. esc_attr($menu_color_opacity) .'); z-index: 1; }'."\r\n";
		}

	} else {
		$options_custom_bg_img = '.navigation-header.inner-menu:not(.fixmenu-clone) { background-image: url("'.remove_base_url(get_template_directory_uri().'/images/low_poly_background.jpg').'"); background-position: top center; background-repeat: no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover; background-attachment: fixed; }'."\r\n";
		$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone) { background-image: url("'.remove_base_url(get_template_directory_uri().'/images/low_poly_background.jpg').'"); background-position: top center; background-repeat: no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover; background-attachment: fixed; }'."\r\n";

		if ( $menu_bg_color ) {
			$options_custom_bg_img .= '.navigation-header.inner-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($menu_bg_color_rgba) .','. esc_attr($menu_color_opacity) .'); }'."\r\n";
			$options_responsive .= '.navigation-header.inner-menu:not(.fixmenu-clone):after { position: absolute; top:0; bottom: 0; left: 0; right: 0; display: block; content:\' \'; background-color: rgba('.esc_attr($menu_bg_color_rgba) .','. esc_attr($menu_color_opacity) .'); z-index: 1; }'."\r\n";
		}
	}

	/* Menu text color*/

	if ( $menu_text_color ) {
		$options_custom_menu_clr .= '.navigation-header.inner-menu .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($menu_text_color) . '; }'."\r\n";
	}

	if ( $menu_active_color ) {
		$options_custom_menu_clr .= '.navigation-header.inner-menu a, .navigation-header.inner-menu a:hover, .navigation-header.inner-menu a:active, .navigation-header.inner-menu a:focus,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu i, .navigation-header.inner-menu i:before, .navigation-header.inner-menu i:hover, .navigation-header.inner-menu i:hover:before,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .featured > a, .navigation-header.inner-menu .current > a:not(.dropdown-toggle), .navigation-header.inner-menu .dropdown-menu > li > a, .navigation-header.inner-menu .dropdown-menu > li > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .dropdown-menu > li > a:active, .navigation-header.inner-menu .dropdown-menu > li > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:hover, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:active, .navigation-header.inner-menu .dropdown:hover:after,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .base_clr_txt, .navigation-header.inner-menu .btn-default:hover, .navigation-header.inner-menu .btn-default:focus { color: ' . esc_attr($menu_active_color) . '; }'."\r\n";

		$options_custom_menu_clr .= '.navigation-header.inner-menu .current > a:not(.dropdown-toggle):after, .navigation-header.inner-menu .dropdown-toggle:before,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .tagcloud > a:hover, .navigation-header.inner-menu .featured > a:hover,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .featured > a:active, .navigation-header.inner-menu .base_clr_bg, .navigation-header.inner-menu .base_clr_bg:hover, .navigation-header.inner-menu .base_clr_bg:active,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .btn.base_clr_bg { background-color: ' . esc_attr($menu_active_color) . '; }'."\r\n";

		$options_custom_menu_clr .= '.navigation-header.inner-menu .featured > a, .navigation-header.inner-menu .featured > a:hover, .navigation-header.inner-menu .featured > a:active,'."\r\n";
		$options_custom_menu_clr .= '.navigation-header.inner-menu .featured > a:focus, .navigation-header.inner-menu .base_clr_brd { border-color: ' . esc_attr($menu_active_color) . '; }'."\r\n";
	}

	$options_responsive .= '}'."\r\n";

	echo $inner_header_style;
	echo $options_custom_menu_clr;
	echo $options_custom_menu_bg;
	echo $options_custom_menu_bg_clr;
	echo $options_custom_bg_img;
	echo $options_responsive;
	/*** END INNER PAGES MENU ***/

	/*
	*
	* START STICKY MENU
	*
	*/
	$sticky_menu_height = isset($options['sticky_menu_height']) ? $options['sticky_menu_height'] : $default;
	$sticky_menu_bg_color = isset($options['sticky_menu_bg_color']) ? $options['sticky_menu_bg_color'] : $default;
	$sticky_menu_bg_color_rgba = hex2rgb($sticky_menu_bg_color);
	$sticky_menu_color_opacity = isset($options['sticky_menu_color_opacity']) ? $options['sticky_menu_color_opacity'] : $default;
	$sticky_menu_color_opacity = $sticky_menu_color_opacity / 100;

	$sticky_menu_text_color = isset($options['sticky_menu_text_color']) ? $options['sticky_menu_text_color'] : $default;
	$sticky_menu_active_color = isset($options['sticky_menu_active_color']) ? $options['sticky_menu_active_color'] : $default;

	$options_sticky_menu = '';
	$options_sticky_responsive = '@media (max-width: 1024px) {'."\r\n";

	if ( $sticky_menu_height ) {
		$options_sticky_menu .= '.fixmenu-clone.navigation-header.inner-menu, .fixmenu-clone.navigation-header { height: ' . intval($sticky_menu_height) . 'px; }'."\r\n";
	}

	if( !$sticky_menu_color_opacity ) $sticky_menu_color_opacity = 1;
	if( $sticky_menu_bg_color ) {
		$options_sticky_menu = '.fixmenu-clone.navigation-header, .fixmenu-clone.navigation-header:after { background-color: rgba(' . esc_attr($sticky_menu_bg_color_rgba) . ',' . esc_attr($sticky_menu_color_opacity) .'); }'."\r\n";
	}

	/* Menu text color*/

	if ( $sticky_menu_text_color ) {
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header.inner-menu .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($sticky_menu_text_color) . '; }'."\r\n";
	}
	if ( $sticky_menu_active_color ) {
		$options_sticky_menu .= '.fixmenu-clone.navigation-header a, .fixmenu-clone.navigation-header a:hover, .fixmenu-clone.navigation-header a:active, .fixmenu-clone.navigation-header a:focus,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header i, .fixmenu-clone.navigation-header i:before, .fixmenu-clone.navigation-header i:hover, .fixmenu-clone.navigation-header i:hover:before,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .featured > a, .fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle), .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:active, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:hover, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:active, .fixmenu-clone.navigation-header .dropdown:hover:after,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .base_clr_txt, .fixmenu-clone.navigation-header .btn-default:hover, .fixmenu-clone.navigation-header .btn-default:focus { color: ' . esc_attr($sticky_menu_active_color) . '; }'."\r\n";

		$options_sticky_menu .= '.fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .dropdown-toggle:before,.fixmenu-clone.navigation-header .featured > a:hover,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .featured > a:active, .fixmenu-clone.navigation-header .base_clr_bg, .fixmenu-clone.navigation-header .base_clr_bg:hover, .fixmenu-clone.navigation-header .base_clr_bg:active,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($sticky_menu_active_color) . '; }'."\r\n";

		$options_sticky_menu .= '.fixmenu-clone.navigation-header .featured > a, .fixmenu-clone.navigation-header .featured > a:hover, .fixmenu-clone.navigation-header .featured > a:active,'."\r\n";
		$options_sticky_menu .= '.fixmenu-clone.navigation-header .featured > a:focus, .fixmenu-clone.navigation-header .base_clr_brd:hover, .fixmenu-clone.navigation-header .base_clr_brd:active, .fixmenu-clone.navigation-header .base_clr_brd { border-color: ' . esc_attr($sticky_menu_active_color) . '; }'."\r\n";
	}

	$options_sticky_responsive .= '}'."\r\n";

	echo $options_sticky_menu;
	echo $options_sticky_responsive;
	/* END STICKY MENU */

	/*
	*
	* START DROPDOWN MENU
	*
	*/

	$dropdown_bg_light = isset($options['dropdown_bg_light']) ? $options['dropdown_bg_light'] : $default;
	$dropdown_bg_opacity = isset($options['dropdown_bg_opacity']) ? $options['dropdown_bg_opacity'] : 100;
	$dropdown_bg_opacity = $dropdown_bg_opacity / 100;

	$dropdown_text_color = isset($options['dropdown_text_color']) ? $options['dropdown_text_color'] : $default;
	$dropdown_active_color = isset($options['dropdown_active_color']) ? $options['dropdown_active_color'] : $default;

	$options_dropdown_menu = '';

	if ( $dropdown_bg_light ) {
		$options_dropdown_menu .= '
			.fixmenu-clone.navigation-header .navigation-bar > .menu-item.dropdown:not(.featured) > a:after, .navigation-header .navigation-bar > .menu-item.dropdown:not(.featured) > a:after { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) . '); }
			.fixmenu-clone.navigation-header .dropdown:hover .dropdown-toggle, .navigation-header .dropdown:hover .dropdown-toggle { position: relative; z-index: 9999; }
			.fixmenu-clone.navigation-header .dropdown-menu, .navigation-header .dropdown-menu { background: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) . '); }
			.fixmenu-clone.navigation-header .dropdown-menu .menu-item a, .navigation-header .dropdown-menu .menu-item a { border-bottom-color: rgba(0,0,0,0.03); }
			.fixmenu-clone.navigation-header .dropdown-menu .menu-item + .menu-item a, .navigation-header .dropdown-menu .menu-item + .menu-item a { border-top-color: rgba(0,0,0,0.05); }
			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover:before, .navigation-header .dropdown-menu > .menu-item.dropdown:hover:before { background-color: #e5e5e5; }
			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a, .navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a { border-top-color: transparent; }
			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown > a, .navigation-header .dropdown-menu > .menu-item.dropdown > a { position: relative; z-index: 10; }
			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover > a:hover, .navigation-header .dropdown-menu > .menu-item.dropdown:hover > a:hover { border-color: transparent; }

			body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .dropdown-menu,
			body.mobile-always .navigation-header .dropdown-menu .dropdown-menu { background-color: #e5e5e5; }

			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:focus, .navigation-header .dropdown-menu > .menu-item > a:focus { background: transparent }

			.fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
			.fixmenu-clone.navigation-header .navigation-bar > .menu-item.dropdown:not(:hover) > a:not(:active):not(:hover),
			.fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: #000; }

			.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
			.navigation-header .navigation-bar > .menu-item.dropdown:not(:hover) > a:not(:active):not(:hover),
			.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: #000; }

			.navigation-header.inner-menu .dropdown-menu .dropdown:not(:hover):not(:active):after,
			.navigation-header.inner-menu .navigation-bar > .menu-item.dropdown:hover > a:not(:active):not(:hover),
			.navigation-header.inner-menu .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: #000; }

			body.mobile-always .navbar-collapse { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) . '); }
			body.mobile-always .fixmenu-clone.navigation-header .navbar-collapse { border-left: 1px solid rgba(0,0,0,0.1); }
			body.mobile-always .navigation-header .navigation-bar > .menu-item > a, body.mobile-always .navigation-header .navigation-bar > .menu-item > a:hover, body.mobile-always .navigation-header .navigation-bar > .menu-item.current > a { border-bottom-color: #ececec; border-top-color: #f8f8f8; }
			body.mobile-always .navigation-header .navigation-bar > .menu-item.dropdown.opened > .dropdown-toggle { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) * 0.3 . '); }
			body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item > a, body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item > a:hover, body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item.current > a { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) * 0.1 . '); border-bottom: 1px solid rgba(217,217,217, 1); }
			body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item > a:first-child { border-top: 1px solid rgba(237,237,237,1); }
			body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a, body.mobile-always .navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a { border-top: 1px solid rgba(237,237,237,1); }
			body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured) > a:not(:active):not(:hover) { color: #000; }

			body.mobile-always .navigation-header .navigation-bar > .menu-item.featured > a:hover,
			body.mobile-always .navigation-header .navigation-bar > .menu-item.featured.current > a:hover,
			body.mobile-always .navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
			body.mobile-always .navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),
			body.mobile-always .navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),
			body.mobile-always .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),
			body.mobile-always .navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: #000; }

			body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured > a:hover,
			body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,
			body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
			body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .dropdown > .dropdown-toggle:not(:hover)
			body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover)
			body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: #000; }

			@media (max-width: 1024px) {
				.navbar-collapse { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) . '); }
				.navigation-header .dropdown-menu .dropdown-menu { background-color: #e5e5e5; }
				.fixmenu-clone.navigation-header .navbar-collapse { border-left: 1px solid rgba(0,0,0,0.1); }
				.navigation-header .navigation-bar > .menu-item.dropdown.opened > .dropdown-toggle { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) * 0.3 . '); }
				.navigation-header .navigation-bar .dropdown-menu .menu-item > a, .navigation-header .navigation-bar .dropdown-menu .menu-item > a:hover, .navigation-header .navigation-bar .dropdown-menu .menu-item.current > a { background-color: rgba(255,255,255,' . esc_attr($dropdown_bg_opacity) * 0.1 . '); border-bottom: 1px solid rgba(217,217,217, 1); }
				.navigation-header .navigation-bar .dropdown-menu .menu-item > a:first-child { border-top: 1px solid rgba(237,237,237,1); }
				.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a, .navigation-header .dropdown-menu > .menu-item.dropdown:hover + .menu-item a { border-top: 1px solid rgba(237,237,237,1); }

				.navigation-header.inner-menu .navigation-bar > .menu-item.featured > a:hover,
				.navigation-header.inner-menu .navigation-bar > .menu-item.featured.current > a:hover,
				.navigation-header.inner-menu .dropdown-menu .dropdown:not(:hover):not(:active):after,
				.navigation-header.inner-menu .navigation-bar > .dropdown > .dropdown-toggle:not(:hover),
				.navigation-header.inner-menu .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),
				.navigation-header.inner-menu .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),
				.navigation-header.inner-menu .navigation-bar > .menu-item:not(.current):not(.featured) > a:not(:active):not(:hover),

				.navigation-header .navigation-bar > .menu-item.featured > a:hover,
				.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,
				.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
				.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),
				.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),
				.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),
				.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover),

				.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured > a:hover,
				.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,
				.fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,
				.fixmenu-clone.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),
				.fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),
				.fixmenu-clone.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),
				.fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: #000; }
				.fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured) > a:not(:active):not(:hover) { color: #000; }
				.fixmenu-clone.navigation-header .dropdown-menu .dropdown-menu, .navigation-header .dropdown-menu .dropdown-menu { background-color: #e5e5e5; }

			}'."\r\n";
	} else {
		$options_dropdown_menu .= '
			.fixmenu-clone.navigation-header .navigation-bar > .menu-item.dropdown:not(.featured) > a:after, .navigation-header .navigation-bar > .menu-item.dropdown:not(.featured) > a:after { background-color: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) . '); }
			.fixmenu-clone.navigation-header .dropdown:hover .dropdown-toggle, .navigation-header .dropdown:hover .dropdown-toggle { position: relative; z-index: 9999; }
			.fixmenu-clone.navigation-header .dropdown-menu, .navigation-header .dropdown-menu { background: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) . '); }
			.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.dropdown:hover:before, .navigation-header .dropdown-menu > .menu-item.dropdown:hover:before { background-color: #000; }

			body.mobile-always .navbar-collapse { background-color: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) . '); }
			body.mobile-always .navigation-header .navigation-bar > .menu-item.dropdown.opened > .dropdown-toggle { background-color: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) * 0.3 . '); }
			body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item > a, body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item > a:hover, body.mobile-always .navigation-header .navigation-bar .dropdown-menu .menu-item.current > a { background-color: rgba(0,0,0,' . esc_attr($dropdown_bg_opacity) * 0.5 . '); }
			body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .dropdown-menu, body.mobile-always .navigation-header .dropdown-menu .dropdown-menu { background-color: #000; }

			@media (max-width: 1024px) {
				.navbar-collapse { background-color: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) . '); }
				.navigation-header .navigation-bar > .menu-item.dropdown.opened > .dropdown-toggle { background-color: rgba(37,37,37,' . esc_attr($dropdown_bg_opacity) * 0.3 . '); }
				.navigation-header .navigation-bar .dropdown-menu .menu-item > a, .navigation-header .navigation-bar .dropdown-menu .menu-item > a:hover, .navigation-header .navigation-bar .dropdown-menu .menu-item.current > a { background-color: rgba(0,0,0,' . esc_attr($dropdown_bg_opacity) * 0.5 . '); }
				.fixmenu-clone.navigation-header .dropdown-menu .dropdown-menu, .navigation-header .dropdown-menu .dropdown-menu { background-color: #000; }
			}'."\r\n";
	}

	if ( $dropdown_text_color ) {
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item.dropdown:not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .navigation-bar > .menu-item.dropdown:not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .navigation-bar > .menu-item.dropdown:not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= 'body.mobile-always .navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= '@media (max-width: 1024px) {'."\r\n";

		$options_dropdown_menu .= '.navigation-header.inner-menu .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .navigation-bar > .dropdown > .dropdown-toggle:not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .navigation-bar > .menu-item:not(.current):not(.featured) > a:not(:active):not(:hover),'."\r\n";

		$options_dropdown_menu .= '.navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_dropdown_menu .= '.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover),'."\r\n";

		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item.featured.current > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .dropdown:not(:hover):not(:active):after,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .dropdown:not(:hover) > .dropdown-toggle:not(:hover),'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .menu-item:not(.active):not(:hover) > a:not(:active):not(:hover),'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .menu-extra-container .navigation-bar .menu-item:not(.featured).current a:not(.dropdown-toggle):not(:hover):not(:active),'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .navigation-bar > .menu-item:not(.current):not(.featured):not(:hover) > a:not(:active):not(:hover) { color: ' . esc_attr($dropdown_text_color) . '; }'."\r\n";

		$options_dropdown_menu .= '}'."\r\n";
	}

	if ( $dropdown_active_color ) {
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu a, .fixmenu-clone.navigation-header .dropdown-menu a:hover, .fixmenu-clone.navigation-header .dropdown-menu a:active, .fixmenu-clone.navigation-header .dropdown-menu a:focus,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu i, .fixmenu-clone.navigation-header .dropdown-menu i:before, .fixmenu-clone.navigation-header .dropdown-menu i:hover, .fixmenu-clone.navigation-header .dropdown-menu i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .current > a:not(.dropdown-toggle), .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:active, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:hover, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:active, .fixmenu-clone.navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .base_clr_txt, .fixmenu-clone.navigation-header .dropdown-menu .btn-default:hover, .fixmenu-clone.navigation-header .dropdown-menu .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .current > a:not(.dropdown-toggle):after, .fixmenu-clone.navigation-header .dropdown-menu .dropdown-toggle:before,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .featured > a:active, .fixmenu-clone.navigation-header .base_clr_bg, .fixmenu-clone.navigation-header .base_clr_bg:hover, .fixmenu-clone.navigation-header .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .featured > a, .fixmenu-clone.navigation-header .dropdown-menu .featured > a:hover, .fixmenu-clone.navigation-header .dropdown-menu .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu .featured > a:focus, .fixmenu-clone.navigation-header .dropdown-menu .base_clr_brd { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.navigation-header .dropdown-menu a, .navigation-header .dropdown-menu a:hover, .navigation-header .dropdown-menu a:active, .navigation-header .dropdown-menu a:focus,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu i, .navigation-header .dropdown-menu i:before, .navigation-header .dropdown-menu i:hover, .navigation-header .dropdown-menu i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .current > a:not(.dropdown-toggle), .navigation-header .dropdown-menu > .menu-item > a, .navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu > .menu-item > a:active, .navigation-header .dropdown-menu > .menu-item > a:focus, .navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu > .menu-item.active > a:hover, .navigation-header .dropdown-menu > .menu-item.active > a:focus, .navigation-header .dropdown-menu > .menu-item.active > a:active, .navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .base_clr_txt, .navigation-header .dropdown-menu .btn-default:hover, .navigation-header .dropdown-menu .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .current > a:not(.dropdown-toggle):after, .navigation-header .dropdown-menu .dropdown-toggle:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .featured > a:active, .navigation-header .base_clr_bg, .navigation-header .base_clr_bg:hover, .navigation-header .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .featured > a, .navigation-header .dropdown-menu .featured > a:hover, .navigation-header .dropdown-menu .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu .featured > a:focus, .navigation-header .dropdown-menu .base_clr_brd { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu a, .navigation-header.inner-menu .dropdown-menu a:hover, .navigation-header.inner-menu .dropdown-menu a:active, .navigation-header.inner-menu .dropdown-menu a:focus,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu i, .navigation-header.inner-menu .dropdown-menu i:before, .navigation-header.inner-menu .dropdown-menu i:hover, .navigation-header.inner-menu .dropdown-menu i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .current > a:not(.dropdown-toggle), .navigation-header.inner-menu .dropdown-menu > .menu-item > a, .navigation-header.inner-menu .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu > .menu-item > a:active, .navigation-header.inner-menu .dropdown-menu > .menu-item > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:hover, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:active, .navigation-header.inner-menu .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .base_clr_txt, .navigation-header.inner-menu .dropdown-menu .btn-default:hover, .navigation-header.inner-menu .dropdown-menu .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .current > a:not(.dropdown-toggle):after, .navigation-header.inner-menu .dropdown-menu .dropdown-toggle:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .featured > a:active, .navigation-header.inner-menu .base_clr_bg, .navigation-header.inner-menu .base_clr_bg:hover, .navigation-header.inner-menu .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .featured > a, .navigation-header.inner-menu .dropdown-menu .featured > a:hover, .navigation-header.inner-menu .dropdown-menu .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu .featured > a:focus, .navigation-header.inner-menu .dropdown-menu .base_clr_brd { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= 'body.mobile-always .navigation-header a, body.mobile-always .navigation-header a:hover, body.mobile-always .navigation-header a:active, body.mobile-always .navigation-header a:focus,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header i, body.mobile-always .navigation-header i:before, body.mobile-always .navigation-header i:hover, body.mobile-always .navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header i, body.mobile-always .navigation-header i:before, body.mobile-always .navigation-header i:hover, body.mobile-always .navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .featured > a, body.mobile-always .navigation-header .current > a:not(.dropdown-toggle), body.mobile-always .navigation-header .dropdown-menu > .menu-item > a, body.mobile-always .navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .dropdown-menu > .menu-item > a:active, body.mobile-always .navigation-header .dropdown-menu > .menu-item > a:focus, body.mobile-always .navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .dropdown-menu > .menu-item.active > a:hover, body.mobile-always .navigation-header .dropdown-menu > .menu-item.active > a:focus, body.mobile-always .navigation-header .dropdown-menu > .menu-item.active > a:active, body.mobile-always .navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .base_clr_txt, body.mobile-always .navigation-header .btn-default:hover, body.mobile-always .navigation-header .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .dropdown-toggle:before, body.mobile-always .navigation-header .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .featured > a:active, body.mobile-always .navigation-header .base_clr_bg, body.mobile-always .navigation-header .base_clr_bg:hover, body.mobile-always .navigation-header .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .featured > a, body.mobile-always .navigation-header .featured > a:hover, body.mobile-always .navigation-header .featured > a:active,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .navigation-header .featured > a:focus, body.mobile-always .navigation-header .base_clr_brd, body.mobile-always .navigation-header .base_clr_brd:hover, body.mobile-always .navigation-header .base_clr_brd:active { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header a, body.mobile-always .fixmenu-clone.navigation-header a:hover, body.mobile-always .fixmenu-clone.navigation-header a:active, body.mobile-always .fixmenu-clone.navigation-header a:focus,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header i, body.mobile-always .fixmenu-clone.navigation-header i:before, body.mobile-always .fixmenu-clone.navigation-header i:hover, body.mobile-always .fixmenu-clone.navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header i, body.mobile-always .fixmenu-clone.navigation-header i:before, body.mobile-always .fixmenu-clone.navigation-header i:hover, body.mobile-always .fixmenu-clone.navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .featured > a, body.mobile-always .fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle), body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a, body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:active, body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:focus, body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:hover, body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:focus, body.mobile-always .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:active, body.mobile-always .fixmenu-clone.navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .base_clr_txt, body.mobile-always .fixmenu-clone.navigation-header .btn-default:hover, body.mobile-always .fixmenu-clone.navigation-header .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .dropdown-toggle:before, body.mobile-always .fixmenu-clone.navigation-header .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .featured > a:active, body.mobile-always .fixmenu-clone.navigation-header .base_clr_bg, body.mobile-always .fixmenu-clone.navigation-header .base_clr_bg:hover, body.mobile-always .fixmenu-clone.navigation-header .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .featured > a, body.mobile-always .fixmenu-clone.navigation-header .featured > a:hover, body.mobile-always .fixmenu-clone.navigation-header .featured > a:active,'."\r\n";
		$options_dropdown_menu .= 'body.mobile-always .fixmenu-clone.navigation-header .featured > a:focus, body.mobile-always .fixmenu-clone.navigation-header .base_clr_brd, body.mobile-always .fixmenu-clone.navigation-header .base_clr_brd:hover, body.mobile-always .fixmenu-clone.navigation-header .base_clr_brd:active { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '@media (max-width: 1024px) {'."\r\n";

		$options_dropdown_menu .= '.navigation-header a, .navigation-header a:hover, .navigation-header a:active, .navigation-header a:focus,'."\r\n";
		$options_dropdown_menu .= '.navigation-header i, .navigation-header i:before, .navigation-header i:hover, .navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header i, .navigation-header i:before, .navigation-header i:hover, .navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .featured > a, .navigation-header .current > a:not(.dropdown-toggle), .navigation-header .dropdown-menu > .menu-item > a, .navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu > .menu-item > a:active, .navigation-header .dropdown-menu > .menu-item > a:focus, .navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-menu > .menu-item.active > a:hover, .navigation-header .dropdown-menu > .menu-item.active > a:focus, .navigation-header .dropdown-menu > .menu-item.active > a:active, .navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .base_clr_txt, .navigation-header .btn-default:hover, .navigation-header .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .dropdown-toggle:before, .navigation-header .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .featured > a:active, .navigation-header .base_clr_bg, .navigation-header .base_clr_bg:hover, .navigation-header .base_clr_bg:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header .featured > a, .navigation-header .featured > a:hover, .navigation-header .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header .featured > a:focus, .navigation-header .base_clr_brd, .navigation-header .base_clr_brd:hover, .navigation-header .base_clr_brd:active { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.navigation-header.inner-menu a, .navigation-header.inner-menu a:hover, .navigation-header.inner-menu a:active, .navigation-header.inner-menu a:focus,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu i, .navigation-header.inner-menu i:before, .navigation-header.inner-menu i:hover, .navigation-header.inner-menu i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu i, .navigation-header.inner-menu i:before, .navigation-header.inner-menu i:hover, .navigation-header.inner-menu i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .featured > a, .navigation-header.inner-menu .current > a:not(.dropdown-toggle), .navigation-header.inner-menu .dropdown-menu > .menu-item > a, .navigation-header.inner-menu .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu > .menu-item > a:active, .navigation-header.inner-menu .dropdown-menu > .menu-item > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:hover, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:focus, .navigation-header.inner-menu .dropdown-menu > .menu-item.active > a:active, .navigation-header.inner-menu .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .base_clr_txt, .navigation-header.inner-menu .btn-default:hover, .navigation-header.inner-menu .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .dropdown-toggle:before, .navigation-header.inner-menu .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .featured > a:active, .navigation-header.inner-menu .base_clr_bg, .navigation-header.inner-menu .base_clr_bg:active,, .navigation-header.inner-menu .base_clr_bg:hover,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .featured > a, .navigation-header.inner-menu .featured > a:hover, .navigation-header.inner-menu .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.navigation-header.inner-menu .featured > a:focus, .navigation-header.inner-menu .base_clr_brd, .navigation-header.inner-menu .base_clr_brd:active, .navigation-header.inner-menu .base_clr_brd:hover { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '.fixmenu-clone.navigation-header a, .fixmenu-clone.navigation-header a:hover, .fixmenu-clone.navigation-header a:active, .fixmenu-clone.navigation-header a:focus,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header i, .fixmenu-clone.navigation-header i:before, .fixmenu-clone.navigation-header i:hover, .fixmenu-clone.navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header i, .fixmenu-clone.navigation-header i:before, .fixmenu-clone.navigation-header i:hover, .fixmenu-clone.navigation-header i:hover:before,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .featured > a, .fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle), .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:active, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active >a,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:hover, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:focus, .fixmenu-clone.navigation-header .dropdown-menu > .menu-item.active > a:active, .fixmenu-clone.navigation-header .dropdown:hover:after,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .base_clr_txt, .fixmenu-clone.navigation-header .btn-default:hover, .fixmenu-clone.navigation-header .btn-default:focus { color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .current > a:not(.dropdown-toggle):after,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .dropdown-toggle:before, .fixmenu-clone.navigation-header .featured > a:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .featured > a:active, .fixmenu-clone.navigation-header .base_clr_bg, .fixmenu-clone.navigation-header .base_clr_bg:active, .fixmenu-clone.navigation-header .base_clr_bg:hover,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .btn.base_clr_bg { background-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .featured > a, .fixmenu-clone.navigation-header .featured > a:hover, .fixmenu-clone.navigation-header .featured > a:active,'."\r\n";
		$options_dropdown_menu .= '.fixmenu-clone.navigation-header .featured > a:focus, .fixmenu-clone.navigation-header .base_clr_brd, .fixmenu-clone.navigation-header .base_clr_brd:hover, .fixmenu-clone.navigation-header .base_clr_brd:active { border-color: ' . esc_attr($dropdown_active_color) . '; }'."\r\n";

		$options_dropdown_menu .= '}'."\r\n";
	}


	echo $options_dropdown_menu;
	/* END DROPDOWN MENU */


	/*
	*
	* START FOOTER DESIGN
	*
	*/
	$footer_text_color = isset($options['footer_text_color']) ? $options['footer_text_color'] : $default;
	$footer_bg_color = isset($options['footer_bg_color']) ? $options['footer_bg_color'] : $default;
	$footer_bg_image = isset($options['footer_bg_image']) ? $options['footer_bg_image'] : $default;
	$color = hex2rgb($footer_bg_color);
	$color_opacity = isset($options['footer_color_opacity']) ? $options['footer_color_opacity'] : $default;
	$opacity = $color_opacity / 100;
	$footer_bg_position = isset($options['footer_bg_position']) ? $options['footer_bg_position'] : $default;
	$footer_bg_repeat = isset($options['footer_bg_repeat']) ? $options['footer_bg_repeat'] : $default;
	$footer_bg_size = isset($options['footer_bg_size']) ? $options['footer_bg_size'] : $default;

	/* footer text color*/
	$options_custom_footer_clr = '';
	if($footer_text_color){ $options_custom_footer_clr = '.navigation-header .navigation-bar > li > a {color: ' . esc_attr($footer_text_color) . '; }'."\r\n"; }
	/* footer background*/
	$options_custom_footer_bg = '';
	if($footer_bg_position){ $options_custom_footer_bg = '#footer { background-position: ' . esc_attr($footer_bg_position) . ' !important; }'."\r\n";}
	if($footer_bg_repeat){ $options_custom_footer_bg .= '#footer { background-repeat: ' . esc_attr($footer_bg_repeat) . ' !important; }'."\r\n";}
	if($footer_bg_size){ $options_custom_footer_bg .= '#footer { background-size: ' . esc_attr($footer_bg_size) . ' !important; }'."\r\n"; }
	/* footer background overlay color*/
	$options_custom_footer_bg_clr = '';
	if($footer_bg_color){ if(!$opacity){ $opacity = 1; } $options_custom_footer_bg_clr = '#footer {background-color: rgba(' . esc_attr($color) . ',' . esc_attr($opacity) .')}'."\r\n"; }
	/* footer background image */
	$options_custom_footer_bg_img = '';
	if ($footer_bg_image) {  $options_custom_footer_bg_img = '#footer {background-image: url("' . remove_base_url(esc_attr($footer_bg_image)) . '");}'."\r\n"; }
	if ($footer_bg_color) {  $options_custom_footer_bg_img .= '#footer:after{content:\' \'; background-color: rgba('.esc_attr($color) .','. esc_attr($opacity) .'); top:0;width: 100%;height: 100%;position: absolute;}'."\r\n";}

	/* copyrights styling */
	$footer_copyrights_options = '';
	if (startuply_option('footer_on', '1') !== '1') {
		$footer_copyrights_options = ".copyright { line-height: 50px; } .footer {padding:0;} ";
	}

	echo $options_custom_footer_clr;
	echo $options_custom_footer_bg;
	echo $options_custom_footer_bg_clr;
	echo $options_custom_footer_bg_img;
	echo $footer_copyrights_options;
	/* END FOOTER DESIGN */




	/*
	*
	* START BASE COLOR
	*
	*/
	$startuply_base_color = isset($options['vivaco_base_color']) ? $options['vivaco_base_color'] : $default;
	$options_color_styles = '';

		if($startuply_base_color){
			$rgba_base = hex2rgb($startuply_base_color);
			$options_color_styles = '
				/**:before,*/ a, a:hover, a:active, a:focus, .fa, .icon, i, i:hover, h1 b, h2 b, h3 b, h4 b, h5 b, h6 b, .h7 b, .featured > a, .current > a:not(.dropdown-toggle),
				.dropdown-menu > .menu-item > a, .dropdown-menu > .menu-item > a:hover, .dropdown-menu > .menu-item > a:focus, .dropdown-menu > .menu-item > a:active, .dropdown-menu > .menu-item.active > a,
				.dropdown-menu > .menu-item.active > a:hover, .dropdown-menu > .menu-item.active > a:focus, .dropdown-menu > .menu-item.active > a:active, .dropdown:hover:after, .vsc-countdown .countdown-period, .base_clr_txt, .base_clr_txt:hover,
				.base_clr_txt:focus, .base_clr_txt:active, .gform_wrapper .button, .base_clr_bg:not(:hover) .btn-outline:not(:active):not(:hover), .lighter-overlay .base_clr_txt, .darker-overlay .base_clr_txt { color: ' . esc_attr($startuply_base_color) . '; }

				::selection { background-color: ' . esc_attr($startuply_base_color) . '; }
				::-moz-selection { background-color: ' . esc_attr($startuply_base_color) . '; }

				a.selected, input[type=submit], input[type=submit]:focus, input[type=submit]:hover, input[type=button], input[type=button]:focus, input[type=button]:hover,
				.current > a:not(.dropdown-toggle):after, .dropdown-toggle:before, .tagcloud > a:hover, .featured > a:hover, .featured > a:active,
				.more-link:hover, .more-link:active, .nav li.active a, .nav li.ui-state-active a, .nav li.ui-state-hover a, .nav li.active a:hover, .nav li.active a:focus,
				.nav li.ui-state-active a:hover, .nav li.ui-state-active a:focus, .nav li.ui-state-hover a:hover, .nav li.ui-state-hover a:focus, .ui-state-active a .tab-info,
				.ui-state-active a .tab-info:hover, .ui-state-active a .tab-info:focus, .ui-state-active a, .bx-controls a, .tagcloud > a:hover, .tagcloud > a:active,
				.ui-slider-range, .gform_wrapper .gf_progressbar_wrapper .gf_progressbar_percentage, .base_clr_bg, .base_clr_bg:hover, .gform_wrapper .button,
				.base_clr_bg:focus, .base_clr_bg:active, .base_clr_bg.gradient { background-color: ' . esc_attr($startuply_base_color) . '; }

				.more-link, .flex-active, .featured > a, .featured > a:hover, .featured > a:active, .featured > a:focus, .base_clr_brd, .base_clr_bg:not(:hover) .btn-outline, .gform_wrapper .button,
				.base_clr_brd:hover, .base_clr_brd:active, .base_clr_brd:focus, input[type=submit], input[type=submit]:focus, input[type=submit]:hover, input[type=button], input[type=button]:focus, input[type=button]:hover { border-color: ' . esc_attr($startuply_base_color) . '; }'."\r\n";

			if (!$disable_border_color) {
				$options_color_styles .= '
					textarea, select, input[type=text], input[type=url], input[type=tel], input[type=number], input[type=color], input[type=email], input[type=password],
					textarea:focus, input[type=text]:focus, input[type=url]:focus, input[type=tel]:focus, input[type=number]:focus, input[type=color]:focus, input[type=email]:focus, input[type=password]:focus,
					textarea:hover, input[type=text]:hover, input[type=url]:hover, input[type=tel]:hover, input[type=number]:hover, input[type=color]:hover, input[type=email]:hover, input[type=password]:hover { border-left-color: ' . esc_attr($startuply_base_color) . '; }'."\r\n";
				}
		}
		echo $options_color_styles;
	/* END BASE COLOR */


	/*
	*
	* START USER CUSTOM CSS FROM THEME OPTIONS
	*
	*/
	$custom_css = startuply_option( 'custom_css', '');

	if ( strlen($custom_css) ) {
		echo $custom_css;
	}

	$custom_css_xs = startuply_option( 'custom_css_xs', '');
	$custom_css_xs_output = '';

	if ( strlen($custom_css_xs) ) {
		$custom_css_xs_output .= '@media (max-width: 767px) {' . "\r\n";
		$custom_css_xs_output .= $custom_css_xs . "\r\n";
		$custom_css_xs_output .= '}' . "\r\n";

		echo $custom_css_xs_output;
	}

	$custom_css_sm = startuply_option( 'custom_css_sm', '');
	$custom_css_sm_output = '';

	if ( strlen($custom_css_sm) ) {
		$custom_css_sm_output .= '@media (min-width: 768px) and (max-width: 991px) {' . "\r\n";
		$custom_css_sm_output .= $custom_css_sm . "\r\n";
		$custom_css_sm_output .= '}' . "\r\n";

		echo $custom_css_sm_output;
	}

	$custom_css_md = startuply_option( 'custom_css_md', '');
	$custom_css_md_output = '';

	if ( strlen($custom_css_md) ) {
		$custom_css_md_output .= '@media (min-width: 992px) and (max-width: 1199px) {' . "\r\n";
		$custom_css_md_output .= $custom_css_md . "\r\n";
		$custom_css_md_output .= '}' . "\r\n";

		echo $custom_css_md_output;
	}

	$custom_css_lg = startuply_option( 'custom_css_lg', '');
	$custom_css_lg_output = '';

	if ( strlen($custom_css_lg) ) {
		$custom_css_lg_output .= '@media (min-width: 1200px) {' . "\r\n";
		$custom_css_lg_output .= $custom_css_lg . "\r\n";
		$custom_css_lg_output .= '}' . "\r\n";

		echo $custom_css_lg_output;
	}

	/* END BASE COLOR */

	$output = ob_get_clean(); // Get generated CSS (output buffering)

	return $output;
}


function remove_base_url($path) { // remove base url to fix https warning when using images with http://.. url
	$url_parts = parse_url($path);
	return $url_parts['path'];
}

?>
