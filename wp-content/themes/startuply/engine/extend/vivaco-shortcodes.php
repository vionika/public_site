<?php
/*
Plugin Name: Vivaco Custom Shortcodes
Description: vivaco-shortcodes
Version: 2.2.0
Author: Vivaco (Alexander)
Author URI: http://vivaco.com
*/

// Hex 2 RGB values
function hex2rgb($hex) {
	$hex = str_replace("#", "", $hex);

	if (strlen($hex) == 3) {
		$r = hexdec(substr($hex, 0, 1) . substr($hex, 0, 1));
		$g = hexdec(substr($hex, 1, 1) . substr($hex, 1, 1));
		$b = hexdec(substr($hex, 2, 1) . substr($hex, 2, 1));
	} else {
		$r = hexdec(substr($hex, 0, 2));
		$g = hexdec(substr($hex, 2, 2));
		$b = hexdec(substr($hex, 4, 2));
	}

	$rgb = array(
		$r,
		$g,
		$b
	);
	return implode(",", $rgb); // returns the rgb values separated by commas
	// return $rgb; // returns an array with the rgb values
}

//setting a random id
if (!function_exists('vsc_random_id')) {
	function vsc_random_id($id_length) {
		$random_id_length = $id_length;
		$rnd_id = crypt(uniqid(rand(), 1), 'DlbkWT*ZQ*_jpORJ*PwokopPlY+NR|frcgz%+WCx|RZPkq*IbO56hQ1o9*b2'); // on php 5.6 crypt without or with simple salt parameter generate E_NOTICE
		$rnd_id = strip_tags(stripslashes($rnd_id));
		$rnd_id = str_replace(".", "", $rnd_id);
		$rnd_id = strrev(str_replace("/", "", $rnd_id));
		$rnd_id = str_replace(range(0, 9), "", $rnd_id);
		$rnd_id = substr($rnd_id, 0, $random_id_length);
		$rnd_id = strtolower($rnd_id);

		return $rnd_id;
	}
}
/*
function vivaco_shortcodes_image_sizes() {
	add_image_size('blog-thumb', 700, 700, false); // Blog thumbnails
	add_image_size('gallery-thumb', 1120, 9999, false); // Gallery thumbnails
}
add_action('init', 'vivaco_shortcodes_image_sizes');
*/


function extend_composer_classes() {
// Content slider
	define( 'CONTENT_SLIDE_TITLE', __( "Content Slide", "vivaco" ) );
	require_once vc_path_dir('SHORTCODES_DIR', 'vc-tabs.php');
	class WPBakeryShortCode_VSC_Content_Slider extends WPBakeryShortCode_VC_Tabs {
		protected $predefined_atts = array(
			'tab_id' => CONTENT_SLIDE_TITLE,
			'title' => ''
		);

		protected function getFileName() {
			return 'vsc_content_slider';
		}

		public function getTabTemplate() {
			return '<div class="wpb_template">' . do_shortcode( '[vc_tab title="' . CONTENT_SLIDE_TITLE . '" tab_id=""][/vc_tab]' ) . '</div>';
		}
	}
}
add_action( 'vc_before_init', 'extend_composer_classes' );

function extend_composer() {
	if (class_exists('WPBakeryVisualComposer')) {

		if (function_exists('vc_map')) {

			// Change default VC attributes.
			$attributes = array(
				"type" => "textfield",
				"heading" => __( "Image size", "vivaco" ),
				"param_name" => "img_size",
				"description" => __( "Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use \"thumbnail\" size.", "vivaco" ),
				"value" => "full"
			);

			vc_remove_param( "vc_gallery", "img_size" );
			vc_remove_param( "vc_single_image", "img_size" );
			vc_add_param( "vc_gallery", $attributes);
			vc_add_param( "vc_single_image", $attributes);

			vc_remove_param("vc_toggle", 'style');
			vc_remove_param("vc_toggle", 'color');
			vc_remove_param("vc_toggle", 'size');


			vc_remove_param( "vc_column_text", "css_animation" );
			vc_add_param("vc_column_text", array(
				'type' => 'colorpicker',
				'group' => 'Change color',
				'heading' => __('Text Color', 'vivaco'),
				'param_name' => 'text_color'
			));

			if ( shortcode_exists( 'contact-form-7' ) ) {
				// add new own contact form 7
				// Update Contact forms, for number forms > 5
				$cf7 = get_posts( 'post_type="wpcf7_contact_form"&posts_per_page=-1&orderby=title&order=ASC' );
				$contact_forms = array();
				if ( $cf7 ) {
					foreach ( $cf7 as $cform ) {
						$contact_forms[ $cform->post_title ] = $cform->ID;
					}
				} else {
					$contact_forms[ __( 'No contact forms found', 'js_composer' ) ] = 0;
				}


				$api_key = startuply_option('vivaco_mailchimp_api_key', '');


				$params = array(
					/*
					array(
						'type' => 'textfield',
						'heading' => __( 'Form title', 'vivaco' ),
						'param_name' => 'title',
						'admin_label' => true,
						'description' => __( 'What text use as form title. Leave blank if no title is needed.', 'vivaco' )
					),*/
					array(
						'type' => 'dropdown',
						'heading' => __( 'Select contact form', 'vivaco' ),
						'param_name' => 'id',
						'value' => $contact_forms,
						'description' => __( 'Choose previously created contact form from the drop down list.', 'vivaco' )
					),
				);


				$params[] = array(
					"type" => "checkbox",
					"heading" => __("Show/Hide", "vivaco"),
					"param_name" => "_wpcf7_vsc_hide_after_send",
					"value" => array(
						__("Hide form on successful submit", "vivaco") => "yes"
					)
				);
				$params[] = array(
					"type" => "checkbox",
					"heading" => __("Redirect/Idle", "vivaco"),
					"param_name" => "_wpcf7_vsc_redirect_after_send",
					"value" => array(
						__("Redirect to another page on successful submit", "vivaco") => "yes"
					)
				);
				$params[] = array(
					"type" => "textfield",
					"heading" => __("Redirect url", "vivaco"),
					"param_name" => "_wpcf7_vsc_redirect_url",
					"admin_label" => true,
					"dependency" => array(
						"element" => "_wpcf7_vsc_redirect_after_send",
						"value" => "yes"
					),
					"description" => __("Please enter full page url with http://", "vivaco"),
				);

				if( !$api_key ) {
					$params[] = array(
						'param_name' => 'custom_warning1', // all params must have a unique name
						'type' => 'custom_markup', // this param type
						'heading' => __( 'MailChimp Settings', 'vivaco' ),
						"dependency" => array(
							"element" => "vsc_use_mailchimp",
							"value" => "yes"
						),
						'value' => __( '<div class="alert alert-info">Please set "Mailchimp Api key" in Startuply options to use MailChimp shortcode functionality <a href="http://kb.mailchimp.com/accounts/management/about-api-keys" target="_blank">Where can i find my API key?</a></div>', 'vivaco' ), // your custom markup
					);
				} else {
					$params[] = array(
						"type" => "checkbox",
						"heading" => __("Mailchimp API", "vivaco"),
						"param_name" => "_wpcf7_vsc_use_mailchimp",
						"value" => array(
							__("Enable Mailchimp for this form", "vivaco") => "yes"
						)
					);
					$params[] = array(
						"type" => "textfield",
						"heading" => __("MailChimp List ID", "vivaco"),
						"param_name" => "_wpcf7_vsc_mailchimp_list_id",
						"admin_label" => true,
						"dependency" => array(
							"element" => "_wpcf7_vsc_use_mailchimp",
							"value" => "yes"
						),
						"description" => __("Enter MailChimp List ID here. <a href=\"http://kb.mailchimp.com/lists/managing-subscribers/find-your-list-id\" target=\"_blank\">Where can i find my List ID?</a>", "vivaco"),
					);
					$params[] = array(
						"type" => "checkbox",
						"heading" => __("Double opt-in", "vivaco"),
						"param_name" => "_wpcf7_vsc_double_opt",
						"dependency" => array(
							"element" => "_wpcf7_vsc_use_mailchimp",
							"value" => "yes"
						),
						"value" => array(
							__("Enable Mailchimp Double Opt-in", "vivaco") => "yes"
						),
						"description" => __("What is <a href=\"http://kb.mailchimp.com/lists/signup-forms/the-double-opt-in-process\" target=\"_blank\">Double Opt-in</a> used for?", "vivaco"),
					);
				}


				vc_map( array(
					'base' => 'contact-form-7-wrapper',
					'name' => __( 'Form Manager', 'vivaco' ),
					'icon' => 'icon-wpb-contactform7',
					'category' => __( 'Content', 'vivaco' ),
					'weight' => 18,
					'description' => __( 'Contact 7 form controls', 'vivaco' ),
					'params' => $params
				) );
			}

			// Custom Map
			vc_map(array(
				"name" => __("Row", "vivaco"),
				"base" => "vc_row",
				"is_container" => true,
				"icon" => "icon-wpb-row",
				"weight" => 100,
				"show_settings_on_create" => true,
				"category" => __("Content", "vivaco"),
				"description" => __("Main content wrapper", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __("Anchor ID", "vivaco"),
						"param_name" => "vsc_id",
						"description" => __("Set an ID if you want to link this section with one page scrolling navigation e.g.: team", "vivaco")
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Row stretch', 'js_composer' ),
						'param_name' => 'full_width',
						'value' => array(
							__('Default (Boxed)','js_composer') => '',
							__('Stretch row (100% width)','js_composer') => 'stretch_row',
							__('Stretch row and content','js_composer') => 'stretch_row_content',
							__('Stretch row and content without spaces','js_composer') => 'stretch_row_content_no_spaces',
						),
						'description' => __( 'This controls the width of the row and contents. Fullscreen rows are only allowed in Fullscreen page template <a target="_blank" class="help" href="https://wpbakery.atlassian.net/wiki/pages/viewpage.action?pageId=3604483">?</a>', 'js_composer' )
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Height options", "vivaco" ),
						"param_name" => "options",
						"value" => array(
							__( "100% window height", "vivaco" ) => "window_height",
							__( "Vertical centering", "vivaco" ) => "centered",
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __('Text color scheme', 'vivaco'),
						"param_name" => "vsc_text_scheme",
						"description" => __("'Light Text' looks good on dark backgrounds while 'Dark Text' looks good on light backgrounds", "vivaco"),
						"value" => array(
							__("Dark Text", 'vivaco') => 'lighter-overlay',
							__("Light Text", 'vivaco') => 'darker-overlay'
						)
					),
					//compatibility fix, removes the type value on save
					array(
						"type" => "holder",
						"param_name" => "vsc_row_type",
						"value" => array(
							__("Default", 'vivaco') => ''
						)
					),
					//compatibility fix, removes the type value on save
					array(
						"type" => "holder",
						"param_name" => "bg_image",
						"value" => array(
							__("Default", 'vivaco') => ''
						)
					),
					//compatibility fix, removes the type value on save
					array(
						"type" => "holder",
						"param_name" => "bg_color",
						"value" => array(
							__("Default", 'vivaco') => ''
						)
					),
					array(
						"type" => "attach_image",
						"heading" => __("Background image", "vivaco"),
						"param_name" => "vsc_bg_image",
						"description" => __("Select rows backgound image", "vivaco")
					),
					array(
						"type" => "checkbox",
						"heading" => __("Use parallax?", "vivaco"),
						"param_name" => "vsc_parallax",
						"value" => array(
							__("Yes, please", "vivaco") => "yes"
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __('Background overlay color', 'vivaco'),
						"param_name" => "vsc_bg_color",
						"description" => __("Background color overlay can be placed on top of background image or used separately", "vivaco")
					)
					,
					array(
						"type" => "textarea",
						"heading" => __('Background overlay gradient', 'vivaco'),
						"param_name" => "vsc_bg_gradient",
						"value" => '',
						"description" => __("Put awesome gradient as an overlay, generate yours at <a target=\"blank\" href=\"http://www.cssmatic.com/gradient-generator\">CSSMatic</a> and just copy-paste the code here!", "vivaco")
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background repeat', 'vivaco'),
						"param_name" => "vsc_bg_repeat",
						"value" => array(
							__('No Repeat', 'vivaco') => 'no-repeat',
							__("Repeat", 'vivaco') => 'repeat',
							__('Repeat-X', 'vivaco') => 'repeat-x',
							__("Repeat-Y", 'vivaco') => 'repeat-y'
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background position', 'vivaco'),
						"param_name" => "vsc_bg_position",
						"value" => array(
							__("Center Center", 'vivaco') => 'center center',
							__("Center Left", 'vivaco') => 'center left',
							__("Center Right", 'vivaco') => 'center right',
							__("Top Center", 'vivaco') => 'top center',
							__('Top Left', 'vivaco') => 'top left',
							__('Top Right', 'vivaco') => 'top right',
							__('Bottom Center', 'vivaco') => 'bottom center',
							__('Bottom Left', 'vivaco') => 'bottom left',
							__('Bottom Right', 'vivaco') => 'bottom right'
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background size', 'vivaco'),
						"param_name" => "vsc_bg_size",
						"value" => array(
							__("Cover", 'vivaco') => 'cover',
							__("Default", 'vivaco') => 'auto',
							__("Contain", 'vivaco') => 'contain'
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "vivaco"),
						"param_name" => "vsc_class",
						"description" => __("Additional class that you can add custom styles to", "vivaco")
					),
					array(
						'type' => 'css_editor',
						'heading' => __( 'Css', 'js_composer' ),
						'param_name' => 'css',
						'group' => __( 'Padding & Margins', 'js_composer' )
					),
					array(
						"type" => "textfield",
						"heading" => __('YouTube video background URL', 'vivaco'),
						"param_name" => "vsc_youtube_url",
						"description" => __("Don't forget to add an 'Anchor ID' above and a 'Background image'. The background image will be used as a cover, for devices that doesn`t play videos automatically (mobile and tablets)", "vivaco"),
						'group' => __( 'Video background', 'js_composer' )
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Video Options", "vivaco" ),
						"param_name" => "vsc_youtube_options",
						"value" => array(
							__( "Disable autoplay", "vivaco" ) => "autoplay",
							__( "Disable sound on load", "vivaco" ) => "sound"
						),
						"dependency" => array(
							"element" => "vsc_youtube_url",
							"not_empty" => true
						),
						'group' => __( 'Video background', 'js_composer' )
					),
					array(
						"type" => "dropdown",
						"heading" => __('YouTube video controls position', 'vivaco'),
						"param_name" => "vsc_youtube_controls",
						"description" => __("Video controls position.", "vivaco"),
						"value" => array(
							__("Left", "vivaco") => "left",
							__("Center", "vivaco") => "center",
							__("Right", "vivaco") => "right",
							__("Disabled", "vivaco") => 'none',
						),
						"dependency" => array(
							"element" => "vsc_youtube_url",
							"not_empty" => true
						),
						'group' => __( 'Video background', 'js_composer' )
					)
				),
				"js_view" => 'VcRowView'
			));

			vc_map( array(
				'name' => __( 'Row', 'js_composer' ), //Inner Row
				'base' => 'vc_row_inner',
				'content_element' => false,
				'is_container' => true,
				'icon' => 'icon-wpb-row',
				'weight' => 1000,
				'show_settings_on_create' => false,
				'description' => __( 'Place content elements inside the row', 'js_composer' ),
				'params' => array(
					array(
						"type" => "colorpicker",
						"heading" => __('Background color', 'vivaco'),
						"param_name" => "vsc_bg_color",
						"description" => __("Background color overlay can be placed on top of background image or used separately", "vivaco")
					),
					array(
						"type" => "attach_image",
						"heading" => __("Background image", "vivaco"),
						"param_name" => "vsc_bg_image",
						"description" => __("Select rows backgound image", "vivaco")
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background repeat', 'vivaco'),
						"param_name" => "vsc_bg_repeat",
						"value" => array(
							__('No Repeat', 'vivaco') => 'no-repeat',
							__("Repeat", 'vivaco') => 'repeat',
							__('Repeat-X', 'vivaco') => 'repeat-x',
							__("Repeat-Y", 'vivaco') => 'repeat-y'
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background position', 'vivaco'),
						"param_name" => "vsc_bg_position",
						"value" => array(
							__("Center Center", 'vivaco') => 'center center',
							__("Center Left", 'vivaco') => 'center left',
							__("Center Right", 'vivaco') => 'center right',
							__("Top Center", 'vivaco') => 'top center',
							__('Top Left', 'vivaco') => 'top left',
							__('Top Right', 'vivaco') => 'top right',
							__('Bottom Center', 'vivaco') => 'bottom center',
							__('Bottom Left', 'vivaco') => 'bottom left',
							__('Bottom Right', 'vivaco') => 'bottom right'
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __('Background size', 'vivaco'),
						"param_name" => "vsc_bg_size",
						"value" => array(
							__("Cover", 'vivaco') => 'cover',
							__("Default", 'vivaco') => 'auto',
							__("Contain", 'vivaco') => 'contain'
						)
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Height", "vivaco" ),
						"param_name" => "height",
						"value" => array(
							__( "100% container height", "vivaco" ) => "window_height",
						)
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Extra class name', 'js_composer' ),
						'param_name' => 'vsc_class',
						'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
					),
					array(
						'type' => 'css_editor',
						'heading' => __( 'Css', 'js_composer' ),
						'param_name' => 'css',
						// 'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' ),
						'group' => __( 'Padding & Margins', 'js_composer' )
					)
				),
				'js_view' => 'VcRowView'
			) );

			vc_map(array(
				"name" => __("Column", "js_composer"),
				"base" => "vc_column",
				"class" => "",
				"icon" => "",
				"wrapper_class" => "",
				"controls" => "full",
				"content_element" => false,
				"is_container" => true,
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "js_composer"),
						"param_name" => "el_class",
						"value" => "",
						"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
					),
					array(
						"type" => "dropdown",
						"heading" => __("Column alignment", "vivaco"),
						"param_name" => "el_align",
						"value" => array(
							__("None", "vivaco") => "",
							__("Left", "vivaco") => 'alignleft',
							__("Center", "vivaco") => "aligncenter",
							__("Right", "vivaco") => "alignright"
						)
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Height", "vivaco" ),
						"param_name" => "height",
						"value" => array(
							__( "100% container height", "vivaco" ) => "full_height",
						)
					),
					array(
						"type" => "css_editor",
						"heading" => __('Css', "js_composer"),
						"param_name" => "css",
						// "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer"),
						"group" => __('Padding & Margins', 'js_composer')
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Width', 'js_composer' ),
						'param_name' => 'width',
						'value' => array(
							__('1 column - 1/12', 'js_composer') => '1/12',
							__('2 columns - 1/6', 'js_composer') => '1/6',
							__('3 columns - 1/4', 'js_composer') => '1/4',
							__('4 columns - 1/3', 'js_composer') => '1/3',
							__('5 columns - 5/12', 'js_composer') => '5/12',
							__('6 columns - 1/2', 'js_composer') => '1/2',
							__('7 columns - 7/12', 'js_composer') => '7/12',
							__('8 columns - 2/3', 'js_composer') => '2/3',
							__('9 columns - 3/4', 'js_composer') => '3/4',
							__('10 columns - 5/6', 'js_composer') => '5/6',
							__('11 columns - 11/12', 'js_composer') => '11/12',
							__('12 columns - 1/1', 'js_composer') => '1/1'
						),
						'group' => __( 'Width & Responsiveness', 'js_composer' ),
						'description' => __( 'Select column width.', 'js_composer' ),
						'std' => '1/1'
					),
					array(
						'type' => 'column_offset',
						'heading' => __( 'Responsiveness', 'js_composer' ),
						'param_name' => 'offset',
						'group' => __( 'Width & Responsiveness', 'js_composer' ),
						'description' => __( 'Adjust column for different screen sizes. Control width, offset and visibility settings.', 'js_composer' )
					)
				),
				"js_view" => 'VcColumnView'
			));

			vc_map( array(
				"name" => __( "Column", "js_composer" ),
				"base" => "vc_column_inner",
				"class" => "",
				"icon" => "",
				"wrapper_class" => "",
				"controls" => "full",
				"allowed_container_element" => false,
				"content_element" => false,
				"is_container" => true,
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __( "Extra class name", "js_composer" ),
						"param_name" => "el_class",
						"value" => "",
						"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" )
					),
					array(
						"type" => "dropdown",
						"heading" => __("Column alignment", "vivaco"),
						"param_name" => "el_align",
						"value" => array(
							__("None", "vivaco") => "",
							__("Left", "vivaco") => 'alignleft',
							__("Center", "vivaco") => "aligncenter",
							__("Right", "vivaco") => "alignright"
						)
					),
					array(
						"type" => "css_editor",
						"heading" => __( 'Css', "js_composer" ),
						"param_name" => "css",
						// "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer"),
						"group" => __( 'Padding & Margins', 'js_composer' )
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Width', 'js_composer' ),
						'param_name' => 'width',
						'value' => array(
							__('1 column - 1/12', 'js_composer') => '1/12',
							__('2 columns - 1/6', 'js_composer') => '1/6',
							__('3 columns - 1/4', 'js_composer') => '1/4',
							__('4 columns - 1/3', 'js_composer') => '1/3',
							__('5 columns - 5/12', 'js_composer') => '5/12',
							__('6 columns - 1/2', 'js_composer') => '1/2',
							__('7 columns - 7/12', 'js_composer') => '7/12',
							__('8 columns - 2/3', 'js_composer') => '2/3',
							__('9 columns - 3/4', 'js_composer') => '3/4',
							__('10 columns - 5/6', 'js_composer') => '5/6',
							__('11 columns - 11/12', 'js_composer') => '11/12',
							__('12 columns - 1/1', 'js_composer') => '1/1'
						),
						'group' => __( 'Width & Responsiveness', 'js_composer' ),
						'description' => __( 'Select column width.', 'js_composer' ),
						'std' => '1/1'
					)
				),
				"js_view" => 'VcColumnView'
			) );

			/* Separator */
			vc_map(array(
				'name' => __('Separator', 'vivaco'),
				'base' => 'vc_separator',
				'icon' => 'icon-wpb-ui-separator',
				'show_settings_on_create' => false,
				'category' => __('Content', 'vivaco'),
				//"controls"	=> 'popup_delete',
				'description' => __('Horizontal separator line', 'vivaco'),
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __('Color', 'vivaco'),
						'param_name' => 'color',
						'value' => getVcShared('colors'),
						'std' => 'grey',
						'description' => __('Separator color.', 'vivaco'),
						'param_holder_class' => 'vc-colored-dropdown'
					),
					array(
						'type' => 'colorpicker',
						'heading' => __('Custom Border Color', 'wpb'),
						'param_name' => 'accent_color',
						'description' => __('Select border color for your element.', 'wpb')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Style', 'vivaco'),
						'param_name' => 'style',
						'value' => getVcShared('separator styles'),
						'description' => __('Separator style.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Element width', 'vivaco'),
						'param_name' => 'el_width',
						'value' => getVcShared('separator widths'),
						'description' => __('Separator element width in percents.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Extra class name', 'vivaco'),
						'param_name' => 'el_class',
						'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vivaco')
					)
				)
			));

			vc_map(array(
				'name' => __('Tab', 'vivaco'),
				'base' => 'vc_tab',
				'allowed_container_element' => 'vc_row',
				'is_container' => true,
				'content_element' => false,
				'params' => array(
					array(
						'type' => 'tab_id',
						'heading' => __('Tab ID', 'vivaco'),
						'param_name' => "tab_id"
					),
					array(
						'type' => 'textfield',
						'heading' => __('Title', 'vivaco'),
						'param_name' => 'title'
					),
					array(
						"type" => "textfield",
						"holder" => "div",
						"heading" => __("Subtitle", "vivaco"),
						"param_name" => "subtitle",
						"value" => ""
					)
				),
				'js_view' => 'VcTabView'
			));


			// Button
			vc_map(array(
				"name" => __("Button", "vivaco"),
				"base" => "vsc-button",
				"weight" => 10,
				"icon" => "icon-buttons",
				"description" => "Eye catching button",
				"class" => "buttons_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Text on the button", "vivaco"),
						"param_name" => "text",
						"value" => "Click me"
					),
					array(
						"type" => "textfield",
						"heading" => __("URL(Link)", "vivaco"),
						"param_name" => "url"
					),
					array(
						"type" => "dropdown",
						"heading" => __("Target", "vivaco"),
						"param_name" => "target",
						"value" => array(
							__("Opens the link in the same window", "vivaco") => '',
							__("Opens the link in a new window", "vivaco") => "yes"
						),
						"description" => __("Set the target of the link", "vivaco")
					),
					array(
						"type" => "dropdown",
						"heading" => __("Button style", "vivaco"),
						"param_name" => "style",
						"value" => array(
							__("Solid color button", "vivaco") => 'btn-solid',
							__("White outline button", "vivaco") => "btn-outline",
							__("Color outline button", "vivaco") => "btn-outline-color",
							__("Clear, no border", "vivaco") => "btn-no-border"
						)
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Button alignment', 'js_composer' ),
						'param_name' => 'align',
						'description' => __( 'Select button alignment.', 'js_comopser' ),
						// compatible with btn2, default left to be compatible with btn1
						'value' => array(
							__( 'Inline', 'js_composer' ) => 'inline',
							// default as well
							__( 'Left', 'js_composer' ) => 'left',
							// default as well
							__( 'Right', 'js_composer' ) => 'right',
							__( 'Center', 'js_composer' ) => 'center'
						),
					),
					array(
						'type' => 'checkbox',
						'heading' => __( 'Add icon?', 'js_composer' ),
						'param_name' => 'add_icon',
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						'dependency' => array(
						'element' => 'add_icon',
						'value' => 'true',
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon Alignment', 'js_composer' ),
						'description' => __( 'Select icon alignment.', 'js_composer' ),
						'param_name' => 'i_align',
						'value' => array(
							__( 'Left', 'js_composer' ) => 'pull-left',
							// default as well
							__( 'Right', 'js_composer' ) => 'pull-right',
						),
						'dependency' => array(
							'element' => 'add_icon',
							'value' => 'true',
						),
					),
					array(
						"type" => "dropdown",
						"heading" => __("Button size", "vivaco"),
						"param_name" => "size",
						"value" => array(
							__("Medium", "vivaco") => "",
							__("Small", "vivaco") => "btn-sm",
							__("Big", "vivaco") => "btn-lg"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Text size", "vivaco"),
						"description" => __("Button text size in px", "vivaco"),
						"param_name" => "text_size",
						"value" => ''
					),
					array(
						"type" => "textfield",
						"heading" => __("Icon size", "vivaco"),
						"description" => __("Icon text size in px", "vivaco"),
						"param_name" => "i_size",
						"value" => ''
					),
					array(
						'type' => 'textfield',
						'heading' => __('Extra class name', 'vivaco'),
						'param_name' => 'el_class',
						'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vivaco')
					),
					array(
						'type' => 'css_editor',
						'heading' => __( 'Css', 'js_composer' ),
						'param_name' => 'css',
						'group' => __( 'Padding & Margins', 'js_composer' )
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Main button color', 'vivaco'),
						'param_name' => 'color'
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Text color', 'vivaco'),
						'param_name' => 'text_color'
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Icon color', 'vivaco'),
						'param_name' => 'i_color'
					),
				)
			));

			/* Button 2 */
			vc_map(array(
				'name' => __('Button', 'vivaco') . " 2",
				'base' => 'vc_button2',
				'icon' => 'icon-wpb-ui-button',
				'category' => __("Content", "vivaco"),
				'description' => __('Eye catching button', 'vivaco'),
				'params' => array(
					array(
						'type' => 'vc_link',
						'heading' => __('URL (Link)', 'vivaco'),
						'param_name' => 'link',
						'description' => __('Button link.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Text on the button', 'vivaco'),
						'holder' => 'button',
						'class' => 'wpb_button',
						'param_name' => 'title',
						'value' => __('Text on the button', 'vivaco'),
						'description' => __('Text on the button.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Style', 'vivaco'),
						'param_name' => 'style',
						'value' => getVcShared('button styles'),
						'description' => __('Button style.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Color', 'vivaco'),
						'param_name' => 'color',
						'value' => getVcShared('colors'),
						'description' => __('Button color.', 'vivaco'),
						'param_holder_class' => 'vc-colored-dropdown'
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Size', 'vivaco'),
						'param_name' => 'size',
						'value' => getVcShared('sizes'),
						'std' => 'md',
						'description' => __('Button size.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Extra class name', 'vivaco'),
						'param_name' => 'el_class',
						'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vivaco')
					)
				)
			));

			/*
			vc_map(array(
				"name" => __("Space", "vivaco"),
				"icon" => "icon-ui-splitter-horizontal",
				"base" => "vsc-space",
				"weight" => 21,
				"description" => "Add space between elements",
				"class" => "space_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Height of the space(px)", "vivaco"),
						"param_name" => "height",
						"value" => 60,
						"description" => __("Set height of the space. You can add white space between elements to separate them beautifully", "vivaco")
					)
				)
			));
			*/

			vc_map(array(
				"name" => __("List", "vivaco"),
				"icon" => "icon-list",
				"description" => "List element with icon style",
				"base" => "vsc-list",
				"weight" => 15,
				"class" => "list_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textarea_html",
						"holder" => "div",
						"class" => "",
						"heading" => __("List Rows", "vivaco"),
						"param_name" => "content",
						"value" => "<ul class=\"customlist\"><li>Lorem ipsum</li><li>Consectetur adipisicing</li><li>Ullamco laboris</li><li>Quis nostrud exercitation</li>",
						"description" => __("Create your list using the WordPress default functionality", "vivaco")
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => __("Extra class name", "vivaco"),
						"param_name" => "el_class"
					),
				)
			));

			vc_map(array(
				"name" => __("Quote", "vivaco"),
				"weight" => 16,
				"base" => "vsc-quote",
				"icon" => "icon-quote",
				"description" => "A dose of inspiration for visitors",
				"class" => "quote_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textarea",
						"class" => "",
						"admin_label" => true,
						"heading" => __("Quote text", "vivaco"),
						"param_name" => "text"
					),
					array(
						"type" => "textfield",
						"class" => "",
						"heading" => __("Quote Author", "vivaco"),
						"param_name" => "author"
					)
				)
			));

			vc_map(array(
				"name" => __("Section title", "vivaco"),
				"icon" => "icon-section-title",
				"weight" => 50,
				"base" => "vsc-section-title",
				"class" => "title_extended",
				"description" => "Set a title and subtitle with style",
				"category" => __("Content", "vivaco"),
				"params" => array(

					array(
						"type" => "textfield",
						"holder" => "div",
						"heading" => __("Title", "vivaco"),
						"param_name" => "title"
					),
					array(
						"type" => "textarea_html",
						"holder" => "div",
						"heading" => __("Subtitle", "vivaco"),
						"param_name" => "content",
						"description" => "*optional"
					),
					array(
						"type" => "dropdown",
						"heading" => __("Text alignment", "vivaco"),
						"param_name" => "align",
						"value" => array(
							__("Auto", "vivaco") => "inherit",
							__("Left", "vivaco") => 'left',
							__("Center", "vivaco") => "center",
							__("Right", "vivaco") => "right"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Section type", "vivaco"),
						"param_name" => "size",
						"value" => array(
							__("Medium", "vivaco") => "normal",
							__("Big", "vivaco") => "big",
							__("Small", "vivaco") => "small"
						),
						"description" => __("Default for section titles, Big for main website title and Small for paragraph titles", "vivaco")
					),
					array(
						"type" => "checkbox",
						"group" => "Custom fonts",
						"heading" => __("Use custom Google font?", "vivaco"),
						"param_name" => "use_google_font",
						"value" => array(
							__("Yes, please", "vivaco") => "yes"
						)
					),
					array(
						"type" => "google_fonts",
						"group" => "Custom fonts",
						"heading" => __("Title font", "vivaco"),
						"param_name" => "title_google_fonts",
						"settings" => array(
							"no_font_style" => true,
							"fields" => array(
								"font_family"=>"Lato",
								"font_family_description" => __("Select font family.","js_composer"),
								"font_style_description" => __("Select font styling.","js_composer")
							)
						),
						"dependency" => array(
							"element" => "use_google_font",
							"value" => "yes"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Custom title size", "vivaco"),
						"param_name" => "title_font_size",
						"description" => "*optional"
					),
					array(
						"type" => "google_fonts",
						"group" => "Custom fonts",
						"heading" => __("Subtitle font", "vivaco"),
						"param_name" => "subtitle_google_fonts",
						"settings" => array(
							"no_font_style" => true,
							"fields" => array(
								"font_family"=>"Lato",
								"font_family_description" => __("Select font family.","js_composer"),
								"font_style_description" => __("Select font styling.","js_composer")
							)
						),
						"dependency" => array(
							"element" => "use_google_font",
							"value" => "yes"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Custom subtitle size", "vivaco"),
						"param_name" => "subtitle_font_size",
						"description" => "*optional"
					),
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "vivaco"),
						"param_name" => "el_class",
						"description" => __("Additional class that you can add custom styles to", "vivaco")
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Title Color', 'vivaco'),
						'param_name' => 'title_color'
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Subtitle Color', 'vivaco'),
						'param_name' => 'subtitle_color'
					)
				)
			));

			// New VC Icon
			vc_map( array(
				'name' => __( 'Icon', 'js_composer' ),
				'base' => 'vc_icon',
				'icon' => 'icon-wpb-vc_icon',
				'category' => __( 'Content', 'js_composer' ),
				'description' => __( 'Icon from icon library', 'js_composer' ),
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __('Display style', 'vivaco'),
						'param_name' => 'display_style',
						'weight' => 1,
						'value' => array(
							__('Block','vivaco') => '',
							__('Inline','vivaco') => 'icon_inline',
						),
						"description" => __( "Set how the icon is displayed: <b>Block</b> - places icon on a new line <b>Inline</b> - renders icon next to other elements", "vivaco" ),
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),

					array(
						'type' => 'colorpicker',
						'heading' => __( 'Custom Icon Color', 'js_composer' ),
						'param_name' => 'custom_color',
						'description' => __( 'Select custom icon color.', 'js_composer' ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						'type' => 'colorpicker',
						'heading' => __( 'Custom Icon Background Color', 'js_composer' ),
						'param_name' => 'custom_background_color',
						'description' => __( 'Select custom icon background color.', 'js_composer' ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Background Style', 'js_composer' ),
						'param_name' => 'background_style',
						'value' => array(
							__( 'None', 'js_composer' ) => '',
							__( 'Circle', 'js_composer' ) => 'rounded',
							__( 'Square', 'js_composer' ) => 'boxed',
							__( 'Rounded', 'js_composer' ) => 'rounded-less',
							__( 'Outline Circle', 'js_composer' ) => 'rounded-outline',
							__( 'Outline Square', 'js_composer' ) => 'boxed-outline',
							__( 'Outline Rounded', 'js_composer' ) => 'rounded-less-outline',
						),
						'description' => __( 'Background style for icon.', 'js_composer' )
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Size', 'js_composer' ),
						'param_name' => 'size',
						'value' => '',
						'std' => '',
						'description' => __( 'Icon size.', 'js_composer' )
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon alignment', 'js_composer' ),
						'param_name' => 'align',
						'value' => array(
							__( 'Align left', 'js_composer' ) => 'left',
							__( 'Align right', 'js_composer' ) => 'right',
							__( 'Align center', 'js_composer' ) => 'center',
						),
						'description' => __( 'Select icon alignment.', 'js_composer' ),
					),
					array(
						'type' => 'vc_link',
						'heading' => __( 'URL (Link)', 'js_composer' ),
						'param_name' => 'link',
						'description' => __( 'Add link to icon.', 'js_composer' )
					),
					array(
						'type' => 'textfield',
						'heading' => __( 'Extra class name', 'js_composer' ),
						'param_name' => 'el_class',
						'description' => __( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'js_composer' )
					),

				),
				'js_view' => 'VcIconElementView_Backend',
			) );

			// Text with Icon
			vc_map(array(
				"name" => __("Text with Icon", "vivaco"),
				"base" => "vsc-text-icon",
				"weight" => 11,
				"icon" => "icon-for-twi",
				"description" => "Text block with eye-catching icon",
				"class" => "twi_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Title", "vivaco"),
						"param_name" => "title"
					),
					array(
						"type" => "textarea_html",
						"heading" => __("Text", "vivaco"),
						"param_name" => "content"
					),
					array(
						"type" => "dropdown",
						"heading" => __("Media Type", "vivaco"),
						"param_name" => "media_type",
						"value" => array(
							__("Font Icon", "vivaco") => "icon-type",
							__("Standard Image", "vivaco") => "img-type"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Icon and text align", "vivaco"),
						"param_name" => "align",
						"value" => array(
							__("Top", "vivaco") => "top",
							__("Left", "vivaco") => "left",
							__("Right", "vivaco") => "right",
							__("Bottom", "vivaco") => "bottom"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Icon Type", "vivaco"),
						"param_name" => "icon_type",
						"dependency" => array(
							'element' => "media_type",
							'value' => "icon-type"
						),
						"value" => array(
							__("Single Icon", "vivaco") => "single_icon",
							__("Solid Shape", "vivaco") => "solid_icon",
							__("Border Shape", "vivaco") => "border_icon"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Icon Shape", "vivaco"),
						"param_name" => "icon_shape",
						"dependency" => array(
							"element" => "icon_type",
							"value" => array("solid_icon", "border_icon")
						),
						"value" => array(
							__("Round", "vivaco") => "round_shape",
							__("Square", "vivaco") => "square_shape",
							__("Round corner Square", "vivaco") => "rounded_square_shape"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Icon Border Style", "vivaco"),
						"param_name" => "icon_border",
						"dependency" => array(
							"element" => "icon_type",
							"value" => "border_icon"
						),
						"value" => array(
							__("Solid", "vivaco") => "solid_border",
							__("Dashed", "vivaco") => "dashed_border",
							__("Dotted", "vivaco") => "dotted_border"
						)
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						'dependency' => array(
							"element" => "media_type",
							"value" => "icon-type"
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),

					array(
						"type" => "textfield",
						"heading" => __("Custom icon size", "vivaco"),
						"param_name" => "icon_size",
						"value" => "",
						"dependency" => array(
							"element" => "media_type",
							"value" => "icon-type"
						),
						"description" => __("Font-size of icon", "vivaco")
					),
					array(
						"type" => "attach_image",
						"heading" => __("Image", "vivaco"),
						"param_name" => "img",
						"dependency" => array(
							"element" => "media_type",
							"value" => "img-type"
						),
						"description" => __("Upload an image for the widget", "vivaco")
					),
					array(
						"type" => "colorpicker",
						"heading" => __("Title Color", "vivaco"),
						"param_name" => "title_color",
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "title",
							"not_empty" => true
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __("Text Color", "vivaco"),
						"param_name" => "text_color",
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "content",
							"not_empty" => true
						)
					),
					//content
					array(
						"type" => "colorpicker",
						"heading" => __("Icon Background Color", "vivaco"),
						"param_name" => "icon_bg_color",
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "icon_type",
							"value" => "solid_icon"
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __("Icon Border Color", "vivaco"),
						"param_name" => "icon_bd_color",
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "icon_type",
							"value" => array( "solid_icon", "border_icon" )
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __("Icon Color", "vivaco"),
						"param_name" => "icon_color",
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "media_type",
							"value" => "icon-type"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "vivaco"),
						"param_name" => "el_class",
					)
				)
			));

			// Canvas Title
			vc_map(array(
				"name" => __("Canvas Title", "vivaco"),
				"base" => "vc_custom_heading",
				"weight" => 10,
				"icon" => "icon-for-canvas-title",
				"description" => "Custom heading with background",
				"class" => "canvas_title",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Text", "vivaco"),
						"param_name" => "text"
					),
					array(
						"type" => "textfield",
						"heading" => __("Font size", "vivaco"),
						"param_name" => "font_size",
						"description" => "Enter font size",
						"value" => "30px"
					),
					array(
						"type" => "dropdown",
						"heading" => __("Element tag", "vivaco"),
						"param_name" => "tag_name",
						"description" => "Select element tag",
						"value" => array(
							__("h1", "vivaco") => "h1",
							__("h2", "vivaco") => "h2",
							__("h3", "vivaco") => "h3",
							__("h4", "vivaco") => "h4",
							__("h5", "vivaco") => "h5",
							__("h6", "vivaco") => "h6"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __("Text align", "vivaco"),
						"param_name" => "align",
						"description" => "Select text alignment.",
						"value" => array(
							__("center", "vivaco") => "center",
							__("left", "vivaco") => "left",
							__("right", "vivaco") => "right"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __("Line Height", "vivaco"),
						"param_name" => "line_height",
						"description" => "Enter line height",
						"value" => "30px"
					),
					array(
						"type" => "textfield",
						"heading" => __("Padding top", "vivaco"),
						"param_name" => "padding_top",
						"description" => "Enter padding from top."
					),
					array(
						"type" => "textfield",
						"heading" => __("Padding bottom", "vivaco"),
						"param_name" => "padding_bottom",
						"description" => "Enter padding from bottom."
					),
					array(
						"type" => "google_fonts",
						"param_name" => "google_fonts",
						"settings" => array(
							"no_font_style" => true,
							"fields" => array(
								"font_family"=>"Lato",
								"font_family_description" => __("Select font family.","js_composer"),
								"font_style_description" => __("Select font styling.","js_composer")
							)
						)
					),
					array(
						"type" => "attach_image",
						"heading" => __("Mask / background image", "vivaco"),
						"param_name" => "mask_img",
						"description" => ""
					),
					array(
						"type" => "checkbox",
						"heading" => __("Overlay", "vivaco"),
						"param_name" => "overlay",
						"value" => array(
							__("Check this box to enable background with overlay", "vivaco") => "yes"
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __("Overlay color", "vivaco"),
						"param_name" => "overlay_color",
						"description" => "",
						"dependency" => array(
							"element" => "overlay",
							"value" => "yes"
						)
					)
				)
			));

			// Progress Bar
			vc_map( array(
				"name" => __( "Progress Bar", "vivaco" ),
				"base" => "vc_progress_bar",
				"icon" => "icon-wpb-graph",
				"weight" => 15,
				"category" => __( "Content", "vivaco" ),
				"description" => __( "Animated progress bar", "vivaco" ),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __( "Progress bar title", "vivaco" ),
						"param_name" => "title",
						"description" => __( "Enter title of progress bar.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Value %", "vivaco" ),
						"param_name" => "value",
						"description" => __( "Value in percents", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Measurement units", "vivaco" ),
						"param_name" => "units",
						"description" => __( "Enter measurement units (if needed) Eg. %, px, points, etc. Value and unit will be appended to the progress bar count.", "vivaco" )
					),
					array(
						"type" => "dropdown",
						"heading" => __("Content position", "vivaco"),
						"param_name" => "position",
						"description" => "Select content position in bar.",
						"value" => array(
							__("Default", "vivaco") => "",
							__("Value inside", "vivaco") => "value_inside",
							__("All inside", "vivaco") => "all_inside"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __( "Bar height", "vivaco" ),
						"param_name" => "height",
						"description" => __( "Enter bar height in px.", "vivaco" ),
						"value" => 20
					),
					array(
						"type" => "textfield",
						"heading" => __( "Round corners", "vivaco" ),
						"param_name" => "round",
						"description" => __( "Enter round value in px for bar.", "vivaco" ),
						"value" => 0
					),
					array(
						"type" => "textfield",
						"heading" => __('Margin Bottom', "vivaco"),
						"param_name" => "margin_bottom"
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Bar custom color", "vivaco" ),
						"param_name" => "bgcolor",
						"description" => __( "Select custom background color for bars.", "vivaco" ),
						"group" => __("Change color", "vivaco")
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Options", "vivaco" ),
						"param_name" => "options",
						"value" => array(
							__( "Add Stripes?", "vivaco" ) => "striped",
							__( "Add Gradient?", "vivaco" ) => "gradient",
							__( "Disable Bar Animation?", "vivaco" ) => "no_animation",
							__( "Add animation for stripes", "vivaco" ) => "stripes_animation"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __( "Extra class name", "vivaco" ),
						"param_name" => "el_class",
						"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "vivaco" )
					)
				)
			) );

			// Pie Chart
			vc_map( array(
				"name" => __( "Pie chart", "vivaco" ),
				"base" => "vc_pie",
				"weight" => 15,
				"class" => "",
				"icon" => "icon-wpb-vc_pie",
				"category" => __( "Content", "vivaco" ),
				"description" => __( "Animated pie chart", "vivaco" ),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __( "Title", "vivaco" ),
						"param_name" => "title",
						"description" => __( "Enter text which will be used as title e.g.: \"Design\",\"CSS\", \"PHP\" etc.", "vivaco" ),
						"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => __( "Pie number", "vivaco" ),
						"param_name" => "label_value",
						"description" => __( "Input integer any value for title.", "vivaco" ),
						"value" => ""
					),
					array(
						"type" => "textfield",
						"heading" => __( "Measurement units", "vivaco" ),
						"param_name" => "units",
						"description" => __( "Enter measurement units (if needed) Eg. %, px, points, etc. Graph value and unit will be appended to the graph title.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Pie chart fill %", "vivaco" ),
						"param_name" => "value",
						"description" => __( "Graph fill value here, choose range between 0 - 100.", "vivaco" ),
						"value" => "50"
					),
					array(
						"type" => "textfield",
						"heading" => __( "Pie size (width)", "vivaco" ),
						"param_name" => "radius",
						"description" => __( "Enter custom radius in pixels.", "vivaco" )
					),

					array(
						"type" => "dropdown",
						"heading" => __( "Title position", "vivaco" ),
						"param_name" => "title_position",
						"dependency" => array(
							"element" => "inside_content",
							"value" => array("icon", "number")
						),
						"value" => array(
							__( "Bottom", "vivaco" ) => "bottom",
							__( "Inside", "vivaco" ) => "inside"
						)
					),
					array(
						"type" => "dropdown",
						"heading" => __( "Inside content", "vivaco" ),
						"param_name" => "inside_content",
						"value" => array(
							__( "Number", "vivaco" ) => "number",
							__( "Icon", "vivaco" ) => "icon",
							__( "Title", "vivaco" ) => "title"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __( "Thickness", "vivaco" ),
						"param_name" => "thickness",
						"description" => __( "Enter pie chart line thickness in pixels.", "vivaco" ),
						"value" => 6
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Options", "vivaco" ),
						"param_name" => "animation",
						"value" => array(
							__( "Remove animation?", "vivaco" ) => "no"
						)
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						'dependency' => array(
							'element' => 'inside_content',
							'value' => 'icon'
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),

					array(
						"type" => "textfield",
						"heading" => __( "Extra class name", "vivaco" ),
						"param_name" => "el_class",
						"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" )
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Bar color", "vivaco" ),
						"param_name" => "bar_color",
						"description" => __( "Select pie chart color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Title color", "vivaco" ),
						"param_name" => "title_color",
						"description" => __( "Select title color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "title",
							"not_empty" => true
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Icon color", "vivaco" ),
						"param_name" => "icon_color",
						"description" => __( "Select icon color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "inside_content",
							"value" => 'icon'
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Value color", "vivaco" ),
						"param_name" => "value_color",
						"description" => __( "Select value color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "inside_content",
							"value" => 'number'
						)
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Units color", "vivaco" ),
						"param_name" => "units_color",
						"description" => __( "Select units color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
						"dependency" => array(
							"element" => "inside_content",
							"value" => 'number'
						)
					)
				)
			) );

			// Counter
			vc_map( array(
				"name" => __( "Counter", "vivaco" ),
				"weight" => 14,
				"base" => "vsc-counter",
				"icon" => "icon-counter",
				"category" => __( "Content", "vivaco" ),
				"description" => __( "Animated counter with title", "vivaco" ),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __( "Counter number", "vivaco" ),
						"param_name" => "value",
						"description" => __( "Input value here.", "vivaco" ),
						"value" => "0",
						"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => __( "Counter text", "vivaco" ),
						"param_name" => "title",
						"description" => __( "Enter text which will be used as widget title. Leave blank if no title is needed.", "vivaco" ),
						"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => __( "Prefix", "vivaco" ),
						"param_name" => "units",
						"description" => __( "Enter measurement units (if needed) Eg. %, px, points, etc.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Number size", "vivaco" ),
						"param_name" => "val_font_size",
						"description" => __( "Enter font size of value with units 	in pixels (default - 100px).", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Text size", "vivaco" ),
						"param_name" => "title_font_size",
						"description" => __( "Enter title font size in pixels (default - 13px).", "vivaco" ),
						"dependency" => array(
							"element" => "title",
							"not_empty" => true
						),
					),
					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),

					array(
						"type" => "dropdown",
						"heading" => __( "Icon position", "vivaco" ),
						"param_name" => "icon_position",
						"dependency" => array(
							"element" => "type",
							"not_empty" => true
						),
						"value" => array(
							__( "Top", "vivaco" ) => "top",
							__( "Left", "vivaco" ) => "left"
						)
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Options", "vivaco" ),
						"param_name" => "options",
						"value" => array(
							__( "Remove animation?", "vivaco" ) => "no_animation",
							__( "Add separator?", "vivaco" ) => "separator"
						)
					),
					array(
						"type" => "textfield",
						"heading" => __( "Extra class name", "vivaco" ),
						"param_name" => "el_class",
						"description" => __( "If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer" )
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Text color", "vivaco" ),
						"param_name" => "title_color",
						"description" => __( "Select title color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Number color", "vivaco" ),
						"param_name" => "value_color",
						"description" => __( "Select value color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Prefix color", "vivaco" ),
						"param_name" => "units_color",
						"description" => __( "Select units color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
					),
					array(
						"type" => "colorpicker",
						"heading" => __( "Icon color", "vivaco" ),
						"param_name" => "icon_color",
						"description" => __( "Select icon color.", "vivaco" ),
						"group" => __("Change color", "vivaco"),
					)
				)
			) );

			// Team member
			vc_map( array(
				"name" => __( "Team member", "vivaco" ),
				"weight" => 20,
				"base" => "vsc-team-member-new",
				"icon" => "icon-team-member",
				"category" => __( "Content", "vivaco" ),
				"description" => __( "Team member shortcode", "vivaco" ),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __( "Full name", "vivaco" ),
						"param_name" => "name",
						"description" => __( "Enter name.", "vivaco" ),
						"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => __( "Company position", "vivaco" ),
						"param_name" => "position",
						"description" => __( "Enter company position.", "vivaco" ),
						"admin_label" => true
					),
					array(
						"type" => "textfield",
						"heading" => __( "Member description", "vivaco" ),
						"param_name" => "description",
						"description" => __( "Enter member description.", "vivaco" )
					),
					array(
						"type" => "attach_image",
						"heading" => __( "Photo", "vivaco" ),
						"param_name" => "photo",
						"description" => __( "Upload photo.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Email address", "vivaco" ),
						"param_name" => "email",
						"description" => __( "Enter email.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Twitter handle", "vivaco" ),
						"param_name" => "twitter",
						"description" => __( "Enter Twitter handle.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __( "Facebook URL", "vivaco" ),
						"param_name" => "facebook",
						"description" => __( "Enter Facebook URL.", "vivaco" )
					),
					array(
						"type" => "textfield",
						"heading" => __("Skype handle", "vivaco"),
						"param_name" => "skype",
						"description" => __("Enter Skype handle.", "vivaco")
					),
					array(
						"type" => "textfield",
						"heading" => __("LinkedIn URL", "vivaco"),
						"param_name" => "linked_in",
						"description" => __("Enter LinkedIn URL.", "vivaco")
					),
					array(
						"type" => "textfield",
						"heading" => __("Google+ URL", "vivaco"),
						"param_name" => "google_plus",
						"description" => __("Enter Google+ URL.", "vivaco")
					),
					array(
						"type" => "checkbox",
						"heading" => __( "Options", "vivaco" ),
						"param_name" => "options",
						"value" => array(
							__( "Magazine cover", "vivaco" ) => "magazine"
						)
					)
				)
			) );

			// Call to Action
			vc_map(array(
				"name" => __("Call to Action", "vivaco"),
				"base" => "vsc-call-to-action",
				"weight" => 12,
				"icon" => "icon-wpb-call-to-action",
				"description" => __("Catch visitors attention with CTA block", "vivaco"),
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"holder" => "div",
						"heading" => __("Main Title", "vivaco"),
						"param_name" => "title"
					),
					array(
						"type" => "textarea_html",
						"heading" => __("Additional text", "vivaco"),
						"param_name" => "content",
						"value" => ""
					),
					array(
						"type" => "dropdown",
						"heading" => __("Media Type", "vivaco"),
						"param_name" => "media_type",
						"value" => array(
							__("Font Icon", "vivaco") => 'icon-type',
							__("Standard Image", "vivaco") => "img-type"
						),
						"description" => __("Pick the media type you want to use for the widget. Icons from FontAwesome <a href='http://fontawesome.io/icons/'>icon list</a> and Line Icons <a href='http://www.startuplywp.com/line-icons.html'>icon list</a> can be used. Type in the icon name you want to use e.g: fa-bolt or icon-seo-icons-24. Standard Image - upload an image(jpg, png, etc.)", "vivaco")
					),

					array(
						'type' => 'dropdown',
						'heading' => __( 'Icon library', 'js_composer' ),
						'value' => array(
							__( 'Startuply Line Icons', 'vivaco' ) => 'startuplyli',
							__( 'Font Awesome', 'js_composer' ) => 'fontawesome',
							__( 'Open Iconic', 'js_composer' ) => 'openiconic',
							__( 'Typicons', 'js_composer' ) => 'typicons',
							__( 'Entypo', 'js_composer' ) => 'entypo',
							__( 'Linecons', 'js_composer' ) => 'linecons',
						),
						"dependency" => array(
							'element' => "media_type",
							'value' => 'icon-type'
						),
						'admin_label' => true,
						'param_name' => 'type',
						'description' => __( 'Select icon library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_startuplyli',
						'value' => 'icon icon-graphic-design-13', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'startuplyli',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'startuplyli',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_fontawesome',
						'value' => 'fa fa-adjust', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display, we use (big number) to display all icons in single page
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'fontawesome',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_openiconic',
						'value' => 'vc-oi vc-oi-dial', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'openiconic',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'openiconic',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_typicons',
						'value' => 'typcn typcn-adjust-brightness', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'typicons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'typicons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_entypo',
						'value' => 'entypo-icon entypo-icon-note', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'entypo',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'entypo',
						),
					),
					array(
						'type' => 'iconpicker',
						'heading' => __( 'Icon', 'js_composer' ),
						'param_name' => 'icon_linecons',
						'value' => 'vc_li vc_li-heart', // default value to backend editor admin_label
						'settings' => array(
							'emptyIcon' => false, // default true, display an "EMPTY" icon?
							'type' => 'linecons',
							'iconsPerPage' => 4000, // default 100, how many icons per/page to display
						),
						'dependency' => array(
							'element' => 'type',
							'value' => 'linecons',
						),
						'description' => __( 'Select icon from library.', 'js_composer' ),
					),

					/*
					array(
						"type" => "textfield",
						"heading" => __("Icon Name", "vivaco"),
						"param_name" => "dicon",
						"value" => "fa-bolt",
						"dependency" => array(
							'element' => "media_type",
							'value' => 'icon-type'
						),
						"description" => __("Icons from FontAwesome <a href='http://fontawesome.io/icons/'>icon list</a> and Line Icons <a href='http://www.startuplywp.com/line-icons.html'>icon list</a> can be used. Type in the icon name you want to use e.g: fa-bolt or icon-seo-icons-24", "vivaco")
					),
					*/
					array(
						"type" => "attach_image",
						"heading" => __("Image", "vivaco"),
						"param_name" => "img",
						"dependency" => array(
							'element' => "media_type",
							'value' => 'img-type'
						),
						"description" => __("Upload an image for the widget", "vivaco")
					),
					array(
						"type" => "dropdown",
						"heading" => __("Style", "vivaco"),
						"param_name" => "btn_style",
						"value" => array(
							__("Solid color button", "vivaco") => 'btn-solid',
							__("White outline button", "vivaco") => "btn-outline",
							__("Color outline button", "vivaco") => "btn-outline-color",
							__("Clear, no border", "vivaco") => "btn-no-border"
						)
					),
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Text on the button", "vivaco"),
						"param_name" => "text",
						"value" => "Button Text"
					),
					array(
						"type" => "textfield",
						"heading" => __("URL(Link)", "vivaco"),
						"param_name" => "url",
						"description" => __("Button Link.", "vivaco")
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Title Color', 'vivaco'),
						'param_name' => 'title_color'
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Text Color', 'vivaco'),
						'param_name' => 'text_color'
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Button Color', 'vivaco'),
						'param_name' => 'button_color'
					),
				)
			));

			/* Message box */
			vc_map(array(
				'name' => __('Message Box', 'vivaco'),
				'base' => 'vc_message',
				'icon' => 'icon-wpb-information-white',
				'wrapper_class' => 'alert',
				'category' => __('Content', 'vivaco'),
				'description' => __('Notification box', 'vivaco'),
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __('Message box type', 'vivaco'),
						'param_name' => 'color',
						'value' => array(
							__('Success', 'vivaco') => 'alert-success',
							__('Informational', 'vivaco') => 'alert-info',
							__('Warning', 'vivaco') => 'alert-warning',
							__('Error', 'vivaco') => "alert-danger"
						),
						'description' => __('Select message type', 'vivaco')
					),
					array(
						'type' => 'textarea_html',
						'holder' => 'div',
						'class' => 'messagebox_text',
						'heading' => __('Message text', 'vivaco'),
						'param_name' => 'content',
						'value' => __('<p>I am message box. Click edit button to change this text.</p>', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Extra class name', 'vivaco'),
						'param_name' => 'el_class',
						'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vivaco')
					)
				),
				'js_view' => 'VcMessageView'
			));

			/* Call to action button 2 */
			vc_map(array(
				'name' => __('Call to Action', 'vivaco') . ' 2',
				'base' => 'vc_cta_button2',
				"weight" => 12,
				'icon' => 'icon-wpb-call-to-action',
				"category" => __("Content", "vivaco"),
				'description' => __('Catch visitors attention with CTA block', 'vivaco'),
				'params' => array(
					array(
						'type' => 'textfield',
						'heading' => __('Heading first line', 'vivaco'),
						'holder' => 'h2',
						'param_name' => 'h2',
						'value' => __('Hey! I am first heading line feel free to change me', 'vivaco'),
						'description' => __('Text for the first heading line.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Heading second line', 'vivaco'),
						'holder' => 'h4',
						'param_name' => 'h4',
						'value' => '',
						'description' => __('Optional text for the second heading line.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('CTA style', 'vivaco'),
						'param_name' => 'style',
						'value' => getVcShared('cta styles'),
						'description' => __('Call to action style.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Element width', 'vivaco'),
						'param_name' => 'el_width',
						'value' => getVcShared('cta widths'),
						'description' => __('Call to action element width in percents.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Text align', 'vivaco'),
						'param_name' => 'txt_align',
						'value' => getVcShared('text align'),
						'description' => __('Text align in call to action block.', 'vivaco')
					),
					array(
						'type' => 'colorpicker',
						'heading' => __('Custom Background Color', 'wpb'),
						'param_name' => 'accent_color',
						'description' => __('Select background color for your element.', 'wpb')
					),
					array(
						'type' => 'textarea_html',
						'holder' => 'div',
						'heading' => __('Promotional text', 'vivaco'),
						'param_name' => 'content',
						'value' => __('<p>I am promo text. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>', 'vivaco')
					),
					array(
						'type' => 'vc_link',
						'heading' => __('URL (Link)', 'vivaco'),
						'param_name' => 'link',
						'description' => __('Button link.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Text on the button', 'vivaco'),
						//'holder' => 'button',
						//'class' => 'wpb_button',
						'param_name' => 'title',
						'value' => __('Text on the button', 'vivaco'),
						'description' => __('Text on the button.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Button style', 'vivaco'),
						'param_name' => 'btn_style',
						'value' => getVcShared('button styles'),
						'description' => __('Button style.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Color', 'vivaco'),
						'param_name' => 'color',
						'value' => getVcShared('colors'),
						'description' => __('Button color.', 'vivaco'),
						'param_holder_class' => 'vc-colored-dropdown'
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Size', 'vivaco'),
						'param_name' => 'size',
						'value' => getVcShared('sizes'),
						'std' => 'md',
						'description' => __('Button size.', 'vivaco')
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Button position', 'vivaco'),
						'param_name' => 'position',
						'value' => array(
							__('Align right', 'vivaco') => 'right',
							__('Align left', 'vivaco') => 'left',
							__('Align bottom', 'vivaco') => 'bottom'
						),
						'description' => __('Select button alignment.', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Extra class name', 'vivaco'),
						'param_name' => 'el_class',
						'description' => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'vivaco')
					)
				)
			));



			// Pricing Table
			$output = '';

			// setup the output of our shortcode
			$output .= '<br />';
			$output .= '[vsc-pricing-table columns="4"] <br />';
			$output .= '[vsc-pricing-column title="BASIC" price="19" currency="$" interval="month"]';
			$output .= '<ul class="list-unstyled">';
			$output .= '<li>24/7 Support</li>';
			$output .= '<li>Free 10GB Storage</li>';
			$output .= '<li>Documentation &amp; Tutorials</li>';
			$output .= '<li>Google Apps Sync</li>';
			$output .= '<li>Up to 10 Projects</li>';
			$output .= '<li>Free Facebook Page</li>';
			$output .= '<li>Up to 3 Users</li>';
			$output .= '</ul>';
			$output .= '[vsc-signup][vsc-button style="" url="#"]Sign Up[/vsc-button][/vsc-signup]<br />';
			$output .= '[/vsc-pricing-column]<br />';
			$output .= '[vsc-pricing-column title="ADVANCED" featured="yes" price="29" currency="$" interval="month"]';
			$output .= '<ul>';
			$output .= '<li>24/7 Support</li>';
			$output .= '<li>Free 20GB Storage</li>';
			$output .= '<li>Documentation &amp; Tutorials</li>';
			$output .= '<li>Google Apps Sync</li>';
			$output .= '<li>Up to 20 Projects</li>';
			$output .= '<li>Free Facebook Page</li>';
			$output .= '<li>Up to 5 Users</li>';
			$output .= '</ul>';
			$output .= '[vsc-signup][vsc-button style="" url="#"]Sign Up[/vsc-button][/vsc-signup]<br />';
			$output .= '[/vsc-pricing-column]<br />';
			$output .= '[vsc-pricing-column title="PROFESSIONAL" price="49" currency="$" interval="month"]';
			$output .= '<ul>';
			$output .= '<li>24/7 Support</li>';
			$output .= '<li>Free 50GB Storage</li>';
			$output .= '<li>Documentation &amp; Tutorials</li>';
			$output .= '<li>Google Apps Sync</li>';
			$output .= '<li>Up to 50 Projects</li>';
			$output .= '<li>Free Facebook Page</li>';
			$output .= '<li>Up to 10 Users</li>';
			$output .= '</ul>';
			$output .= '[vsc-signup][vsc-button style="" url="#"]Sign Up[/vsc-button][/vsc-signup]<br />';
			$output .= '[/vsc-pricing-column]<br />';
			$output .= '[vsc-pricing-column title="ULTIMATE" price="99" currency="$" interval="month"]';
			$output .= '<ul>';
			$output .= '<li>24/7 Support</li>';
			$output .= '<li>Unlimited Storage</li>';
			$output .= '<li>Documentation &amp; Tutorials</li>';
			$output .= '<li>Google Apps Sync</li>';
			$output .= '<li>Unlimited Projects</li>';
			$output .= '<li>Free Facebook Page</li>';
			$output .= '<li>Unlimited Users</li>';
			$output .= '</ul>';
			$output .= '[vsc-signup][vsc-button style="" url="#"]Sign Up[/vsc-button][/vsc-signup]<br />';
			$output .= '[/vsc-pricing-column]<br />';
			$output .= '[/vsc-pricing-table]<br />';


			vc_map(array(
				"name" => __("Pricing Table", "vivaco"),
				"icon" => "icon-table",
				"description" => "Pricing table element",
				"base" => "vsc-table_placebo",
				"weight" => 12,
				"class" => "pricing_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textarea_html",
						"holder" => "div",
						"class" => "",
						"heading" => __("Pricing Table Example", "vivaco"),
						"param_name" => "content",
						"value" => $output,
						"description" => __("This is an example of a pricing table with 4 columns. Edit it and make it your own.", "vivaco")
					)
				)
			));


			$testimonials_list = get_posts(array(
				'post_type' => 'testimonials',
				'posts_per_page' => -1,
				'post_status' => 'publish'
			));

			$testimonials_array = array();
			foreach ($testimonials_list as $testimonial_item) {
				$testimonials_array[$testimonial_item->post_title] = $testimonial_item->ID;
			}

			vc_map(array(
				"name" => __("Testimonials Slider", "vivaco"),
				"icon" => "icon-testimonials",
				"description" => "Customer feedback",
				"weight" => 15,
				"base" => "vsc-testimonials-slider",
				"class" => "testimonials_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"admin_label" => true,
						"heading" => __("Testimonials slider title", "vivaco"),
						"param_name" => "text",
						"value" => ""
					),
					array(
						"type" => "checkbox",
						"heading" => __("Testimonials from", "vivaco"),
						"param_name" => "ids",
						"admin_label" => true,
						"value" => $testimonials_array,
						"description" => __("Select which testimonials you want to display on a slider.", "vivaco")
					)
				)
			));

			$form_tpl = '{form}{name}{email}{submit}{response}{/form}';

			vc_map(array(
				"name" => __("Newsletter", "vivaco"),
				"description" => "Mailchimp subscribe",
				"weight" => 15,
				"base" => "vsc-newsletter",
				"class" => "newsletter_extended",
				"icon" => "icon-newsletter",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"heading" => __("MailChimp API key", "vivaco"),
						"param_name" => "api_key",
						"admin_label" => true,
						"description" => __("Enter MailChimp API key here. <a href=\"mailchinp.help.html\">Where can i find my api key?</a>", "vivaco")
					),
					array(
						"type" => "textfield",
						"heading" => __("MailChimp List ID", "vivaco"),
						"param_name" => "list_id",
						"admin_label" => true,
						"description" => __("Enter MailChimp List ID here. <a href=\"mailchinp.help.html\">Where can i find my List ID?</a>", "vivaco")
					),
					array(
						"type" => "checkbox",
						"heading" => __("Double opt", "vivaco"),
						"param_name" => "double_opt",
						"value" => array(
							__("Check this box to control whether a double opt-in confirmation messege is sent", "vivaco") => "yes"
						),
						"description" => __("Flag to control whether a double opt-in confirmation message is sent.", "vivaco")
					),
					array(
						"type" => "textarea_html",
						/*"holder" => "hidden",*/
						"heading" => __("Form HTML", "vivaco"),
						//"param_name" => "form_html",
						"param_name" => "content",
						//"value" => base64_encode($form_tpl),
						"value" => $form_tpl,
						"description" => "Define a title for the section"
					)
				)
			));


			// Portfolio Grid

			$portfolio_categs = get_terms('portfolio_cats', array(
				'hide_empty' => false
			));
			$portfolio_cats_array = array();
			foreach ($portfolio_categs as $portfolio_categ) {
				$portfolio_cats_array[$portfolio_categ->name] = $portfolio_categ->name;
			}



			vc_map(array(
				"name" => __("Portfolio Grid", "vivaco"),
				"icon" => "icon-sticky-notes",
				"base" => "vsc-portfolio-grid",
				"description" => "Masonry grid layout for portfolio items",
				"weight" => 20,
				"class" => "portfolio_grid_extended",
				"category" => __("Content", "vivaco"),
				"params" => array(
					array(
						"type" => "textfield",
						"class" => "",
						"admin_label" => true,
						"heading" => __("Number of items to display", "vivaco"),
						"param_name" => "number",
						"value" => 10,
						"description" => __("Use '-1' to include all your items", "vivaco")
					),
					array(
						"type" => "textfield",
						"class" => "",
						"admin_label" => true,
						"heading" => __("Number of columns", "vivaco"),
						"param_name" => "columns_number",
						"value" => 4,
						"description" => __("", "vivaco")
					),
					array(
						"type" => "textfield",
						"class" => "",
						"admin_label" => true,
						"heading" => __("Gutter width", "vivaco"),
						"param_name" => "gutter_width",
						"value" => '',
						"description" => __("this setting controls spacing between items e.g.: \"15\" will put 15px space from every side of an item", "vivaco")
					),
					array(
						"type" => "dropdown",
						"class" => "",
						"heading" => __("Show category filter controls?", "vivaco"),
						"param_name" => "show_filters",
						"value" => array(
							__( "Yes", "vivaco" ) => 'On',
							__( "No", "vivaco" ) => 'Off'
						),
						"description" => __("", "vivaco")
					),
					array(
						"type" => "checkbox",
						"class" => "",
						"heading" => __("Portfolio categories", "vivaco"),
						"param_name" => "categories",
						"value" => $portfolio_cats_array,
						"description" => __("Select from which categories to display (at least 1 required)", "vivaco")
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Custom Hover Color', 'vivaco'),
						'param_name' => 'hover_color',
					),
				)
			));

			$tab_id_1 = 'slide' . time() . '-1-' . rand( 0, 100 );
			$tab_id_2 = 'slide' . time() . '-2-' . rand( 0, 100 );

			vc_map( array(
				"name" => __("Content Slider", "vivaco"),
				"base" => "vsc_content_slider",
				"weight" => 25,
				'show_settings_on_create' => true,
				'is_container' => true,
				'container_not_allowed' => true,
				'icon' => 'icon-wpb-ui-tab-content-vertical',
				'category' => __( 'Content', 'vivaco' ),
				'wrapper_class' => 'vc_clearfix',
				'description' => __( 'Simple content slider', 'js_composer' ),
				"params" => array(
					array(
						"type" => "checkbox",
						"heading" => __( "Slider Options", "vivaco" ),
						"param_name" => "slider_options",
						"value" => array(
							__( "Autoplay", "vivaco" ) => "autoplay",
							__( "Show pagination", "vivaco" ) => "pagination",
							__( "Show arrows", "vivaco" ) => "arrows",
							__( "Infinite loop", "vivaco" ) => "loop"
						)
					),
					array(
						"type" => "textfield",
						"heading" => "Speed",
						"param_name" => "speed",
						"description" => "Slide transition duration (in ms)"
					),
					array(
						"type" => "textfield",
						"heading" => "Autoplay delay",
						"param_name" => "autoplay_delay",
						"description" => "The amount of time (in ms) between each auto transition"
					),
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "vivaco"),
						"param_name" => "el_class",
						"description" => __("Some simple params.", "vivaco")
					),
					array(
						'type' => 'css_editor',
						'heading' => __( 'Css', 'js_composer' ),
						'param_name' => 'css',
						'group' => __( 'Padding & Margins', 'js_composer' )
					)
				),
				'custom_markup' => '
				<div class="wpb_tabs_holder wpb_holder vc_clearfix vc_container_for_children">
				<ul class="tabs_controls"></ul>
				%content%
				</div>'
				,
				'default_content' => '
				[vc_tab title="' . __( 'Slide 1', 'vivaco' ) . '" tab_id="' . $tab_id_1 . '"][/vc_tab]
				[vc_tab title="' . __( 'Slide 2', 'vivaco' ) . '" tab_id="' . $tab_id_2 . '"][/vc_tab]
				',
					'js_view' => 'VcTabsView'
			));



			vc_map(array(
				'name' => __('Countdown', 'vivaco'),
				'base' => 'vsc-countdown',
				'description' => 'Countdown',
				'weight' => 15,
				'class' => 'vsc_countdown',
				'category' => __('Content', 'vivaco'),
				'params' => array(
					array(
						'type' => 'dropdown',
						'heading' => __('Style', 'vivaco'),
						'param_name' => 'layout',
						'value' => array(
							__('Default','js_composer') => 'normal',
							__('Inline','js_composer') => 'line',
							/*__('Boxed','js_composer') => 'boxed',*/
						),
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Count Type ( down or up )', 'vivaco'),
						'param_name' => 'count',
						'value' => array(
							__('Down','js_composer') => 'down',
							__('Up','js_composer') => 'up',
						),
						'std' => 'down',
					),
					array(
						'type' => 'textfield',
						'param_name' => 'notice_countdown',
						'description' => '<p class="alert alert-info">Yeah, You choose countup, be sure for date. you must write a older date. eg for 1 day ago. <strong>'. date( 'M d Y', strtotime('-1 days') ) .'</strong></p>',
						'dependency' => array(
							'element' => 'count',
							'value' => 'up'
						),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Date', 'vivaco'),
						'param_name' => 'date',
						'value' => date( 'M d Y' ),
						'description' => __('Currently value is today, change date, write your own upcoming date', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Format', 'vivaco'),
						'param_name' => 'format',
						'value' => 'yowdhms',
						'description' => __('Currently value is today, change date, write your own upcoming date', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'param_name' => 'notice_countdown_format',
						'description' => '
							<p class="alert alert-info">
								<strong>y</strong>: year <strong>o</strong>: month <strong>w</strong>: week <strong>d</strong>: day <strong>h</strong>: hour <strong>m</strong>: minuites <strong>s</strong>: second<br /><br />
								if you write <strong>YOWDHMS</strong> as uppercase this is mean optional if there is year it will show else hide.<br />
								Eg. Formats: <strong>dhms</strong> or <strong>wdh</strong> or <strong>od</strong> you know...
							</p>',
					),
					array(
						'type' => 'textfield',
						'heading' => __('Date font size in px', 'vivaco'),
						'param_name' => 'num_size',
						'value' => '',
						'description' => __('Adjust number text size here', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Text font size in px', 'vivaco'),
						'param_name' => 'txt_size',
						'value' => '',
						'description' => __('Adjust number text size here', 'vivaco'),
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Custom Number Color', 'vivaco'),
						'param_name' => 'number_color',
					),
					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Custom Text Color', 'vivaco'),
						'param_name' => 'text_color',
					),

					array(
						'type' => 'colorpicker',
						'group' => 'Change color',
						'heading' => __('Custom Separator Color', 'vivaco'),
						'param_name' => 'separator_color',
					),
				)
			));


			$product_pricing_type = array();
			$product_pricing_type[__('Custom HTML', 'vivaco')] = 'simple';

			$edd_product = array(__('', 'vivaco') => '-1');

			if (is_plugin_active('easy-digital-downloads/easy-digital-downloads.php')) {
				$edd_downloads = get_posts( 'post_type="download"&posts_per_page=-1&orderby=title&order=ASC' );

				if ( $edd_downloads ) {
					foreach ( $edd_downloads as $download ) {
						$edd_product[ $download->post_title ] = $download->ID;
					}
					$product_pricing_type[__('EDD Item', 'vivaco')] = 'edd';
				} else {
					$edd_product[ __( 'No download found', 'js_composer' ) ] = 0;
				}
			}

			vc_map(array(
				'name' => __('Single product', 'vivaco'),
				"icon" => "icon-table",
				'base' => 'vsc-product-pricing',
				'description' => 'EDD or Custom HTML',
				'weight' => 115,
				'class' => 'vsc_product_pricing',
				'category' => __('Content', 'vivaco'),
				'params' => array(
					array(
						"type" => "textfield",
						"heading" => __("Extra class name", "js_composer"),
						"param_name" => "el_class",
						"value" => "",
						"description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Style', 'vivaco'),
						'param_name' => 'price_type',
						'value' => $product_pricing_type,
						'description' => __('Please choose product type', 'vivaco'),
					),
					array(
						'type' => 'dropdown',
						'heading' => __('Product', 'vivaco'),
						'param_name' => 'product_id',
						'value' => $edd_product,
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'edd'
						),
						'description' => __('Please enter Product Title', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Custom product title', 'vivaco'),
						'param_name' => 'product_title',
						'admin_label' => true,
						'description' => __('Please enter Custom Product Title', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Currency', 'vivaco'),
						'param_name' => 'product_currency',
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'simple'
						),
						'description' => __('Please enter Product currency, default $', 'vivaco'),
						'std' => '$',
					),
					array(
						'type' => 'textfield',
						'heading' => __('Price', 'vivaco'),
						'param_name' => 'product_price',
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'simple'
						),
						'description' => __('Please enter Product price', 'vivaco'),
					),
					array(
						'type' => 'textfield',
						'heading' => __('Period', 'vivaco'),
						'param_name' => 'product_period',
						// 'value' => array(
						// 	__('None', 'vivaco') => '',
						// 	__('Daily', 'vivaco') => 'Daily',
						// 	__('Weekly', 'vivaco') => 'Weekly',
						// 	__('Monthly', 'vivaco') => 'Monthly',
						// 	__('Yearly', 'vivaco') => 'Yearly',
						// ),
						'description' => __('Please enter Period', 'vivaco'),
					),
					array(
						"type" => "checkbox",
						"heading" => __("Featured", "vivaco"),
						"param_name" => "product_featured",
						"value" => array(
							__("Display as feature", "vivaco") => "yes"
						)
					),
					array(
						'type' => 'textarea_html',
						'holder' => 'div',
						'class' => '',
						'heading' => __('Features', 'vivaco'),
						'param_name' => 'content',
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'simple'
						),
						'value' => '<ul class="list-unstyled"><li><strong>Free</strong> First</li><li><strong>Unlimited</strong> Second</li><li><strong>Unlimited</strong> Third</li><li><strong>Unlimited</strong> Fourth</li></ul>',
						'description' => __('Create your list Features', 'vivaco')
					),
					array(
						'type' => 'textfield',
						'heading' => __('Buy button text', 'vivaco'),
						'param_name' => 'product_buy_text',
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'edd'
						),
						'description' => __('Please enter buy text', 'vivaco'),
					),
					array(
						'type' => 'textarea_raw_html',
						'heading' => __('Buy button', 'vivaco'),
						'param_name' => 'product_buy_link',
						'dependency' => array(
							'element' => 'price_type',
							'value' => 'simple'
						),
						'value' => '<a class="btn btn-outline base_clr_txt" href="#" style="">GET IT NOW</a>',
						'description' => __('Please enter buy button link', 'vivaco'),
					),
				)
			));

		}
	}

	//Remove some shortcodes from Grid VC editor shortcodes in backend
	if (function_exists('vc_remove_element')) {
		vc_remove_element("vc_cta_button");
		vc_remove_element("vc_cta_button2");
		vc_remove_element("vc_teaser_grid");
		vc_remove_element("vc_posts_slider");
		vc_remove_element("vc_images_carousel");
		vc_remove_element("vc_carousel");
		vc_remove_element("vc_button");
		//vc_remove_element("vc_text_separator");
		//vc_remove_element("vc_icon");
		//vc_remove_element("vc_button2");
	}

}
add_action('init', 'extend_composer', 9999);

function startuply_change_succession() {
	if (function_exists('vc_map')) {
		// Change succession for default elements.

		//ROW 1
		vc_map_update('vc_row', array( "weight" => 100 ));
		vc_map_update( "vsc-section-title", array( "weight" => 90 ));
		vc_map_update( "vc_column_text", array( "weight" => 80, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_empty_space", array( "weight" => 79, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_single_image", array( "weight" => 78, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_gallery", array( "weight" => 77,'category' => __("Content", "vivaco")));

		//ROW 2
		vc_map_update( "vc_tabs", array( "weight" => 76, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_tour", array( "weight" => 75, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_accordion", array( "weight" => 74, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vsc-button", array( "weight" => 73, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_icon", array( "weight" => 72, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-text-icon", array( "weight" => 71, 'category' => __("Content", "vivaco")));

		//ROW 3
		vc_map_update( "vc_separator", array( "weight" => 70, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_progress_bar", array( "weight" => 69, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_pie", array( "weight" => 68, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-counter", array( "weight" => 67, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-countdown", array( "weight" => 66, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_custom_heading", array( "weight" => 65, 'category' => __("Content", "vivaco")));

		//ROW 4
		vc_map_update( "vsc_content_slider", array( "weight" => 64, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vsc-team-member-new", array( "weight" => 63, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-testimonials-slider", array( "weight" => 62, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_toggle", array( "weight" => 61, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vsc-quote", array( "weight" => 59, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-list", array( "weight" => 60, 'category' => __("Content", "vivaco")));


		//ROW 5
		vc_map_update( "vc_message", array( "weight" => 57, 'category' => __("Content", "vivaco") ));
		vc_map_update( "contact-form-7-wrapper", array( "weight" => 56, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-newsletter", array( "weight" => 55, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-call-to-action", array( "weight" => 54, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_video", array( "weight" => 53, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_gmaps", array( "weight" => 52, 'category' => __("Content", "vivaco")));
		//vc_map_update( "vsc-table_placebo", array( "weight" => 52, 'category' => __("Content", "vivaco")));
		vc_map_update( "vsc-product-pricing", array( "weight" => 51,'category' => __("Content", "vivaco")));
		if ( shortcode_exists( "VC_edd" ) ) {
			vc_map_update( "VC_edd", array( "weight" => 50,'category' => __("Content", "vivaco")));
		}


		//ROW 6

		vc_map_update( "vsc-portfolio-grid", array( "weight" => 49, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_basic_grid", array( "weight" => 48, 'category' => __("Content", "vivaco") ));
		vc_map_update( "vc_media_grid", array( "weight" => 47, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_masonry_grid", array( "weight" => 46, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_masonry_media_grid", array( "weight" => 45, 'category' => __("Content", "vivaco")));

		//ROW 7
		if ( shortcode_exists( "rev_slider_vc" ) ) {
			vc_map_update( "rev_slider_vc", array( "weight" => 45, 'category' => __("Content", "vivaco")));
		}
		vc_map_update( "vc_widget_sidebar", array( "weight" => 44, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_raw_html", array( "weight" => 43, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_raw_js", array( "weight" => 42, 'category' => __("Content", "vivaco")));
		vc_map_update( "css_animation", array( "weight" => 41, 'category' => __("Content", "vivaco")));
		vc_map_update( "templatera", array( "weight" => 40, 'category' => __("Content", "vivaco")));

		//ROW 8
		vc_map_update( "vc_flickr", array( "weight" => 39, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_facebook", array( "weight" => 38, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_tweetmeme", array( "weight" => 37, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_googleplus", array( "weight" => 36, 'category' => __("Content", "vivaco")));
		vc_map_update( "vc_pinterest", array( "weight" => 35, 'category' => __("Content", "vivaco")));

		if ( shortcode_exists( "templatera" ) ) {
			vc_map_update( "templatera", array( 'category' => __("Content", "vivaco")));
		}

		if ( shortcode_exists( "gravityform" ) ) {
			vc_map_update( "gravityform", array( 'category' => __("Content", "vivaco")));
		}

		// Fix the new Google maps link + add small guidance
		if (class_exists('WPBMap')) {
			$param = WPBMap::getParam( 'vc_gmaps', 'link' );
			$param['description'] = sprintf( __( 'Visit %s to create your map. 1) Find location 2) Click "Settings" (cog icon) at the bottom right of the screen and choose "Share or embed map" 3) Click "Embed map" tab to reveal iframe code 4) Copy iframe code and paste it here.', 'js_composer' ), '<a href="https://www.google.com/maps" target="_blank">Google maps</a>' );
			vc_update_shortcode_param( 'vc_gmaps', $param );
		}
	}
}
add_action( 'wp_loaded', 'startuply_change_succession' , 9999);

if (!function_exists('vivaco_enqueue_scripts')) {

	// frontend register styles
	function vivaco_enqueue_scripts() {
		$version = '2.2.0';
	}

	add_action('wp_enqueue_scripts', 'vivaco_enqueue_scripts');
}

if (!function_exists('vivaco_admin_enqueue_scripts')) {
	// back-end register styles

	//Add custom Visual Composer styles and icons
	function vivaco_admin_enqueue_scripts($hook) {
		$version = '2.2.0';

		if (function_exists('vc_map')) {
			wp_enqueue_style('extend-composer', get_template_directory_uri() . '/css/extend-composer.css', array(), $version);
		}
	}

	add_action('admin_enqueue_scripts', 'vivaco_admin_enqueue_scripts');
}

//Message box
function vc_message_shortcode($atts, $content = null) {
	$output = $class = $color = $text = $el_class = '';
	extract(shortcode_atts(array(
		'color' => 'alert-success',
		'text' => '',
		'el_class' => ''
	), $atts));

	$icons = array(
		'alert-success' => 'icon-badges-votes-10',
		'alert-info' => 'icon-alerts-18',
		'alert-warning' => 'icon-alerts-01',
		'alert-danger' => 'icon-badges-votes-11'
	);

	$class .= 'alert ' . $el_class . ' ' . $color;

	$output .= '<div class="' . $class . '">';
	$output .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>';
	$output .= $content . '<i class="icon ' . $icons[$color] . '"></i>';
	$output .= '</div>';

	return $output;

}
add_shortcode('vc_message', 'vc_message_shortcode');



/*-----------------------------------------------------------------------------------*/
/*	Page Title
/*-----------------------------------------------------------------------------------*/
function vsc_page_title_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => ''
	), $atts));

	return get_the_title($id);
}

add_shortcode('vsc-page-title', 'vsc_page_title_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Page Excerpt
/*-----------------------------------------------------------------------------------*/
function vsc_page_excerpt_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => ''
	), $atts));

	global $post;
	$save_post = $post;
	$post = get_post($id);
	setup_postdata($post); // hello
	$output = get_the_excerpt();
	$post = $save_post;
	return $output;
}

add_shortcode('vsc-page-excerpt', 'vsc_page_excerpt_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Page Content
/*-----------------------------------------------------------------------------------*/
function vsc_page_content_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => ''
	), $atts));

	$content_post = get_post($id);
	setup_postdata($post); // hello
	$post_content = $content_post->post_content;

	return do_shortcode($post_content);
}

add_shortcode('vsc-page-content', 'vsc_page_content_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Page Thumbnail
/*-----------------------------------------------------------------------------------*/
function vsc_page_thumbnail_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'id' => '',
		'thumb_size' => 'gallery-thumb'
	), $atts));

	return get_the_post_thumbnail($id, explode('x', $thumb_size, 2));
}

add_shortcode('vsc-page-thumbnail', 'vsc_page_thumbnail_shortcode');


/*-----------------------------------------------------------------------------------*/
/*	Buttons
/*-----------------------------------------------------------------------------------*/

function vsc_button_shortcode($atts, $content = null) {
	$css = $type = $icon_startuplyli = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypoicons = $icon_linecons = $color = $style = $text = $size = $url = $icon = $target = $icon_right = '';
	$btn_style = $custom_class = $custom_styles = $b_custom_styles = $button_style = $icon_i = $icon_p = $b_target = $align = '';

	extract(shortcode_atts(array(
		'css' => '',
		'type' => 'startuplyli',
		'icon_startuplyli' => 'icon icon-graphic-design-13',
		'icon_fontawesome' => 'fa fa-adjust',
		'icon_openiconic' => 'vc-oi vc-oi-dial',
		'icon_typicons' => 'typcn typcn-adjust-brightness',
		'icon_entypoicons' => 'entypo-icon entypo-icon-note',
		'icon_linecons' => 'vc_li vc_li-heart',
		'icon_entypo' => 'entypo-icon entypo-icon-note',
		'color' => '',
		'style' => 'btn-solid',
		'text' => '',
		'size' => '',
		'text_size' => '',
		'text_color' => '',
		'url' => '',
		'target' => '',
		'icon_right' => '',
		'add_icon' => '',
		'align' => 'inline',
		'i_size' => '',
		'i_color' => '',
		'i_align' => 'pull-left',
		'el_class' => ''
	), $atts));

	/*
	echo "<pre>";
		print_r($atts);
	echo "</pre>";
	*/

	$btn_style = '';

	vc_icon_element_fonts_enqueue( $type );

	if ( $style == 'btn-solid' ) {
		$btn_style = 'btn-solid base_clr_bg';
	} else if ( $style == 'btn-no-border' ) {
		$btn_style = 'btn-no-border base_clr_txt';
	} else if ( $style == 'btn-outline-color' ) {
		$btn_style = 'btn-outline-color base_clr_txt base_clr_bg base_clr_brd';
	} else if ( $style == 'alt' ) { // what?
		$btn_style = 'btn-outline-color base_clr_txt base_clr_bg base_clr_brd';
	} else if ( $style == '' ) {
		$btn_style = 'btn-solid base_clr_bg';
	} else {
		$btn_style = 'btn-outline base_clr_txt';
	}

	if ( $color != '' ) {
		$custom_class = 'btn_' . vsc_random_id(10);
		$button_style = ' <style> .' . $custom_class . '.base_clr_brd { border-color: ' . $color . '; } .' . $custom_class . '.base_clr_txt { color: ' . $color . '; } .' . $custom_class . '.base_clr_bg { background-color: ' . $color . '; } </style>';
	}

	if ($text_size != '') {$b_custom_styles .= 'font-size:' . esc_attr( $text_size ) .'px; line-height: '. esc_attr( $text_size ) .'px;';};
	if ($text_color != '') {$b_custom_styles .= 'color:' . esc_attr( $text_color ) .';';};
	if ($i_color != '') {$custom_styles = 'color:' . esc_attr( $i_color ) .';';};
	if ($i_size != '') {$custom_styles .= 'font-size:' . esc_attr( $i_size ) .'px; line-height: '. esc_attr( $text_size ) .'px;';};

	$align_before = '';
	$align_after= '';
	if ($align != '' && $align != 'inline') {

		if ($align == 'center' || $align == 'left' || $align == 'right')
			$align_before .= '<div class="align-wrap" style="text-align: '. esc_attr( $align ) .';">';
			$align_after .= '</div>';

	};



	$i_align = ($i_align != '') ? $i_align : 'pull-left';

	if ( 'true' === $add_icon ) {
		$icon_i = ($type != '') ? '<span class="vc_icon_element-icon base_clr_txt '. $i_align .' '.esc_attr( ${"icon_" . $type} ).'"' . ' style="' . $custom_styles .'"></span>' : '';
	} else {
		$icon_i = '';
	}

	if ( !empty($el_class) ) { $vsc_class = $el_class;} else { $vsc_class = vc_shortcode_custom_css_class( $css, ' ' ); }

	$b_target = ($target != '') ? 'target="_blank"' : '';
	$class = ($el_class != '') ? $el_class : '';

	if ($url) {
		return $align_before . '<a ' . $b_target . 'class="btn '.$vsc_class.' '.$custom_class.' '.$size.' '.$btn_style . ' ' . $icon_p . '" href="' . $url . '" style="' . $b_custom_styles .'">' . $icon_i . '' . $text . $content . '</a>' . $button_style . $align_after;
	} else {
		return $align_before . '<a class="btn ' . $custom_class . ' ' . $size . ' ' . $btn_style . ' ' . $icon_p . '" href="">' . $icon_i . '' . $text . $content . '</a>' . $button_style . $align_after;
	}
}

add_shortcode('vsc-button', 'vsc_button_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Space
/*-----------------------------------------------------------------------------------*/
/*
function vsc_space_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'height' => '60'
	), $atts));
	return '<div style="clear:both; width:100%; height:' . $height . 'px"></div>';
}

add_shortcode('vsc-space', 'vsc_space_shortcode');
*/

/*-----------------------------------------------------------------------------------*/
/*	Lists
/*-----------------------------------------------------------------------------------*/

function vsc_list_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'icon' => 'ok',
		'el_class' => ''
	), $atts));

	return '<div class="customlist list-icon-fa-' . $icon . ' ' . $el_class .'">' . do_shortcode($content) . '</div>';
}

add_shortcode('vsc-list', 'vsc_list_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Quote Shortcode
/*-----------------------------------------------------------------------------------*/

function vsc_quote_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'text' => '',
		'author' => ''
	), $atts));

	$output = '';

	$output .= '<div class="single-quote">';
	$output .= '<blockquote>"' . $text . '"</blockquote>';
	$output .= '<span class="quote-author">' . $author . '</span>';
	$output .= '</div>';

	return $output;
}

add_shortcode('vsc-quote', 'vsc_quote_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Section Title Shortcode
/*-----------------------------------------------------------------------------------*/

function vsc_title_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'align' => '',
		'size' => 'normal',
		'use_google_font' => '',
		'title_google_fonts' => '',
		'title_font_size' => '',
		'subtitle_google_fonts' => '',
		'subtitle_font_size' => '',
		'title_color' => '',
		'subtitle_color' => '',
		'el_class' => ''
	), $atts));

	$output = $content_class = $custom_title_style = $custom_subtitle_style = $t_font_size = $s_font_size = $t_color = $s_color = '';

	if ($title_color != '') $t_color = ' color: ' . $title_color . ';';
	if ($subtitle_color != '') $s_color = ' color: ' . $subtitle_color . ';';

	if ( $title_font_size !== '' ) $t_font_size = 'font-size: ' . intval($title_font_size) . 'px;';
	if ( $subtitle_font_size !== '' ) $s_font_size = 'font-size: ' . intval($subtitle_font_size) . 'px;';


	$custom_title_style_font = '';
	$custom_subtitle_style_font = '';
	if (!empty($use_google_font) && $use_google_font == 'yes') {
		if (!empty($title_google_fonts)) {
			/*echo '<pre>'.print_r($title_google_fonts, 1).'</pre>';*/
			//Google fonts output: font_family:Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic|font_style:900%20bold%20regular%3A900%3Anormal
			preg_match('#font_family:(.*?)\|#s', $title_google_fonts, $matches);
			$google_fonts_family = explode(":", urldecode($matches[1])); //returns Lato

			preg_match('#font_style:(.*?)$#s', $title_google_fonts, $matches);
			$google_fonts_styles = explode(":", urldecode($matches[1]));

			$custom_title_style_font .= ' font-family: \'' . $google_fonts_family[0] . '\';';
			$custom_title_style_font .= ' font-weight: ' . $google_fonts_styles[1] . ';';
			$custom_title_style_font .= ' font-style: ' . $google_fonts_styles[2] . ';';

			startuply_typography_enqueue_google_font($google_fonts_family[0]);
		}

		if (!empty($subtitle_google_fonts)) {
			preg_match('#font_family:(.*?)\|#s', $subtitle_google_fonts, $matches);
			$google_fonts_family = explode(":", urldecode($matches[1])); //returns Lato

			preg_match('#font_style:(.*?)$#s', $subtitle_google_fonts, $matches);
			$google_fonts_styles = explode(":", urldecode($matches[1]));

			$custom_subtitle_style_font .= ' font-family: \'' . $google_fonts_family[0] . '\';';
			$custom_subtitle_style_font .= ' font-weight: ' . $google_fonts_styles[1] . ';';
			$custom_subtitle_style_font .= ' font-style: ' . $google_fonts_styles[2] . ';';

			startuply_typography_enqueue_google_font($google_fonts_family[0]);
		}
	}

	$custom_title_style = ' style="' . $t_font_size . $t_color . $custom_title_style_font . '"';
	$custom_subtitle_style = ' style="' . $s_font_size . $s_color . $custom_subtitle_style_font . '"';

	$output .= '<div class="section-wrap '. $el_class .'">';

	if ($size == 'big') {
		$output .= '<h1' . $custom_title_style . ' class="section-title text-' . $align . '">' . $title . '</h1>';
		$content_class = ' class="sub-hero-header heading-font"';
	} else if ($size == 'normal') {
		$output .= '<h2' . $custom_title_style . ' class="section-title text-' . $align . '">' . $title . '</h2>';
		$content_class = ' class="sub-header"';
	} else if ($size == 'small') {
		$output .= '<h3' . $custom_title_style . ' class="section-title text-' . $align . '">' . $title . '</h3>';
		$content_class = ' class="sub-title"';
	} else {
		$output .= '<h3' . $custom_title_style . '  class="section-title text-' . $align . '">' . $title . '</h3>';
	}

	if ($content != '') {
		$output .= '<div class="sub-title text-' . $align . '"><p' . $content_class . $custom_subtitle_style . '>' . do_shortcode($content) . '</p></div>';
	}

	$output .= '</div>';

	return $output;
}

add_shortcode('vsc-section-title', 'vsc_title_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	Social Share Blog
/*-----------------------------------------------------------------------------------*/

function vsc_social_shortcode($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => ''
	), $atts));

	wp_enqueue_script('vsc-social');

	$output = '';

	$output .= '<div class="share-options">';
	if (!empty($title)) {
		$output .= '<h6>' . $title . '</h6>';
	}
	$output .= '<a href="" class="twitter-sharer" onClick="twitterSharer()"><i class="fa fa-twitter"></i></a>';
	$output .= '<a href="" class="facebook-sharer" onClick="facebookSharer()"><i class="fa fa-facebook"></i></a>';
	$output .= '<a href="" class="pinterest-sharer" onClick="pinterestSharer()"><i class="fa fa-pinterest"></i></a>';
	$output .= '<a href="" class="google-sharer" onClick="googleSharer()"><i class="fa fa-google-plus"></i></a>';
	$output .= '<a href="" class="delicious-sharer" onClick="deliciousSharer()"><i class="fa fa-share"></i></a>';
	$output .= '<a href="" class="linkedin-sharer" onClick="linkedinSharer()"><i class="fa fa-linkedin"></i></a>';
	$output .= '</div>';
	$output .= '<p></p>';

	return $output;
}

add_shortcode('vsc-social-block', 'vsc_social_shortcode');

/*-----------------------------------------------------------------------------------*/
/*	VC Icon
/*-----------------------------------------------------------------------------------*/

function vsc_icon ($atts, $content = null) {
$icon = $color = $size = $align = $el_class = $custom_color = $link = $background_style = $background_color =
$type = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypoicons = $icon_linecons = $css = $style1 = $style2 = $css_class = $custom_styles = '';

$defaults = array(
	'type' => 'startuplyli',
	'icon_startuplyli' => 'icon icon-graphic-design-13',
	'icon_fontawesome' => 'fa fa-adjust',
	'icon_openiconic' => 'vc-oi vc-oi-dial',
	'icon_typicons' => 'typcn typcn-adjust-brightness',
	'icon_entypoicons' => 'entypo-icon entypo-icon-note',
	'icon_linecons' => 'vc_li vc_li-heart',
	'icon_entypo' => 'entypo-icon entypo-icon-note',
	'css' => '',
	'color' => '',
	'custom_color' => '',
	'background_style' => '',
	'background_color' => '',
	'size' => '',
	'align' => 'left',
	'el_class' => '',
	'link' => '',
	'css_animation' => '',
	'display_style' => '',
);

/** @var array $atts - shortcode attributes */
$atts = vc_shortcode_attribute_parse( $defaults, $atts );
extract( $atts );

/*
echo "<pre>";
echo "-----";
var_dump($atts);
echo "</pre>";
*/

$class = $el_class ;
//$css_class = apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class, $this->settings['base'], $atts );
$css_class .= $css_animation;
// Enqueue needed icon font.
vc_icon_element_fonts_enqueue( $type );

$url = vc_build_link( $link );
$has_style = false;
if ( strlen( $background_style ) > 0 ) {
	$has_style = true;
	if ( strpos( $background_style, 'outline' ) !== false ) {
		$background_style .= ' vc_icon_element-outline'; // if we use outline style it is border in css
	} else {
		$background_style .= ' vc_icon_element-background';
	}
}

if( $has_style ) {
	$style1 = 'vc_icon_element-have-style';
	$style2 = 'vc_icon_element-have-style-inner';
}

if ($custom_color != '') {$custom_styles = 'color:' . esc_attr( $custom_color ) .';';};
if ($size != '') {$custom_styles .= 'font-size:' . esc_attr( $size ) .'px;';};
if ($display_style != '') {$d_style = esc_attr( $display_style );} else {$d_style = '';};

$output = '<div class="vc_icon_element vc_icon_element-outer'.esc_attr( $css_class ).' vc_icon_element-align-'.esc_attr( $align ) .' '. $style1 .' '.$d_style.' ' . $class .'"><div class="vc_icon_element-inner '. $style2 .'" '.esc_attr( $background_style ).' '.esc_attr( $background_color ).'"><span class="vc_icon_element-icon '.esc_attr( ${"icon_" . $type} ).'"' . ' style="' . $custom_styles .'"></span>';

		if ( strlen( $link ) > 0 && strlen( $url['url'] ) > 0 ) {

			$output .= '<a class="vc_icon_element-link" href="' . esc_attr( $url['url'] ) . '" title="' . esc_attr( $url['title'] ) . '" target="' . ( strlen( $url['target'] ) > 0 ? esc_attr( $url['target'] ) : '_self' ) . '"></a>';

		}

		//$output .= '</div></div>'.$this->endBlockComment( ".vc_icon_element" ); //old one
		$output .= '</div></div>';


	return $output;

}
add_shortcode('vc_icon', 'vsc_icon');


/*-----------------------------------------------------------------------------------*/
/*	Text with Icon
/*-----------------------------------------------------------------------------------*/

function vsc_text_with_icon($atts, $content = null) {
	$type = $icon_startuplyli = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = '';

	extract(shortcode_atts(array(
		'type' => 'startuplyli',
		'icon_startuplyli' => 'icon icon-graphic-design-13',
		'icon_fontawesome' => 'fa fa-adjust',
		'icon_openiconic' => 'vc-oi vc-oi-dial',
		'icon_typicons' => 'typcn typcn-adjust-brightness',
		'icon_entypoicons' => 'entypo-icon entypo-icon-note',
		'icon_linecons' => 'vc_li vc_li-heart',
		'icon_entypo' => 'entypo-icon entypo-icon-note',

		'title' => '',
		'media_type' => 'icon-type', // default value
		'align' => 'top', // default value
		'icon_type' => 'single_icon', // default value
		'icon_shape' => 'round_shape', // default value
		'icon_border' => 'solid_border', // default value
		'icon' => '',
		'icon_size' => '',
		'img' => '',
		'title_color' => '',
		'text_color' => '',
		'icon_bg_color' => '',
		'icon_bd_color' => '',
		'icon_color' => '',
		'el_class' => ''
	), $atts));

	$ialign = '';
	if ($align == 'top') {
		$ialign = 'icon-top';
	} else if ($align == 'left') {
		$ialign = 'icon-left';
	} else if ($align == 'right') {
		$ialign = 'icon-right';
	} else if ($align == 'bottom') {
		$ialign = 'icon-bottom';
	}

	$itype = '';
	if ($icon_type == 'single_icon') {
		$itype = 'icon-single';
	} else if ($icon_type == 'solid_icon') {
		$itype = 'icon-solid';
	} else if ($icon_type == 'border_icon') {
		$itype = 'icon-border';
	}

	$ishape = '';
	if ($icon_shape == 'round_shape') {
		$ishape = 'icon-round';
	} else if ($icon_shape == 'square_shape') {
		$ishape = 'icon-square';
	} else if ($icon_shape == 'rounded_square_shape') {
		$ishape = 'icon-round-square';
	}

	$iborder = '';
	if ($icon_border == 'solid_border') {
		$iborder = 'icon-border-solid';
	} else if ($icon_border == 'dashed_border') {
		$iborder = 'icon-border-dashed';
	} else if ($icon_border == 'dotted_border') {
		$iborder = 'icon-border-dotted';
	}

	$container_style = '';
	if ( $icon_size != '' ) {
		$icon_size = intval($icon_size);

		$padding = $icon_size;

		if ( $icon_type != 'single_icon' ) {
			$icon_size = $icon_size / 2;

			if ( $icon_size < 16 ) {
				$icon_size = 16;
				$padding = 32;
			}
		}

		if ($align == 'top') {
			$container_style = 'padding-top: ' . ($padding + 10) . 'px;';
		} else if ($align == 'left') {
			$container_style = 'padding-left: ' . ($padding + 15) . 'px;';
		} else if ($align == 'right') {
			$container_style = 'padding-right: ' . ($padding + 15) . 'px;';
		} else if ($align == 'bottom') {
			$container_style = 'padding-bottom: ' . ($padding) . 'px;';
		}
	}

	$istyle = '';
	if ($icon_type == 'solid_icon' && $icon_bg_color != '') {
		$istyle .= 'background-color: ' . $icon_bg_color . '; ';
	}
	if ($icon_type != 'single_icon' && $icon_bd_color != '') {
		$istyle .= 'border-color: ' . $icon_bd_color . '; ';
	}
	if ($icon_color != '') {
		$istyle .= 'color: ' . $icon_color . ';';
	}

	if ( $icon_size != '' ) {
		$istyle .= ' font-size: ' . $icon_size . 'px;';
	}

	$title_style = '';
	if ( $title_color != '' ) {
		$title_style = 'style="color: ' . $title_color . ';"';
	}

	$text_style = '';
	if ( $text_color != '' ) {
		$text_style = 'style="color: ' . $text_color . ';"';
	}

	$output = '';
	$output .= '<article style="' . $container_style . '" class="vsc-service-elem vsc-text-icon ' . $el_class . ' ' . $ialign . ' ' . $itype . ' ' . $ishape . ' ' . $iborder . '">';
	$output .= '<div class="vsc-service-icon">';

	if (!empty($type)) {

		if (!empty(${"icon_" . $type})) {
			$icon = trim(esc_attr( ${"icon_" . $type} ));
		}

		if ( strpos($icon, ' icon') === false ) {
			$icon = 'icon '.$icon;
		}

		//echo '<pre>'.print_r($type, 1).'<br/>'.print_r($icon, 1).'</pre>';
		vc_icon_element_fonts_enqueue( $type );

	} else {
		$icon = 'fa icon '.$icon;
	}

	// base_clr_bg
	if ($media_type == 'icon-type') {
		$output .= '<i class="' . $icon . ( ($icon_type == 'solid_icon') ? ' base_clr_brd base_clr_bg' : '' ) . ( ($icon_type == 'border_icon') ? ' base_clr_brd' : '' ) . '" style="' . $istyle . '"></i>';
	} else if ($media_type == 'img-type') {
		$img_val = '';
		if (function_exists('wpb_getImageBySize')) {
			$img_val = wpb_getImageBySize(array(
				'attach_id' => (int) $img,
				'thumb_size' => 'full'
			));
		}

		$output .= $img_val['thumbnail'];
	}
	$output .= '</div>';

	$output .= '<div class="vsc-service-content">';
	if ($title != '') {
		$output .= '<h6 ' . $title_style . '>' . $title . '</h6>';
	}

	if (!empty($content)) {
		$output .= '<p ' . $text_style . '>' . do_shortcode($content) . '</p>';
	}
	$output .= '</div>';

	$output .= '</article>';
	return $output;

}

add_shortcode('vsc-text-icon', 'vsc_text_with_icon');

/*-----------------------------------------------------------------------------------*/
/*	Canvas Title
/*-----------------------------------------------------------------------------------*/

function vsc_canvas_title($atts, $content = null) {
	extract(shortcode_atts(array(
		'text' => '',
		'tag_name' => 'h1',
		'google_fonts' => '',
		'font_size' => '',
		'align' => 'center',
		'line_height' => '',
		'padding_top' => '',
		'padding_bottom' => '',
		'mask_img' => '',
		'overlay' => '',
		'overlay_color' => '',
	), $atts));

	$output = $img_val = $headline_style = '';

	//Google fonts output: font_family:Lato%3A100%2C100italic%2C300%2C300italic%2Cregular%2Citalic%2C700%2C700italic%2C900%2C900italic|font_style:900%20bold%20regular%3A900%3Anormal
	preg_match('#font_family:(.*?)\|#s', $google_fonts, $matches);
	$google_fonts_family = explode(":", urldecode($matches[1])); //returns Lato

	preg_match('#font_style:(.*?)$#s', $google_fonts, $matches);
	$google_fonts_styles = explode(":", urldecode($matches[1]));

	if ( $mask_img != null && function_exists('wpb_getImageBySize')) {
		$img_array = wpb_getImageBySize(array( 'attach_id' => (int) $mask_img, 'thumb_size' => 'full' ));
		$img_val = $img_array['p_img_large'][0];
	}

	if ( $tag_name == '' ) $tag_name = 'h1';
	if ( $overlay_color != '' ) $overlay_style = ' background-color: ' . $overlay_color . ';';
	if ( $img_val != '' ) $headline_style .= '-webkit-text-fill-color: transparent; background: -webkit-linear-gradient(transparent, transparent), url(' . $img_val . ') top center no-repeat; background: -o-linear-gradient(transparent, transparent); -webkit-background-clip: text; background-size: cover;';
	if ( $padding_top != '' ) $headline_style .= ' padding-top: ' . intval($padding_top) . 'px;';
	if ( $padding_bottom != '' ) $headline_style .= ' padding-bottom: ' . intval($padding_bottom) . 'px;';
	if ( $line_height != '' ) $headline_style .= ' line-height: ' . intval($line_height) . 'px; height: ' . ( intval($padding_top) + intval($padding_bottom) + intval($line_height) ) . 'px;';
	if ( $font_size != '' ) $headline_style .= ' font-size: ' . intval($font_size) . 'px;';
	if ( $align !== '' ) $headline_style .= ' text-align: ' . $align . ';';
	if ( $google_fonts != '' ) {
		$headline_style .= ' font-family: \'' . $google_fonts_family[0] . '\';';
		$headline_style .= ' font-weight: ' . $google_fonts_styles[1] . ';';
		$headline_style .= ' font-style: ' . $google_fonts_styles[2] . ';';
	}


	if ( $overlay == 'yes' ) {
		$output .= '<div class="canvas-title-block" style="background-image: url(' . $img_val . ');"><i class="canvas-overlay" style="' . $overlay_style . '"></i>';
	}

	$output .= '<' . $tag_name . (($img_val != '') ? ' data-img=' . $img_val : '') . ' class="canvas-headline" style="' . $headline_style . '">';

	if ( $overlay == 'yes' ) {
		$output .= '<span><i class="text">' . $text . '</i></span>';
	} else {
		$output .= '<i class="text">' . $text . '</i>';
	}

	$output .= '</' . $tag_name . '>';

	if ( $overlay == 'yes' ) {
		$output .= '</div>';
	}

	wp_enqueue_script('svg-text', array('jquery'), true);
	startuply_typography_enqueue_google_font($google_fonts_family[0]);
	return $output;
}

add_shortcode('vc_custom_heading', 'vsc_canvas_title');

/*-----------------------------------------------------------------------------------*/
/*	Progress Bar
/*-----------------------------------------------------------------------------------*/

function adjustBrightness($hex, $steps) {
	// Steps should be between -255 and 255. Negative = darker, positive = lighter
	$steps = max(-255, min(255, $steps));

	// Format the hex color string
	$hex = str_replace('#', '', $hex);
	if (strlen($hex) == 3) $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);

	// Get decimal values
	$r = hexdec(substr($hex,0,2));
	$g = hexdec(substr($hex,2,2));
	$b = hexdec(substr($hex,4,2));

	// Adjust number of steps and keep it inside 0 to 255
	$r = max(0,min(255,$r + $steps));
	$g = max(0,min(255,$g + $steps));
	$b = max(0,min(255,$b + $steps));

	$r_hex = str_pad(dechex($r), 2, '0', STR_PAD_LEFT);
	$g_hex = str_pad(dechex($g), 2, '0', STR_PAD_LEFT);
	$b_hex = str_pad(dechex($b), 2, '0', STR_PAD_LEFT);

	return '#'.$r_hex.$g_hex.$b_hex;
}

function vsc_progress_bar($atts, $content = null) {
	extract( shortcode_atts( array(
		'title' => '',
		'value' => '50',
		'units' => '',
		'position' => '',
		'height' => '',
		'round' => '',
		'bgcolor' => '',
		'options' => '',
		'el_class' => '',
		'margin_bottom' => ''
	), $atts ) );
	wp_enqueue_script( 'waypoints' );

	// Name, Percents???, Height, Edges(on/off)???, Fill(Default/Gradient), Striped(on/off) Content Position(Default/Numbers Inside/All Inside),
	// Margin Bottom, Color, Font-size???, Gradient Settings???, Animate Stripes???, Units, Bar color???, Stripes Settings???

	$no_animation = ''; //replace with valid option

	$output = $bar_options = $css_class = $percentage_value = $el_style = $bar_style = $bar_class = '';

	$options = explode( ',', $options );
	if ( in_array( 'striped', $options ) ) $bar_options .= ' striped';
	if ( in_array( 'gradient', $options ) ) $bar_options .= ' gradient';
	if ( in_array( 'no_animation', $options ) ) $bar_options .= ' no-animation';
	if ( in_array( 'stripes_animation', $options ) ) $bar_options .= ' stripes-animation';

	if ( $units == '' ) $units = '%';
	if ( intval($round) >= intval($height)/2 ) $round = intval($height)/2;

	$el_style = 'style="';
	if ( $margin_bottom != '' ) $el_style .= ' margin-bottom: ' . intval($margin_bottom) . 'px;';
	if ( $round != 0 && $round != '' ) $el_style .= ' border-radius: ' . intval($round) . 'px;';
	$el_style .= '"';

	if ( $value != '' ) $percentage_value = intval($value);
	elseif ( $value > 100.00 ) $percentage_value = 100;
	else $percentage_value = 0;

	$css_class = ' class="vsc_progress_bar wpb_content_element heading-font ' . $el_class;
	if ( $no_animation == 'yes' ) $css_class .= ' no-animation';
	if ( $position == 'value_inside' ) $css_class .= ' value_inside"';
	elseif ( $position == 'all_inside' ) $css_class .= ' all_inside"';
	else $css_class .= '"';

	$bar_class = ' class="vsc_bar base_clr_bg' . $bar_options . '"';

	$bar_style = ' style="';
	if ( $height != '' ) $bar_style .= ' height: ' . intval($height) . 'px;';
	if ( $height != '' && $position != '' ) $bar_style .= ' line-height: ' . intval($height) . 'px;';
	if ( $round != 0 && $round != '' ) $bar_style .= ' border-radius: ' . intval($round) . 'px;';
	if ( $bgcolor != '' ) $bar_style .= ' background: ' . $bgcolor . ';';

	if ( in_array( 'gradient', $options ) ) {
		$gradient_color = adjustBrightness($bgcolor, 35);
		$bar_style .= ' background: -webkit-gradient(linear, left top, right top, color-stop(0%,' . $bgcolor . '), color-stop(100%,' . $gradient_color . '));';
		$bar_style .= ' background: -webkit-linear-gradient(left, ' . $bgcolor . ' 0%,' . $gradient_color . ' 100%);';
		$bar_style .= ' background: -moz-linear-gradient(left, ' . $bgcolor . ' 0%, ' . $gradient_color . ' 100%);';
		$bar_style .= ' background: -ms-linear-gradient(left, ' . $bgcolor . ' 0%,' . $gradient_color . ' 100%);';
		$bar_style .= ' background: -o-linear-gradient(left, ' . $bgcolor . ' 0%,' . $gradient_color . ' 100%);';
		$bar_style .= ' background: linear-gradient(to right, ' . $bgcolor . ' 0%,' . $gradient_color . ' 100%);';
		$bar_style .= ' filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'' . $bgcolor . '\', endColorstr=\'' . $gradient_color . '\',GradientType=1 );';
	}

	$bar_style .= '"';

	$output = '<div' . $css_class . ' ' . $el_style . '>';
	$output .= '<span' . $bar_class . $bar_style . ' data-percentage-value="' . $percentage_value . '" data-value="' . intval($value) . '"><small class="vsc_label">' . $title . '</small><small class="vsc-bar-value">' . $value . $units . '</small></span>';
	$output .= '</div>';

	return $output;
}

add_shortcode('vc_progress_bar', 'vsc_progress_bar');

/*-----------------------------------------------------------------------------------*/
/*	Pie Chart
/*-----------------------------------------------------------------------------------*/

function vsc_pie_chart($atts, $content = null) {
	$title = $el_class = $el_attrs = $value = $label_value= $units = $t_style = $i_style = $v_style = '';
	$type = $icon_startuplyli = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = '';

	extract(shortcode_atts(array(
		'type' => 'startuplyli',
		'icon_startuplyli' => 'icon icon-graphic-design-13',
		'icon_fontawesome' => 'fa fa-adjust',
		'icon_openiconic' => 'vc-oi vc-oi-dial',
		'icon_typicons' => 'typcn typcn-adjust-brightness',
		'icon_entypoicons' => 'entypo-icon entypo-icon-note',
		'icon_linecons' => 'vc_li vc_li-heart',
		'icon_entypo' => 'entypo-icon entypo-icon-note',

		'title' => '',
		'value' => '50',
		'label_value' => '',
		'units' => '',
		'inside_content' => 'number',
		'title_position' => 'bottom',
		'icon' => '',
		'thickness' => '6',
		'radius' => '',
		'animation' => '',
		'el_class' => '',
		'bar_color' => '',
		'title_color' => '',
		'icon_color' => '',
		'value_color' => '',
		'units_color' => ''
	), $atts));

	wp_enqueue_script('vsc-custom-pie', array( 'jquery', 'waypoints', 'progressCircle' ), true);

	if ( !empty($bar_color) ) {
		preg_match( '/#([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?\b/', $bar_color, $matches);

		if( !empty($matches) ) $bar_color = 'rgba(' . hex2rgb($bar_color) . ',1)';
	}

	$css_class = ' class= "vsc_pie_chart heading-font wpb_content_element ' . $el_class;
	if ( $animation == 'no' ) $css_class .= ' no-animation';
	$css_class .= '"';

	$el_attrs .= ' data-pie-value="'. intval($value) .'"';
	$el_attrs .= ' data-pie-label-value="'. intval($label_value) .'"';
	$el_attrs .= ' data-pie-units="'. $units .'"';
	$el_attrs .= ' data-pie-units-color="'. $units_color .'"';
	$el_attrs .= ' data-pie-color="' . $bar_color . '"';
	$el_attrs .= ' data-thickness="' . intval($thickness) . '"';

	if ( !empty($title_color) ) $t_style = ' style="color: ' . $title_color . ';"';
	if ( !empty($icon_color) ) $i_style = ' style="color: ' . $icon_color . ';"';
	if ( !empty($value_color) ) $v_style = ' style="color: ' . $value_color . ';"';

	$bg_styles = ' style="border-width: '. intval($thickness) . 'px; ' . ( ( !empty($bar_color) ) ? 'border-color: ' . $bar_color . ';' : '') . '"';

	$output = "\n\t".'<div' . $css_class . $el_attrs .'>';
	$output .= "\n\t\t" . '<div class="wpb_wrapper">';
	$output .= "\n\t\t\t" . '<div class="vsc_pie_wrapper" data-radius="' . intval($radius) . '">';
	$output .= "\n\t\t\t" . '<span class="vsc_pie_chart_back base_clr_brd"' . $bg_styles . '></span>';

	if (!empty($type)) {

		if (!empty(${"icon_" . $type})) {
			$icon = esc_attr( ${"icon_" . $type} );
		}

		if ( strpos($icon, ' icon') === false ) {
			$icon = 'icon '.$icon;
		}

		vc_icon_element_fonts_enqueue( $type );

	} else {
		$icon = 'fa icon '.$icon;
	}


	if ( $title_position == 'inside' && $title != '' && $inside_content != 'title' ) $output .= "\n\t\t\t" . '<div class="vsc_pie_chart_inside">';
	if ( $inside_content == 'number' ) $output .= "\n\t\t\t" . '<span class="vsc_pie_chart_value"' . $v_style . '></span>';
	elseif ( $inside_content == 'icon' ) $output .= "\n\t\t\t" . '<span class="vsc_pie_chart_icon"><i class="' . $icon . '"' . $i_style . '></i></span>';
	if ( $title_position == 'inside' && $title != '' || $inside_content == 'title' ) $output .= "\n\t\t\t" . '<h4 class="wpb_heading wpb_pie_chart_heading"' . $t_style . '>' . $title . '</h4>';
	if ( $title_position == 'inside' && $title != '' && $inside_content != 'title' ) $output .= "\n\t\t\t" . '</div>';

	$output .= "\n\t\t\t" . '<canvas width="101" height="101"></canvas>';
	$output .= "\n\t\t\t" . '</div>';

	if ( $title_position == 'bottom' && $title != '' && $inside_content != 'title' ) $output .= '<h4 class="wpb_heading wpb_pie_chart_heading"' . $t_style . '>' . $title . '</h4>';

	$output .= "\n\t\t".'</div>';
	$output .= "\n\t".'</div>';

	return $output;
}

add_shortcode('vc_pie', 'vsc_pie_chart');


/*-----------------------------------------------------------------------------------*/
/*	Counter
/*-----------------------------------------------------------------------------------*/

function vsc_counter($atts, $content = null) {
	$title = $value = $units = $title_font_size = $val_font_size = $icon = $icon_position = $options = '';
	$animation = $separator = $el_class = $label_color = $value_color = $units_color = $title_color = $icon_color = $tstyle = '';
	$type = $icon_startuplyli = $icon_fontawesome = $icon_openiconic = $icon_typicons = $icon_entypo = $icon_linecons = '';

	extract(shortcode_atts(array(
		'type' => 'startuplyli',
		'icon_startuplyli' => 'icon icon-graphic-design-13',
		'icon_fontawesome' => 'fa fa-adjust',
		'icon_openiconic' => 'vc-oi vc-oi-dial',
		'icon_typicons' => 'typcn typcn-adjust-brightness',
		'icon_entypoicons' => 'entypo-icon entypo-icon-note',
		'icon_linecons' => 'vc_li vc_li-heart',
		'icon_entypo' => 'entypo-icon entypo-icon-note',

		'title' => '',
		'value' => '0',
		'units' => '',
		'title_font_size' => '',
		'val_font_size' => '',
		'icon' => '',
		'icon_position' => 'top',
		'options' => '',
		'el_class' => '',
		'label_color' => '',
		'title_color' => '',
		'icon_color' => '',
		'value_color' => '',
		'units_color' => ''
	), $atts));

	wp_enqueue_script('vsc-counter', array( 'jquery', 'waypoints' ), true);

	$options = explode( ',', $options );
	if ( in_array( 'no_animation', $options ) ) $animation .= 'no';
	if ( in_array( 'separator', $options ) ) $separator .= 'yes';

	$css_class = ' class= "vsc_counter wpb_content_element heading-font ' . $el_class;
	if ( $animation != '' ) $css_class .= ' no-animation';
	if ( $separator != '' ) $css_class .= ' separator';
	$css_class .= '"';

	$tstyle = ' style="'. ( ( $title_color != '' ) ? 'color: ' . $title_color . ';' : '' ) . ( ( $title_font_size != '' ) ? ' font-size: ' . intval($title_font_size) . 'px;' : '' ) . '"';

	$output = "\n\t" . '<div' . $css_class . ( ( $animation != 'no' ) ? ' data-counter-value="' . intval($value) . '" data-counter-units="' . $units .'"' : '' ) . '>';
	$output .= "\n\t\t" . '<span class="vsc_counter_label base_clr_txt ' . $icon_position . '"' . ( ( $label_color != '' ) ? ' style="color: ' . $label_color . ';"' : '' ) . '>';


	if (!empty($type)) {
		$icon = esc_attr( ${"icon_" . $type} );
		if ( strpos($icon, ' icon') === false ) {
			$icon = 'icon '.$icon;
		}

		vc_icon_element_fonts_enqueue( $type );

	} else {
		$icon = 'fa icon '.$icon;
	}

	if ( $icon != 'fa icon ' ) $output .= "\n\t\t\t" . '<i class="fa icon ' . $icon . '"' . ( ( $icon_color != '' ) ? 'style="color: ' . $icon_color . ';"' : '' ) . '></i>';

	$output .= "\n\t\t\t" . '<em class="vsc_counter_value" style="' . ( ( $val_font_size != '' ) ? ' font-size: ' . intval($val_font_size) . 'px;' : '' ) . ( ( $value_color != '' ) ? ' color: ' . $value_color . ';' : '' ) . '"><span style="'.( ( $value_color != '' ) ? ' color: ' . $value_color . ';' : '' ).'" class="vsc_counter_value_place">';
	$output .= ( ( $animation == 'no' ) ? intval($value) : '' );
	$output .= '</span> <i class="units"' . ( ( $units_color != '' ) ? ' style="color: ' . $units_color . ';"' : '' ) . '>' . $units . '</i>' . '</em></span>';

	if ( $title != '' ) $output .= "\n\t\t" . '<h4 class="vsc_counter_title"' . $tstyle . '>' . $title . '</h4>';

	$output .= "\n\t" . '</div>';

	return $output;
}

add_shortcode('vsc-counter', 'vsc_counter');


/*-----------------------------------------------------------------------------------*/
/*	Team Member
/*-----------------------------------------------------------------------------------*/

/* Old */

function vsc_team_member($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => ''
	), $atts));

	global $post;

	$args = array(
		'post_type' => 'team',
		'posts_per_page' => 1,
		'p' => $id
	);

	$team_query = new WP_Query($args);
	if ($team_query->have_posts()):
		while ($team_query->have_posts()):
			$team_query->the_post();

			$member_text = get_post_meta($post->ID, 'vsc_member_text', true);
			$position = get_post_meta($post->ID, 'vsc_member_position', true);
			$twitter = get_post_meta($post->ID, 'vsc_member_twitter', true);
			$facebook = get_post_meta($post->ID, 'vsc_member_facebook', true);
			$email = get_post_meta($post->ID, 'vsc_member_mail', true);
			$linkedin = get_post_meta($post->ID, 'vsc_member_linkedin', true);
			$google = get_post_meta($post->ID, 'vsc_member_google', true);

			$team_thumb_icon = get_post_meta($post->ID, 'vsc_team_thumb_icon', true);

			$mail = $email;

			$image = get_the_post_thumbnail($id, 'member-thumb', array(
				'class' => 'img-responsive img-circle'
			));
			$url_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

			$retour = '';
			$retour .= '<div class="team-member member">';
			$retour .= '<div class="thumb-wrapper">';


			$retour .= '<div class="title-wrap">';
			$retour .= '<p class="h7 base_clr_txt">';

			$member_name = get_the_title();
			$member_name = str_replace(' ', '<br/>', $member_name );
			$retour .= $member_name;

			$retour .= '</p>';
			$retour .= '</div>';

			$retour .= '<div class="overlay img-circle"></div>';

			$retour .= '<div class="team-text">';
			$retour .= '<div class="grid-child">';
			if (!empty($position)) {
				$retour .= '<p class="thin team-subtitle">' . $position . '</p>';
			}
			if (!empty($member_text)) {
				$retour .= '<p class="member-content">' . wp_kses_post($member_text) . '</p>';
			}
			$retour .= '</div>';
			$retour .= '<div class="helper">';
			$retour .= '</div>';
			$retour .= '</div>';


			$retour .= $image;
			$retour .= '</div>';


			$retour .= '<div class="socials">';
			if (!empty($mail)) {
				$retour .= '<a href="mailto:' . $mail . '"><i class="icon icon-chat-messages-13"></i></a>';
			}
			if (!empty($facebook)) {
				$retour .= '<a href="' . esc_url($facebook) . '"><i class="icon icon-socialmedia-08"></i></a>';
			}
			if (!empty($twitter)) {
				$retour .= '<a href="' . esc_url($twitter) . '"><i class="icon icon-socialmedia-07"></i></a>';
			}
			if (!empty($google)) {
				$retour .= '<a href="' . esc_url($google) . '"><i class="icon icon-socialmedia-16"></i></a>';
			}
			if (!empty($linkedin)) {
				$retour .= '<a href="' . esc_url($linkedin) . '"><i class="icon icon-socialmedia-05"></i></a>';
			}
			$retour .= '</div>';


			$retour .= '</div>';
		endwhile;
	else:
		$retour = '';
		$retour .= "nothing found.";
	endif;

	//Reset Query
	wp_reset_query();

	return $retour;
}

add_shortcode('vsc-team-member', 'vsc_team_member');


/* New */

function vsc_team_member_new($atts, $content = null) {
	$name = $position = $description = $photo = $email = $twitter = $facebook = $skype = $linked_in = $google_plus = $options = '';
	$output = $name_str = $position_str = $img_val = $img = $description_str = $social = $twitter_url = $facebook_url = '';
	$magazine = false;

	extract(shortcode_atts(array(
		"name" => '',
		"position" => '',
		"description" => '',
		"photo" => '',
		"email" => '',
		"twitter" => '',
		"facebook" => '',
		"skype" => '',
		"linked_in" => '',
		"google_plus" => '',
		"options" => ''
	), $atts));

	$options = explode( ',', $options );
	if ( in_array( 'magazine', $options ) ) $magazine = true;

	if ($photo != '') {
		if (function_exists('wpb_getImageBySize')) {
			$img_val = wpb_getImageBySize(array(
				'attach_id' => (int) $photo,
				'thumb_size' => 'full'
			));
		}

		$img = $img_val['p_img_large'][0];
	} else {
		$img = '<img src="" class="no-image" alt="">';
	}

	if ($name != '') {
		$name_str = '<h3 class="member-name base_clr_txt">' . $name . '</h3>';
	}

	if ($position != '') {
		$position_str = '<span class="heading-font">' . $position . '</span>';
	}

	if ($description != '') {
		$description_str = '<p>';
		if ($magazine) $description_str .= $position_str;
		$description_str .= $description . '</p>';
	}

	$socials = array();

	if (!empty($email)) $socials[] = $email;
	if (!empty($twitter)) $socials[] = $twitter;
	if (!empty($facebook)) $socials[] = $facebook;
	if (!empty($skype)) $socials[] = $skype;
	if (!empty($linked_in)) $socials[] = $linked_in;
	if (!empty($google_plus)) $socials[] = $google_plus;

	if (count($socials) != 0) {
		$social = '<ul class="socials-block' . ((count($socials) < 3) ? ' center' : '') . '">';

		if ($email != '') {
			$social .= "\n\t\t" . '<li><a href="mailto:' . $email . '" class="base_clr_brd base_clr_bg email" title="' . $email . '">';
			if ($magazine) $social .= '<i class="icon icon-chat-messages-13"></i>';
			else $social .= '<i class="fa fa-envelope-o"></i>';
			$social .= '</a></li>';

			// if ($magazine)
		}

		if ($twitter != '') {
			$twitter_url = str_replace(array(
					'https://twitter.com/',
					'http://twitter.com/'
				), array(
					'',
					''
				), $twitter);

			$social .= "\n\t\t" . '<li><a href="https://twitter.com/' . $twitter_url . '" class="base_clr_brd base_clr_bg twitter" title="' . $twitter_url . '" target="_blank">';
			if ($magazine) $social .= '<i class="icon icon-socialmedia-07"></i>';
			else $social .= '<i class="fa fa-twitter"></i>';
			$social .= '</a></li>';
		}

		if ($facebook != '') {
			$facebook_url = str_replace(array(
					'https://www.facebook.com/',
					'http://www.facebook.com/'
				), array(
					'',
					''
				), $facebook);

			$social .= "\n\t\t" . '<li><a href="https://facebook.com/' . $facebook_url . '" class="base_clr_brd base_clr_bg facebook" title="' . $facebook_url . '" target="_blank">';
			if ($magazine) $social .= '<i class="icon icon-socialmedia-08"></i>';
			else $social .= '<i class="fa fa-facebook"></i>';
			$social .= '</a></li>';
		}

		if ($skype != '') {
			$social .= "\n\t\t" . '<li><a href="skype:' . $skype . '?chat" class="base_clr_brd base_clr_bg skype" title="' . $skype . '">';
			if ($magazine) $social .= '<i class="icon icon-socialmedia-09"></i>';
			else $social .= '<i class="fa fa-skype"></i>';
			$social .= '</a></li>';
		}

		if ($linked_in != '') {
			$linked_in_url = str_replace(array(
					'https://www.linkedin.com/',
					'http://www.linkedin.com/',
					'profile/view?id=',
					'&trk=nav_responsive_tab_profile_pic'
				), array(
					'',
					'',
					'',
					''
				), $linked_in);

			$social .= "\n\t\t" . '<li><a href="https://www.linkedin.com/profile/view?id=' . $linked_in_url . '" class="base_clr_brd base_clr_bg linked_in" title="' . $linked_in_url . '" target="_blank">';
			if ($magazine) $social .= '<i class="icon icon-socialmedia-05"></i>';
			else $social .= '<i class="fa fa-linkedin"></i>';
			$social .= '</a></li>';
		}

		if ($google_plus != '') {
			$google_plus_url = str_replace(array(
					'https://plus.google.com/',
					'http://plus.google.com/',
					'u/0/',
					'/posts'
				), array(
					'',
					'',
					'',
					''
				), $google_plus);

			$social .= "\n\t\t" . '<li><a href="https://plus.google.com/' . $google_plus_url . '" class="base_clr_brd base_clr_bg google_plus" title="' . $google_plus_url . '" target="_blank">';
			if ($magazine) $social .= '<i class="icon icon-socialmedia-16"></i>';
			else $social .= '<i class="fa fa-google-plus"></i>';
			$social .= '</a></li>';
		}

		$social .= '</ul>';

	}

	$output .= "\n\t" . '<div class="vsc_team_member wpb_content_element' . (($magazine) ? ' magazine' : '') . '">';
	$output .= "\n\t\t" . '<div class="photo-wrapper base_clr_bg" style="background-image: url(' . $img . ');">';
	$output .= "\n\t\t\t" . '<div class="meta-overlay">';
	$output .= "\n\t\t\t\t" . '<div class="text-wrapper">';
	$output .= "\n\t\t\t\t\t" . '<div class="text-container">';
	$output .= "\n\t\t\t\t\t" . $description_str;
	$output .= "\n\t\t\t\t\t" . '</div>';
	$output .= "\n\t\t\t\t" . '</div>';
	$output .= "\n\t\t\t\t" . $social;
	$output .= "\n\t\t\t" . '</div>';
	if ($magazine) $output .= "\n\t\t\t" . $name_str;
	$output .= "\n\t\t" . '</div>';
	if (!$magazine) $output .= "\n\t\t" . $name_str;
	if (!$magazine) $output .= "\n\t\t" . $position_str;
	$output .= "\n\t\t" . $social;
	$output .= "\n\t" . '</div>';

	return $output;
}

add_shortcode('vsc-team-member-new', 'vsc_team_member_new');


/*-----------------------------------------------------------------------------------*/
/*	Call to action
/*-----------------------------------------------------------------------------------*/

function vsc_call_to_action($atts, $content = null) {
	extract(shortcode_atts(array(

		'type' => 'startuplyli',
		'icon_startuplyli' => 'icon icon-graphic-design-13',
		'icon_fontawesome' => 'fa fa-adjust',
		'icon_openiconic' => 'vc-oi vc-oi-dial',
		'icon_typicons' => 'typcn typcn-adjust-brightness',
		'icon_entypoicons' => 'entypo-icon entypo-icon-note',
		'icon_linecons' => 'vc_li vc_li-heart',
		'icon_entypo' => 'entypo-icon entypo-icon-note',
		'media_type' => 'icon-type', // default value

		'title' => '',
		'dicon' => '',
		'istyle' => '',
		'img' => '',
		'text' => 'Button Text',
		'url' => '',
		'title_color' => '',
		'text_color' => '',
		'button_color' => '',
		'btn_style' => 'btn-solid'
	), $atts));

	$cstyle = '';
	if ($istyle == 'bold') {
		$cstyle = 'bold-fill';
	} else if ($istyle == 'thin') {
		$cstyle = 'thin-fill';
	} else if ($istyle == 'free') {
		$cstyle = 'no-fill';
	}

	$btn_class = 'btn ';

	if ( $btn_style == 'btn-solid' ) {
		$btn_class .= 'btn-solid base_clr_bg';
	} else if ( $btn_style == 'btn-no-border' ) {
		$btn_class .= 'btn-no-border base_clr_txt';
	} else if ( $btn_style == 'btn-outline-color' ) {
		$btn_class .= 'btn-outline-color base_clr_txt base_clr_bg base_clr_brd';
	} else if ( $btn_style == 'alt' ) { // what?
		$btn_class .= 'btn-outline-color base_clr_txt base_clr_bg base_clr_brd';
	} else if ( $btn_style == '' ) {
		$btn_class .= 'btn-solid base_clr_bg';
	} else {
		$btn_class .= 'btn-outline base_clr_txt';
	}

	$i_color = '';
	$title_style = '';

	if ($title_color != '') {
		$title_style = 'style="color: ' . $title_color . ';"';
		$i_color = ' color: ' . $title_color . ';';
	}

	$text_style = '';
	if ($text_color != '') $text_style = 'style="color: ' . $text_color . ';"';

	$button_style = '';
	$block_class = '';
	if ($button_color != '') {
		$block_class = 'block_' . vsc_random_id(10);
		$button_style = '<style> .' . $block_class . ' .base_clr_brd { border-color: ' . $button_color . '; } .' . $block_class . ' .base_clr_txt { color: ' . $button_color . '; } .' . $block_class . ' .base_clr_bg { background-color: ' . $button_color . '; } </style>';
	}

	$icon_style = 'style="position: absolute; left: 20px; margin-left: 0; margin-right: 0;' . $i_color . '"';

	$icon = '';

	if ($media_type == 'icon-type') {

		//if (!empty(${"icon_" . $type})) {
			$dicon = esc_attr( ${"icon_" . $type} );
		//}

		$icon .= '<i class="pull-left ' . $dicon . '" ' . $icon_style . '></i>';

	} else if ($media_type == 'img-type') {
		$img_val = '';
		if (function_exists('wpb_getImageBySize')) {
			$img_val = wpb_getImageBySize(array(
				'attach_id' => (int) $img,
				'thumb_size' => 'full'
			));
		}

		$icon .= $img_val['thumbnail'];
	}

	$output = '';
	$output = $button_style . '
<div class="long-block light ' . $cstyle . ' ' . $block_class .'">
<div class="container">
<div class="col-md-12 col-lg-9">
' . $icon . '
<article class="pull-left" style="padding-left: 83px;">
<h2 ' . $title_style . ' >' . $title . '</h2>
<p class="thin" ' . $text_style . '>' . $content . '</p>
</article>
</div>

<div class="col-md-12 col-lg-3">
<div class="pull-left btn-wrapper" style="padding-left: 83px;">
	<a href="' . $url . '" class="' . $btn_class . '">' . $text . '</a>
</div>
</div>
</div>
</div>';

	return $output;
}

add_shortcode('vsc-call-to-action', 'vsc_call_to_action');

/*-----------------------------------------------------------------------------------*/
/*	Pricing Table
/*-----------------------------------------------------------------------------------*/

//pricing table placebo

function vsc_pricing_table_placebo($atts, $content = null) {
	return do_shortcode($content);
}
add_shortcode('vsc-table_placebo', 'vsc_pricing_table_placebo');


// body
function vsc_pricing_table($atts, $content = null) {
	global $vsc_table;
	extract(shortcode_atts(array(
		'columns' => '4'
	), $atts));

	$columnsNr = '';
	$finished_table = '';

	switch (intval($columns)) {
		case '2':
			$columnsNr .= 'col-sm-6';
			break;
		case '3':
			$columnsNr .= 'col-sm-4';
			break;
		case '4':
			$columnsNr .= 'col-sm-3';
			break;
		case '6':
			$columnsNr .= 'col-sm-2';
			break;
	}

	do_shortcode($content);

	$columnContent = '';
	if (is_array($vsc_table)) {

		for ($i = 0; $i < count($vsc_table); $i++) {
			$columnClass = 'pricing-column';
			$n = $i + 1;
			$columnClass .= ($n % 2) ? '' : ' even-column';
			$columnClass .= ($vsc_table[$i]['featured']) ? ' featured-column' : '';
			$columnClass .= ($n == count($vsc_table)) ? ' last-column' : '';
			$columnClass .= ($n == 1) ? ' first-column' : '';
			$columnContent .= '<div class="' . $columnClass . ' ' . $columnsNr . '">';

			$columnContent .= '<div class="package-column heading-font base_clr_bg">';

			if (($vsc_table[$i]['featured']) == '1') {
				$columnContent .= '<div class="column-shadow"></div>';
			}
			$columnContent .= '<div class="package-title">' . $vsc_table[$i]['title'] . '</div><div class="package-value package-price base_clr_txt"><div class="price"><span class="package-currency currency">' . $vsc_table[$i]['currency'] . '</span>' . $vsc_table[$i]['price'] . '</div><div class="period base_clr_txt"><span class="package-time">' . $vsc_table[$i]['interval'] . '</span></div></div>';
			$columnContent .= '<div class="package-features package-detail">' . str_replace(array(
				"\r\n",
				"\n",
				"\r",
				"<p></p>"
			), array(
				"",
				"",
				"",
				""
			), $vsc_table[$i]['content']) . '</div>';
			$columnContent .= '</div>';

			$columnContent .= '</div>';

		}
		$finished_table = '<div class="pricing-table">' . $columnContent . '</div>';
	}

	$vsc_table = '';

	return $finished_table;

}

add_shortcode('vsc-pricing-table', 'vsc_pricing_table');


// Single Column
function vsc_shortcode_pricing_column($atts, $content = null) {
	global $vsc_table;
	extract(shortcode_atts(array(
		'title' => '',
		'price' => '',
		'currency' => '',
		'interval' => '',
		'featured' => 'false'
	), $atts));

	$featured = strtolower($featured);

	$column['title'] = $title;
	$column['price'] = $price;
	$column['currency'] = $currency;
	$column['interval'] = $interval;
	$column['featured'] = ($featured == 'true' || $featured == 'yes' || $featured == '1') ? true : false;
	$column['content'] = do_shortcode($content);

	$vsc_table[] = $column;

}

add_shortcode('vsc-pricing-column', 'vsc_shortcode_pricing_column');

/* Initialising shortcodes */
function shortcode_init($atts, $content = null) {
	return '<div class="signup">' . do_shortcode($content) . '</div>';
}

add_shortcode('vsc-signup', 'shortcode_init');

/*-----------------------------------------------------------------------------------*/
/*	Testimonial Item
/*-----------------------------------------------------------------------------------*/

function vsc_testimonials($atts, $content = null) {
	extract(shortcode_atts(array(
		"id" => '',
		"photo" => '',
		"desc" => '',
		"who" => ''
	), $atts));

	$photo = ($photo === 'true');
	$desc = ($desc === 'true');
	$who = ($who === 'true');

	//print_r($atts);

	global $post;

	$args = array(
		'post_type' => 'testimonials',
		'posts_per_page' => 1,
		'p' => $id
	);
	$my_query = new WP_Query($args);
	if ($my_query->have_posts()):
		while ($my_query->have_posts()):
			$my_query->the_post();

			$testimonial_desc = get_post_meta($post->ID, 'vsc_testimonial_desc', true);
			$testimonial_name = get_the_title($post->ID);
			$testimonial_details = get_post_meta($post->ID, 'vsc_testimonial_details', true);

			$testimonial_icon = get_post_meta($post->ID, 'vsc_testimonials_thumb_icon', true);
			$testimonial_icon = 'flat_image'; //temp hack

			$image = get_the_post_thumbnail($id, 'testimonials-thumb', array(
				'class' => 'testimonials-avatar sm-pic img-circle pull-left'
			));
			$url_image = wp_get_attachment_url(get_post_thumbnail_id($post->ID));

			//$retour = '<div class="testimonial-item">';
			$retour = '';
			$photo_html = '';
			$desc_html = '';
			$who_html = '';

			if ($testimonial_icon != 'flat_image') {
				$photo_html .= '<a href="' . $url_image . '" data-pretty="prettyPhoto" title="' . get_the_title() . '">';
				$photo_html .= '<span class="item-on-hover"><span class="hover-image"><i class="fa fa-search"></i></span></span>';
				$photo_html .= $image;
				$photo_html .= '</a>';
			} else {
				$photo_html .= $image;
			}

			$desc_html .= '<p>' . wp_kses_post($testimonial_desc) . '</p>';

			$who_html .= '<p><span class="testimonial-name base_clr_txt">' . esc_html($testimonial_name) . '</span><span class="testimonial-position">' . esc_html($testimonial_details) . '</span></p>';

			if (!$photo && !$desc && !$who) {
				$retour .= $photo_html;
				$retour .= $desc_html;
				$retour .= $who_html;
			} else {
				if ($photo) {
					$retour .= $photo_html;
				}
				if ($desc) {
					$retour .= $desc_html;
				}
				if ($who) {
					$retour .= $who_html;
				}
			}
		//$retour .='</div>';
		endwhile;
	else:
		$retour = '';
		$retour .= "nothing found.";
	endif;

	//Reset Query
	wp_reset_query();

	return $retour;
}

add_shortcode("vsc-testimonial", "vsc_testimonials");


/*-----------------------------------------------------------------------------------*/
/*	Testimonial Carousel for Visual Composer
/*-----------------------------------------------------------------------------------*/

function vsc_testimonials_slider($atts, $content = null) {
	extract(shortcode_atts(array(
		"text" => '',
		"ids" => ''
	), $atts));

	wp_enqueue_script('flexslider', array('jquery'), true);

	$controls_block = $content_block = '';

	$testimonial_ids = explode(",", $ids);

	$rnd_id = vsc_random_id(3);
	$token = wp_generate_password(5, false, false);

	$content_block  = '<section class="section light feedback">';
	$content_block .= '<div class="container">';
	$content_block .= '<div class="section-header"><h2>' . $text . '</h2></div>';
	$content_block .= '<div class="section-content">';
	$content_block .= '<div class="col-sm-10 col-sm-offset-1">';
	$content_block .= '<div class="testimonials-slider heading-font flexslider center" data-animation="fadeInTop">';
	$content_block .= '<ul class="slides">';

	foreach ($testimonial_ids as $tid) {
		$content_block .= '<li data-id="' . $tid . '"><div class="testimonial resp-center clearfix"><blockquote>' . do_shortcode("[vsc-testimonial id=" . $tid . " desc=true]") . "</blockquote></div></li>";
	}

	$content_block .= '</ul></div></div></div></div></section>';

	$controls_block  = '<div id="feedback-controls" class="section light">';
	$controls_block .= '<div class="container">';
	$controls_block .= '<div class="col-md-10 col-md-offset-1">';
	$controls_block .= '<div class="flex-manual">';

	foreach ($testimonial_ids as $tid) {
		$controls_block .= '<div data-href="' . $tid . '" class="col-xs-12 col-sm-4 wrap"><div class="switch">' . do_shortcode("[vsc-testimonial id=" . $tid . " photo=true who=true]") . "</div></div>";
	}

	$controls_block .= '</div></div></div></div>';

	$output = $content_block;
	$output .= $controls_block;

	return $output;
}

add_shortcode("vsc-testimonials-slider", "vsc_testimonials_slider");


/*-----------------------------------------------------------------------------------*/
/*	Newsletter subscribe
/*-----------------------------------------------------------------------------------*/

function vsc_newsletter($atts, $content = null) {
	extract(shortcode_atts(array(
		"api_key" => '',
		"list_id" => '',
		"double_opt" => '',
		/*"form_html" => ''*/
	), $atts));

	$rnd_id = vsc_random_id(3);

	//$output = do_shortcode(rawurldecode(base64_decode(strip_tags($form_html))));
	$output = do_shortcode($content);

	$dopt = isset($double_opt) && $double_opt == 'yes' ? 'true' : 'false';

	$output = str_replace(array(
		'{form}',
		'{name}',
		'{email}',
		'{submit}',
		'{response}',
		'{/form}'
	), array(
		'<form id="subscribe_' . $rnd_id . '" class="form subscribe-form" method="post"><input type="hidden" class="apiKey" name="apiKey" value="' . $api_key . '" /><input type="hidden" class="listId" name="listId" value="' . $list_id . '" /><input type="hidden" class="dopt" name="dopt" value="' . $dopt . '" />',
		'<div class="col-sm-4"><input type="text" class="NewsletterName form-control" name="NewsletterName" placeholder="Your name" /></div>',
		'<div class="col-sm-4"><input type="email" class="NewsletterEmail form-control" name="NewsletterEmail" placeholder="your@email.com" /></div>',
		'<div class="col-sm-4"><input class="btn base_clr_txt btn-outline" type="submit" value="SUBSCRIBE" /></div>',
		'<span class="wrapper-response-block"><span class="response_wrap" id="response_' . $rnd_id . '"></span></span>',
		'</form>'
	), $output);

	return $output;
}
add_shortcode("vsc-newsletter", "vsc_newsletter");


/*-----------------------------------------------------------------------------------*/
/*	Portfolio Grid Shortcode
/*-----------------------------------------------------------------------------------*/

function vsc_portfolio_grid($atts, $content = null) {
	extract(shortcode_atts(array(
		"number" => "-1",
		"categories" => "",
		"columns_number" => "4",
		"show_filters" => "On",
		"gutter_width" => "",
		"hover_color" => ""
	), $atts));

	global $post;

	//setting a random id
	$rnd_id = vsc_random_id(3);

	$token = wp_generate_password(5, false, false);

	wp_enqueue_script('vsc-isotope');
	wp_enqueue_script('vsc-custom-isotope-portfolio');

	$args = array(
		'grid_manager' => 1,
		'grid_phone' => 1,
		'grid_tablet' => 2,
		'grid_small' => $columns_number,
		'grid_wide' => $columns_number,
		'grid_very_wide' => $columns_number,
		'grid_normal' => $columns_number,
		'grid_gutter_width' => 0
	);

	wp_localize_script('vsc-custom-isotope-portfolio', 'vsc_grid_' . $token, array(
		'id' => $rnd_id, 'gutter_width' => $gutter_width, 'vals' => $args
	));

	// wp_localize_script( 'vsc-custom-isotope-portfolio', 'vals', $args );

	$layout = get_post_meta($post->ID, 'vsc_portfolio_columns', true);
	$navig = get_post_meta($post->ID, 'vsc_portfolio_navigation', true);
	$nav_number = get_post_meta($post->ID, 'vsc_nav_number', true);

	$cats = explode(",", $categories);

	$portfolio_categs = get_terms('portfolio_cats', array(
		'hide_empty' => false
	));
	$categ_list = '';

	foreach ($cats as $categ) {
		foreach ($portfolio_categs as $portfolio_categ) {
			if ($categ === $portfolio_categ->name) {
				$categ_list .= $portfolio_categ->slug . ', ';
			}
		}
	}

	//fallback categories
	$args = array(
		'post_type' => 'portfolio',
		'taxonomy' => 'portfolio_cats'
	);
	$categ_fall = get_categories($args);
	$categ_use = array();
	$i = 0;
	foreach ($categ_fall as $cate) {
		$categ_use[$i] = $cate->name;
		$i++;
	}
	$cats = array_filter($cats);
	if (empty($cats)) {
		$cats = array_merge($cats, $categ_use);
	}


	$term_list = '';
	$list = '';

	if ($show_filters == 'On'){
		foreach ($cats as $cat) {
			$to_replace = array(
				' ',
				'/',
				'&'
			);
			$intermediate_replace = strtolower(str_replace($to_replace, '-', $cat));
			$str = preg_replace('/--+/', '-', $intermediate_replace);
			if (function_exists('icl_t')) {
				$term_list .= '<li><a class="btn btn-outline-color base_clr_txt base_clr_bg base_clr_brd" href="#filter" data-option-value=".' . get_taxonomy_cat_ID($cat) . '">' . icl_t('Portfolio Category', 'Term ' . get_taxonomy_cat_ID($cat) . '', $cat) . '</a></li>';
			} else
				$term_list .= '<li><a class="btn btn-outline-color base_clr_txt base_clr_bg base_clr_brd" href="#filter" data-option-value=".' . get_taxonomy_cat_ID($cat) . '">' . $cat . '</a></li>';
			$list .= $cat . ', ';
		}
	}


	$output = '';
	$output .= '<div class="vivaco-grid" id="gridwrapper_' . $rnd_id . '" data-token="' . $token . '">';
	$output .= '<div id="options">';
	$output .= '<ul id="filters" class="option-set clearfix" data-option-key="filter">';
	if ($show_filters == 'On'){
		$output .= '<li><a class="btn btn-outline-color base_clr_txt base_clr_bg base_clr_brd selected" href="#filter" data-option-value="*">' . __('All', 'vivaco') . '</a></li>';
	}
	$output .= $term_list;
	$output .= '</ul>';
	$output .= '</div>';
	$output .= '<div class="space"></div>';

	$output .= '<div id="portfolio-wrapper">';
	$output .= '<ul class="portfolio grid isotope grid_' . $rnd_id . '">';

	if ( $number != '-1' ) $number = intval($number);

	$args = array(
		'post_type' => 'portfolio',
		'posts_per_page' => $number,
		'term' => 'portfolio_cats',
		'portfolio_cats' => $categ_list,
		'orderby' => 'date',
		'order' => 'desc'
	);

	$my_query = new WP_Query($args);
	if ($my_query->have_posts()) {
		while ($my_query->have_posts()):
			$my_query->the_post();

			$terms = get_the_terms(get_the_ID(), 'portfolio_cats');
			$term_val = '';
			if ($terms) {
				foreach ($terms as $term) {
					$term_val .= get_taxonomy_cat_ID($term->name) . ' ';
				}
			}

			$port_box = get_post_meta($post->ID, 'vsc_port_box', true);
			$port_thumbnail = get_post_meta($post->ID, 'vsc_port_thumbnail', true);
			$port_link = get_post_meta($post->ID, 'vsc_port_link', true);
			$port_video = get_post_meta($post->ID, 'vsc_port_video', true);

			$lgal = get_post_meta($post->ID, 'vsc_port_gallery', true);

			$gal_output = '';
			if (!empty($lgal)) {
				foreach ($lgal as $gal_item) {
					$gal_item_url = $gal_item['vsc_gl_url']['url'];
					$gal_item_title = get_post($gal_item['vsc_gl_url']['id'])->post_excerpt;

					$gal_output .= '<a class="hidden_image " href="' . $gal_item_url . '" title="' . $gal_item_title . '"></a>';

				}
			}

			$thumb_id = get_post_thumbnail_id($post->ID);

			$image_url = wp_get_attachment_url($thumb_id);

			$grid_thumbnail = $image_url;
			$item_class = 'item-small';

			switch ($port_thumbnail) {
				case 'portfolio-big':
					//$grid_thumbnail = aq_resize($image_url, 566, 440, true);
					$grid_thumbnail = $image_url;
					$item_class = 'item-wide';
					break;
				case 'portfolio-small':
					//$grid_thumbnail = aq_resize($image_url, 305, 251, true);
					$grid_thumbnail = $image_url;
					$item_class = 'item-small';
					break;
				case 'half-horizontal':
					//$grid_thumbnail = aq_resize($image_url, 566, 219, true);
					$grid_thumbnail = $image_url;
					$item_class = 'item-long';
					break;
				case 'half-vertical':
					//$grid_thumbnail = aq_resize($image_url, 281, 440, true);
					$grid_thumbnail = $image_url;
					$item_class = 'item-high';
					break;
			}

			$copy = $terms;
			$res = '';
			if ($terms) {
				foreach ($terms as $term) {
					if (function_exists('icl_t')) {
						$res .= icl_t('Portfolio Category', 'Term ' . get_taxonomy_cat_ID($term->name) . '', $term->name);
					} else
						$res .= $term->name;
					if (next($copy)) {
						$res .= ', ';
					}
				}
			}

			$output .= '<li class="grid-item ' . $term_val . ' ' . $item_class . '">';

			$grid_thumbnail_size = false;

			$grid_thumbnail_path = $_SERVER['DOCUMENT_ROOT'] . parse_url($grid_thumbnail, PHP_URL_PATH);
			if ( !empty($grid_thumbnail) && file_exists($grid_thumbnail_path) ) {
				$grid_thumbnail_size = getimagesize($grid_thumbnail_path);
			}

			$inner_output = '';
			if ($grid_thumbnail_size) {
				$inner_output .= '<img src="' . $grid_thumbnail . '" width="' . $grid_thumbnail_size[0] . '" height="' . $grid_thumbnail_size[1] . '" alt="" />';
			} else {
				$inner_output .= '<img src="' . $grid_thumbnail . '" alt="" />';
			}

			$inner_output .= '<div class="grid-item-on-hover wave-mouseover" data-color="' . $hover_color . '">';
			$inner_output .= '<div class="grid-child">';
			$inner_output .= '<div class="grid-text-title"><h3 class="grid-item-title">' . get_the_title() . '</h3></div>';
			$inner_output .= '<div class="grid-text-subtitle"><span class="grid-item-subtitle">' . $res . '</span></div>';
			$inner_output .= '</div>';
			$inner_output .= '<div class="helper"></div>';
			$inner_output .= '</div>';

			$test_link = '';
			if ($port_box == 'link_to_page') {
				$test_link = '<a class="portfolio" href="' . get_permalink($post->ID) . '">' . $inner_output . '</a>';
			} else if ($port_box == 'link_to_link') {
				$test_link = '<a class="portfolio" href="' . $port_link . '">' . $inner_output . '</a>';
			} else if ($port_box == 'lightbox_to_image') {
				$test_link = '<a class="portfolio" href="' . wp_get_attachment_url($thumb_id) . '" data-pretty="prettyPhoto[port_gal]" title="' . get_the_title() . '">' . $inner_output . '</a>';
			} else if ($port_box == 'lightbox_to_video') {
				$test_link = '<a class="portfolio" href="' . $port_video . '" data-pretty="prettyPhoto[port_gal]" title="' . get_the_title() . '">' . $inner_output . '</a>';
			} else if ($port_box == 'lightbox_to_gallery') {
				$test_link = '<a href="' . wp_get_attachment_url($thumb_id) . '" data-pretty="prettyPhoto[gallery_' . $post->ID . ']" title="' . get_the_title() . '" >' . $inner_output . '</a>' . $gal_output;
			}

			$output .= $test_link;

			$output .= '</li>';
		endwhile;
	}
	wp_reset_query();
	$output .= '</ul>';
	$output .= '</div>';
	$output .= '</div>';
	$output .= '<div class="space"></div>';

	return $output;
}
add_shortcode("vsc-portfolio-grid", "vsc_portfolio_grid");

function vsc_countdown($atts, $content = null) {
	extract( shortcode_atts( array(
		'layout' => '',
		'count' => 'down',
		'notice_countdown' => '',
		'date' => '',
		'format' => '',
		'notice_countdown_format' => '',
		'number_color' => '',
		'text_color' => '',
		'separator_color' => '',
		'num_size' => '',
		'txt_size' => '',
	), $atts ) );

	wp_enqueue_script( 'vsc-countdown' );

	$format = ( $format ) ? ' data-format="'. $format .'"' : '';
	$count = ( $count ) ? ' data-count="'. $count .'"' : '';

	$uniqid_class = '';
	$output = '';

	if ( $number_color || $text_color || $separator_color || $num_size || $txt_size) {
		$uniqid = uniqid();
		$uniqid_class = ' vsc-countdown-'. $uniqid;
		$custom_style = '';

		$custom_style .= ( $number_color ) ? '.vsc-countdown.vsc-countdown-'. $uniqid .' .countdown-section .countdown-amount{color:'. $number_color .';}' : '';
		$custom_style .= ( $text_color ) ? '.vsc-countdown.vsc-countdown-'. $uniqid .' .countdown-section .countdown-period{color:'. $text_color .';}' : '';
		$custom_style .= ( $separator_color ) ? '.vsc-countdown.vsc-countdown-'. $uniqid .' .countdown-section:after{background-color:'. $separator_color .';}' : '';
		$custom_style .= ( $num_size ) ? '.vsc-countdown.vsc-countdown-'. $uniqid .' .countdown-section .countdown-amount {font-size:'. $num_size .';}' : '';
		$custom_style .= ( $txt_size ) ? '.vsc-countdown.vsc-countdown-'. $uniqid .' .countdown-section .countdown-period {font-size:'. $txt_size .';}' : '';

		$output .= '<style id="custom-shortcode-css-'.$uniqid.'" type="text/css">'. vsc_css_compress( $custom_style ) .'</style>';
	}

	$output .= '<span class="vsc-countdown vsc-countdown-'. $layout . $uniqid_class .'" data-date="'. $date .'"'. $format . $count .'></span>';
	return $output;
}
add_shortcode("vsc-countdown", "vsc_countdown");





function vsc_accent($atts, $content = null) {
	extract(shortcode_atts(array(
		"class" => "base_clr_txt",
		"tag" => "span"
	), $atts));
	$output = "<{$tag} class=\"{$class}\">{$content}</{$tag}>";

	return do_shortcode($output);
}
add_shortcode("accent", "vsc_accent");



function contact_form_7_wrapper($atts, $content = null) {
	extract(shortcode_atts(array(
		'title' => '',
		'id' => '',
		'_wpcf7_vsc_use_mailchimp' => 'false',
		'_wpcf7_vsc_mailchimp_list_id' => '',
		'_wpcf7_vsc_double_opt' => 'false',
		'_wpcf7_vsc_redirect_after_send' => 'false',
		'_wpcf7_vsc_redirect_url' => '',
		'_wpcf7_vsc_hide_after_send' => 'false'
	), $atts));

	$_wpcf7_vsc_use_mailchimp = $_wpcf7_vsc_use_mailchimp === 'yes';
	$_wpcf7_vsc_double_opt = $_wpcf7_vsc_double_opt === 'yes';
	$_wpcf7_vsc_redirect_after_send = $_wpcf7_vsc_redirect_after_send === 'yes';
	$_wpcf7_vsc_hide_after_send = $_wpcf7_vsc_hide_after_send === 'yes';

	$token = wp_generate_password(5, false, false);

	wp_enqueue_script( 'vsc-custom-contact-form-7' );

	wp_localize_script( 'vsc-custom-contact-form-7', 'vsc_custom_contact_form_7_' . $token,
		array(
			'_wpcf7_vsc_use_mailchimp' => $_wpcf7_vsc_use_mailchimp,
			'_wpcf7_vsc_mailchimp_list_id' => $_wpcf7_vsc_mailchimp_list_id,
			'_wpcf7_vsc_double_opt' => $_wpcf7_vsc_double_opt,
			'_wpcf7_vsc_redirect_after_send' => $_wpcf7_vsc_redirect_after_send,
			'_wpcf7_vsc_redirect_url' => empty($_wpcf7_vsc_redirect_url) ? $_wpcf7_vsc_redirect_url : base64_encode($_wpcf7_vsc_redirect_url),
			'_wpcf7_vsc_hide_after_send' => $_wpcf7_vsc_hide_after_send
		) );

	$output = "<div class=\"contact-form-7-data\" data-token=\"{$token}\">[contact-form-7 id=\"{$id}\" title=\"{$title}\"]</div>";

	return do_shortcode($output); // redirect to default contact-form-7 shortcode
}
add_shortcode("contact-form-7-wrapper", "contact_form_7_wrapper");





function vsc_product_pricing($atts, $content = null) {
	extract(shortcode_atts(array(
		'el_class' => '',
		'price_type' => 'simple',
		'product_id' => '',
		'product_title' => '',
		'product_currency' => '',
		'product_price' => '',
		'product_period' => '',
		'product_featured' => '',
		'product_buy_link' => '',
		'product_buy_text' => ''
	), $atts));

	$product_price_from = '';
	$is_simple = '';
	$internal_buy_html = '';

	if( is_plugin_active('easy-digital-downloads/easy-digital-downloads.php') &&
		$price_type === 'edd' &&
		!empty($product_id) ) {

		//$product_title = get_the_title($product_id); // we will override product title here
		$is_simple = 'edd_product';
		$product_currency = edd_currency_symbol();

		if(edd_has_variable_prices($product_id)) {
			$product_price_from = '<span class="price-from">'.__('from: ', 'vivaco' ).'</span>';
		}

		$product_price = number_format (edd_get_download_price($product_id),0);

		$product = get_post($product_id);
		if ( $product ) {
			$content = apply_filters('the_content', $product->post_content);
			$content = str_replace(']]>', ']]&gt;', $content);
		}

		$button_behavior = get_post_meta( $product_id, '_edd_button_behavior', true );

		$options = array(
			'download_id' => $product_id,
			'price' => false,
			'direct' => (!empty($button_behavior) && $button_behavior == 'direct') ? true : false,
			'class' => 'btn btn-outline base_clr_txt'
		);

		if(!empty($product_buy_text)) {
			$options['text'] = $product_buy_text;
		}

		$internal_buy_html = '<div class="signup">'.edd_get_purchase_link($options).'</div>';
	}

	if( $price_type === 'simple' || $price_type == '') {
		$internal_buy_html = '<div class="signup">
			'.rawurldecode(base64_decode($product_buy_link)).'
		</div>';
		$is_simple = 'simple_product';
		$content = do_shortcode($content);
	}

	$featured = '';
	if( $product_featured === 'yes') {
		$featured = ' featured';
	}

	$output = '';

$output = <<<CONTENT
		<div class="pricing-column $el_class $is_simple">
			<div class="package-column heading-font base_clr_bg$featured">
				<div class="column-shadow"></div>
				<div class="package-title">$product_title</div>
				<div class="package-value package-price base_clr_txt">
					<div class="price">
						$product_price_from<span class="package-currency currency">$product_currency</span>$product_price
					</div>
					<div class="period base_clr_txt">
						<span class="package-time">$product_period</span>
					</div>
				</div>
				<div class="package-features package-detail">
					$content
					$internal_buy_html
				</div>
			</div>
		</div>
CONTENT;

	return do_shortcode($output);
}
add_shortcode("vsc-product-pricing", "vsc_product_pricing");




/*-----------------------------------------------------------------------------------*/
/*	Shortcodes Filter
/*-----------------------------------------------------------------------------------*/
add_filter("the_content", "vivaco_the_content_filter");

function vivaco_the_content_filter($content) {
	// array of custom shortcodes
	$block = join("|", array(
		"vsc-portfolio-grid",
		"vsc-blog-grid",
		"vsc-signup",
		"vsc-pricing-column",
		"vsc-button",
		"vsc-funfact",
		"vsc-skillbar",
		"vsc-column"
	));

	// opening tag
	$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/", "[$2$3]", $content);

	// closing tag
	$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/", "[/$2]", $rep);

	return $rep;

}




/* enabled shortcodes in widgets */
add_filter('widget_text', 'do_shortcode');




/* extend composer fonts */
function startuply_extend_composer_fonts() {

	function vc_iconpicker_type_startuplyli( $icons ) {
		$startuplyli_icons = array();

		// key:class part name => {name: Category name, up_to: from 1 to up_to number class part}
		$categories = array(
			'alerts' => array('name' => 'Alerts', 'up_to' => 18 ),
			'arrows' => array('name' => 'Arrows', 'up_to' => 41 ),
			'badges-votes' => array('name' => 'Badges Votes', 'up_to' => 16 ),
			'chat-messages' => array('name' => 'Chat Messages', 'up_to' => 16 ),
			'documents-bookmarks' => array('name' => 'Documents Bookmarks', 'up_to' => 16 ),
			'ecology' => array('name' => 'Ecology', 'up_to' => 16 ),
			'education-science' => array('name' => 'Education', 'up_to' => 20 ),
			'emoticons' => array('name' => 'Emoticons', 'up_to' => 35 ),
			'faces-users' => array('name' => 'Faces Users', 'up_to' => 7 ),
			'filetypes' => array('name' => 'File Types', 'up_to' => 17 ),
			'food' => array('name' => 'Food', 'up_to' => 18 ),
			'graphic-design' => array('name' => 'Graphic Design', 'up_to' => 34 ),
			'medical' => array('name' => 'Medical', 'up_to' => 28 ),
			'multimedia' => array('name' => 'Multimedia', 'up_to' => 40 ),
			'nature' => array('name' => 'Nature', 'up_to' => 14 ),
			'office' => array('name' => 'Office', 'up_to' => 61 ),
			'party' => array('name' => 'Party', 'up_to' => 11 ),
			'realestate-living' => array('name' => 'Realestate Living', 'up_to' => 16 ),
			'seo-icons' => array('name' => 'SEO', 'up_to' => 42 ),
			'shopping' => array('name' => 'Shopping', 'up_to' => 27 ),
			'socialmedia' => array('name' => 'Social Media', 'up_to' => 29 ),
			'sport' => array('name' => 'Sport', 'up_to' => 18 ),
			'text-hierarchy' => array('name' => 'Text Hierarchy', 'up_to' => 10 ),
			'touch-gestures' => array('name' => 'Touch Gestures', 'up_to' => 24 ),
			'travel-transportation' => array('name' => 'Travel', 'up_to' => 20 ),
			'weather' => array('name' => 'Weather', 'up_to' => 14 ),
		);

		$count = 0;
		foreach ($categories as $key => $category) {
			$values = array();
			for ( $idx = 1; $idx <= $category['up_to']; $idx++ ) {
				$values[] = array( sprintf("icon icon-%s-%02d", $key, $idx) => $category['name'] );
			}
			if ( $key == 'emoticons' ) {
				$values[] = array( 'icon-emoticons-artboard-80' => $category['name'] );
			}
			$count += count($values);

			$startuplyli_icons[$category['name']] = $values;
		}

		return array_merge( $icons, $startuplyli_icons );
	}
	add_filter( 'vc_iconpicker-type-startuplyli', 'vc_iconpicker_type_startuplyli', 1 /* first in list */ );


	function vc_iconpicker_type_startuplyli_register_css( $icons ) {
		wp_enqueue_style( 'startuply_lineicons_1', get_template_directory_uri() . '/fonts/LineIcons/font-lineicons.css', false, WPB_VC_VERSION, 'screen' );
	}
	add_action( 'vc_base_register_front_css', 'vc_iconpicker_type_startuplyli_register_css' );
	add_action( 'vc_base_register_admin_css', 'vc_iconpicker_type_startuplyli_register_css' );

	/* remove existing sample */

	function startuply_destroy_array_element_by_key($haystack, $needle){
		$output = array();

		foreach($haystack as $key => $value){
			if( $key && in_array( $key, $needle ) === true ){
				unset($key);
			}elseif(is_array($value)){
				$output[$key] = startuply_destroy_array_element_by_key($value, $needle);
			}else{
				$output[$key] = $value;
			}
		}
		return $output;
	}

	// function startuply_remove_icon_type_fontawesome($icons) {
	// 	$remove_icons = array('fa fa-adjust', 'fa fa-anchor');

	// 	return startuply_destroy_array_element_by_key($icons, $remove_icons);
	// }
	// function startuply_remove_icon_type_openiconic($icons) {
	// 	$remove_icons = array('vc-oi vc-oi-dial', 'vc-oi vc-oi-pilcrow');

	// 	return startuply_destroy_array_element_by_key($icons, $remove_icons);
	// }
	// function startuply_remove_icon_type_typicons($icons) {
	// 	$remove_icons = array();

	// 	return startuply_destroy_array_element_by_key($icons, $remove_icons);
	// }
	// function startuply_remove_icon_type_entypo($icons) {
	// 	$remove_icons = array();

	// 	return startuply_destroy_array_element_by_key($icons, $remove_icons);
	// }
	// function startuply_remove_icon_type_linecons($icons) {
	// 	$remove_icons = array();

	// 	return startuply_destroy_array_element_by_key($icons, $remove_icons);
	// }


	// add_filter( 'vc_iconpicker-type-fontawesome', 'startuply_remove_icon_type_fontawesome', 999 );
	// add_filter( 'vc_iconpicker-type-openiconic', 'startuply_remove_icon_type_openiconic', 999 );
	// add_filter( 'vc_iconpicker-type-typicons', 'startuply_remove_icon_type_typicons', 999 );
	// add_filter( 'vc_iconpicker-type-entypo', 'startuply_remove_icon_type_entypo', 999 );
	// add_filter( 'vc_iconpicker-type-linecons', 'startuply_remove_icon_type_linecons', 999 );
}
add_action( 'vc_before_init', 'startuply_extend_composer_fonts' );

function startuply_set_vc_as_theme() {
    vc_set_as_theme(true);
}
add_action( 'vc_before_init', 'startuply_set_vc_as_theme' );
