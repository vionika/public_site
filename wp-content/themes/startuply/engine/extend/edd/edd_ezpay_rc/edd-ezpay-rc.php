<?php
/**
 *
 * EZpay EDD Recurring
 *
 * @author EZpay Integrations
 * @copyright 2015 EZpay
 * @package EZpay
 * @version 1.5.0
 *
 */

$use_edd_recurring = startuply_option('edd_recurring_on', 1);

if ( $use_edd_recurring == 1 ) {
	// Exit if accessed directly
	if ( !defined( 'ABSPATH' ) ) exit;



	class EDD_EZpay_rc {

		static $plugin_path;
		static $plugin_dir;

		public function __construct() {

			require( self::$plugin_path . '/extra/' . 'edd-ezpay-rc-customer.php' );
			require( self::$plugin_path . '/extra/' . 'edd-ezpay-rc-paypal-ipn.php' );

			if ( is_admin() ) {
				require( self::$plugin_path . '/extra/' . 'admin/class-subscriber-reports-table.php' );
				require( self::$plugin_path . '/extra/' . 'admin/reports.php' );
				require( self::$plugin_path . '/extra/' . 'admin/metabox.php' );
			}

			add_action( 'init', array( $this, 'register_post_statuses' ) );

			add_action( 'init', array( $this, 'maybe_add_remove_fees' ) );

			add_action( 'edd_checkout_error_checks', array( $this, 'checkout_errors' ), 10, 2 );

			add_action( 'edd_process_verified_download', array( $this, 'process_download' ), 10, 2 );

			add_action( 'edd_payment_receipt_after', array( $this, 'receipt' ), 10, 2 );

			add_action( 'edd_insert_payment', array( $this, 'process_test_payment' ), 10, 2 );

			add_action( 'edd_paypal_subscr_signup', array( 'EDD_EZpay_rc_PayPal_IPN', 'process_paypal_subscr_signup' ) );

			add_action( 'edd_paypal_subscr_payment', array( 'EDD_EZpay_rc_PayPal_IPN', 'process_paypal_subscr_payment' ) );

			add_action( 'edd_paypal_subscr_cancel', array( 'EDD_EZpay_rc_PayPal_IPN', 'process_paypal_subscr_cancel' ) );

			add_action( 'edd_paypal_subscr_eot', array( 'EDD_EZpay_rc_PayPal_IPN', 'process_paypal_subscr_eot' ) );

			add_action( 'edd_pre_get_payments', array( $this, 'enable_child_payments' ), 100 );

			add_filter( 'edd_straight_to_gateway_purchase_data', array( $this, 'edd_straight_to_gateway_purchase_data' ), 99, 1);

			add_filter( 'edd_payment_statuses', array( $this, 'register_edd_cancelled_status' ) );

			add_filter( 'edd_allowed_download_stati', array( $this, 'add_allowed_payment_status' ) );
			add_filter( 'edd_is_payment_complete', array( $this, 'is_payment_complete' ), 10, 3 );

			add_filter( 'edd_payments_table_views', array( $this, 'payments_view' ) );

			add_filter( 'edd_add_to_cart_item', array( $this, 'add_subscription_cart_details' ), 10, 2 );

			add_filter( 'edd_purchase_data_before_gateway', array( $this, 'gateway_data' ), 10, 2 );

			add_filter( 'edd_paypal_redirect_args', array( $this, 'paypal_gateway_data' ), 10, 2 );

			add_filter( 'edd_get_total_earnings_args', array( $this, 'earnings_query' ) );
			add_filter( 'edd_get_earnings_by_date_args', array( $this, 'earnings_query' ) );
			add_filter( 'edd_get_users_purchases_args', array( $this, 'has_purchased_query' ) );

			add_filter( 'eddpdfi_is_invoice_link_allowed', array( $this, 'is_invoice_allowed' ), 10, 2 );

			add_shortcode( 'edd_recurring_cancel', array( $this, 'cancel_link' ) );

			$customers = new EDD_EZpay_rc_Customer();
		}


		public function register_post_statuses() {
			register_post_status( 'cancelled', array(
				'label'                     => _x( 'Cancelled', 'Cancelled payment status', 'vivaco' ),
				'public'                    => true,
				'exclude_from_search'       => false,
				'show_in_admin_all_list'    => true,
				'show_in_admin_status_list' => true,
				'label_count'               => _n_noop( 'Cancelled <span class="count">(%s)</span>', 'Cancelled <span class="count">(%s)</span>', 'vivaco' )
			)  );
			register_post_status( 'edd_subscription', array(
				'label'                     => _x( 'Subscription', 'Subscription payment status', 'vivaco' ),
				'public'                    => true,
				'exclude_from_search'       => false,
				'show_in_admin_all_list'    => true,
				'show_in_admin_status_list' => true,
				'label_count'               => _n_noop( 'Subscription <span class="count">(%s)</span>', 'Subscription <span class="count">(%s)</span>', 'vivaco' )
			)  );
		}


		public function add_allowed_payment_status( $status ) {
			$status[] = 'cancelled';
			return $status;
		}


		public function is_payment_complete( $ret, $payment_id, $status ) {

			if( 'cancelled' == $status ) {

				$ret = true;

			} elseif( 'edd_subscription' == $status ) {

				$parent = get_post_field( 'post_parent', $payment_id );
				if( edd_is_payment_complete( $parent ) ) {
					$ret = true;
				}

			}

			return $ret;
		}


		public function register_edd_cancelled_status( $stati ) {
			$stati['edd_subscription'] = __( 'Subscription', 'vivaco' );
			$stati['cancelled'] = __( 'Cancelled', 'vivaco' );
			return $stati;
		}


		public function payments_view( $views ) {
			$base               = admin_url( 'edit.php?post_type=download&page=edd-payment-history' );
			$payment_count      = wp_count_posts( 'edd_payment' );
			$current            = isset( $_GET['status'] ) ? $_GET['status'] : '';

			$subscription_count = '&nbsp;<span class="count">(' . $payment_count->edd_subscription   . ')</span>';
			$views['edd_subscription'] = sprintf(
				'<a href="%s"%s>%s</a>',
				add_query_arg( 'status', 'edd_subscription', $base ),
				$current === 'edd_subscription' ? ' class="current"' : '',
				__( 'Subscription Payment', 'vivaco' ) . $subscription_count
			);

			$cancelled_count    = '&nbsp;<span class="count">(' . $payment_count->cancelled   . ')</span>';
			$views['cancelled'] = sprintf(
				'<a href="%s"%s>%s</a>',
				add_query_arg( 'status', 'cancelled', $base ),
				$current === 'cancelled' ? ' class="current"' : '',
				__( 'Cancelled', 'vivaco' ) . $cancelled_count
			);

			return $views;
		}


		public function maybe_add_remove_fees() {
			if( is_admin() ) {
				return;
			}

			$has_recurring = false;
			$cart_details  = edd_get_cart_contents();

			if( ! $cart_details ) {
				return;
			}

			foreach( $cart_details as $item ) {

				if( isset( $item['options'] ) && isset( $item['options']['recurring'] ) ) {

					$has_recurring = true;
					$fee_amount    = $item['options']['recurring']['signup_fee'];
				}

			}

			if( $has_recurring ) {
				EDD()->fees->add_fee( $fee_amount, __( 'Signup Fee', 'vivaco' ), 'signup_fee' );
			} else {
				EDD()->fees->remove_fee( 'signup_fee' );
			}

		}

		public function checkout_errors( $valid_data, $post_data ) {

			// Retrieve the cart contents
			$cart_items = edd_get_cart_contents();

			/********* Check for multiple recurring products *********/

			// If less than 2 items in the cart, get out
			if( count( $cart_items ) < 2 )
				return;

			$has_recurring = false;

			// Loops through each item to see if any of them are recurring
			foreach( $cart_items as $cart_item ) {

				$item_id   = $cart_item['id'];
				$options   = $cart_item['options'];
				$price_id  = isset( $options['price_id'] ) ? intval( $options['price_id'] ) : null;

				// Only one subscription can be purchased at a time. Throw an error is more than one.
				// This also throws an error if a recurring and non recurring product are purchased at once.
				if( ( ! empty( $price_id ) && self::is_price_recurring( $item_id, $price_id ) ) || self::is_recurring( $item_id ) ) {
					$has_recurring = true;
					edd_set_error( 'subscription_invalid', __( 'Sorry, it\'s not possible purchase both subscriptions and downloads within the same checkout session -  these are completely different products', 'edd') );
					break;
				}

			}


			/********* Ensure users create an account *********/

			// Only check if guest checkout is enabled
			if( ! edd_no_guest_checkout() && $has_recurring && ! is_user_logged_in() ) {

				// If customer is purchasing as a guest, we must throw an error

				// TODO: this doesn't work yet

				if( isset( $valid_data['new_user_data'] ) && $valid_data['new_user_data'] = '-1' ) {
					//edd_set_error( 'must_be_user', __( 'You must login or register to purchase a subscription.', 'edd') );
				}

			}

		}


		public function process_download( $download_id = 0, $email = '' ) {
			// Allow user to download by default
			$has_access  = true;

			// Check if this is a variable priced product
			$is_variable = isset( $_GET['price_id'] ) && (int) $_GET['price_id'] !== false ? true : false;

			if( $is_variable && edd_has_variable_prices( $download_id ) ) {
				$recurring = self::is_price_recurring( $download_id, (int) $_GET['price_id'] );
			} else {
				$recurring = self::is_recurring( $download_id );
			}

			if( ! $recurring )
				return; // Product isn't recurring

			$user_data = get_user_by( 'email', $email );

			// No user found so access is denied
			$rc_user_id = -1;
			$has_access = false;

			if( $user_data ) {
				$rc_user_id = $user_data->ID;
				// Check for active subscription
				if( EDD_EZpay_rc_Customer::is_customer_active( $rc_user_id ) ) {
					$has_access = true;
				}
			}

			if( ! apply_filters( 'edd_recurring_download_has_access', $has_access, $rc_user_id, $download_id, $is_variable ) ) {

				wp_die(
					sprintf(
						__( 'You must have an active subscription to %s in order to download this file.', 'vivaco' ),
						get_the_title( $download_id )
					),
					__( 'Access Denied', 'vivaco' )
				);
			}

		}

		static function add_subscription_cart_details( $cart_item ) {
			$download_id 	= $cart_item['id'];
			$price_id 		= isset( $cart_item['options']['price_id'] ) ? intval( $cart_item['options']['price_id'] ) : null;

			if( ! is_null( $price_id ) && $price_id !== false ) {
				// add the recurring info for a variable price
				if( self::is_price_recurring( $download_id, $price_id ) ) {

					$cart_item['options']['recurring'] = array(
						'period'     => self::get_period( $price_id, $download_id ),
						'times'      => self::get_times( $price_id, $download_id ),
						'signup_fee' => self::get_signup_fee( $price_id, $download_id ),
					);

				}

			} else {

				// add the recurring info for a normal priced item
				if( self::is_recurring( $download_id ) ) {

					$cart_item['options']['recurring'] = array(
						'period'    => self::get_period_single( $download_id ),
						'times'      => self::get_times_single( $download_id ),
						'signup_fee' => self::get_signup_fee_single( $download_id ),
					);

				}

			}

			return $cart_item;

		}


		static function periods() {
			$periods = array(
				'day'   => _x( 'Daily', 'Billing period', 'vivaco' ),
				'week'  => _x( 'Weekly', 'Billing period', 'vivaco' ),
				'month' => _x( 'Monthly', 'Billing period', 'vivaco' ),
				'year'  => _x( 'Yearly', 'Billing period', 'vivaco' ),
			);

			$periods = apply_filters( 'edd_recurring_periods', $periods );

			return $periods;
		}


		static function get_period( $price_id, $post_id = null ) {
			global $post;

			if ( ! $post_id && is_object( $post ) )
				$post_id = $post->ID;

			$prices = get_post_meta( $post_id, 'edd_variable_prices', true);

			if ( isset( $prices[ $price_id ][ 'period' ] ) )
				return $prices[ $price_id ][ 'period' ];

			return 'never';
		}


		static function get_period_single( $post_id ) {
			global $post;

			$period = get_post_meta( $post_id, 'edd_period', true );

			if ( $period )
				return $period;

			return 'never';
		}


		static function get_times( $price_id, $post_id = null ) {
			global $post;

			if ( empty( $post_id ) && is_object( $post ) )
				$post_id = $post->ID;

			$prices = get_post_meta( $post_id, 'edd_variable_prices', true);

			if ( isset( $prices[ $price_id ][ 'times' ] ) )
				return intval( $prices[ $price_id ][ 'times' ] );

			return 0;
		}

		static function get_signup_fee( $price_id, $post_id = null ) {
			global $post;

			if ( empty( $post_id ) && is_object( $post ) )
				$post_id = $post->ID;

			$prices = get_post_meta( $post_id, 'edd_variable_prices', true);

			if ( isset( $prices[ $price_id ][ 'signup_fee' ] ) )
				return floatval( $prices[ $price_id ][ 'signup_fee' ] );

			return 0;
		}


		static function get_times_single( $post_id ) {
			global $post;

			$times = get_post_meta( $post_id, 'edd_times', true );

			if ( $times )
				return $times;

			return 0;
		}


		static function get_signup_fee_single( $post_id ) {
			global $post;

			$signup_fee = get_post_meta( $post_id, 'edd_signup_fee', true );

			if ( $signup_fee )
				return $signup_fee;

			return 0;
		}


		static function is_price_recurring( $download_id = 0, $price_id ) {

			global $post;

			if ( empty( $download_id ) && is_object( $post ) )
				$download_id = $post->ID;

			$prices = get_post_meta( $download_id, 'edd_variable_prices', true);
			$period = self::get_period( $price_id, $download_id );

			if ( isset( $prices[ $price_id ][ 'recurring' ] ) && 'never' != $period )
				return true;

			return false;

		}


		static function is_recurring( $download_id = 0 ) {

			global $post;

			if ( empty( $download_id ) && is_object( $post ) )
				$download_id = $post->ID;

			if( get_post_meta( $download_id, 'edd_recurring', true ) == 'yes' )
				return true;

			return false;

		}


		public function gateway_data( $purchase_data, $valid_data ) {

			// Modify the data sent to the PayPal Standard gateway
			if( 'paypal' == $purchase_data['gateway'] ) {
				if( ! empty( $purchase_data['fees']['signup_fee'] ) ) {
					//$purchase_data['price'] -= $purchase_data['fees']['signup_fee']['amount'];
					unset( $purchase_data['fees']['signup_fee'] );
				}
			}

			return $purchase_data;
		}

		public function edd_straight_to_gateway_purchase_data($purchase_data) {

			foreach( $purchase_data['downloads'] as $key => $download ) {

				$result = self::add_subscription_cart_details($download);

				if( empty($purchase_data['fees']['signup_fee']['amount']) ) {
					$purchase_data['fees']['signup_fee']['amount'] = 0.0;
				}

				$purchase_data['fees']['signup_fee']['amount'] += $result['options']['recurring']['signup_fee'];
				$purchase_data['price'] += $result['options']['recurring']['signup_fee'];

				$purchase_data['downloads'][$key] = $result;
			}

			return $purchase_data;
		}

		public function paypal_gateway_data( $paypal_args = array(), $purchase_data = array() ) {

			foreach( $purchase_data['downloads'] as $download ) {
				if( isset( $download['options'] ) && isset( $download['options']['recurring'] ) ) {

					// Set this purchase as a recurring payment
					$paypal_args['cmd'] = '_xclick-subscriptions';

					// Attempt to rebill failed payments
					$paypal_args['sra'] = '1';

					// Set signup fee, if any
					if( ! empty( $download['options']['recurring']['signup_fee'] ) ) {
						$purchase_data['price'] -= $download['options']['recurring']['signup_fee'];
						$paypal_args['a1'] = $download['options']['recurring']['signup_fee'] + $purchase_data['price'];
					}

					// Set the recurring amount
					$paypal_args['a3']  = $purchase_data['price'];

					if( ! empty( $paypal_args['item_name_1'] ) ) {
						// Set purchase description
						$paypal_args['item_name']  = $paypal_args['item_name_1'];
					}

					// Set the recurring period
					switch( $download['options']['recurring']['period'] ) {
						case 'day' :
							$paypal_args['t3'] = 'D';
							$paypal_args['t2'] = 'D';
						break;
						case 'week' :
							$paypal_args['t3'] = 'W';
							$paypal_args['t1'] = 'W';
						break;
						case 'month' :
							$paypal_args['t3'] = 'M';
							$paypal_args['t1'] = 'M';
						break;
						case 'year' :
							$paypal_args['t3'] = 'Y';
							$paypal_args['t1'] = 'Y';
						break;
					}

					// One period unit (every week, every month, etc)
					$paypal_args['p3'] = '1';
					$paypal_args['p1'] = '1';

					// How many times should the payment recur?
					$times = intval( $download['options']['recurring']['times'] );

					switch( $times ) {
						// Unlimited
						case '0' :
							$paypal_args['src'] = '1';
							break;
						// Recur the number of times specified
						default :
							$paypal_args['srt'] = $times;
							break;
					}

				}

			}

			return $paypal_args;

		}


		public function process_test_payment( $payment_id, $payment_data ) {

			if( ! isset( $_POST['edd-gateway'] ) || $_POST['edd-gateway'] != 'manual' )
				return;

			foreach( $payment_data['downloads'] as $download ) {

				if( isset( $download['options'] ) && isset( $download['options']['recurring'] ) ) {

					$user_id  = $payment_data['user_info']['id'];

					// Set user as subscriber
					EDD_EZpay_rc_Customer::set_as_subscriber( $user_id );

					// Set the customer's status to active
					EDD_EZpay_rc_Customer::set_customer_status( $user_id, 'active' );

					// Calculate the customer's new expiration date
					$new_expiration = EDD_EZpay_rc_Customer::calc_user_expiration( $user_id, $payment_id );

					// Set the customer's new expiration date
					EDD_EZpay_rc_Customer::set_customer_expiration( $user_id, $new_expiration );

					// Store the original payment ID in the customer meta
					EDD_EZpay_rc_Customer::set_customer_payment_id( $user_id, $payment_id );

				}

			}

		}


		static function record_subscription_payment( $parent_id = 0, $amount = '', $txn_id = '', $unique_key = 0 ) {

			global $edd_options;

			if( self::payment_exists( $unique_key ) )
				return;

			// increase the earnings for each product in the subscription
			$downloads = edd_get_payment_meta_downloads( $parent_id );
			if( $downloads ) {
				foreach( $downloads as $download ) {
					edd_increase_earnings( $download['id'], $amount );
				}
			}

			// setup the payment daya
		    $payment_data = array(
		    	'parent'        => $parent_id,
		        'price'         => $amount,
		        'user_email'    => edd_get_payment_user_email( $parent_id ),
		        'purchase_key'  => get_post_meta( $parent_id, '_edd_payment_purchase_key', true ),
		        'currency'      => edd_get_option( 'currency', 'usd' ),
		        'downloads'     => $downloads,
		        'user_info'     => edd_get_payment_meta_user_info( $parent_id ),
		        'cart_details'  => edd_get_payment_meta_cart_details( $parent_id ),
		        'status'        => 'edd_subscription',
		        'gateway'       => edd_get_payment_gateway( $parent_id )
		    );

		    // record the subscription payment
		    $payment = edd_insert_payment( $payment_data );

		    if( ! empty( $unique_key ) )
		    	update_post_meta( $payment, '_edd_recurring_' . $unique_key, '1' );

			// Record transaction ID
			if( ! empty( $txn_id ) )
				edd_insert_payment_note( $payment, sprintf( __( 'PayPal Transaction ID: %s', 'edd' ) , $txn_id ) );

			// Update the expiration date of license keys, if EDD Software Licensing is active
			if( function_exists( 'edd_software_licensing' ) ) {
				$licenses = edd_software_licensing()->get_licenses_of_purchase( $payment );

				if( ! empty( $licenses ) ) {
					foreach( $licenses as $license ) {
						// Update the expiration dates of the license key
						edd_software_licensing()->renew_license( $license->ID, $payment );
					}
				}
			}

			do_action( 'edd_recurring_record_payment', $payment, $parent_id, $amount, $txn_id, $unique_key );

		}

		static function payment_exists( $unique_key = 0 ) {
			global $wpdb;

			if( empty( $unique_key ) )
				return false;

			$unique_key = esc_sql( $unique_key );

			$purchase = $wpdb->get_var( "SELECT post_id FROM $wpdb->postmeta WHERE meta_key = '_edd_recurring_{$unique_key}' LIMIT 1" );

			if ( $purchase != NULL )
				return true;

			return false;
		}


		public function is_purchase_recurring( $purchase_data ) {

			foreach( $purchase_data['downloads'] as $download ) {

				if( isset( $download['options'] ) && isset( $download['options']['recurring'] ) )
					return true;
			}

			return false;

		}


		public function earnings_query( $args ) {
			$args['post_status'] = array( 'publish', 'revoked', 'cancelled', 'edd_subscription' );
			return $args;
		}


		public function has_purchased_query( $args ) {
			$args['status'] = array( 'publish', 'revoked', 'cancelled', 'edd_subscription' );
			return $args;
		}


		public function receipt( $payment, $receipt_args ) {

			$downloads = edd_get_payment_meta_downloads( $payment->ID );
			$download  = isset( $downloads[0] ) ? $downloads[0] : $downloads[1];
			if( ! isset( $download['options']['recurring'] ) )
				return;
			$period    = $download['options']['recurring']['period'];
			$times     = $download['options']['recurring']['times'];
			$details   = '';

			if( $times > 0 ) {
				switch( $period ) {
					case 'day' :
						$details = sprintf( _n( 'Daily, %d Time', 'Daily, %d Times', $times, 'vivaco' ), $times );
					break;
					case 'week' :
						$details = sprintf( _n( 'Weekly, %d Time', 'Weekly, %d Times', $times, 'vivaco' ), $times );
					break;
					case 'month' :
						$details = sprintf( _n( 'Monthly, %d Time', 'Monthly, %d Times', $times, 'vivaco' ), $times );
					break;
					case 'year' :
						$details = sprintf( _n( 'Yearly, %d Time', 'Yearly, %d Times', $times, 'vivaco' ), $times );
					break;
				}
			} else {
				switch( $period ) {
					case 'day' :
						$details = __( 'Daily', 'vivaco' );
					break;
					case 'week' :
						$details = __( 'Weekly', 'vivaco' );
					break;
					case 'month' :
						$details = __( 'Monthly', 'vivaco' );
					break;
					case 'year' :
						$details = __( 'Yearly', 'vivaco' );
					break;
				}
			}

			if( ! empty( $details ) ) { ?>
			<tr>
				<td><strong><?php _e( 'Recurring Details', 'edd' ); ?>:</strong></td>
				<td><?php echo $details; ?></td>
			</tr>
			<?php
			}
		}


		public function enable_child_payments( $query ) {
			$query->__set( 'post_parent', null );
		}

		public function is_invoice_allowed( $ret, $payment_id ) {

			$payment_status = get_post_status( $payment_id );

			if( 'edd_subscription' == $payment_status ) {

				$parent = get_post_field( 'post_parent', $payment_id );
				if( edd_is_payment_complete( $parent ) ) {
					$ret = true;
				}

			}

			return $ret;
		}

		public function cancel_link( $atts, $content = null ) {
			global $user_ID;
			if( ! is_user_logged_in() || ! EDD_EZpay_rc_Customer::is_customer_active( $user_ID ) )
				return;

			$atts = shortcode_atts( array(
				'text' => ''
			), $atts );

			$cancel_url = 'https://www.paypal.com/cgi-bin/customerprofileweb?cmd=_manage-paylist';
			$link       = '<a href="%s" class="edd-recurring-cancel" target="_blank" title="%s">%s</a>';
			$link       = sprintf(
				$link,
				$cancel_url,
				__( 'Cancel your subscription', 'vivaco' ),
				empty( $atts['text'] ) ? __( 'Cancel Subscription', 'vivaco' ) : esc_html( $atts['text'] )
			);

			return apply_filters( 'edd_recurring_cancel_link', $link, $user_ID );
		}

	}

	EDD_EZpay_rc::$plugin_path = untrailingslashit( plugin_dir_path( __FILE__ ) );
	EDD_EZpay_rc::$plugin_dir  = untrailingslashit( get_template_directory_uri(). '/engine/extend/edd/edd_ezpay_rc/' );

	new EDD_EZpay_rc();
}
