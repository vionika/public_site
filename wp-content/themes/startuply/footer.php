<footer id="footer" class="footer light">
		<div class="container">

			<?php if (startuply_option('footer_on', '1') === '1') { ?>

			<div class="footer-content row">

			<?php

			$layout_footer = array();

			$footer_widgets = startuply_option('footer_widgets', '3_widget');

			if ($footer_widgets == '1_widget') {
				$layout_footer = array(12);
			} elseif ($footer_widgets == '2_widget') {
				$layout_footer = array(6,6);
			} elseif ($footer_widgets == '3_widget') {
				$layout_footer = array(4,4,4);
			} elseif ($footer_widgets == '4_widget') {
				$layout_footer = array(3,3,3,3);
			} elseif ($footer_widgets == '3x1_big_widget') {
				$layout_footer = array(6,3,3);
			} elseif ($footer_widgets == '3x2_big_widget') {
				$layout_footer = array(3,6,3);
			} elseif ($footer_widgets == '3x3_big_widget') {
				$layout_footer = array(3,3,6);
			} elseif ($footer_widgets == '4x1_big_widget') {
				$layout_footer = array(6,2,2,2);
			} elseif ($footer_widgets == '4x4_big_widget') {
				$layout_footer = array(2,2,2,6);
			}

			$widget_number = 1;
			$class_prefix = 'col-sm-';

			foreach ($layout_footer as $col) {
				echo '<div class="'.$class_prefix.$col.'">';

				if(is_active_sidebar("sidebar_footer_$widget_number")) {
					dynamic_sidebar("sidebar_footer_$widget_number");
				}
				echo '</div>';
				$widget_number++;
			} ?>

			</div>

			<?php } ?>

			<div class="copyright"><?php
				$copy = startuply_option('footer_copy', 'Copyright &copy; 2015 StartuplyWP.com');
				echo $copy;
			?></div>
        </div>


	</footer>

	<div class="back-to-top"><a href="#"><i class="fa fa-angle-up fa-3x"></i></a></div>

	<?php wp_footer(); ?>

</body>
</html>
