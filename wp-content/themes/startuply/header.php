<?php
/**
 * The Header
 * @version 2.2
 *
 */
?><!DOCTYPE html>
<!--[if IE 6]><html class="ie ie6 no-js" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 7]><html class="ie ie7 no-js" <?php language_attributes(); ?>><![endif]-->
<!--[if IE 8]><html class="ie ie8 no-js" <?php language_attributes(); ?>><![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<!-- WordPress header -->
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
	<?php if (!startuply_option('responsive_on')) : ?>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
	<?php else: ?>
		<meta name="viewport" content="width=1200" />
	<?php endif; ?>

	<!-- Startuply favicon -->
	<?php
	$site_favicon = startuply_option('vivaco_favicon');
	if (!$site_favicon) {
		$site_favicon = get_template_directory_uri().'/images/favicon.ico';
	}
	if(!isset($is_home)) {$is_home = '';};

	$options = startuply_get_all_option();

	// Startuply logo
	if ( !empty($options['site_logo']) ) {
		$logo_src = $options['site_logo'];
		$logo_path = $_SERVER['DOCUMENT_ROOT'] . parse_url($logo_src, PHP_URL_PATH);

//		if( file_exists($logo_path) ) {
			$logo_size = getimagesize($logo_path);
			$logo_width = 117; //$logo_size[0];
			$logo_height = 28; //$logo_size[1];
			$logo_img = '<img src="' . $logo_src . '" width="' . $logo_width . '" height="' . $logo_height . '" alt="logo" />';
//		}
	}

	if ( !empty($options['site_sticky_logo']) ) {
		$logo_sticky_src = $options['site_sticky_logo'];
	}else if ( !empty($logo_src) ){
		$logo_sticky_src = $logo_src;
	}

	if ( !empty($logo_sticky_src) ) {
		$logo_sticky_path = $_SERVER['DOCUMENT_ROOT'] . parse_url($logo_sticky_src, PHP_URL_PATH);

//		if( file_exists($logo_sticky_path) ) {
			$logo_sticky_size = getimagesize($logo_sticky_path);
			$logo_sticky_width = 117;// $logo_sticky_size[0];
			$logo_sticky_height = 28; //$logo_sticky_size[1];
			$logo_sticky_img = '<img src="' . $logo_sticky_src . '" width="' . $logo_sticky_width . '" height="' . $logo_sticky_height . '" alt="logo" class="sticky-logo"/>';
//		}
	}

	if ( !empty($options['retina_site_logo']) ) {
		$retina_logo_src = $options['retina_site_logo'];
		$retina_logo_path = $_SERVER['DOCUMENT_ROOT'] . parse_url($retina_logo_src, PHP_URL_PATH);

//		if( file_exists($retina_logo_path) ) {
			$retina_logo_size = getimagesize($retina_logo_path);
			$retina_logo_width = 117; //floor($retina_logo_size[0] / 2);
			$retina_logo_height = 28; //floor($retina_logo_size[1] / 2);
			$retina_logo_img = '<img src="' . $retina_logo_src . '" width="' . $retina_logo_width . '" height="' . $retina_logo_height . '" alt="logo" class="retina" />';
//		}
	}

	if ( !empty($options['retina_site_sticky_logo']) ) {
		$retina_logo_sticky_src = $options['retina_site_sticky_logo'];
		$retina_logo_sticky_path = $_SERVER['DOCUMENT_ROOT'] . parse_url($retina_logo_sticky_src, PHP_URL_PATH);

//		if( file_exists($retina_logo_sticky_path) ) {
			$retina_logo_sticky_size = getimagesize($retina_logo_sticky_path);
			$retina_logo_sticky_width = 117; //floor($retina_logo_sticky_size[0] / 2);
			$retina_logo_sticky_height = 28; //floor($retina_logo_sticky_size[1] / 2);
			$retina_logo_sticky_img = '<img src="' . $retina_logo_sticky_src . '" width="' . $retina_logo_sticky_width . '" height="' . $retina_logo_sticky_height . '" alt="logo" class="sticky-logo retina" />';
//		}
	}

	if ( empty($retina_logo_src) && !empty($logo_src) ) {
		$retina_logo_img = '<img src="' . $logo_src . '" width="' . $logo_width . '" height="' . $logo_height . '" alt="logo" class="retina" />';
	}

	if ( empty($retina_logo_sticky_src) && !empty($logo_sticky_src) ) {
		$retina_logo_sticky_img = '<img src="' . $logo_sticky_src . '" width="' . $logo_sticky_width . '" height="' . $logo_sticky_height . '" alt="logo" class="sticky-logo retina" />';
	}

	if ( empty($logo_img) ) {
		$logo_img = '<img src="' . get_template_directory_uri() . '/images/logo.png" width="117" height="28" alt="logo" />';
	}

	if ( empty($retina_logo_img) ) {
		$retina_logo_img = '<img src="' . get_template_directory_uri() . '/images/logo-retina.png" width="117" height="28" alt="logo" class="retina" />';
	}	

	if ( empty($logo_sticky_img) ) {
		$logo_sticky_img = '<img src="' . get_template_directory_uri() . '/images/logo-inverted.png" width="117" height="28" alt="logo" class="sticky-logo" />';
	}

	if ( empty($retina_logo_sticky_img) ) {
		$retina_logo_sticky_img = '<img src="' . get_template_directory_uri() . '/images/logo-retina-inverted.png" width="117" height="28" alt="logo" class="sticky-logo retina" />';
	}

	//getting current page menu meta box value
	$postid = get_queried_object_id();
	$menu_style = get_post_meta($postid, '_startuply_menu_style_key', true);
	if(!isset($menu_style) || !strlen($menu_style) || is_tag()) {
		$menu_style = 'inner-menu'; // fallback to default
	}

	$header_style = ''; // not defined, but use
	?>
	<link rel="shortcut icon" href="<?php echo esc_attr($site_favicon); ?>">

	<!-- Wordpress head functions -->
	<?php wp_head(); ?>
</head>

<body id="landing-page" <?php body_class(); ?>>

	<div id="mask">

	<?php if (empty($options['loading_gif'])) { ?>
		<div class="preloader"><div class="spin base_clr_brd"><div class="clip left"><div class="circle"></div></div><div class="gap"><div class="circle"></div></div><div class="clip right"><div class="circle"></div></div></div></div>
		<?php
		} else {
			$loading_gif = isset($options['loading_gif']) ? $options['loading_gif'] : '';
		?>
		<div id="custom_loader"><img src="<?php echo esc_attr($loading_gif); ?>" alt="loading"/></div>
	<?php } ?>

	</div>

	<header>
		<nav class="navigation navigation-header <?php echo $menu_style;?> <?php echo $is_home;?>" <?php echo $header_style;?> role="navigation">
			<div class="container">
				<div class="navigation-brand">
					<div class="brand-logo">
						<a href="<?php echo home_url(); ?>" class="logo"><?php echo $logo_img; echo $retina_logo_img; echo $logo_sticky_img; echo $retina_logo_sticky_img; ?></a>
						<span class="sr-only"><?php echo bloginfo( 'name' ); ?></span>
					</div>
					<button class="navigation-toggle visible-xs" type="button" data-target=".navbar-collapse">
						<span class="icon-bar base_clr_bg"></span>
						<span class="icon-bar base_clr_bg"></span>
						<span class="icon-bar base_clr_bg"></span>
					</button>
				</div>
				<div class="navbar-collapse collapsed">
					<div class="menu-wrapper">
						<!-- Left menu -->
						<?php wp_nav_menu( array( 'theme_location' => 'left_menu', 'menu_class' => 'navigation-bar navigation-bar-left', 'fallback_cb' => false, 'items_wrap' => startuply_edd_cart_wrap(), 'walker' => new wp_bootstrap_navwalker() ) ); ?>
						<!-- Right menu -->
						<div class="right-menu-wrap">

						<?php $registration_instead_right_menu_on = startuply_option('registration_instead_right_menu_on', -1);
						if ( $registration_instead_right_menu_on != 1 && (!current_user_can('manage_options') && $user_ID ) ) : // Not register, or Super Admin or Administator only  ?>

							<ul id="menu-demo-menu" class="navigation-bar">
								<li class="menu-item featured">
									<a class="user-profile base_clr_txt" href="<?php echo get_permalink( get_page_by_title( 'User login & control panel' ) ); ?>"><?php _e('My Account','vivaco'); ?></a>
								</li>
								<li class="menu-item">
									<?php wp_loginout(); ?>
								</li>
							</ul>

						<?php else: ?>

							<?php wp_nav_menu( array( 'theme_location' => 'right_menu', 'menu_class' => 'navigation-bar navigation-bar-right', 'fallback_cb' => false, 'walker' => new wp_bootstrap_navwalker() ) ); ?>

						<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>
