<?php
/**
 * Theme AJAX handlers
 *
 * @author 8guild
 * @package Appica 2
 * @since 1.0.0
 */

if ( is_admin() ) {
	// Load more posts
	add_action( 'wp_ajax_appica_load_more_posts', 'appica_load_more_posts' );
	add_action( 'wp_ajax_nopriv_appica_load_more_posts', 'appica_load_more_posts' );

	// Featured Video
	add_action( 'wp_ajax_appica_featured_video', 'appica_ajax_featured_video' );
}

/**
 * Appica AJAX handler for "Load More" button at home page
 *
 * Outputs HTML
 *
 * @since 1.0.0
 */
function appica_load_more_posts() {
	// Check nonce.
	if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'appica-ajax' ) ) {
		wp_send_json_error( 'Wrong nonce' ); // die();
	}

	$per_page = (int) get_option( 'posts_per_page' );
	$cur_page = (int) $_POST['page'];

	$query = new WP_Query( array(
		'post_type'           => 'post',
		'post_status'         => 'publish',
		'paged'               => $cur_page,
		'posts_per_page'      => $per_page,
		'ignore_sticky_posts' => true
	) );

	if ( $query->have_posts() ) {

		$posts = array();

		while( $query->have_posts() ) {
			$query->the_post();
			ob_start();
			get_template_part( 'template-parts/content', 'home' );
			$posts[] = ob_get_clean();
		}
		wp_reset_postdata();

		wp_send_json_success( $posts );
	}

	wp_send_json_error();
}

/**
 * AJAX callback for Featured Video
 *
 * Outputs embed video or error string
 *
 * @since 1.0.0
 */
function appica_ajax_featured_video() {
	// Verify nonce
	if ( empty( $_POST['nonce'] ) || ! wp_verify_nonce( $_POST['nonce'], 'appica-ajax' ) ) {
		wp_send_json_error( 'Nonce is not valid' );
	}

	$url = esc_url_raw( $_POST['url'] );
	// Just die in silence
	if ( empty( $url ) ) {
		wp_send_json_error();
	}

	// Else get oEmbed code
	$embed = wp_oembed_get( $url, array( 'width' => 510 ) );

	if ( false === $embed ) {
		wp_send_json_error( esc_html__( 'URL is not valid or provider do not support oEmbed protocol', 'appica' ) );
	}

	wp_send_json_success( $embed );
}