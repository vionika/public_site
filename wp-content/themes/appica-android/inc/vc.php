<?php
/**
 * Visual Composer actions & filters
 *
 * @author 8guild
 * @package Appica 2
 */

/**
 * Setup Visual Composer for theme.
 *
 * Also, this function could be defined in theme "core" plugin.
 */
function appica_vc_before_init() {
	vc_disable_frontend();
	vc_set_as_theme( true );

	// Allow post by default
	vc_set_default_editor_post_types( array( 'page', 'post', 'appica_portfolio' ) );

	// Set path to directory where Visual Composer
	// should look for template files for content elements.
	vc_set_shortcodes_templates_dir( get_template_directory() . '/vc-templates' );
}

add_action( 'vc_before_init', 'appica_vc_before_init' );

/**
 * Customize some Visual Composer default shortcodes
 *
 * Also, this function could be defined in theme "core" plugin.
 */
function appica_vc_after_init() {
	/*
	 * Remove unnecessary & unsupported shortcodes
	 */
	vc_remove_element( 'vc_custom_heading' );
	vc_remove_element( 'vc_posts_grid' );
	vc_remove_element( 'vc_posts_slider' );
	vc_remove_element( 'vc_basic_grid' );
	vc_remove_element( 'vc_media_grid' );
	vc_remove_element( 'vc_masonry_grid' );
	vc_remove_element( 'vc_masonry_media_grid' );
	vc_remove_element( 'vc_facebook' );
	vc_remove_element( 'vc_tweetmeme' );
	vc_remove_element( 'vc_googleplus' );
	vc_remove_element( 'vc_pinterest' );
	vc_remove_element( 'vc_cta_button' );
	vc_remove_element( 'vc_cta_button2' );
	vc_remove_element( 'vc_flickr' );
	vc_remove_element( 'vc_toggle' );
	vc_remove_element( 'vc_gallery' );
	vc_remove_element( 'vc_wp_categories' );
	vc_remove_element( 'vc_wp_posts' );
	vc_remove_element( 'vc_wp_meta' );
	vc_remove_element( 'vc_wp_calendar' );
	vc_remove_element( 'vc_wp_tagcloud' );
	vc_remove_element( 'vc_wp_rss' );
	vc_remove_element( 'vc_pie' );
	vc_remove_element( 'vc_gallery' );
	vc_remove_element( 'vc_images_carousel' );
	vc_remove_element( 'vc_gmaps' );
	vc_remove_element( 'vc_btn' );
	vc_remove_element( 'vc_button' );
	vc_remove_element( 'vc_button2' );
	vc_remove_element( 'vc_message' );
	vc_remove_element( 'vc_cta' );
	vc_remove_element( 'vc_tabs' );
	vc_remove_element( 'vc_tour' );
	vc_remove_element( 'vc_accordion' );
	vc_remove_element( 'vc_tta_accordion' );
	vc_remove_element( 'vc_tta_pageable' );
	
	/**
	 * @var array Localized values "Show" and "Hide"
	 */
	$show_hide_value = array(
		esc_html__( 'Show', 'appica' ) => 'show',
		esc_html__( 'Hide', 'appica' ) => 'hide',
	);

	$left_right_value = array(
		esc_html__( 'Left', 'appica' )  => 'left',
		esc_html__( 'Right', 'appica' ) => 'right',
	);

	/**
	 * @var array Value for yes/no dropdown
	 */
	$value_yes_no = array(
		esc_html__( 'Yes', 'appica' ) => 'yes',
		esc_html__( 'No', 'appica' )  => 'no',
	);

	/**
	 * @var array Value left/right for shortcode params
	 */
	$value_left_right = array(
		esc_html__( 'Left', 'appica' )   => 'left',
		esc_html__( 'Right', 'appica' )  => 'right'
	);

	/**
	 * @var string Icon heading name
	 */
	$heading_icon = esc_html__( 'Icon', 'appica' );
	$overlay = esc_html__( 'Overlay', 'appica' );

	/**
	 * @var array Field "Icon Library" allow choosing different icons
	 */
	$field_icon_library = array(
		'type'       => 'dropdown',
		'param_name' => 'icon_lib',
		'heading'    => esc_html__( 'Icon library', 'appica' ),
		'value'      => array(
			'Flaticons'    => 'flaticons',
			'Font Awesome' => 'fontawesome',
			'Open Iconic'  => 'openiconic',
			'Typicons'     => 'typicons',
			'Entypo'       => 'entypo',
			'Linecons'     => 'linecons',
		)
	);

	/**
	 * @var array Icon from Font Awesome pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_fontawesome = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_fontawesome',
		'heading'    => $heading_icon,
		'settings'   => array(
			'emptyIcon'    => true,
			'iconsPerPage' => 200
		),
		'dependency' => array(
			'element' => 'icon_lib',
			'value'   => 'fontawesome'
		)
	);

	/**
	 * @var array Icon from Openiconic pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_openiconic = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_openiconic',
		'heading'    => $heading_icon,
		'settings'   => array(
			'type'         => 'openiconic',
			'emptyIcon'    => true,
			'iconsPerPage' => 200
		),
		'dependency' => array(
			'element' => 'icon_lib',
			'value'   => 'openiconic'
		)
	);

	/**
	 * @var array Icon from Typicons pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_typicons = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_typicons',
		'heading'    => $heading_icon,
		'settings'   => array(
			'type'         => 'typicons',
			'emptyIcon'    => true,
			'iconsPerPage' => 200
		),
		'dependency' => array(
			'element' => 'icon_lib',
			'value'   => 'typicons'
		)
	);

	/**
	 * @var array Icon from Entypo pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_entypo = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_entypo',
		'heading'    => $heading_icon,
		'settings'   => array(
			'type'         => 'entypo',
			'emptyIcon'    => true,
			'iconsPerPage' => 300
		),
		'dependency' => array(
			'element' => 'icon_lib',
			'value'   => 'entypo'
		)
	);

	/**
	 * @var array Icon from Linecons pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_linecons = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_linecons',
		'heading'    => $heading_icon,
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'linecons' ),
		'settings'   => array(
			'type'         => 'linecons',
			'emptyIcon'    => false,
			'iconsPerPage' => 200,
		),
	);

	/**
	 * @var array Icon from Flaticons pack. Depends on {@see $field_icon_library}
	 */
	$field_icon_flaticons = array(
		'type'       => 'iconpicker',
		'param_name' => 'icon_flaticons',
		'heading'    => $heading_icon,
		'dependency' => array( 'element' => 'icon_lib', 'value' => 'flaticons' ),
		'settings'   => array(
			'type'         => 'flaticons',
			'emptyIcon'    => true,
			'iconsPerPage' => 350,
		),
	);

	/**
	 * @var array "Title" shortcode param
	 */
	$field_title = array(
		'type'        => 'textfield',
		'param_name'  => 'title',
		'weight'      => 10,
		'heading'     => esc_html__( 'Title', 'appica' ),
		'description' => esc_html__( 'Enter text which will be used as widget title. Leave blank if no title is needed.', 'appica' )
	);

	/**
	 * @var array "Subtitle" shortcode param
	 */
	$field_subtitle = array(
		'type'       => 'textfield',
		'param_name' => 'subtitle',
		'weight'     => 9,
		'heading'    => esc_html__( 'Subtitle', 'appica' )
	);

	/**
	 * @var array "Text Align" shortcode param
	 */
	$field_text_align = array(
		'type'        => 'dropdown',
		'param_name'  => 'alignment',
		'weight'      => 7,
		'heading'     => esc_html__( 'Text align', 'appica' ),
		'description' => esc_html__( 'Applied only for "tab" column', 'appica' ),
		'std'         => 'left',
		'value'       => array_merge( $value_left_right, array(
			esc_html__( 'Center', 'appica' ) => 'center'
		) ),
	);

	/**
	 * @var array Content position for vc_tour & vc_tabs shortcodes
	 */
	$field_content_position = array(
		'type'        => 'dropdown',
		'param_name'  => 'position',
		'weight'      => 6,
		'heading'     => esc_html__( 'Content position', 'appica' ),
		'description' => esc_html__( 'Choose your content column position', 'appica' ),
		'std'         => 'right',
		'value'       => $value_left_right
	);

	/*
	 * Add "Caption" tab with parameters to vc_single_image
	 */
	$caption = esc_html__( 'Caption', 'appica' );
	vc_add_params( 'vc_single_image', array(
		array(
			'type'        => 'checkbox',
			'param_name'  => 'is_caption_used',
			'heading'     => esc_html__( 'Use caption?', 'appica' ),
			'description' => esc_html__( 'Add a fancy small description under the image', 'appica' ),
			'group'       => $caption,
			'value'       => array( esc_html__( 'Yes', 'appica' ) => 'yes' )
		),
		array(
			'type'        => 'textfield',
			'param_name'  => 'caption_line',
			'heading'     => esc_html__( 'Enter your caption line', 'appica' ),
			'description' => esc_html__( 'Leave this field empty if you want to show the caption from media library.', 'appica' ),
			'group'       => $caption
		),
		array(
			'type'       => 'dropdown',
			'param_name' => 'caption_align',
			'heading'    => esc_html__( 'Caption alignment', 'appica' ),
			'group'       => $caption,
			'std'        => 'right',
			'value'      => $left_right_value
		),
		array(
			'type'        => 'textfield',
			'param_name'  => 'caption_class',
			'heading'     => esc_html__( 'Caption extra class name', 'appica' ),
			'description' => esc_html__( 'Add extra classes, if you wish to style particular content element differently.', 'appica' ),
			'group'       => $caption
		)
	) );

	/*
	 * Rename "Row" to "Section"
	 */
	vc_map_update( 'vc_row', array(
		'name' => esc_html__( 'Section', 'appica' )
	) );

	/*
	 * Remove "Row stretch" param from vc_row
	 * Make .vc_row always full width
	 */
	vc_remove_param( 'vc_row', 'full_width' );

	/**
	 * Remove vc_row params:
	 * - parallax
	 * - el_id
	 *
	 * Because Appica 2 has own implementation of vc_row
	 * and does not support these features.
	 *
	 * @since 1.1.0
	 * @since 1.3.0 Remove "parallax_image" param
	 */
	vc_remove_param( 'vc_row', 'parallax' );
	vc_remove_param( 'vc_row', 'el_id' );
	vc_remove_param( 'vc_row', 'parallax_image' );

	/**
	 * @since 2.0.0
	 */
	vc_remove_param( 'vc_row', 'gap' );
	vc_remove_param( 'vc_row', 'full_height' );
	vc_remove_param( 'vc_row', 'columns_placement' );
	vc_remove_param( 'vc_row', 'equal_height' );
	vc_remove_param( 'vc_row', 'content_placement' );
	vc_remove_param( 'vc_row', 'video_bg' );
	vc_remove_param( 'vc_row', 'video_bg_url' );
	vc_remove_param( 'vc_row', 'video_bg_parallax' );

	/*
	 * Add "Badge" tab with parameters to vc_row & vc_row_inner
	 */
	$badge = esc_html__( 'Badge', 'appica' );
	$vc_row_badge = array(
		array(
			'type'        => 'dropdown',
			'param_name'  => 'badge',
			'heading'     => esc_html__( 'Display Badge', 'appica' ),
			'description' => esc_html__( 'N.B. To use badge "container" in "General" tab must set to "yes".', 'appica' ),
			'group'       => $badge,
			'std'         => 'hide',
			'value'       => $show_hide_value
		),
		array(
			'type'       => 'dropdown',
			'param_name' => 'badge_align',
			'heading'    => esc_html__( 'Align', 'appica' ),
			'group'      => $badge,
			'std'        => 'left',
			'value'      => $left_right_value
		),
		array(
			'type'        => 'textfield',
			'param_name'  => 'badge_title',
			'heading'     => esc_html__( 'Badge Title', 'appica' ),
			'description' => esc_html__( 'Will be shown near the badge. How text will be aligned depends on previous param.', 'appica' ),
			'group'       => $badge
		),
		array_merge( $field_icon_library, array( 'group' => $badge ) ),
		array_merge( $field_icon_fontawesome, array( 'group' => $badge ) ),
		array_merge( $field_icon_openiconic, array( 'group' => $badge ) ),
		array_merge( $field_icon_typicons, array( 'group' => $badge ) ),
		array_merge( $field_icon_entypo, array( 'group' => $badge ) ),
		array_merge( $field_icon_linecons, array( 'group' => $badge ) ),
		array_merge( $field_icon_flaticons, array( 'group' => $badge ) ),
		array(
			'type'               => 'dropdown',
			'param_name'         => 'badge_pc',
			'heading'            => esc_html__( 'Predefined Color', 'appica' ),
			'description'        => esc_html__( 'Choose one from predefined colors', 'appica' ),
			'param_holder_class' => 'appica-badge-color',
			'group'              => $badge,
			'std'                => 'primary',
			'value'              => array(
				esc_html__( 'Primary', 'appica' ) => 'primary',
				esc_html__( 'Success', 'appica' ) => 'success',
				esc_html__( 'Info', 'appica' )    => 'info',
				esc_html__( 'Warning', 'appica' ) => 'warning',
				esc_html__( 'Danger', 'appica' )  => 'danger'
			)
		),
		array(
			'type'        => 'colorpicker',
			'param_name'  => 'badge_cc',
			'heading'     => esc_html__( 'Custom Color', 'appica' ),
			'description' => esc_html__( 'This param overrides previous.', 'appica' ),
			'group'       => $badge
		),
		array(
			'type'        => 'colorpicker',
			'param_name'  => 'badge_btc',
			'heading'     => esc_html__( 'Border Top Color', 'appica' ),
			'description' => esc_html__( 'Custom fancy line above badge. Just leave empty if you don\'t want to see it.', 'appica' ),
			'group'       => $badge
		),
		array(
			'type'        => 'colorpicker',
			'param_name'  => 'badge_tc',
			'heading'     => esc_html__( 'Text Color', 'appica' ),
			'description' => esc_html__( 'Customize your badges text color.', 'appica' ),
			'value'       => '#bebebe',
			'group'       => $badge
		),
		array(
			'type'        => 'textfield',
			'param_name'  => 'badge_class',
			'heading'     => esc_html__( 'Extra class name', 'appica' ),
			'description' => esc_html__( 'Add extra classes, if you wish to style particular content element differently.', 'appica' ),
			'group'       => $badge
		)
	);
	vc_add_params( 'vc_row', $vc_row_badge );
	vc_add_params( 'vc_row_inner', $vc_row_badge );

	/*
	 * Add "Content color" param to vc_row
	 */
	vc_add_param( 'vc_row', array(
		'type'        => 'dropdown',
		'param_name'  => 'content_color',
		'heading'     => esc_html__( 'Content color', 'appica' ),
		'description' => esc_html__( 'You can adjust this color in Global Colors Settings: for dark customize Body Text Color / for light customize Light Text Color', 'appica' ),
		'std'         => 'dark',
		'value'       => array(
			esc_html__( 'Light', 'appica' ) => 'light',
			esc_html__( 'Dark', 'appica' )  => 'dark'
		)
	) );

	/**
	 * Add "ID" field to vc_row params
	 *
	 * @since 1.1.0 Rename param and change description
	 * @since 1.0.0
	 */
	vc_add_param( 'vc_row', array(
		'type'        => 'textfield',
		'param_name'  => 'uniq_id',
		'heading'     => esc_html__( 'Row ID', 'appica' ),
		'description' => esc_html__( 'Enter row ID (Note: make sure it is unique and valid according to <a href="http://www.w3schools.com/tags/att_global_id.asp" target="_blank">w3c specification</a>). Also used for anchored navigation.', 'appica' ),
		'value'       => 'r' . uniqid()
	) );

	/*
	 * Add "Container" dropdown to vc_row
	 */
	vc_add_param( 'vc_row', array(
		'type'       => 'dropdown',
		'param_name' => 'is_container',
		'heading'    => esc_html__( 'Add container to row', 'appica' ),
		'std'        => 'yes',
		'value'      => $value_yes_no
	) );

	/*
	 * Add "Offset" param to vc_column_inner
	 */
	vc_add_param( 'vc_column_inner', array(
		'type'        => 'column_offset',
		'param_name'  => 'offset',
		'heading'     => esc_html__( 'Responsiveness', 'appica' ),
		'group'       => esc_html__( 'Width & Responsiveness', 'appica' ),
		'description' => esc_html__( 'Adjust column for different screen sizes. Control width, offset and visibility settings.', 'appica' )
	) );

	if ( shortcode_exists( 'appica_button' ) ) {
		/*
		 * Change params of "Appica Button" shortcode
		 */
		vc_remove_param( 'appica_button', 'style' );

		// replace "type" param with another values
		vc_remove_param( 'appica_button', 'type' );
		vc_add_param( 'appica_button', array(
			'type'       => 'dropdown',
			'param_name' => 'type',
			'weight'     => 2,
			'heading'    => esc_html__( 'Button type', 'appica' ),
			'std'        => 'default',
			'value'      => array(
				esc_html__( 'Default', 'appica' ) => 'default',
				esc_html__( 'Flat', 'appica' )    => 'flat'
			)
		) );

		// add "Wave Color" param
		vc_add_param( 'appica_button', array(
			'type'       => 'dropdown',
			'param_name' => 'waves',
			'weight'     => 1,
			'heading'    => esc_html__( 'Wave Color', 'appica' ),
			'std'        => 'dark',
			'value'      => array(
				esc_html__( 'Dark', 'appica' )  => 'dark',
				esc_html__( 'Light', 'appica' ) => 'light'
			)
		) );

		// disable icons in Android version for Flat button
		vc_add_param( 'appica_button', array_merge(
			$field_icon_library,
			array( 'dependency' => array( 'element' => 'type', 'value' => 'default' ) )
		) );
	}

	if ( shortcode_exists( 'appica_fancy_text' ) ) {
		/*
		 * Add "Color" param to "Fancy Text" shortcode
		 */
		vc_add_param( 'appica_fancy_text', array(
			'type'       => 'dropdown',
			'param_name' => 'color',
			'weight'     => 10,
			'heading'    => esc_html__( 'Text Color', 'appica' ),
			'std'        => 'primary',
			'value'      => array(
				esc_html__( 'Default', 'appica' ) => 'default',
				esc_html__( 'Primary', 'appica' ) => 'primary',
				esc_html__( 'Success', 'appica' ) => 'success',
				esc_html__( 'Info', 'appica' )    => 'info',
				esc_html__( 'Warning', 'appica' ) => 'warning',
				esc_html__( 'Danger', 'appica' )  => 'danger'
			)
		) );
	}

	/*
	 * vc_tta_tabs
	 */
	vc_remove_param( 'vc_tta_tabs', 'title' );
	vc_remove_param( 'vc_tta_tabs', 'style' );
	vc_remove_param( 'vc_tta_tabs', 'shape' );
	vc_remove_param( 'vc_tta_tabs', 'color' );
	vc_remove_param( 'vc_tta_tabs', 'spacing' );
	vc_remove_param( 'vc_tta_tabs', 'gap' );
	vc_remove_param( 'vc_tta_tabs', 'pagination_style' );
	vc_remove_param( 'vc_tta_tabs', 'pagination_color' );
	vc_remove_param( 'vc_tta_tabs', 'title' );
	vc_remove_param( 'vc_tta_tabs', 'no_fill_content_area' );
	vc_remove_param( 'vc_tta_tabs', 'tab_position' );
	vc_remove_param( 'vc_tta_tabs', 'alignment' );
	vc_remove_param( 'vc_tta_tabs', 'autoplay' );
	vc_remove_param( 'vc_tta_tabs', 'active_section' );
	vc_remove_param( 'vc_tta_tabs', 'css' );
	vc_remove_param( 'vc_tta_tabs', 'el_class' );
	vc_add_params( 'vc_tta_tabs', array(
		$field_title,
		$field_subtitle,
		array(
			'type'       => 'textarea',
			'param_name' => 'description',
			'weight'     => 8,
			'heading'    => esc_html__( 'Description', 'appica' )
		),
		$field_text_align,
		$field_content_position,
	) );

	vc_remove_param( 'vc_tta_tour', 'style' );
	vc_remove_param( 'vc_tta_tour', 'shape' );
	vc_remove_param( 'vc_tta_tour', 'color' );
	vc_remove_param( 'vc_tta_tour', 'spacing' );
	vc_remove_param( 'vc_tta_tour', 'gap' );
	vc_remove_param( 'vc_tta_tour', 'no_fill' );
	vc_remove_param( 'vc_tta_tour', 'title' );
	vc_remove_param( 'vc_tta_tour', 'c_align' );
	vc_remove_param( 'vc_tta_tour', 'autoplay' );
	vc_remove_param( 'vc_tta_tour', 'collapsible_all' );
	vc_remove_param( 'vc_tta_tour', 'c_icon' );
	vc_remove_param( 'vc_tta_tour', 'c_position' );
	vc_remove_param( 'vc_tta_tour', 'active_section' );
	vc_remove_param( 'vc_tta_tour', 'css' );
	vc_remove_param( 'vc_tta_tour', 'no_fill_content_area' );
	vc_remove_param( 'vc_tta_tour', 'tab_position' );
	vc_remove_param( 'vc_tta_tour', 'alignment' );
	vc_remove_param( 'vc_tta_tour', 'controls_size' );
	vc_remove_param( 'vc_tta_tour', 'pagination_style' );
	vc_remove_param( 'vc_tta_tour', 'pagination_color' );
	vc_remove_param( 'vc_tta_tour', 'el_class' );
	vc_add_params( 'vc_tta_tour', array(
		$field_title,
		$field_subtitle,
		$field_content_position,
		$field_text_align,
	) );

	/*
	 * vc_tta_section
	 */
	vc_remove_param( 'vc_tta_section', 'title' );
	vc_remove_param( 'vc_tta_section', 'tab_id' );
	vc_remove_param( 'vc_tta_section', 'add_icon' );
	vc_remove_param( 'vc_tta_section', 'i_position' );
	vc_remove_param( 'vc_tta_section', 'i_type' );
	vc_remove_param( 'vc_tta_section', 'i_icon_fontawesome' );
	vc_remove_param( 'vc_tta_section', 'i_icon_openiconic' );
	vc_remove_param( 'vc_tta_section', 'i_icon_typicons' );
	vc_remove_param( 'vc_tta_section', 'i_icon_entypo' );
	vc_remove_param( 'vc_tta_section', 'i_icon_linecons' );
	vc_remove_param( 'vc_tta_section', 'el_class' );
	vc_add_params( 'vc_tta_section', array(
		array(
			'param_name'  => 'title',
			'type'        => 'textfield',
			'weight'      => 10,
			'heading'     => esc_html__( 'Title', 'appica' ),
			'description' => esc_html__( 'Will appear on the tab', 'appica' ),
		),
		array(
			'param_name'  => 'tab_id',
			'type'        => 'el_id',
			'weight'      => 10,
			'settings'    => array( 'auto_generate' => true ),
			'heading'     => esc_html__( 'Section ID', 'appica' ),
			'description' => wp_kses( __( 'Enter section ID. Note: make sure it is unique and valid according to <a href="http://www.w3.org/TR/html-markup/global-attributes.html#common.attrs.id" target="_blank">w3c specification</a>.', 'appica' ), array(
				'a' => array( 'href' => array(), 'target' => array() ),
			) ),
		),
		array(
			'param_name'  => 'animation',
			'type'        => 'dropdown',
			'weight'      => 10,
			'heading'     => esc_html__( 'Animation', 'appica' ),
			'description' => esc_html__( 'Applicable only for tabs!', 'appica' ),
			'std'         => 'top',
			'value'       => array(
				esc_html__( 'Fade', 'appica' )     => 'fade',
				esc_html__( 'Scale', 'appica' )    => 'scale',
				esc_html__( 'Scale Up', 'appica' ) => 'scaleup',
				esc_html__( 'Left', 'appica' )     => 'left',
				esc_html__( 'Right', 'appica' )    => 'right',
				esc_html__( 'Top', 'appica' )      => 'top',
				esc_html__( 'Bottom', 'appica' )   => 'bottom',
				esc_html__( 'Flip', 'appica' )     => 'flip',
			),
		),
	) );

	/**
	 * Disable the CSS Animations.
	 *
	 * You can enable all animations by adding filter:
	 * add_filter( 'appica_disable_css_animation', '__return_false' );
	 *
	 * @since 2.0.1
	 *
	 * @param bool $is_animation Enable or disable animations. Default is disabled.
	 */
	if ( apply_filters( 'appica_disable_css_animation', true ) ) {
		vc_remove_param( 'vc_column_text', 'css_animation' );
		vc_remove_param( 'vc_icon', 'css_animation' );
		vc_remove_param( 'vc_single_image', 'css_animation' );
	}
}

if ( is_admin() ) {
	add_action( 'vc_after_init', 'appica_vc_after_init' );
}
