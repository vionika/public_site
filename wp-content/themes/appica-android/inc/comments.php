<?php
/**
 * Actions and filters for comments list and comment form
 *
 * @author 8guild
 * @package Appica
 */

/**
 * Output an Appica 2 theme specific comment template.
 *
 * @author 8guild
 * @since 1.0.0
 * @see wp_list_comments()
 *
 * @param object $comment Comment to display.
 * @param array  $args    An array of arguments.
 * @param int    $depth   Depth of comment.
 */
function appica_comment( $comment, $args, $depth ) {
	// Extra comment wrap class
	$extra_class = appica_get_class_set( array(
		$args['has_children'] ? 'parent' : ''
	) );

	// here goes div.comment
	?>
	<div id="comment-<?php comment_ID(); ?>" <?php comment_class( $extra_class ); ?>>
		<div class="comment-meta">
			<div class="column">
				<div class="author vcard">
					<?php if ( 0 != $args['avatar_size'] ) : ?>
					<span class="ava"><?php echo get_avatar( $comment, $args['avatar_size'] ); ?></span>
					<?php endif; ?>
					<span><?php esc_html_e( 'by', 'appica' ); ?></span> <span class="fake-link"><?php comment_author(); ?></span>
				</div>
			</div>
			<div class="column text-right">
				<span><?php
				echo human_time_diff( get_comment_time('U'), current_time('timestamp') ), ' ';
				esc_html_e( 'ago', 'appica' );
				?></span>
				<?php
				comment_reply_link( array_merge( $args, array(
					'add_below' => 'comment',
					'depth'     => $depth,
					'max_depth' => $args['max_depth'],
					'before'    => '',
					'after'     => ''
				) ) );
				?>
			</div>
		</div>

	<?php if ( '0' == $comment->comment_approved ) : ?>
		<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'appica' ); ?></p>
	<?php endif; ?>

	<?php comment_text();
}

/**
 * Appica theme specific comment ending.
 *
 * @see Walker::end_el()
 * @see wp_list_comments()
 *
 * @param object $comment The comment object. Default current comment.
 * @param array  $args    An array of arguments.
 * @param int    $depth   Depth of comment.
 */
function appica_comment_end( $comment, $args, $depth ) {
	echo '</div>'; // close opening div#comment-%d
}

/**
 * Returns supported level of nesting for comments list.
 * Depends on threaded_comment option and CSS support.
 *
 * @see wp_list_comments
 *
 * @return int|string
 */
function appica_comments_nesting_level() {
	// Respect the "Enable threaded comments" option in Settings > Discussion
	if ( false === (bool) get_option( 'thread_comments' ) ) {
		return '';
	}

	/**
	 * Filter the comments nesting level for {@see wp_list_comments}
	 *
	 * @param int $level max_depth argument
	 */
	return apply_filters( 'appica_comments_nesting_level', 2 );
}

/**
 * Filter the comment reply link: add extra class .reply-btn
 *
 * @author 8guild
 * @since 1.0.0
 *
 * @param string $link Reply link markup
 *
 * @return string
 */
function appica_reply_link_class( $link ) {
	$link = str_replace( "class='comment-reply-link", "class='comment-reply-link reply-btn", $link );

	return $link;
}

add_filter( 'comment_reply_link', 'appica_reply_link_class' );

/**
 * Wrap the comment form in div.box-float
 *
 * @since 1.0.0
 *
 * @see comment_form()
 */
function appica_comment_form_before() {
	echo '<div class="box-float comments space-top">';
}

add_action( 'comment_form_before', 'appica_comment_form_before' );

/**
 * Close div.box-float
 *
 * @since 1.0.0
 *
 * @see comment_form()
 * @see appica_comment_form_before()
 */
function appica_comment_form_after() {
	echo '</div>';
}

add_action( 'comment_form_after', 'appica_comment_form_after' );

/**
 * Wrap comment form fields (author, email) with div.row
 *
 * @see comment_form()
 *
 * @author 8guild
 * @since 1.0.0
 */
function appica_comment_form_before_fields() {
	echo '<div class="row">';
}

add_action( 'comment_form_before_fields', 'appica_comment_form_before_fields' );

/**
 * Close div.row wrapper after comment form fields (author, email)
 *
 * @see    comment_form()
 *
 * @author 8guild
 * @since  1.0.0
 */
function appica_comment_form_after_fields() {
	echo '</div>'; // close div.row
}

add_action( 'comment_form_after_fields', 'appica_comment_form_after_fields' );

/**
 * Filter the default comment form fields, such as 'author', 'email', 'url'
 *
 * @param array $fields Default fields
 *
 * @return array
 */
function appica_comment_form_default_fields( $fields ) {
	// Remove URL field
	unset( $fields['url'] );

	$commenter = wp_get_current_commenter();
	$req       = get_option( 'require_name_email' );
	$aria_req  = $req ? 'required' : '';

	$author = '<div class="col-sm-6"><div class="form-control">'
	          . '<input type="text" name="author" id="cf_name" value="%2$s" %3$s>'
	          . '<label for="cf_name">%1$s</label>'
	          . '<span class="error-label"></span><span class="valid-label"></span>'
	          . '</div></div>';

	$email = '<div class="col-sm-6"><div class="form-control">'
	         . '<input type="email" name="email" id="cf_email" value="%2$s" aria-describedby="email-notes" %3$s>'
	         . '<label for="cf_email">%1$s</label>'
	         . '<span class="error-label"></span><span class="valid-label"></span>'
	         . '</div></div>';

	$fields = array(
		'author' => sprintf( $author, esc_html__( 'Name', 'appica' ), esc_attr( $commenter['comment_author'] ), $aria_req ),
		'email'  => sprintf( $email, esc_html__( 'Email', 'appica' ), esc_attr( $commenter['comment_author_email'] ), $aria_req ),
	);

	return $fields;
}

add_filter( 'comment_form_default_fields', 'appica_comment_form_default_fields' );

/**
 * Filter the comment form default arguments.
 * Also, change the comment textarea template.
 *
 * @param array $args The default comment form arguments.
 *
 * @return array
 */
function appica_comment_form_defaults( $args ) {
	// Remove comment notes before and after
	$args['comment_notes_before'] = $args['comment_notes_after'] = '';

	$comment_field = '<div class="form-control">'
	                 . '<textarea name="comment" id="cf_comment" rows="7" aria-required="true" required></textarea>'
	                 . '<label for="cf_comment">%1$s</label>'
	                 . '<span class="error-label"></span><span class="valid-label"></span>'
	                 . '</div>';

	$_args = array(
		'title_reply'   => sprintf( '<h3 class="text-gray text-right text-light">%s</h3>', esc_html_x( 'Leave a comment', 'comments form title', 'appica' ) ),
		'id_form'       => 'comment-form',
		'class_submit'  => 'btn btn-success btn-float waves-effect waves-light',
		'label_submit'  => esc_html_x( 'Comment', 'comment form submit', 'appica' ),
		'comment_field' => sprintf( $comment_field, esc_html_x( 'Comment', 'noun', 'appica' ), esc_html__( 'Enter your comment', 'appica' ) ),
	);

	return array_merge( $args, $_args );
}

add_filter( 'comment_form_defaults', 'appica_comment_form_defaults' );

/**
 * Add new hidden field with comments list order. Required for AJAX.
 *
 * @since    1.0.0
 *
 * @param string $fields    The HTML-formatted hidden id field comment elements.
 * @param int    $post_id   The post ID.
 * @param int    $replytoid The id of the comment being replied to.
 *
 * @return string
 */
function appica_comment_form_hidden_fields( $fields, $post_id, $replytoid ) {
	$order = get_option( 'comment_order' );
	$fields .= "<input type='hidden' name='comments_order' id='comments_order' value='{$order}' />\n";

	return $fields;
}

add_filter( 'comment_id_fields', 'appica_comment_form_hidden_fields', 10, 3 );

/**
 * Comment form AJAX callback
 *
 * @param int $comment_ID     Comment ID
 * @param int $comment_status Comment status: 0 - not approved, 1 - approved
 */
function appica_ajax_comment_callback( $comment_ID, $comment_status ) {
	if ( empty( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || 'xmlhttprequest' !== strtolower( $_SERVER['HTTP_X_REQUESTED_WITH'] ) ) {
		die;
	}

	// Get the comment data
	$comment = get_comment( $comment_ID );
	// Allow the email to the author to be sent
	wp_notify_postauthor( $comment_ID );

	if ( 0 == $comment->comment_approved ) {
		/**
		 * Remove comment content, if not approved.
		 * @see /appica/comments.php
		 */
		$comment->comment_content = '';
	}

	$depth = appica_comments_nesting_level();
	$args = array(
		'style'        => 'div',
		'callback'     => 'appica_comment',
		'end-callback' => 'appica_comment_end',
		'max_depth'    => $depth,
		'per_page'     => -1,
		'type'         => 'comment',
		'reply_text'   => esc_html__( 'Reply', 'appica' ),
		'avatar_size'  => 48,
		'short_ping'   => true,
		'echo'         => false,
	);

	$comments[0] = $comment;
	$response = wp_list_comments( $args, $comments );

	/*
	 * As we pass only one comment to wp_list_comments(), function returns html with .depth-1 class.
	 * For top-level comments it is not a problem, but for replies (Appica 2 supports 2-level nested comments)
	 * class have to be .depth-2 or higher
	 *
	 * Remove the Reply link, too!
	 */
	if ( ! empty( $depth ) && (int) $comment->comment_parent > 0 ) {
		$response = str_replace( 'depth-1', "depth-{$depth}", $response );
		$response = preg_replace( '/<a class=[\'|"]comment-reply-link \\b[^>]*>.*?<\\/a>/iu', '', $response, 1 );
	}

	// Kill the script, returning the comment HTML
	wp_send_json_success( array( 'comment_id' => $comment_ID, 'comment' => $response ) );
}

add_action( 'comment_post', 'appica_ajax_comment_callback', 20, 2 );