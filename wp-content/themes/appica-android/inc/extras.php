<?php
/**
 * Custom functions that act independently of the theme templates
 *
 * Eventually, some of the functionality here could be replaced by core features
 *
 * @author 8guild
 */


/*
 * Filters and actions used in all themes
 */


/**
 * Modify TinyMCE. Add "style_formats"
 *
 * @link https://codex.wordpress.org/TinyMCE_Custom_Styles#Using_style_formats
 *
 * @param array $init_array
 *
 * @return mixed
 */
function appica_mce_before_init( $init_array ) {
	$style_formats = array(
		array(
			'title'   => esc_html__( 'Text Muted', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-muted'
		),
		array(
			'title'   => esc_html__( 'Text Gray', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-gray'
		),
		array(
			'title'   => esc_html__( 'Text Primary', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-primary'
		),
		array(
			'title'   => esc_html__( 'Text Success', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-success'
		),
		array(
			'title'   => esc_html__( 'Text Info', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-info'
		),
		array(
			'title'   => esc_html__( 'Text Warning', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-warning'
		),
		array(
			'title'   => esc_html__( 'Text Danger', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-danger'
		),
		array(
			'title'   => esc_html__( 'UPPERCASE text', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-uppercase'
		),
		array(
			'title'    => esc_html__( 'Lead text', 'appica' ),
			'selector' => 'p',
			'classes'  => 'lead'
		),
		array(
			'title'   => esc_html__( 'Small text', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-sm'
		),
		array(
			'title'   => esc_html__( 'Extra Small text', 'appica' ),
			'inline'  => 'span',
			'classes' => 'text-xs'
		),
		array(
			'title'    => esc_html__( 'Fancy Link', 'appica' ),
			'selector' => 'a',
			'classes'  => 'link'
		)
	);

	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;
}

add_filter( 'tiny_mce_before_init', 'appica_mce_before_init' );

/**
 * Add "styleselect" button to TinyMCE second row
 *
 * @param array $buttons TinyMCE Buttons
 *
 * @return mixed
 */
function appica_mce_buttons_2( $buttons ) {
	array_unshift( $buttons, 'styleselect' );

	return $buttons;
}

add_filter( 'mce_buttons_2', 'appica_mce_buttons_2' );

/**
 * Some additional mime types
 *
 * @param array $mime_types
 *
 * @return array
 */
function appica_extended_mime_types( $mime_types ) {
	$extended = array(
		'svg' => 'image/svg+xml',
		'ico' => 'image/vnd.microsoft.icon'
	);

	foreach ( $extended as $ext => $mime ) {
		$mime_types[ $ext ] = $mime;
	}

	return $mime_types;
}

add_filter( 'upload_mimes', 'appica_extended_mime_types' );

/**
 * Show favicon preview (from .ico format), not an icon
 *
 * @param string $icon    Path to the mime type icon.
 * @param string $mime    Mime type.
 * @param int    $post_id Attachment ID. Will be equal 0
 *                        if the function passed the mime type.
 *
 * @return mixed
 */
function appica_mime_type_icon( $icon, $mime, $post_id ) {
	$src   = false;
	$mimes = array(
		'image/x-icon',
		'image/svg+xml',
	);

	if ( in_array( $mime, $mimes, true ) && $post_id > 0 ) {
		$src = wp_get_attachment_image_src( $post_id );
	}

	return is_array( $src ) ? array_shift( $src ) : $icon;
}

add_filter( 'wp_mime_type_icon', 'appica_mime_type_icon', 10, 3 );

/**
 * Callback for "tiny_mce_plugins" filter.
 *
 * This function used to remove the emoji plugin from tinymce.
 *
 * @param  array $plugins
 *
 * @return array $plugins Difference between the two arrays
 */
function appica_disable_wp_emoji_in_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Disable the emoji's
 */
function appica_disable_wp_emoji() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );

	add_filter( 'tiny_mce_plugins', 'appica_disable_wp_emoji_in_tinymce' );
}

add_action( 'init', 'appica_disable_wp_emoji' );


/*
 * Theme specific filters and actions
 */


/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */
function appica_body_class( $classes ) {
	// Adds a class of group-blog to blogs with more than 1 published author.
	if ( is_multi_author() ) {
		$classes[] = 'group-blog';
	}

	if ( ! is_front_page() ) {
		$classes[] = 'gray-bg';
	}

	$type = appica_get_page_settings( 'type', 'title' );
	if ( ! in_array( $type, array( 'intro', 'revslider' ) )
	     && ! is_page()
	) {
		$classes[] = 'offset-top';
	}

	if ( ! in_array( $type, array( 'intro', 'revslider' ) )
	     && ! is_home()
	     && appica_navbar_is_sticky()
	) {
		$classes[] = 'fixed-footer';
	}

	return $classes;
}

add_filter( 'body_class', 'appica_body_class' );

/**
 * Callback for "post_class" filter.
 *
 * Filter the list of CSS classes for the current post.
 *
 * @param array  $classes An array of post classes.
 * @param string $class   A comma-separated list of extra classes.
 * @param int    $post_id The post ID.
 *
 * @return array
 */
function appica_post_class( $classes, $class, $post_id ) {
	if ( is_home() ) {
		$is_wide = absint( appica_get_post_settings( 'is_wide', 0 ) );
		$classes[] = (bool) $is_wide ? 'w2' : '';
	} elseif ( is_page() ) {
		// always wrap a page into .container, except the Front
		// and if fluid is enabled in Page Settings
		if ( 'page' === get_post_type( $post_id )
		     && 'boxed' === appica_get_page_settings( 'layout', 'boxed' )
		) {
			$classes[] = 'container';
		}

		// wrap the page with class .content-wrap, if "Sticky navbar" is enabled,
		// user is on front page and intro screen is disabled.
		if ( in_array( appica_get_page_settings( 'type', 'title' ), array( 'none', 'title' ) )
		     && appica_navbar_is_sticky()
		) {
			$wrapper[] = 'content-wrap';
		}
	}

	return $classes;
}

add_filter( 'post_class', 'appica_post_class', 10, 3 );

/**
 * Flush out the transients used in appica_categorized_blog.
 */
function appica_flush_category_transient() {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}
	// Like, beat it. Dig?
	delete_transient( 'appica_categories' );
}

add_action( 'edit_category', 'appica_flush_category_transient' );
add_action( 'save_post', 'appica_flush_category_transient' );

/**
 * Change excerpt "more" string to "...'
 *
 * @param string $excerpt Current excerpt line
 *
 * @since 1.0.0
 *
 * @return string
 */
function appica_excerpt_more( $excerpt ) {
	return str_replace( '[&hellip;]', '...', $excerpt );
}

add_filter( 'excerpt_more', 'appica_excerpt_more' );

/**
 * Filter the meta boxes, shown by default for New Post screen
 *
 * @param array     $hidden An array of meta boxes hidden by default
 * @param WP_Screen $screen object of the current screen
 *
 * @return array
 */
function appica_default_hidden_meta_boxes( $hidden, $screen ) {
	// Filters only post screen and if "postexcerpt" present in $hidden
	$key = array_search( 'postexcerpt', $hidden );
	if ( 'post' == $screen->post_type && $key !== false ) {
		unset( $hidden[ $key ] );
	}

	return $hidden;
}

add_filter( 'default_hidden_meta_boxes', 'appica_default_hidden_meta_boxes', 10, 2 );

/**
 * Filter the post tags output
 *
 * @param array $tags Post tags
 *
 * @return string
 */
function appica_post_tags_filter( $tags ) {
	if ( empty( $tags ) ) {
		return $tags;
	}

	$_tags = array();
	foreach ( $tags as $tag ) {
		// Note, added # before tag name
		$_tags[] = sprintf( '<a href="%1$s" rel="tag">#%2$s</a>', get_term_link( $tag, $tag->taxonomy ), $tag->name );
	}
	unset( $tag );

	return implode( ' ', $_tags );
}

add_filter( 'appica_get_the_tags', 'appica_post_tags_filter' );

/**
 * Returns a list of socials networks, specified in Theme Options
 *
 * @see appica_the_socials
 * @see appica_intro_socials
 * @see appica_navbar_socials
 * @see appica_offcanvas_socials
 *
 * @return mixed
 */
function appica_get_socials() {
	return appica_option_get( 'socials_networks', array() );
}

/**
 * Returns the "Flaticons" icons pack for Equip "icon" field, if flaticons
 * is specified as a source, and {@see appica_vc_iconpicker_flaticons}.
 *
 * @return array
 */
function appica_get_icons_flaticons() {
	return array(
		'flaticon-account4',
		'flaticon-add179',
		'flaticon-add180',
		'flaticon-add181',
		'flaticon-add182',
		'flaticon-add183',
		'flaticon-add184',
		'flaticon-add186',
		'flaticon-adjust6',
		'flaticon-airplane106',
		'flaticon-android11',
		'flaticon-android12',
		'flaticon-apple70',
		'flaticon-attachment19',
		'flaticon-auto1',
		'flaticon-automatic2',
		'flaticon-automatic3',
		'flaticon-back57',
		'flaticon-backspace1',
		'flaticon-bed24',
		'flaticon-been',
		'flaticon-birthday20',
		'flaticon-black394',
		'flaticon-black395',
		'flaticon-black396',
		'flaticon-black397',
		'flaticon-black398',
		'flaticon-black399',
		'flaticon-black400',
		'flaticon-black401',
		'flaticon-black402',
		'flaticon-blank30',
		'flaticon-blank31',
		'flaticon-blank32',
		'flaticon-blank33',
		'flaticon-blogger12',
		'flaticon-blueetooth',
		'flaticon-bluetooth21',
		'flaticon-bluetooth22',
		'flaticon-bluetooth23',
		'flaticon-bluetooth24',
		'flaticon-bluetooth25',
		'flaticon-bookmark45',
		'flaticon-bookmark46',
		'flaticon-bookmark47',
		'flaticon-bookmark48',
		'flaticon-briefcase49',
		'flaticon-briefcase50',
		'flaticon-brightness10',
		'flaticon-brochure6',
		'flaticon-bubble8',
		'flaticon-bug18',
		'flaticon-burn9',
		'flaticon-button11',
		'flaticon-call47',
		'flaticon-call48',
		'flaticon-call49',
		'flaticon-camera59',
		'flaticon-camera60',
		'flaticon-cancel19',
		'flaticon-caps1',
		'flaticon-caps',
		'flaticon-car145',
		'flaticon-car146',
		'flaticon-car147',
		'flaticon-cell10',
		'flaticon-cell11',
		'flaticon-cell12',
		'flaticon-change3',
		'flaticon-chat75',
		'flaticon-chat76',
		'flaticon-check51',
		'flaticon-check52',
		'flaticon-chemistry17',
		'flaticon-circle107',
		'flaticon-circle108',
		'flaticon-circles23',
		'flaticon-circumference',
		'flaticon-city24',
		'flaticon-clapperboard4',
		'flaticon-clapperboard5',
		'flaticon-clear5',
		'flaticon-clipboard99',
		'flaticon-clock100',
		'flaticon-close47',
		'flaticon-closed64',
		'flaticon-cloud302',
		'flaticon-cloud303',
		'flaticon-cloud304',
		'flaticon-cloud305',
		'flaticon-cloud306',
		'flaticon-cloud307',
		'flaticon-compass106',
		'flaticon-connection21',
		'flaticon-copy31',
		'flaticon-create2',
		'flaticon-create3',
		'flaticon-credit98',
		'flaticon-crop13',
		'flaticon-crop14',
		'flaticon-cut23',
		'flaticon-dark55',
		'flaticon-developer2',
		'flaticon-device4',
		'flaticon-device5',
		'flaticon-disc30',
		'flaticon-do10',
		'flaticon-double126',
		'flaticon-download162',
		'flaticon-download164',
		'flaticon-download166',
		'flaticon-downwards',
		'flaticon-drafts',
		'flaticon-drop25',
		'flaticon-drop26',
		'flaticon-drop27',
		'flaticon-earth205',
		'flaticon-ellipsis1',
		'flaticon-email107',
		'flaticon-emoticon117',
		'flaticon-end3',
		'flaticon-enter5',
		'flaticon-exit13',
		'flaticon-expand38',
		'flaticon-expand39',
		'flaticon-facebook56',
		'flaticon-fast46',
		'flaticon-favorite21',
		'flaticon-favorite22',
		'flaticon-filled13',
		'flaticon-film61',
		'flaticon-filter20',
		'flaticon-flash25',
		'flaticon-flash26',
		'flaticon-folder215',
		'flaticon-forward18',
		'flaticon-forward19',
		'flaticon-framed1',
		'flaticon-front15',
		'flaticon-front16',
		'flaticon-front17',
		'flaticon-full46',
		'flaticon-gamepad3',
		'flaticon-gamepad4',
		'flaticon-get',
		'flaticon-gmail3',
		'flaticon-go10',
		'flaticon-good4',
		'flaticon-good5',
		'flaticon-google117',
		'flaticon-google118',
		'flaticon-google119',
		'flaticon-google120',
		'flaticon-google121',
		'flaticon-google122',
		'flaticon-google123',
		'flaticon-google124',
		'flaticon-google125',
		'flaticon-google126',
		'flaticon-google127',
		'flaticon-google128',
		'flaticon-google129',
		'flaticon-google130',
		'flaticon-google131',
		'flaticon-google132',
		'flaticon-google133',
		'flaticon-google134',
		'flaticon-google135',
		'flaticon-google136',
		'flaticon-google137',
		'flaticon-gps25',
		'flaticon-gps26',
		'flaticon-gps27',
		'flaticon-gps28',
		'flaticon-graduate32',
		'flaticon-halffilled1',
		'flaticon-hangouts',
		'flaticon-headset11',
		'flaticon-headset12',
		'flaticon-help18',
		'flaticon-help19',
		'flaticon-hide3',
		'flaticon-high20',
		'flaticon-high21',
		'flaticon-high22',
		'flaticon-history6',
		'flaticon-home149',
		'flaticon-horizontal39',
		'flaticon-hotel68',
		'flaticon-https',
		'flaticon-import',
		'flaticon-insert4',
		'flaticon-instagram16',
		'flaticon-invert1',
		'flaticon-keyboard53',
		'flaticon-keyboard54',
		'flaticon-label31',
		'flaticon-landscape10',
		'flaticon-laptop117',
		'flaticon-left216',
		'flaticon-left217',
		'flaticon-left218',
		'flaticon-light88',
		'flaticon-link60',
		'flaticon-linkedin24',
		'flaticon-list88',
		'flaticon-list89',
		'flaticon-location41',
		'flaticon-locked57',
		'flaticon-locked58',
		'flaticon-low34',
		'flaticon-magic20',
		'flaticon-man459',
		'flaticon-man460',
		'flaticon-map102',
		'flaticon-map103',
		'flaticon-mark1',
		'flaticon-mark2',
		'flaticon-medium5',
		'flaticon-medium6',
		'flaticon-medium7',
		'flaticon-memory1',
		'flaticon-menu55',
		'flaticon-merge',
		'flaticon-microphone84',
		'flaticon-microphone85',
		'flaticon-microsoft7',
		'flaticon-microsoft8',
		'flaticon-missed',
		'flaticon-mountain31',
		'flaticon-mountain32',
		'flaticon-mountains14',
		'flaticon-move26',
		'flaticon-new102',
		'flaticon-new103',
		'flaticon-nfc1',
		'flaticon-notifications1',
		'flaticon-notifications2',
		'flaticon-notifications',
		'flaticon-painter14',
		'flaticon-panoramic1',
		'flaticon-parking14',
		'flaticon-pause44',
		'flaticon-person325',
		'flaticon-phone370',
		'flaticon-phone371',
		'flaticon-phone372',
		'flaticon-phone373',
		'flaticon-phone374',
		'flaticon-phone375',
		'flaticon-photo210',
		'flaticon-photo211',
		'flaticon-photo212',
		'flaticon-photographic1',
		'flaticon-pinterest33',
		'flaticon-planet29',
		'flaticon-play105',
		'flaticon-play106',
		'flaticon-play107',
		'flaticon-play108',
		'flaticon-play109',
		'flaticon-plus80',
		'flaticon-poll',
		'flaticon-power106',
		'flaticon-previous14',
		'flaticon-printer88',
		'flaticon-problems',
		'flaticon-progress10',
		'flaticon-public10',
		'flaticon-public11',
		'flaticon-public9',
		'flaticon-puzzle37',
		'flaticon-radio51',
		'flaticon-random5',
		'flaticon-rate',
		'flaticon-read5',
		'flaticon-receipt9',
		'flaticon-record9',
		'flaticon-refresh55',
		'flaticon-refresh56',
		'flaticon-reminder6',
		'flaticon-replay4',
		'flaticon-reply18',
		'flaticon-report',
		'flaticon-rewind45',
		'flaticon-right237',
		'flaticon-right244',
		'flaticon-ring24',
		'flaticon-rotate11',
		'flaticon-rotate12',
		'flaticon-round50',
		'flaticon-round51',
		'flaticon-round52',
		'flaticon-round53',
		'flaticon-round54',
		'flaticon-round55',
		'flaticon-round56',
		'flaticon-round57',
		'flaticon-round58',
		'flaticon-rounded54',
		'flaticon-rounded55',
		'flaticon-rounded56',
		'flaticon-rounded57',
		'flaticon-rounded58',
		'flaticon-rounded59',
		'flaticon-rounded60',
		'flaticon-rubbish',
		'flaticon-save20',
		'flaticon-schedule2',
		'flaticon-screen44',
		'flaticon-screen45',
		'flaticon-screen46',
		'flaticon-screen47',
		'flaticon-screen48',
		'flaticon-screen49',
		'flaticon-sd7',
		'flaticon-sd8',
		'flaticon-search100',
		'flaticon-searching41',
		'flaticon-select3',
		'flaticon-select4',
		'flaticon-send12',
		'flaticon-send13',
		'flaticon-send14',
		'flaticon-server40',
		'flaticon-set5',
		'flaticon-set6',
		'flaticon-settings49',
		'flaticon-settings50',
		'flaticon-share39',
		'flaticon-shared1',
		'flaticon-shining2',
		'flaticon-shining3',
		'flaticon-shopping231',
		'flaticon-shopping232',
		'flaticon-show4',
		'flaticon-show5',
		'flaticon-show6',
		'flaticon-show7',
		'flaticon-show8',
		'flaticon-shuffle24',
		'flaticon-sim2',
		'flaticon-smartphone19',
		'flaticon-smartphone20',
		'flaticon-sms5',
		'flaticon-sms6',
		'flaticon-sms7',
		'flaticon-snake4',
		'flaticon-sort52',
		'flaticon-speech108',
		'flaticon-split4',
		'flaticon-square181',
		'flaticon-stop46',
		'flaticon-swap2',
		'flaticon-swap3',
		'flaticon-switch27',
		'flaticon-switch28',
		'flaticon-switch29',
		'flaticon-switch30',
		'flaticon-synchronization3',
		'flaticon-synchronization4',
		'flaticon-synchronization5',
		'flaticon-tab3',
		'flaticon-tablet95',
		'flaticon-tack',
		'flaticon-tag71',
		'flaticon-telephone105',
		'flaticon-thermostat1',
		'flaticon-three168',
		'flaticon-three170',
		'flaticon-thumb53',
		'flaticon-thumb54',
		'flaticon-tick7',
		'flaticon-timelapse',
		'flaticon-traffic21',
		'flaticon-tumblr22',
		'flaticon-turn17',
		'flaticon-turn18',
		'flaticon-turn19',
		'flaticon-turn20',
		'flaticon-turn21',
		'flaticon-turn22',
		'flaticon-turn23',
		'flaticon-twitter47',
		'flaticon-two375',
		'flaticon-two385',
		'flaticon-two393',
		'flaticon-underline6',
		'flaticon-underline7',
		'flaticon-undo19',
		'flaticon-unlocked43',
		'flaticon-up176',
		'flaticon-upload119',
		'flaticon-upload120',
		'flaticon-usb33',
		'flaticon-user157',
		'flaticon-user158',
		'flaticon-users25',
		'flaticon-verification24',
		'flaticon-videocall',
		'flaticon-view12',
		'flaticon-virtual2',
		'flaticon-visibility1',
		'flaticon-voice32',
		'flaticon-voicemail1',
		'flaticon-volume47',
		'flaticon-volume49',
		'flaticon-volume50',
		'flaticon-volume51',
		'flaticon-warning37',
		'flaticon-watch16',
		'flaticon-waving',
		'flaticon-web37',
		'flaticon-website12',
		'flaticon-wifi81',
		'flaticon-wifi82',
		'flaticon-wifi83',
		'flaticon-window57',
		'flaticon-work3',
		'flaticon-workspace',
		'flaticon-world96',
		'flaticon-write20',
		'flaticon-youtube35'
	);
}

add_filter( 'equip_get_icons_flaticons', 'appica_get_icons_flaticons' );

/**
 * Callback for vc_base_register_front_css, vc_base_register_admin_css
 *
 * Register all the styles for VC Iconpicker to be enqueue later
 */
function appica_vc_iconpicker_register_css() {
	$theme_uri = get_template_directory_uri();

	wp_register_style( 'flaticons', $theme_uri . '/css/vendor/flaticon.css', array(), null );
}

add_action( 'vc_base_register_front_css', 'appica_vc_iconpicker_register_css' );
add_action( 'vc_base_register_admin_css', 'appica_vc_iconpicker_register_css' );

/**
 * Callback for vc_backend_editor_enqueue_js_css, vc_frontend_editor_enqueue_js_css
 *
 * Used to enqueue all needed files when VC editor is rendering
 */
function appica_vc_iconpicker_enqueue_css() {
	wp_enqueue_style( 'flaticons' );
}

add_action( 'vc_backend_editor_enqueue_js_css', 'appica_vc_iconpicker_enqueue_css' );
add_action( 'vc_frontend_editor_enqueue_js_css', 'appica_vc_iconpicker_enqueue_css' );

/**
 * Filter shortcodes param "iconpicker": add new icon pack "Flaticons"
 *
 * @since 2.0.0
 *
 * @return array New icons
 */
function appica_vc_iconpicker_flaticons() {
	$flaticons  = appica_get_icons_flaticons();
	$_flaticons = array();
	foreach ( $flaticons as $icon ) {
		$_flaticons[] = array( $icon => ".{$icon}" );
	}

	return $_flaticons;
}

add_filter( 'vc_iconpicker-type-flaticons', 'appica_vc_iconpicker_flaticons' );

/**
 * Enqueue the CSS for the Front-end site, when "flaticons"
 * pack is selected in the icons library.
 *
 * @see vc_icon_element_fonts_enqueue
 *
 * @param string $font Library name
 */
function appica_vc_iconpicker_flaticons_css( $font ) {
	if ( 'flaticons' === $font && wp_style_is( $font ) ) {
		$theme_uri = get_template_directory_uri();
		wp_enqueue_style( 'flaticons', $theme_uri . '/css/vendor/flaticon.css', array(), null );
	}
}

add_action( 'vc_enqueue_font_icon_element', 'appica_vc_iconpicker_flaticons_css' );

/**
 * Display the Intro Section or Revolution Slider
 *
 * Based on Page Settings
 */
function appica_page_header() {
	$type = appica_get_page_settings( 'type', 'title' );
	if ( in_array( $type, array( 'none', 'title' ) ) ) {
		return;
	}

	if ( 'intro' === $type ) {
		appica_the_intro();
	} elseif ( 'revslider' === $type ) {
		appica_the_revolution_slider();
	}

	echo '<div class="content-wrap">';
}

add_action( 'appica_header_before', 'appica_page_header', 999 );

/**
 * Close the opened div.content-wrap
 *
 * @see appica_page_header
 */
function appica_page_footer() {
	$type = appica_get_page_settings( 'type', 'title' );
	if ( in_array( $type, array( 'none', 'title' ) ) ) {
		return;
	}

	echo '</div>'; // close div.content-wrap
}

add_action( 'appica_footer_before', 'appica_page_footer', 999 );

/**
 * Return "Page Settings" meta box for current entry.
 *
 * @uses get_queried_object
 *
 * @see  inc/meta-boxes.php
 * @see  Appica_Cpt_Intro
 * @see  Appica_Cpt_Portfolio
 *
 * @param string $key     Name of required option or "all" for whole bunch of settings
 * @param mixed  $default A fallback if option is failed
 *
 * @return mixed Anyway returns array with default settings if key field is not specified.
 *               If $key field given this function may return this field value or $default
 *               if given $key not found in settings array.
 */
function appica_get_page_settings( $key = 'all', $default = false ) {
	$post     = get_queried_object();
	$settings = false;

	if ( $post instanceof WP_Post ) {
		$settings = appica_meta_box_get( $post->ID, '_appica_page_settings' );
	}

	// do not return empty set
	if ( ! is_array( $settings ) || empty( $settings ) ) {
		$settings = array();
	}

	// posts and pages have different set of settings
	// but I am not worried about it and merge with all
	// possible default values
	$settings = wp_parse_args( $settings, array(
		'layout'    => 'boxed',
		'type'      => 'title',
		'intro'     => 0,
		'revslider' => 0,
	) );

	if ( 'all' === $key ) {
		return $settings;
	}

	$result = $default;
	if ( array_key_exists( $key, $settings ) ) {
		$result = $settings[ $key ];
	}

	return $result;
}

/**
 * Return "Post Settings" meta box for current entry.
 *
 * @uses get_post
 * @uses get_queried_object
 *
 * @since 2.0.0
 *
 * @param string $key     Name of required option or "all" for whole bunch of settings
 * @param mixed  $default A fallback if option is failed
 *
 * @return mixed Anyway returns array with default settings if key field is not specified.
 *               If $key field given this function may return this field value or $default
 *               if given $key not found in settings array.
 */
function appica_get_post_settings( $key = 'all', $default = false ) {
	$settings = false;
	if ( in_the_loop() ) {
		$post = get_post();
	} else {
		$post = get_queried_object();
	}

	if ( $post instanceof WP_Post ) {
		$settings = appica_meta_box_get( $post->ID, '_appica_post_settings' );
	}

	// do not return empty set
	if ( ! is_array( $settings ) || empty( $settings ) ) {
		$settings = array();
	}

	// posts and pages have different set of settings
	// but I am not worried about it and merge with all
	// possible default values
	$settings = wp_parse_args( $settings, array(
		'sidebar'      => 'left',
		'search'       => 1,
		'is_wide'      => 0,
		'custom_title' => '',
		'overlay'      => 'primary',
	) );

	if ( 'all' === $key ) {
		return $settings;
	}

	$result = $default;
	if ( array_key_exists( $key, $settings ) ) {
		$result = $settings[ $key ];
	}

	return $result;
}

/**
 * Filter path to modified shortcodes folder for Android version
 *
 * @since 1.0.0
 * @since 2.0.0 Move directory to root
 *
 * @return string Path to modified folder
 */
function appica_android_shortcodes_directory() {
	return wp_normalize_path( get_template_directory() . '/shortcodes' );
}

add_filter( 'appica_shortcodes_templates_path', 'appica_android_shortcodes_directory', 9 );

/**
 * Change excerpt length, global filter.
 *
 * @param int $length The number of words to display
 *
 * @since 1.0.0
 *
 * @return int
 */
function appica_excerpt_length( $length ) {
	return 9;
}

/**
 * Reduce the length of custom excerpt (if specified manually), globally in theme.
 *
 * @param string $excerpt Current excerpt
 *
 * @since 1.0.0
 *
 * @return string
 */
function appica_trim_excerpt( $excerpt ) {
	if ( false === strpos( $excerpt, '...' ) && str_word_count( $excerpt ) > 9 ) {
		$excerpt = wp_trim_words( $excerpt, 9, ' ...' );
	}

	return $excerpt;
}

/**
 * Hide "Device Color" field in Intro settings meta box
 * in Android theme version
 *
 * @since 2.0.0
 */
add_filter( 'appica_intro_hide_device_color', '__return_true' );

/**
 * Fix the TGM message styling
 *
 * @since 2.0.1
 */
function appica_fix_tgm_styles() {
	?>
	<style type="text/css">#setting-error-tgmpa{display: block;}</style>
	<?php
}

add_action( 'admin_print_styles', 'appica_fix_tgm_styles', 99 );

/**
 * Echoes the navbar class set
 *
 * @see  header.php
 * @uses appica_get_class_set
 */
function appica_navbar_class() {
	$classes = array();

	$classes[] = 'navbar';

	// if "sticky" navbar is enabled
	// @see inc/options.php
	// @since 2.0.0 based on page settings
	if ( appica_navbar_is_sticky() ) {
		$type          = appica_get_page_settings( 'type', 'title' );
		$allowed_types = array( 'intro', 'revslider' );

		if ( in_array( $type, $allowed_types ) ) {
			$classes[] = 'navbar-sticky';
		} else {
			$classes[] = 'navbar-fixed-top';
		}
	}

	/**
	 * Filter the navbar classes
	 *
	 * @param array $classes Array of classes
	 */
	$classes = apply_filters( 'appica_navbar_class', $classes );

	echo esc_attr( appica_get_class_set( $classes ) );
}

/**
 * Footer class set
 *
 * @since 1.0.0
 * @since 2.0.1 Add filter and rename
 */
function appica_footer_class() {
	$classes = array( 'footer', 'padding-top-3x' );

	$type  = appica_get_page_settings( 'type', 'title' );
	$types = array( 'intro', 'revslider' );

	if ( ! in_array( $type, $types ) ) {
		$classes[] = 'fw-bg';
		$classes[] = 'top-inner-shadow';
	}

	/**
	 * Filter the footer classes
	 *
	 * @param array $classes Array with footer classes
	 */
	$classes = apply_filters( 'appica_footer_class', $classes );

	echo appica_get_class_set( $classes );
}