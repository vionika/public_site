<?php
/**
 * Custom template tags for this theme.
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package Appica
 * @author 8guild
 */

/**
 * Check if Google Fonts is used
 *
 * @since 1.0.0
 *
 * @return bool
 */
function appica_is_google_font() {
	$is_font = absint( appica_option_get( 'typography_is_google', 0 ) );

	return (bool) $is_font;
}

/**
 * Returns true if a blog has more than 1 category.
 *
 * @return bool
 */
function appica_categorized_blog() {
	if ( false === ( $all_the_cool_cats = get_transient( 'appica_categories' ) ) ) {
		// Create an array of all the categories that are attached to posts.
		$all_the_cool_cats = get_categories( array(
			'fields'     => 'ids',
			'hide_empty' => 1,

			// We only need to know if there is more than one category.
			'number'     => 2,
		) );

		// Count the number of categories that are attached to the posts.
		$all_the_cool_cats = count( $all_the_cool_cats );

		set_transient( 'appica_categories', $all_the_cool_cats );
	}

	if ( $all_the_cool_cats > 1 ) {
		// This blog has more than 1 category so appica_categorized_blog should return true.
		return true;
	} else {
		// This blog has only 1 category so appica_categorized_blog should return false.
		return false;
	}
}

if ( ! function_exists( 'appica_entry_footer' ) ) :
	/**
	 * Prints HTML to posts footer with meta information for the categories, tags, published date,
	 * social sharings and comments. Allowed only for posts.
	 *
	 * This template tag call inside The Loop, so, you can use other tags.
	 *
	 * @since 1.0.0
	 */
	function appica_entry_footer() {
		if ( 'post' !== get_post_type() ) {
			return;
		}

		/**
		 * Filter the categories view, displayed in single post entry footer.
		 *
		 * @since 1.0.0
		 *
		 * @link http://codex.wordpress.org/Function_Reference/get_the_category_list
		 *
		 * @param string $categories Categories list, separated by comma
		 */
		$categories = apply_filters( 'appica_get_the_category_list', get_the_category_list( ', ' ) );
		/**
		 * Filter the tags view, displayed in single post entry footer.
		 *
		 * @since 1.0.0
		 *
		 * @link http://codex.wordpress.org/Function_Reference/get_the_tags
		 *
		 * @param array $tags Array of tags
		 */
		$tags = apply_filters( 'appica_get_the_tags', get_the_tags() );

		$by_author = sprintf(
			esc_html_x( 'by %s', 'post author', 'appica' ),
			'<span class="author vcard"><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);
		?>
		<div class="post-meta space-top-2x">
			<div class="column"><?php appica_posted_on(); ?></div>
			<div class="column">
				<div class="social-buttons text-right">
					<a href="#comments" class="comment-count scroll" data-offset-top="160"><i class="flaticon-chat75"></i><?php comments_number( '0', '1', '%' ) ?></a>
					<?php appica_social_share_buttons(); ?>
				</div>
			</div>
		</div>
		<div class="post-meta last-child">
		<?php if ( ( $categories && appica_categorized_blog() ) ) : ?>
			<div class="column">
				<span><?php esc_html_e( 'In', 'appica' ); ?> </span><?php echo $categories; ?>
				<?php echo $by_author; ?>
			</div>
		<?php endif; ?>
		<?php if ( $tags ) : ?>
			<div class="column text-right">
				<?php echo $tags; ?>
			</div>
		<?php endif; ?>
		</div><?php
	}
endif;

if ( ! function_exists( 'appica_entry_footer_wo_social' ) ) :
	/**
	 * Prints entry meta data to post footer.
	 *
	 * Similar to {@see appica_entry_footer}, excepts comments counter, social share buttons and tags are not displayed.
	 * This template tag call inside The Loop, so, you can use other tags.
	 *
	 * @since 1.0.0
	 */
	function appica_entry_footer_wo_social() {
		if ( 'post' !== get_post_type() ) {
			return;
		}

		/**
		 * Filter the categories view, displayed in single post entry footer.
		 *
		 * @since 1.0.0
		 * @link  http://codex.wordpress.org/Function_Reference/get_the_category_list
		 *
		 * @param string $categories Categories list, separated by comma
		 */
		$categories = apply_filters( 'appica_get_the_category_list', get_the_category_list( ', ' ) );

		$by_author = sprintf(
			esc_html_x( 'by %s', 'post author', 'appica' ),
			'<span class="author vcard"><a href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
		);

		/*
		 * Edge case: no title.
		 * In that case wrap a date into permalink.
		 */
		$post_title  = get_the_title();
		$is_no_title = ( '' === $post_title );
		?>
		<div class="post-meta">
		<div class="column">
			<span><?php esc_html_e( 'In', 'appica' ); ?> </span><?php echo $categories; ?>
			<?php echo $by_author; ?>
		</div>
		<div class="column text-right">
			<?php if ( $is_no_title ) : ?>
				<a href="<?php the_permalink()?>">
					<span><?php appica_posted_on(); ?></span>
				</a>
			<?php else: ?>
				<span><?php appica_posted_on(); ?></span>
			<?php endif; ?>

		</div>
		</div><?php
	}
endif;

if ( ! function_exists( 'appica_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 *
	 * @since 1.0.0
	 */
	function appica_posted_on() {
		printf( '<time class="entry-date" datetime="%1$s">%2$s</time>',
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date() )
		);
	}
endif;

if ( ! function_exists( 'appica_social_share_buttons' ) ) :
	/**
	 * Render the share button in Single Post.
	 *
	 * This function is used in Loop.
	 *
	 * @since 1.0.0
	 */
	function appica_social_share_buttons() {
		// Prepare the content for share
		$text = esc_attr( get_the_title() );
		$url = esc_url( get_the_permalink() );

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id() );
		$img = $thumb[0];
		?>
		<a href="#" class="sb-twitter appica-twitter-share" data-text="<?php echo $text; ?>" data-url="<?php echo $url; ?>">
			<i class="bi-twitter"></i>
		</a>
		<a href="#" class="sb-google-plus appica-google-share" data-url="<?php echo $url; ?>"><i class="bi-gplus"></i></a>
		<a href="#" class="sb-facebook appica-facebook-share" data-url="<?php echo $url; ?>"><i class="bi-facebook"></i></a>
		<a href="#" class="sb-pinterest appica-pinterest-share"
		   data-url="<?php echo $url; ?>"
		   data-media="<?php echo $img; ?>"
		   data-description="<?php echo $text; ?>">
			<i class="bi-pinterest-circled"></i>
		</a>
		<?php
	}
endif;

if ( ! function_exists( 'appica_paginate_links' ) ) :
	/**
	 * Show pagination links for posts, archives, search results etc
	 *
	 * @since 1.0.0
	 */
	function appica_paginate_links() {
		$navigation = '';
		$navigation .= '<div class="pagination space-top-3x space-bottom-3x">';
		$navigation .= paginate_links( array(
			'type'      => 'plain',
			'prev_text' => _x( 'Newer', 'navigation', 'appica' ),
			'next_text' => _x( 'Older', 'navigation', 'appica' )
		) );
		$navigation .= '</div>';

		echo $navigation;
	}
endif;

/**
 * Display the favicon. Based on theme settings.
 *
 * Fallback for old WP.
 *
 * @since 1.0.0
 * @since 2.0.0 Add the has_site_login check
 */
function appica_the_favicon() {
	if ( function_exists( 'has_site_icon' ) && has_site_icon() ) {
		return;
	}

	$favicon = appica_option_image( 'global_favicon' );
	if ( empty( $favicon ) ) {
		return;
	}

	$favicon_url = esc_url( $favicon );
	printf( '<link rel="shortcut icon" href="%s" type="image/x-icon">', $favicon_url );
	printf( '<link rel="icon" href="%s" type="image/x-icon">', $favicon_url );
}

if ( ! function_exists( 'appica_preloader' ) ) :
	/**
	 * Show preloader
	 *
	 * @since 1.0.0
	 */
	function appica_preloader() {
		$logo = appica_option_get( 'global_preloader_logo' );
		$text = appica_option_get( 'global_preloader_text' );

		$html = '';

		$logo = ( is_array( $logo ) && array_key_exists( 'url', $logo ) && '' !== $logo['url'] ) ? $logo['url'] : '';
		$text = ( '' === $text ) ? '' : esc_html( $text );

		if ( '' !== $logo || '' !== $text ) {
			$html .= '<div class="logo">';
			$html .= ( '' === $logo ) ? '' : sprintf( '<img src="%s">', $logo );
			$html .= ( '' === $text ) ? '' : "<span>{$text}</span>";
			$html .= '</div>';
		}

		echo '<div id="preloader">', $html, '</div>';
	}
endif;

if ( ! function_exists( 'appica_subscribe_modal_form' ) ) :
	/**
	 * Subscribe form modal dialog
	 *
	 * @since 1.0.0
	 */
	function appica_subscribe_modal_form() {
		$action = appica_option_get( 'socials_mailchimp' );

		if ( empty( $action ) ) {
			return;
		}

		$label = appica_option_get( 'socials_subscribe_label', esc_html__( 'Subscribe', 'appica' ) );

		// MailChimp prepare Anti-Spam
		$request_uri = parse_url( htmlspecialchars_decode( $action ), PHP_URL_QUERY );
		parse_str( $request_uri , $c );
		$mc_antispam = sprintf( 'b_%1$s_%2$s', $c['u'], $c['id'] );

		unset( $request_uri, $c );

		?>
		<div class="modal fade" id="subscribe-page">
			<div class="modal-dialog">
				<div class="modal-form">
					<form method="post" action="<?php echo esc_url( $action ); ?>" id="subscribe-form" autocomplete="off" target="_blank">
						<h3 class="modal-title"><?php echo esc_html( $label ); ?></h3>
						<div class="form-control space-top-2x">
							<input type="text" name="NAME" id="si-name" required>
							<label for="si-name"><?php esc_html_e( 'Name', 'appica' ); ?></label>
							<span class="error-label"></span>
							<span class="valid-label"></span>
						</div>
						<div class="form-control">
							<input type="email" name="EMAIL" id="si_email" required>
							<label for="si_email"><?php esc_html_e( 'Email', 'appica' ); ?></label>
							<span class="error-label"></span>
							<span class="valid-label"></span>
						</div>
						<div style="position: absolute; left: -5000px;">
							<input type="text" name="<?php echo esc_attr( $mc_antispam ); ?>" tabindex="-1" value="">
						</div>
						<div class="clearfix modal-buttons">
							<div class="pull-right">
								<button type="button" class="btn btn-flat btn-default waves-effect" data-dismiss="modal"><?php esc_html_e( 'Cancel', 'appica' ); ?></button>
								<button type="submit" class="btn btn-flat btn-primary waves-effect waves-primary"><?php esc_html_e( 'Submit', 'appica' ); ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<?php
	}
endif;

/**
 * Display navigation when applicable, according to settings.
 *
 * @since 1.0.0
 */
function appica_posts_navigation() {
	if ( $GLOBALS['wp_query']->max_num_pages <= 1 ) {
		return;
	}

	$type = appica_option_get( 'global_blog_pagination', 'pagination' );

	// Count total number of published posts
	$total    = (int) wp_count_posts()->publish;
	$per_page = (int) get_option( 'posts_per_page' );
	// Determine current page
	$paged = get_query_var( 'paged' );
	$paged = ( $paged ) ? $paged : 1;

	$navigation = '';

	switch ( $type ) {
		case 'infinite-scroll':
			$navigation .= _appica_infinite_scroll( $total, $per_page, $paged );
			break;

		case 'load-more':
			$navigation .= '<div class="pagination space-top space-bottom">';
			$navigation .= _appica_load_more_posts( $total, $per_page, $paged );
			$navigation .= '</div>';
			break;

		default:
			$navigation .= '<div class="pagination space-top space-bottom">';
			$navigation .= paginate_links( array(
				'type'      => 'plain',
				'prev_text' => esc_html_x( 'Newer', 'navigation', 'appica' ),
				'next_text' => esc_html_x( 'Older', 'navigation', 'appica' )
			) );
			$navigation .= '</div>';
			break;
	}

	echo $navigation;
}

/**
 * Load More posts button for blog navigation
 *
 * @since  1.0.0
 * @access private
 *
 * @param int $total    Total number of published posts
 * @param int $per_page Posts per page. See settings.
 * @param int $paged    Current page
 *
 * @return string
 */
function _appica_load_more_posts( $total, $per_page, $paged ) {
	// Determine number of post to be loaded
	$number = $total - ( $paged * $per_page );

	// Hide "Load More" button if number of entries less than zero,
	// e.g. user load last page and other conditions not fired
	if ( $number <= 0 ) {
		return '';
	}

	// If number of posts greater, than per_page option - show per_per value
	// Variable is needed to show value, if it less than per_page
	$number = ( $number > $per_page ) ? $per_page : $number;
	// Note: empty space before (<span>...
	$more   = esc_html__( 'Show more', 'appica' ) . sprintf( ' (<span class="count">%s</span>)', $number );

	// 1 - more, 2 - current page, 3 - total posts count
	$template = '<a href="#" data-page="%2$s" data-total="%3$s" data-per-page="%4$s" class="load-more load-more-posts">'
	            . '<i class="flaticon-show5"></i>%1$s</a>';

	// Paged always have to be + 1, because we need to load next page, not current
	$navigation = sprintf( $template, $more, $paged + 1, $total, $per_page );

	return $navigation;
}

/**
 * Infinite Scroll for blog navigation
 *
 * @since  1.0.0
 * @access private
 *
 * @param int $total    Total number of published posts
 * @param int $per_page Posts per page. See settings.
 * @param int $paged    Current page
 *
 * @return string
 */
function _appica_infinite_scroll( $total, $per_page, $paged ) {
	// Determine number of post to be loaded
	$number = $total - ( $paged * $per_page );

	// Hide "Load More" button if number of entries less than zero,
	// e.g. user load (not first) page and other conditions not fired
	if ( $number <= 0 ) {
		return '';
	}

	// 1 - total, 2 - per page, 3 - current page
	$template = '<div class="hidden" id="appica-infinite-scroll" data-total="%1$s" data-per-page="%2$s" data-page="%3$s" data-max-pages="%4$s"></div>';

	// Paged always have to be + 1, because we need to load next page, not current
	return sprintf( $template, $total, $per_page, $paged + 1, $GLOBALS['wp_query']->max_num_pages );
}

if ( ! function_exists( 'appica_the_socials' ) ) :
	/**
	 * Prints the socials
	 *
	 * Target is configured in Theme Options
	 *
	 * @uses appica_get_networks
	 *
	 * @param array  $socials A key-value pairs of networks and urls to socials
	 */
	function appica_the_socials( $socials ) {
		if ( empty( $socials ) ) {
			return;
		}

		/**
		 * @var array Allowed targets for socials links
		 */
		$allowed_targets = array( '_blank', '_self' );

		$target   = appica_option_get( 'socials_networks_target', '_blank' );
		$target   = in_array( $target, $allowed_targets, true ) ? $target : '_blank';
		$networks = appica_get_networks( get_template_directory() . '/misc/socials.ini' );

		?>
		<div class="social-buttons"><?php
		foreach ( (array) $socials as $network => $url ) :
			printf( '<a href="%1$s" class="%3$s" target="%4$s"><i class="%2$s"></i></a>',
				esc_url( $url ),
				esc_attr( $networks[ $network ]['icon'] ),
				esc_attr( $networks[ $network ]['helper'] ),
				$target
			);
		endforeach;
		?>
		</div><?php
	}
endif;

/**
 * Display the Page Title, based on Page Settings
 *
 * @since 2.0.0
 */
function appica_page_title() {
	$type = appica_get_page_settings( 'type', 'title' );
	if ( 'none' === $type || 'intro' === $type || 'revslider' === $type ) {
		return;
	}

	$classes   = array();
	$classes[] = 'page-heading';
	$classes[] = 'text-right';

	if ( is_home() ) :
		if ( 'page' === (string) get_option( 'show_on_front' ) ) {
			$page_id = (int) get_option( 'page_for_posts' );
		} else {
			$page_id = 0;
		}

		if ( 0 === $page_id ) {
			return;
		}

		$title = get_the_title( $page_id );
		if ( empty( $title ) ) {
			return;
		}

		?>
		<div class="<?php echo appica_get_class_set( $classes ); ?>">
			<div class="container">
				<?php get_search_form(); ?>
				<?php appica_the_text( esc_html( get_the_title( $page_id ) ), '<h2>', '</h2>' ); ?>
			</div>
		</div>
		<?php
	elseif ( is_page() ) :
		?>
		<div class="page-heading text-right">
			<div class="container">
				<?php the_title( '<h2>', '<h2>' ); ?>
			</div>
		</div>
		<?php
	elseif( is_single() ) :
		$is_search = (bool) absint( appica_get_post_settings( 'search', 1 ) );
		$custom    = esc_html( appica_get_post_settings( 'custom_title' ) );
		$is_title  = ( ! empty( $custom ) );

		if ( ! $is_title && $is_search ) {
			$classes[] = 'no-title';
		}

		if ( ! $is_title && ! $is_search ) {
			$classes[] = 'no-content';
		}

		?>
		<div class="<?php echo appica_get_class_set( $classes ); ?>">
			<div class="container">
				<?php
				if ( $is_search ) {
					get_search_form();
				}

				appica_the_text( $custom, '<h2>', '</h2>' );
				unset( $custom, $is_search, $is_title );
				?>
			</div>
		</div>
		<?php
	endif;
}

/**
 * Check if Featured Video used.
 * Video has priority over Featured Image.
 *
 * @author 8guild
 * @since  1.0.0
 *
 * @return bool
 */
function appica_has_featured_video() {
	$meta = get_post_meta( get_the_ID(), '_appica_featured_video', true );

	if ( ! empty( $meta ) ) {
		return true;
	}

	return false;
}

/**
 * Show Featured Video
 *
 * @author 8guild
 * @since 1.0.0
 */
function appica_the_featured_video() {
	$url = get_post_meta( get_the_ID(), '_appica_featured_video', true );
	$embed = wp_oembed_get( $url );

	if ( false !== $embed ) {
		echo $embed;
	}
}

/**
 * Checks if "Sticky Navbar" is enabled in Theme Options
 *
 * @see inc/options.php
 *
 * @since 2.0.0
 *
 * @return bool
 */
function appica_navbar_is_sticky() {
	$is_sticky = absint( appica_option_get( 'navbar_is_sticky', 0 ) );

	return (bool) $is_sticky;
}

if ( ! function_exists( 'appica_navbar_logo' ) ) :
	/**
	 * Show navbar logo and title
	 *
	 * @since 1.0.0
	 */
	function appica_navbar_logo() {
		$logo  = appica_option_get( 'navbar_logo' );
		$title = appica_option_get( 'navbar_title' );
		$title = ( '' === $title ) ? '' : esc_html( $title );

		if ( is_array( $logo ) && array_key_exists( 'url', $logo ) && '' !== $logo['url'] ) {
			$logo = sprintf( '<img src="%1$s" alt="%2$s">', esc_url( $logo['url'] ), $title );
		} else {
			$logo = sprintf( '<img src="%1$s" alt="%2$s">', appica_image_uri( 'img/logo-small.png', false ), $title );
		}

		if ( is_front_page() && ! is_home() ) {
			printf( '<a href="#" class="logo scrollup">%1$s %2$s</a>', (string) $logo, $title );
		} else {
			printf( '<a href="%1$s" class="logo">%2$s %3$s</a>', home_url( '/' ), (string) $logo, $title );
		}
	}
endif;

if ( ! function_exists( 'appica_navbar_socials' ) ) :
	/**
	 * Render navbar social networks
	 *
	 * Based on social networks list from "Socials" options
	 *
	 * @since 1.0.0
	 * @since 2.0.0 {@uses appica_get_socials} and {@uses appica_the_socials}
	 */
	function appica_navbar_socials() {
		$is_social = absint( appica_option_get( 'navbar_is_social', 0 ) );

		if ( false === (bool) $is_social ) {
			return;
		}

		appica_the_socials( appica_get_socials() );
	}
endif;

if ( ! function_exists( 'appica_navbar_download_button' ) ) :
	/**
	 * Print navbar download button and helper text
	 *
	 * @since 1.0.0
	 */
	function appica_navbar_download_button() {
		$is_btn = absint( appica_option_get( 'navbar_is_download', 0 ) );
		if ( false === (bool) $is_btn ) {
			echo '<div class="btn invisible">&nbsp;</div>';
			return;
		}

		$text = appica_option_get( 'navbar_download_button_text' );
		$url  = appica_option_get( 'navbar_download_button_url' );

		// just a bit safer
		$url = ( '' === $url ) ? '#' : $url;

		printf(
			'<a href="%1$s" class="btn btn-flat btn-light icon-left waves-effect waves-light" target="_blank"><i class="flaticon-download164"></i> %2$s</a>',
			esc_url( $url ), esc_html( $text )
		);
	}
endif;

if ( ! function_exists( 'appica_navbar_subscribe' ) ) :
	/**
	 * Print navbar subscribe/login button
	 *
	 * @since 1.0.0
	 */
	function appica_navbar_subscribe() {
		$is_subscribe = absint( appica_option_get( 'navbar_is_subscribe', 0 ) );

		if ( false === (bool) $is_subscribe ) {
			return;
		}

		$text = appica_option_get( 'socials_subscribe_label', esc_html__( 'Subscribe', 'appica' ) );

		printf( '<a href="#" class="action-btn" data-toggle="modal" data-target="#subscribe-page">%s</a>', esc_html( $text ) );
	}
endif;

if ( ! function_exists( 'appica_footer_app' ) ) :
	/**
	 * Display footer app
	 *
	 * @since 1.0.0
	 */
	function appica_footer_app() {
		$is_app = appica_option_get( 'footer_is_app', false );

		if ( false === (bool) $is_app ) {
			return;
		}

		$url = appica_option_get( 'footer_app_url', '#' );
		$url = ( '' === $url || '#' === $url ) ? '#' : esc_url( $url );

		$name = appica_option_get( 'footer_app_name' );
		$name = ( '' !== $name ) ? "<h2>{$name}</h2>" : '';

		$logo = appica_option_get( 'footer_logo' );
		$logo = ( is_array( $logo ) && array_key_exists( 'url', $logo ) && '' !== $logo['url'] )
			? esc_url( $logo['url'] )
			: '';

		$is_tagline = (bool) appica_option_get( 'footer_is_app_tagline' );
		$tagline    = ( true === $is_tagline )
			? sprintf( '<p>%s</p>', esc_html( appica_option_get( 'footer_app_tagline' ) ) )
			: '';

		$is_rating = (bool) appica_option_get( 'footer_is_app_rating' );
		$rating    = ( true === $is_rating )
			? round( appica_option_get( 'footer_app_rating' ) )
			: 0;

		$is_counter = (bool) appica_option_get( 'footer_is_app_ratings_counter' );
		$counter    = ( true === $is_counter )
			? sprintf( '<span>(%s)</span>', appica_option_get( 'footer_app_ratings_counter' ) )
			: '';

		// start gather output HTML
		$html = '';
		$html .= sprintf( '<a href="%1$s" class="footer-head waves-effect waves-button waves-float" target="_blank">', $url );
		$html .= ( '' === $logo ) ? '' : sprintf( '<div class="logo"><img src="%1$s" alt="%2$s"></div>', $logo, $name );

		// .info block, show if user specify name OR rating OR counter OR tagline
		$html .= ( '' !== $name || false !== $is_rating ||  false !== $is_counter || false !== $is_tagline ) ? '<div class="info">' : '';

		// App name
		$html .= $name;

		// app rating & counter
		$html .= ( true === $is_rating ) ? '<div class="rating">' : '';
		for ( $i = 0; $i < $rating; $i++ ) {
			$html .= '<i class="bi-star"></i>';
		}
		// display counters
		$html .= $counter;
		// end of div.rating
		$html .= ( true === $is_rating ) ? '</div>' : '';
		// display tagline
		$html .= $tagline;

		// end of .info block
		$html .= ( '' !== $name || false !== $is_rating ||  false !== $is_counter || false !== $is_tagline ) ? '</div>' : '';
		$html .= '</a>';

		echo '<div class="footer-head-wrap">', $html, '</div>';
	}
endif;

if ( ! function_exists( 'appica_the_copyright' ) ) :
	/**
	 * Echoes the footer copyright contents
	 *
	 * @since 1.0.0
	 * @since 2.0.1 Add wp_kses
	 */
	function appica_the_copyright() {
		$copyright = appica_option_get( 'footer_copyright' );
		if ( empty( $copyright ) ) {
			return;
		}

		echo wp_kses( $copyright, array(
			'strong' => array(),
			'em'     => array(),
			'b'      => array(),
			'i'      => array(),
			'a'      => array( 'href' => array(), 'target' => array() ),
			'p'      => array(),
			'del'    => array(),
			'span'   => array( 'style' => array() ),
		) );
	}
endif;

/**
 * Check if footer device is enabled
 *
 * @since 1.0.0
 *
 * @return bool
 */
function appica_is_footer_device() {
	$is_device = absint( appica_option_get( 'footer_is_device' ) );

	return (bool) $is_device;
}

/**
 * Check if footer nav location is enabled
 *
 * @since 1.0.0
 *
 * @return bool
 */
function appica_is_footer_nav() {
	$is_nav = absint( appica_option_get( 'footer_is_nav', 1 ) );

	return (bool) $is_nav;
}

/**
 * Check if user add image to footer device screen
 *
 * @since 1.0.0
 *
 * @return bool
 */
function appica_is_footer_device_screen() {
	$screen = appica_option_image( 'footer_device_screen' );

	return (bool) $screen;
}

if ( ! function_exists( 'appica_footer_device_screen' ) ) :
	/**
	 * Show footer device image
	 *
	 * @since 1.0.0
	 */
	function appica_footer_device_screen() {
		$attachment = appica_option_image( 'footer_device_screen' );

		printf( '<img src="%s">', esc_url( $attachment ) );
	}
endif;

/**
 * Check if Off-canvas navigation is enabled
 *
 * @since 1.0.0
 *
 * @return bool
 */
function appica_is_offcanvas_search() {
	$is_search = absint( appica_option_get( 'offcanvas_is_search', 0 ) );

	return (bool) $is_search;
}

if ( ! function_exists( 'appica_offcanvas_socials' ) ) :
	/**
	 * Print social networks list in Off-Canvas navigation
	 *
	 * @since 1.0.0
	 */
	function appica_offcanvas_socials() {
		$is_social = appica_option_get( 'offcanvas_is_socials' );

		if ( false === (bool) $is_social ) {
			return;
		}

		appica_the_socials( appica_get_socials() );
	}
endif;

if ( ! function_exists( 'appica_offcanvas_logo' ) ) :
	/**
	 * Show logo, title, subtitle in Off-canvas navigation
	 *
	 * @since 1.0.0
	 */
	function appica_offcanvas_logo() {
		$logo     = appica_option_image( 'offcanvas_logo' );
		$title    = appica_option_get( 'offcanvas_title' );
		$subtitle = appica_option_get( 'offcanvas_subtitle' );

		$is_front_page = is_front_page();

		$home_url = $is_front_page ? '#' : home_url( '/' );
		$classes  = $is_front_page ? 'offcanvas-logo scrollup' : 'offcanvas-logo';
		$icon     = '';

		if ( $logo ) {
			$icon = sprintf( '<div class="icon"><img src="%s"></div>', $logo );
		}

		if ( ! empty( $title ) ) {
			$title = sprintf( '<div class="title">%1$s%2$s</div>',
				esc_html( $title ),
				appica_get_text( esc_html( $subtitle ), '<span>', '</span>' )
			);
		}

		printf( '<a href="%1$s" class="%4$s">%2$s%3$s</a>',
			esc_url( $home_url ),
			$icon,
			$title,
			esc_attr( $classes )
		);
	}
endif;

if ( ! function_exists( 'appica_offcanvas_button' ) ) :
	/**
	 * Show download button in Off-Canvas navigation
	 *
	 * @since 1.0.0
	 */
	function appica_offcanvas_button() {
		$is_button = appica_option_get( 'offcanvas_is_download' );

		if ( false === (bool) $is_button ) {
			return;
		}

		$label = appica_option_get( 'offcanvas_download_label', esc_html__( 'Download', 'appica' ) );
		$url   = appica_option_get( 'offcanvas_download_url' );
		$url   = ( '' === $url ) ? '#' : $url;

		printf(
			'<a href="%1$s" class="btn btn-flat btn-light icon-left waves-effect waves-light" target="_blank"><i class="flaticon-download164"></i> %2$s</a>',
			esc_url( $url ), esc_html( $label )
		);
	}
endif;

if ( ! function_exists( 'appica_offcanvas_subscribe' ) ) :
	/**
	 * Show subscribe link in Off-canvas navigation
	 *
	 * @since 1.0.0
	 */
	function appica_offcanvas_subscribe() {
		$is_subscribe = appica_option_get( 'offcanvas_is_subscribe' );

		if ( false === (bool) $is_subscribe ) {
			return;
		}

		$text = appica_option_get( 'socials_subscribe_label', esc_html__( 'Subscribe', 'appica' ) );

		printf( '<a href="#" class="light-color nav-link" data-toggle="modal" data-target="#subscribe-page">%s</a>', esc_html( $text ) );
	}
endif;

if ( ! function_exists( 'appica_intro_socials' ) ) :
	/**
	 * Show social networks in Intro screen
	 *
	 * @since 1.0.0
	 */
	function appica_intro_socials() {
		$is_social = absint( appica_option_get( 'intro_is_social', 0 ) );
		if ( false === (bool) $is_social ) {
			return;
		}

		appica_the_socials( appica_get_socials() );
	}
endif;

if ( ! function_exists( 'appica_intro_subscribe' ) ) :
	/**
	 * Show Intro subscribe
	 *
	 * @since 1.0.0
	 */
	function appica_intro_subscribe() {
		$is_subscribe = absint( appica_option_get( 'intro_is_subscribe', 0 ) );

		if ( false === (bool) $is_subscribe ) {
			return;
		}

		$text = appica_option_get( 'socials_subscribe_label', esc_html__( 'Subscribe', 'appica' ) );
		printf( '<a href="#" data-toggle="modal" data-target="#subscribe-page">%s</a>',
			esc_html( $text )
		);
	}
endif;

if ( ! function_exists( 'appica_intro_scroll' ) ) :
	/**
	 * Show intro "Scroll for more" button
	 *
	 * @since 1.0.0
	 * @since 2.0.0 Based on {@see Appica_Cpt_Intro}, get $settings as a param
	 *
	 * @param array $settings Array of settings
	 */
	function appica_intro_scroll( $settings ) {
		if ( false === (bool) absint( $settings['is_scroll'] ) ) {
			return;
		}

		printf( '<a href="%1$s" class="scroll-more scroll" data-offset-top="-5"><i class="icon"></i><span>%2$s</span></a>',
			esc_attr( '#' . ltrim( $settings['more_anchor'], '#' ) ),
			appica_get_text( esc_html( $settings['more_text'] ), '<span>', '</span>' )
		);
	}
endif;

if ( ! function_exists( 'appica_intro_logo' ) ) :
	/**
	 * Show Intro: logo, title & subtitle if exists
	 *
	 * Do not print any html, if options not filled
	 *
	 * @since 1.0.0
	 * @since 2.0.0 Based on {@see Appica_Cpt_Intro}, get $settings as a param
	 *
	 * @param array $settings Intro settings
	 */
	function appica_intro_logo( $settings ) {
		$title = esc_html( $settings['title'] );
		$logo  = appica_get_image_tag( $settings['logo'], 'full', array( 'alt' => $title ) );

		printf( '<h1 class="logo">%1$s%2$s%3$s</h1>',
			$logo,
			$title,
			appica_get_text( esc_html( $settings['subtitle'] ), '<span>', '</span>' )
		);
	}
endif;

if ( ! function_exists( 'appica_intro_screen' ) ) :
	/**
	 * Show Intro screen image (inside device)
	 *
	 * @since 1.0.0
	 * @since 2.0.0 Based on {@see Appica_Cpt_Intro}, get $settings as a param
	 *
	 * @param array $settings Intro settings
	 */
	function appica_intro_screen( $settings ) {
		printf( '<div class="phone">%s</div>',
			appica_get_image_tag( $settings['device_screen'] )
		);
	}
endif;

if ( ! function_exists( 'appica_intro_download' ) ) :
	/**
	 * Show Intro download button(s)
	 *
	 * @since 1.0.0
	 * @since 2.0.0 Based on {@see Appica_Cpt_Intro}, get $settings as a param
	 *
	 * @param array $settings Intro settings array
	 */
	function appica_intro_download( $settings ) {
		if ( false === (bool) absint( $settings['is_download'] ) ) {
			return;
		}

		if ( empty( $settings['google_play']['url'] ) || empty( $settings['google_play']['text'] ) ) {
			$google_play_btn = '';
		} else {
			$google_play_btn = sprintf( '<a href="%1$s" class="btn btn-default btn-float waves-effect btn-google-play" target="%4$s"><img src="%3$s">%2$s</a>',
				esc_url( $settings['google_play']['url'] ),
				appica_get_text( esc_html( $settings['google_play']['text'] ), '<span>', '</span>' ),
				appica_image_uri( 'img/google-play.png', false ),
				esc_attr( $settings['google_play']['target'] )
			);
		}

		if ( empty( $settings['app_store']['url'] ) || empty( $settings['app_store']['text'] ) ) {
			$app_store_btn = '';
		} else {
			$app_store_btn = sprintf( '<a href="%1$s" class="btn btn-default btn-app-store" target="%4$s"><i class="bi-apple"></i><div>%2$s%3$s</div></a>',
				esc_url( $settings['app_store']['url'] ),
				appica_get_text( esc_html( $settings['app_store']['text'] ), '<span>', '</span>' ),
				esc_html__( 'App Store', 'appica' ),
				esc_attr( $settings['app_store']['target'] )
			);
		}

		printf( '<div class="download">%1$s%2$s%3$s</div>',
			appica_get_text( esc_html( $settings['download_helper'] ), '<p>', '</p>' ),
			$google_play_btn,
			$app_store_btn
		);
	}
endif;

if ( ! function_exists( 'appica_intro_features' ) ) :
	/**
	 * Show Intro features
	 *
	 * @since 1.0.0
	 * @since 2.0.0 Based on {@see Appica_Cpt_Intro}, get $settings as a param
	 *
	 * @param array $settings Intro settings array
	 */
	function appica_intro_features( $settings ) {
		if ( false === (bool) absint( $settings['is_features'] ) ) {
			return;
		}

		$first  = appica_parse_array( $settings, 'feature_first_' );
		$second = appica_parse_array( $settings, 'feature_second_' );
		$third  = appica_parse_array( $settings, 'feature_third_' );

		$features = array( $first, $second, $third );
		unset( $first, $second, $third );

		echo '<div class="intro-features">';

		foreach ( $features as $feature ) {
			if ( empty( $feature['icon'] ) || empty( $feature['title'] ) ) {
				continue;
			}

			$feature_class = appica_get_class_set( array(
				'icon-block',
				'icon-block-horizontal',
				'box-float',
			) );

			$feature_attr = appica_get_html_attributes( array(
				'class'                 => esc_attr( $feature_class ),
				'data-transition-delay' => absint( $feature['transition'] ),
			) );

			printf( '<div %1$s>%2$s<div class="text">%3$s%4$s</div></div>',
				$feature_attr,
				appica_get_text( esc_attr( $feature['icon'] ), '<div class="icon va-middle"><i class="', '"></i></div>' ),
				appica_get_text( esc_html( $feature['title'] ), '<h3>', '</h3>' ),
				appica_get_text( wp_kses( $feature['description'], array(
					'a'      => array( 'href' => array(), 'target' => array() ),
					'br'     => array(),
					'em'     => array(),
					'strong' => array(),
				) ), '<p>', '</p>' )
			);
		}

		echo '</div>'; // .intro-features
	}
endif;

if ( ! function_exists( 'appica_the_intro' ) ) :
	/**
	 * Display the Intro Section
	 *
	 * @since 2.0.0
	 *
	 * @see Appica_Cpt_Intro
	 */
	function appica_the_intro() {
		$intro_id = absint( appica_get_page_settings( 'intro' ) );
		if ( empty( $intro_id ) ) {
			return;
		}

		$settings = appica_meta_box_get( $intro_id, '_appica_intro_settings' );
		$settings = wp_parse_args( $settings, array(
			'bg'                         => 0,
			'logo'                       => 0,
			'title'                      => '',
			'subtitle'                   => '',
			'device_screen'              => 0,
			'is_scroll'                  => 0,
			'more_text'                  => '',
			'more_anchor'                => '',
			'is_download'                => 0,
			'download_helper'            => '',
			'app_store'                  => array( 'text' => '', 'url' => '', 'target' => '_self' ),
			'google_play'                => array( 'text' => '', 'url' => '', 'target' => '_self' ),
			'is_features'                => 0,
			'feature_first_transition'   => 100,
			'feature_first_icon'         => '',
			'feature_first_title'        => '',
			'feature_first_description'  => '',
			'feature_second_transition'  => 300,
			'feature_second_icon'        => '',
			'feature_second_title'       => '',
			'feature_second_description' => '',
			'feature_third_transition'   => 500,
			'feature_third_icon'         => '',
			'feature_third_title'        => '',
			'feature_third_description'  => '',
		) );

		$intro_attr = appica_get_html_attributes( array(
			'class' => 'intro',
			'style' => appica_get_image_bg( $settings['bg'] ),
		) );

		echo '<section ', $intro_attr, '>';

		?>
		<div class="container">
			<div class="column-wrap">
				<div class="column c-left">
					<div class="navi">
						<div class="nav-toggle nav-toggle-float" data-offcanvas="open">
							<span class="waves-effect waves-light"><i class="flaticon-menu55"></i></span>
						</div>
						<?php appica_intro_subscribe(); ?>
					</div>
					<?php
					appica_intro_scroll( $settings );
					?>
				</div>

				<div class="column c-middle">
					<?php
					appica_intro_logo( $settings );
					appica_intro_screen( $settings );
					?>
				</div>

				<div class="column c-right">
					<?php
					appica_intro_socials();
					appica_intro_features( $settings );
					appica_intro_download( $settings );
					?>
				</div>
			</div>
		</div>
		<?php

		echo '</section>';
	}
endif;

if ( ! function_exists( 'appica_the_revolution_slider' ) ) :
	/**
	 * Display the Revolution Slider as page header
	 *
	 * @since 2.0.0
	 *
	 * @uses  putRevSlider
	 */
	function appica_the_revolution_slider() {
		$slider_id = appica_get_page_settings( 'revslider' );
		if ( empty( $slider_id ) || ! function_exists( 'putRevSlider' ) ) {
			return;
		}

		?>
		<section class="intro-slider">
			<div class="container">
				<div class="column-wrap">
					<div class="column c-left">
						<div class="navi">
							<div class="nav-toggle nav-toggle-float" data-offcanvas="open">
								<span class="waves-effect waves-light"><i class="flaticon-menu55"></i></span>
							</div>
							<?php appica_intro_subscribe(); ?>
						</div>
					</div>
					<div class="column c-middle"></div>
					<div class="column c-right">
						<?php appica_intro_socials(); ?>
					</div>
				</div>
			</div>
		</section>
		<?php
		$post = get_queried_object();
		if ( $post instanceof WP_Post ) {
			putRevSlider( $slider_id, $post->post_name );
		}
	}
endif;
