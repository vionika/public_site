<?php
/**
 * Custom theme meta boxes
 *
 * @author 8guild
 * @package Appica
 */

if ( ! defined( 'EQUIP_VERSION' ) || ! is_admin() ) {
	return;
}

/**
 * Fetch a list of Intro Sections
 *
 * @see Appica_Cpt_Intro
 *
 * @since 2.0.0
 *
 * @return array [post_id => title]
 */
function appica_get_intro_sections() {
	$cache_key = appica_cache_key( 'appica_intro_sections' );
	$sections  = wp_cache_get( $cache_key );
	if ( false === $sections ) {
		$sections = array(
			0 => esc_html__( 'Select an Intro Section', 'appica' ),
		);

		$query = new WP_Query( array(
			'post_type'      => 'appica_intro',
			'post_status'    => 'publish',
			'posts_per_page' => - 1,
			'no_found_rows'  => true,
		) );

		if ( $query->have_posts() ) {
			while ( $query->have_posts() ) :
				$query->the_post();
				$sections[ get_the_ID() ] = esc_html( get_the_title() );
			endwhile;

			wp_cache_set( $cache_key, $sections, '', DAY_IN_SECONDS );
		}
		wp_reset_postdata();
	}

	return $sections;
}

/**
 * Return list of Revolution Sliders
 *
 * @since 2.0.0
 *
 * @return array [alias => title]
 */
function appica_get_revolution_sliders() {
	$sliders = array(
		0 => esc_html__( 'Select a slider', 'appica' ),
	);

	if ( ! class_exists( 'RevSlider', false ) ) {
		return $sliders;
	}

	$revslider = new RevSlider();
	$_sliders  = $revslider->getArrSliders();

	if ( $_sliders ) {
		foreach ( $_sliders as $slider ) {
			$sliders[ $slider->getAlias() ] = $slider->getTitle();
		}
		unset( $slider );
	}

	return $sliders;
}

/**
 * Add meta boxes
 *
 * @since 2.0.0
 */
function appica_add_meta_boxes() {
	$page_settings = array(
		array(
			'key'       => 'layout',
			'field'     => 'select',
			'title'     => esc_html__( 'Choose the layout type', 'appica' ),
			'attr'      => array( 'class' => 'widefat' ),
			'default'   => 'boxed',
			'options'   => array(
				'boxed' => esc_html__( 'Boxed', 'appica' ),
				'fluid' => esc_html__( 'Fluid', 'appica' ),
			),
		),
		array(
			'key'     => 'type',
			'field'   => 'select',
			'title'   => esc_html__( 'Choose the header type', 'appica' ),
			'attr'    => array( 'class' => 'widefat' ),
			'default' => 'title',
			'options' => array(
				'none'      => esc_html__( 'None', 'appica' ),
				'title'     => esc_html__( 'Page Title', 'appica' ),
				'intro'     => esc_html__( 'Intro Section', 'appica' ),
				'revslider' => esc_html__( 'Revolution Slider', 'appica' ),
			),
		),
		array(
			'key'       => 'intro',
			'field'     => 'select',
			'title'     => esc_html__( 'Intro Section', 'appica' ),
			'desc'      => esc_html__( 'Choose one of previously created Intro Sections', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
			'required'  => array( 'type', '=', 'intro' ),
			'options'   => call_user_func( 'appica_get_intro_sections' ),
		),
		array(
			'key'       => 'revslider',
			'field'     => 'select',
			'title'     => esc_html__( 'Revolution Slider', 'appica' ),
			'desc'      => esc_html__( 'Choose one of previously created sliders', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
			'required'  => array( 'type', '=', 'revslider' ),
			'options'   => call_user_func( 'appica_get_revolution_sliders' ),
		),
	);

	equip_meta_box_add( '_appica_page_settings', $page_settings, array(
		'title'  => esc_html__( 'Page Settings', 'appica' ),
		'screen' => 'page',
		'engine' => 'simple',
	) );

	$post_settings = array(
		array(
			'key'       => 'sidebar',
			'field'     => 'select',
			'title'     => esc_html__( 'Sidebar Position', 'appica' ),
			'desc'      => esc_html__( 'Choose sidebar position, or disable it', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
			'default'   => 'left',
			'options'   => array(
				'left'  => esc_html__( 'Left', 'appica' ),
				'right' => esc_html__( 'Right', 'appica' ),
				'none'  => esc_html__( 'No sidebar', 'appica' ),
			),
		),
		array(
			'key'     => 'search',
			'field'   => 'select',
			'title'   => esc_html__( 'Search On/Off', 'appica' ),
			'attr'    => array( 'class' => 'widefat' ),
			'default' => 1,
			'options' => array(
				1 => esc_html__( 'On', 'appica' ),
				0 => esc_html__( 'Off', 'appica' ),
			),
		),
		array(
			'key'       => 'is_wide',
			'field'     => 'select',
			'title'     => esc_html__( 'Select Wide or Normal post', 'appica' ),
			'desc'      => esc_html__( 'This option affects only blog post tile', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
			'default'   => 0,
			'options'   => array(
				0 => esc_html__( 'Normal', 'appica' ),
				1 => esc_html__( 'Wide', 'appica' ),
			),
		),
		array(
			'key'       => 'custom_title',
			'field'     => 'input',
			'title'     => esc_html__( 'Custom Post Title', 'appica' ),
			'desc'      => esc_html__( 'Title will be rendered above standard post title', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
		),
		array(
			'key'       => 'overlay',
			'field'     => 'select',
			'title'     => esc_html__( 'Choose post overlay color', 'appica' ),
			'desc'      => esc_html__( 'Affects only "Recent Posts" widget', 'appica' ),
			'desc_wrap' => '<p class="description">%s</p>',
			'attr'      => array( 'class' => 'widefat' ),
			'default'   => 'primary',
			'options'   => array(
				'primary' => esc_html__( 'Primary', 'appica' ),
				'success' => esc_html__( 'Success', 'appica' ),
				'info'    => esc_html__( 'Info', 'appica' ),
				'warning' => esc_html__( 'Warning', 'appica' ),
				'danger'  => esc_html__( 'Danger', 'appica' ),
			),
		),
	);

	equip_meta_box_add( '_appica_post_settings', $post_settings, array(
		'title'  => esc_html__( 'Post Settings', 'appica' ),
		'screen' => 'post',
		'engine' => 'simple'
	) );
}

add_action( 'current_screen', 'appica_add_meta_boxes' );

/**
 * Add theme meta boxes.
 *
 * @since 1.0.0
 *
 * @param string  $post_type Post Type
 */
function appica_meta_boxes( $post_type ) {
	// Allowed post types
	$screens = array( 'post', 'page' );
	if ( ! in_array( $post_type, $screens, true ) ) {
		return;
	}

	add_meta_box( 'appica-featured-video', esc_html__( 'Featured Video', 'appica' ), 'appica_render_video_meta_box', 'post', 'side', 'default' );
}

add_action( 'add_meta_boxes', 'appica_meta_boxes' );

/**
 * Render "Featured Video" meta box
 *
 * @since 1.0.0
 *
 * @param WP_Post $post Post object
 */
function appica_render_video_meta_box( $post ) {
	wp_nonce_field( 'appica_mb_nonce', 'appica_mb_nonce_field' );

	$meta_box_name = '_appica_featured_video';
	$meta_box_value = get_post_meta( $post->ID, $meta_box_name, true );

	$video = '';
	if ( ! empty( $meta_box_value ) ) {
		$video = wp_oembed_get( $meta_box_value );
	}

	printf( '<div class="appica-video-holder">%1$s</div>', $video );
	printf(
		'<input type="text" class="%2$s widefat" id="appica-featured-video" name="%2$s" value="%1$s" placeholder="%3$s">',
		esc_url_raw( $meta_box_value ), $meta_box_name, esc_html__( 'Video URL', 'appica' )
	);
}

/**
 * Save all custom theme meta boxes
 *
 * @since 1.0.0
 *
 * @param int $post_id Post ID
 */
function appica_save_meta_boxes( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( ! array_key_exists( 'appica_mb_nonce_field' , $_POST )
	     || ! wp_verify_nonce( $_POST['appica_mb_nonce_field'], 'appica_mb_nonce' )
	) {
		return;
	}

	$types = array( 'post', 'page' );
	$type  = $_POST['post_type'];
	// If current screen post or page and current user can edit this screen
	if ( ! in_array( $type, $types, true ) || ! current_user_can( "edit_{$type}", $post_id ) ) {
		return;
	}

	// Featured Video
	if ( array_key_exists( '_appica_featured_video', $_POST ) ) {
		$meta_box_name = '_appica_featured_video';
		$meta_box_value = esc_url_raw( $_POST[ $meta_box_name ] );
		update_post_meta( $post_id, $meta_box_name, $meta_box_value );
		unset( $meta_box_name, $meta_box_value );
	}
}

add_action( 'save_post', 'appica_save_meta_boxes' );
