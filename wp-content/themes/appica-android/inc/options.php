<?php
/**
 * Theme Settings via Redux Framework
 *
 * @since      1.0.0
 * @author     8guild
 * @package    Appica
 * @subpackage Settings
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
    die;
}

if ( ! class_exists( 'Redux', false ) || ! is_admin() ) {
	return;
}

/**
 * Theme options slug.
 *
 * This is the variable where all option data is stored in the database.
 * It also acts as the global variable in which data options are retrieved via code.
 *
 * @var string
 */
$appica_options = 'appica_options';

/**
 * @var WP_Theme Theme object
 */
$theme = wp_get_theme();

/**
 * @var string Translated string for multiple usage
 */
$text_appica = esc_html__( 'Appica', 'appica' );

/**
 * @var string Localized string "Title"
 */
$text_title = esc_html__( 'Title', 'appica' );

/**
 * @var string Localized string "Subtitle"
 */
$text_subtitle = esc_html__( 'Subtitle', 'appica' );

/**
 * @var string "Enable" localized text
 */
$text_enable = esc_html__( 'Enable', 'appica' );

/**
 * @var string "Disable" localized text
 */
$text_disable = esc_html__( 'Disable', 'appica' );

/**
 * @var string "Show" localized
 */
$text_show = esc_html__( 'Show', 'appica' );

/**
 * @var string "Hide" localized
 */
$text_hide = esc_html__( 'Hide', 'appica' );

/**
 * @var string Localized string "Some HTML is allowed here."
 */
$text_html_allowed = esc_html__( 'Some HTML is allowed here.', 'appica' );

/**
 * @var string Localized string of allowable HTML tags
 */
$text_tags_allowed = esc_html__( 'Tags &lt;a&gt;, &lt;br&gt;, &lt;em&gt;, &lt;strong&gt; are allowed.', 'appica' );

/**
 * @var array font-weight possible values
 */
$font_weight = array(
    'lighter' => 'Lighter',
    '100'     => '100',
    '200'     => '200',
    '300'     => '300',
    '400'     => 'Normal (400)',
    '500'     => '500',
    '600'     => '600',
    '700'     => 'Bold (700)',
    '800'     => '800',
    '900'     => '900',
    'bolder'  => 'Bolder'
);

/**
 * @var array text-transform possible values
 */
$text_transform = array(
    'none'       => 'None',
    'capitalize' => 'Capitalize',
    'lowercase'  => 'Lowercase',
    'uppercase'  => 'Uppercase'
);

/*
 * Set global args
 */
Redux::setArgs( $appica_options, array(
	'opt_name'           => $appica_options,
	'display_name'       => $theme->get( 'Name' ),
	'display_version'    => $theme->get( 'Version' ),
	'page_slug'          => 'appica',
	'page_priority'      => '59.3',
	'page_permissions'   => 'edit_theme_options',
	'page_icon'          => 'icon-themes',
	'page_title'         => $text_appica,
	'menu_title'         => $text_appica,
	'menu_icon'          => 'dashicons-editor-textcolor',
	'menu_type'          => 'menu',
	'allow_sub_menu'     => true,
	'class'              => 'appica',
	'admin_bar_icon'     => 'dashicons-admin-generic',
	'output'             => false,
	'output_tag'         => false,
	'hide_expand'        => true,
	'disable_save_warn'  => true,
	'customizer'         => false,
	'dev_mode'           => false,
	'update_notice'      => false,
	'system_info'        => false,
	'show_import_export' => true,
	'into_text'          => '',
	'footer_text'        => '<p>' . esc_html__( 'Appica theme by 8guild', 'appica' ) . '</p>',
	'admin_bar_links'    => array(),
	'share_icons'        => array(
		array(
			'url'   => 'https://twitter.com/8Guild',
			'title' => 'Twitter',
			'icon'  => 'el el-twitter',
		),
		array(
			'url'   => 'https://www.facebook.com/8guild',
			'title' => 'Facebook',
			'icon'  => 'el el-facebook',
		),
		array(
			'url'   => 'https://plus.google.com/u/0/b/109505223181338808677/109505223181338808677/posts',
			'title' => 'Google+',
			'icon'  => 'el el-googleplus',
		),
		array(
			'url'   => 'http://dribbble.com/8guild',
			'title' => 'Dribbble',
			'icon'  => 'el el-dribbble',
		),
		array(
			'url'   => 'https://www.behance.net/8Guild',
			'title' => 'Behance',
			'icon'  => 'el el-behance',
		),
	),
) );

/*
 * Global Settings section
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Global Settings', 'appica' ),
    'icon'   => 'el-icon-globe',
    'fields' => array(
        array(
            'type'  => 'media',
            'id'    => 'global_favicon',
            'title' => esc_html__( 'Custom Favicon', 'appica' ),
            'desc'  => '<p class="description">' . esc_html__( 'Upload your custom favicon here in .ico or .png format.', 'appica' ) . '</p>'
        ),
        array(
            'type'  => 'media',
            'id'    => 'global_preloader_logo',
            'title' => esc_html__( 'Preloader Logo', 'appica' ),
            'desc'  => '<p class="description">' . esc_html__( 'Logo optimal size is 240x240px', 'appica' ) . '</p>'
        ),
        array(
            'type'    => 'text',
            'id'      => 'global_preloader_text',
            'title'   => esc_html__( 'Preloader Text', 'appica' ),
            'default' => 'Appica 2'
        ),
        array(
            'id'      => 'global_blog_pagination',
            'type'    => 'select',
            'title'   => esc_html__( 'Blog Pagination Type', 'appica' ),
            'default' => 'pagination',
            'options' => array(
                'pagination'      => esc_html__( 'Page links', 'appica' ),
                'load-more'       => esc_html__( 'Load More', 'appica' ),
                'infinite-scroll' => esc_html__( 'Infinite Scroll', 'appica' )
            )
        )
    )
) );

/*
 * Global Colors
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Global Colors', 'appica' ),
    'desc'   => esc_html__( 'Set colors that are globally applied everywhere in the theme.', 'appica' ),
    'icon'   => 'el-icon-tint',
    'fields' => array(
        array(
            'type'        => 'color',
            'id'          => 'color_body_font',
            'title'       => esc_html__( 'Body Font Color', 'appica' ),
            'transparent' => false,
            'default'     => '#757575'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_primary',
            'title'       => esc_html__( 'Primary Color', 'appica' ),
            'transparent' => false,
            'default'     => '#008fed'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_success',
            'title'       => esc_html__( 'Success Color', 'appica' ),
            'transparent' => false,
            'default'     => '#96cb4b'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_info',
            'title'       => esc_html__( 'Info Color', 'appica' ),
            'transparent' => false,
            'default'     => '#4b62ff'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_warning',
            'title'       => esc_html__( 'Warning Color', 'appica' ),
            'transparent' => false,
            'default'     => '#ff6d00'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_danger',
            'title'       => esc_html__( 'Danger Color', 'appica' ),
            'transparent' => false,
            'default'     => '#e91e63'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_text_light',
            'title'       => esc_html__( 'Light Gray Color', 'appica' ),
            'transparent' => false,
            'default'     => '#c4c4c4'
        ),
        array(
            'type'        => 'color',
            'id'          => 'color_text_dark',
            'title'       => esc_html__( 'Dark Gray Color', 'appica' ),
            'transparent' => false,
            'default'     => '#999'
        )
    )
) );

/*
 * Typography section
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Typography', 'appica' ),
    'desc'   => esc_html__( 'Customize your typography', 'appica' ),
    'icon'   => 'el-icon-text-width',
    'fields' => array(
        array(
            'type'    => 'switch',
            'id'      => 'typography_is_google',
            'title'   => esc_html__( 'Enable/Disable Google Fonts', 'appica' ),
            'on'      => $text_enable,
            'off'     => $text_disable,
            'default' => true
        ),
        array(
            'type'     => 'text',
            'id'       => 'typography_google_font',
            'title'    => esc_html__( 'Google Font link', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Go to <a href="http://www.google.com/fonts" target="_blank">google.com/fonts</a>, click "Quick-use" button and follow the instructions. From step 3 copy the "href" value and paste in field above.', 'appica' ) . '</p>',
            'default'  => '//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800',
            'required' => array( 'typography_is_google', '=', 1 )
        ),
        array(
            'type'    => 'text',
            'id'      => 'typography_font_family',
            'title'   => esc_html__( 'Body Font Family', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Put chosen google font (do not forget about quotation marks) along with fallback fonts, separated by comma.', 'appica' ) . '</p>',
            'default' => '\'Open Sans\', Helvetica, Arial, sans-serif'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_body_font_size',
            'title'   => esc_html__( 'Body Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 16
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_smaller_font_size',
            'title'   => esc_html__( 'Smaller Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 14
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h1_font_size',
            'title'   => esc_html__( 'Heading 1 (h1) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 72px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 72,
            'step'    => 1,
            'default' => 48
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h1_font_weight',
            'title'   => esc_html__( 'Heading 1 (h1) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '300'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h1_text_transform',
            'title'   => esc_html__( 'Heading 1 (h1) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h2_font_size',
            'title'   => esc_html__( 'Heading 2 (h2) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 36
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h2_font_weight',
            'title'   => esc_html__( 'Heading 2 (h2) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '300'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h2_text_transform',
            'title'   => esc_html__( 'Heading 2 (h2) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h3_font_size',
            'title'   => esc_html__( 'Heading 3 (h3) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 24
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h3_font_weight',
            'title'   => esc_html__( 'Heading 3 (h3) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '300'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h3_text_transform',
            'title'   => esc_html__( 'Heading 3 (h3) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h4_font_size',
            'title'   => esc_html__( 'Heading 4 (h4) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 18
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h4_font_weight',
            'title'   => esc_html__( 'Heading 4 (h4) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '400'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h4_text_transform',
            'title'   => esc_html__( 'Heading 4 (h4) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h5_font_size',
            'title'   => esc_html__( 'Heading 5 (h5) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 16
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h5_font_weight',
            'title'   => esc_html__( 'Heading 5 (h5) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '600'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h5_text_transform',
            'title'   => esc_html__( 'Heading 5 (h5) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_h6_font_size',
            'title'   => esc_html__( 'Heading 6 (h6) Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 14
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h6_font_weight',
            'title'   => esc_html__( 'Heading 6 (h6) Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '700'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_h6_text_transform',
            'title'   => esc_html__( 'Heading 6 (h6) Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        ),
        array(
            'type'    => 'slider',
            'id'      => 'typography_badge_font_size',
            'title'   => esc_html__( 'Badge Font size', 'appica' ),
            'desc'    => '<p class="description">' . esc_html__( 'Min: 1px, Max: 50px, Step: 1px', 'appica' ) . '</p>',
            'min'     => 0,
            'max'     => 50,
            'step'    => 1,
            'default' => 14
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_badge_font_weight',
            'title'   => esc_html__( 'Badge Font weight', 'appica' ),
            'options' => $font_weight,
            'default' => '400'
        ),
        array(
            'type'    => 'select',
            'id'      => 'typography_badge_text_transform',
            'title'   => esc_html__( 'Badge Text Transform', 'appica' ),
            'options' => $text_transform,
            'default' => 'none'
        )
    )
) );

/*
 * Socials section
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Socials', 'appica' ),
    'desc'   => esc_html__( 'Setup your social networks', 'appica' ),
    'icon'   => 'el-icon-group',
    'fields' => array(
	    array(
		    'id'                => 'socials_networks',
		    'type'              => 'socials',
		    'title'             => esc_html__( 'Social Networks List', 'appica' ),
		    'subtitle'          => esc_html__( 'Select as many social networks, as you want', 'appica' ),
		    'validate_callback' => 'appica_redux_validate_socials',
	    ),
	    array(
		    'type'    => 'select',
		    'id'      => 'socials_networks_target',
		    'title'   => esc_html__( 'Open social links in', 'appica' ),
		    'desc'    => '<p class="description">' . esc_html__( 'This option affects social links in all advertised locations, like off-canvas, intro section and navbar.', 'appica' ) . '</p>',
		    'default' => '_blank',
		    'options' => array(
			    '_self'  => esc_html__( 'Current tab', 'appica' ),
			    '_blank' => esc_html__( 'New tab', 'appica' )
		    )
	    ),
        array(
            'id'       => 'socials_mailchimp',
            'type'     => 'text',
            'title'    => esc_html__( 'MailChimp URL', 'appica' ),
            'subtitle' => esc_html__( 'Setup your subscription url globally', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'This URL can be retrieved from your mailchimp dashboard > Lists > your desired list > list settings > forms. in your form creation page you will need to click on "share it" tab then find "Your subscribe form lives at this URL:". Its a short URL so you will need to visit this link. Once you get into the your created form page, then copy the full address and paste it here in this form. URL look like http://YOUR_USER_NAME.us6.list-manage.com/subscribe?u=d5f4e5e82a59166b0cfbc716f&id=4db82d169b', 'appica' ) . '</p>'
        ),
        array(
            'id'       => 'socials_subscribe_label',
            'type'     => 'text',
            'title'    => esc_html__( 'Subscribe link text', 'appica' ),
            'subtitle' => esc_html__( 'Global text, applied in intro, navbar, offcanvas, etc', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Note: this text also applied in modal window as a title.', 'appica' ) . '</p>'
        ),
	    array(
		    'id'    => 'flickr_api_key',
		    'type'  => 'text',
		    'title' => esc_html__( 'Flickr API Key', 'appica' ),
		    'desc'  => wp_kses( __( '<div class="description"><ol><li>Go to <a href="https://www.flickr.com/services/apps/create" target="_blank">https://www.flickr.com/services/apps/create</a></li><li>Click "Request an API Key"</li><li>Choose the application type</li><li>Register you app</li><li>Copy the "key" value in the field above</li></ol></div>', 'appica' ), array(
			    'div' => array( 'class' => array() ),
			    'ol'  => array(),
			    'li'  => array(),
			    'a'   => array( 'href' => array(), 'target' => array() ),
		    ) ),
	    ),
        array(
            'id'    => 'twitter_screen_name',
            'type'  => 'text',
            'title' => esc_html__( 'Twitter Name', 'appica' )
        ),
        array(
            'id'    => 'twitter_consumer_key',
            'type'  => 'text',
            'title' => esc_html__( 'Twitter Consumer Key', 'appica' ),
            'desc'  => wp_kses( __( '<div class="description"><ol><li>Go to <a href="https://apps.twitter.com/" target="_blank">https://apps.twitter.com/</a></li><li>Click on "Create new app"</li><li>Register your app</li><li>Go to "Keys and Access Tokens"</li><li>Copy consumer key and secret from "Application Settings" section and paste in respective fields</li></ol></div>', 'appica' ), array(
	            'div' => array( 'class' => array() ),
	            'ol'  => array(),
	            'li'  => array(),
	            'a'   => array( 'href' => array(), 'target' => array() ),
            ) ),
        ),
        array(
            'id'    => 'twitter_consumer_secret',
            'type'  => 'text',
            'title' => esc_html__( 'Twitter Consumer Secret', 'appica' )
        )
    )
) );

/*
 * Intro section
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Intro Section', 'appica' ),
    'desc'   => esc_html__( 'Customize your Intro Screen', 'appica' ),
    'icon'   => 'el-icon-photo',
    'fields' => array(
        array(
            'type'     => 'switch',
            'id'       => 'intro_is_social',
            'title'    => esc_html__( 'Social Networks', 'appica' ),
            'subtitle' => esc_html__( 'Show social networks in Intro Screen?', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Note: You have to setup social networks list in "Socials" settings section.', 'appica' ) . '</p>',
            'on'       => $text_show,
            'off'      => $text_hide,
            'default'  => true,
            'required' => array( 'intro_is_enabled', '=', 1 )
        ),
        array(
            'type'     => 'switch',
            'id'       => 'intro_is_subscribe',
            'title'    => esc_html__( 'Enable/Disable Subscription', 'appica' ),
            'subtitle' => esc_html__( 'MailChimp', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'You can customize your MailChimp link in "Socials" section. Also, do not forget to fill in link label.', 'appica' ) . '</p>',
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true,
            'required' => array( 'intro_is_enabled', '=', 1 )
        ),
    )
) );

/*
 * Off-canvas navigation section
 */
Redux::setSection( $appica_options, array(
    'icon'   => 'el-icon-lines',
    'title'  => esc_html__( 'Off-Canvas Navi', 'appica' ),
    'desc'   => esc_html__( 'Customize the off-canvas navigation panel', 'appica' ),
    'fields' => array(
        array(
            'type'     => 'switch',
            'id'       => 'offcanvas_is_search',
            'title'    => esc_html__( 'Enable/Disable Search', 'appica' ),
            'subtitle' => esc_html__( 'Show or hide search form in off-canvas navigation', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'switch',
            'id'       => 'offcanvas_is_socials',
            'title'    => esc_html__( 'Enable/Disable Socials', 'appica' ),
            'subtitle' => esc_html__( 'Show or hide social networks list in off-canvas navigation', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'media',
            'id'       => 'offcanvas_logo',
            'title'    => esc_html__( 'Logo', 'appica' ),
            'subtitle' => esc_html__( 'Inside the Off-Canvas panel', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Optimal size is 180x180px', 'appica' ) . '</p>',
            'mode'     => 'image'
        ),
        array(
            'type'        => 'text',
            'id'          => 'offcanvas_title',
            'title'       => $text_title,
            'placeholder' => 'Appica 2'
        ),
        array(
            'type'        => 'text',
            'id'          => 'offcanvas_subtitle',
            'title'       => $text_subtitle,
            'placeholder' => 'Flatter, lighter, appler'
        ),
        array(
            'type'     => 'spinner',
            'id'       => 'offcanvas_anchor_el_num',
            'title'    => esc_html__( 'Number of menu items in Anchor menu', 'appica' ),
            'subtitle' => esc_html__( 'per one column', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Min: 1, Max: 100, Step: 1', 'appica' ) . '</p>',
            'min'      => 1,
            'max'      => 100,
            'step'     => 1,
            'default'  => 7
        ),
        array(
            'type'     => 'switch',
            'id'       => 'offcanvas_is_download',
            'title'    => esc_html__( 'Enable/Disable Download Button', 'appica' ),
            'subtitle' => esc_html__( 'Show or hide custom button link in off-canvas navigation', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'        => 'text',
            'id'          => 'offcanvas_download_label',
            'title'       => esc_html__( 'Download Button Text', 'appica' ),
            'placeholder' => esc_html__( 'Download or any other string', 'appica' ),
            'required'    => array( 'offcanvas_is_download', '=', 1 )
        ),
        array(
            'type'        => 'text',
            'id'          => 'offcanvas_download_url',
            'title'       => esc_html__( 'Download Button URL', 'appica' ),
            'placeholder' => 'http://...',
            'required'    => array( 'offcanvas_is_download', '=', 1 )
        ),
        array(
            'type'     => 'switch',
            'id'       => 'offcanvas_is_subscribe',
            'title'    => esc_html__( 'Enable/Disable Subscribe', 'appica' ),
            'subtitle' => esc_html__( 'Hide or show MailChimp subscription link', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'You can customize your MailChimp URL in "Socials" section', 'appica' ) . '</p>',
            'on'       => esc_html__( 'Enable', 'appica' ),
            'off'      => esc_html__( 'Disable', 'appica' ),
            'default'  => true
        )
    )
) );

/*
 * Navbar section
 */
Redux::setSection( $appica_options, array(
    'icon'   => 'el-icon-lines',
    'title'  => esc_html__( 'Navbar', 'appica' ),
    'fields' => array(
        array(
            'type'     => 'switch',
            'id'       => 'navbar_is_sticky',
            'title'    => esc_html__( 'Sticky Navbar', 'appica' ),
            'subtitle' => esc_html__( 'Enable/Disable Sticky Navbar', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'If enabled this makes navbar stick to top of the page when scrolling. Note, that on Front Page this option also enabled Sticky Footer, which makes footer fixed to the bottom of the page and revealed on scroll.', 'appica' ) . '</p>',
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => false
        ),
        array(
            'type'     => 'media',
            'id'       => 'navbar_logo',
            'title'    => esc_html__( 'Logo', 'appica' ),
            'subtitle' => esc_html__( 'Choose logo for navigation bar', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Optimal size is 96x96px', 'appica' ) . '</p>',
            'mode'     => 'image'
        ),
        array(
            'type'     => 'text',
            'id'       => 'navbar_title',
            'title'    => $text_title,
            'subtitle' => esc_html__( 'Navbar title', 'appica' ),
            'default'  => 'Appica 2'
        ),
        array(
            'type'     => 'switch',
            'id'       => 'navbar_is_social',
            'title'    => esc_html__( 'Social Networks', 'appica' ),
            'subtitle' => esc_html__( 'Enable/Disable Social Networks in Navbar', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Note: You have to setup social networks list in Socials settings section.', 'appica' ) . '</p>',
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'switch',
            'id'       => 'navbar_is_download',
            'title'    => esc_html__( 'Download Button', 'appica' ),
            'subtitle' => esc_html__( 'Enable/Disable Download Button', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'text',
            'id'       => 'navbar_download_button_text',
            'title'    => esc_html__( 'Download Button Text', 'appica' ),
            'required' => array( 'navbar_is_download', '=', 1 ),
            'default'  => esc_html__( 'Download', 'appica' )
        ),
        array(
            'type'     => 'text',
            'id'       => 'navbar_download_button_url',
            'title'    => esc_html__( 'Download Button URL', 'appica' ),
            'required' => array( 'navbar_is_download', '=', 1 )
        ),
        array(
            'id'       => 'navbar_is_subscribe',
            'type'     => 'switch',
            'title'    => esc_html__( 'Enable/Disable Subscription', 'appica' ),
            'subtitle' => esc_html__( 'MailChimp', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'You can customize your MailChimp URL in "Socials" section', 'appica' ) . '</p>',
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'          => 'slider',
            'id'            => 'navbar_width',
            'title'         => esc_html__( 'Navbar max-width to convert to mobile version', 'appica' ),
            'desc'          => '<p class="description">' . esc_html__( 'Since we do not know how much information will be in your navbar that\'s why you to decide when it converts to mobile version to prevent content reflow.', 'appica' ) . '</p>' . '<p class="description">' . esc_html__( 'Min: 1, Max: 1400, Step: 1', 'appica' ) . '</p>',
            'default'       => 991,
            'min'           => 0,
            'max'           => 1400,
            'step'          => 1,
            'display_value' => 'text',
            'float_mark'    => '.'
        )
    )
) );

/*
 * Footer section
 */
Redux::setSection( $appica_options, array(
    'title'  => esc_html__( 'Footer', 'appica' ),
    'desc'   => esc_html__( 'Customize your Footer', 'appica' ),
    'icon'   => 'el-icon-tasks',
    'fields' => array(
        array(
            'type'     => 'switch',
            'id'       => 'footer_is_nav',
            'title'    => esc_html__( 'Footer navigation', 'appica' ),
            'subtitle' => esc_html__( 'Enable/Disable Footer Menu Location', 'appica' ),
            'desc'     => esc_html__( 'See Appearance > Menus', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'editor',
            'id'       => 'footer_copyright',
            'title'    => esc_html__( 'Copyright', 'appica' ),
            'subtitle' => esc_html__( 'Enter your copyrights', 'appica' ),
            'default'  => '2015 &copy; 8Guild. Premium themes',
            'args'     => array(
                'wpautop'       => false,
                'media_buttons' => false,
                'teeny'         => true
            )
        ),
        array(
            'type'     => 'switch',
            'id'       => 'footer_is_device',
            'title'    => esc_html__( 'Device', 'appica' ),
            'subtitle' => esc_html__( 'Enable/Disable Device', 'appica' ),
            'on'       => $text_enable,
            'off'      => $text_disable,
            'default'  => true
        ),
        array(
            'type'     => 'media',
            'id'       => 'footer_device_screen',
            'title'    => esc_html__( 'Device Screen', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Optimal size is 682x383px', 'appica' ) . '</p>',
            'mode'     => 'image',
            'required' => array( 'footer_is_device', '=', 1 )
        ),
        array(
            'type'    => 'switch',
            'id'      => 'footer_is_app',
            'title'   => esc_html__( 'Enable/Disable App Widget', 'appica' ),
            'on'      => $text_enable,
            'off'     => $text_disable,
            'default' => true
        ),
        array(
            'type'     => 'media',
            'id'       => 'footer_logo',
            'title'    => esc_html__( 'App Logo', 'appica' ),
            'subtitle' => esc_html__( 'Choose your custom footer app logo', 'appica' ),
            'desc'     => '<p class="description">' . esc_html__( 'Logo optimal size is 240x240px', 'appica' ) . '</p>',
            'mode'     => 'image',
            'required' => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'          => 'footer_app_name',
            'type'        => 'text',
            'title'       => esc_html__( 'Your App Name', 'appica' ),
            'placeholder' => esc_html__( 'Your App Name', 'appica' ),
            'required'    => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'          => 'footer_app_url',
            'type'        => 'text',
            'title'       => esc_html__( 'Your App URL', 'appica' ),
            'placeholder' => 'http://...',
            'required'    => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'       => 'footer_is_app_tagline',
            'type'     => 'switch',
            'title'    => esc_html__( 'Tagline', 'appica' ),
            'subtitle' => esc_html__( 'Show / Hide your app custom tagline', 'appica' ),
            'on'       => $text_show,
            'off'      => $text_hide,
            'default'  => false,
            'required' => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'          => 'footer_app_tagline',
            'type'        => 'text',
            'title'       => esc_html__( 'Your App tagline', 'appica' ),
            'desc'        => esc_html__( 'E.g. "Flatter, lighter, appler..." or any custom string', 'appica' ),
            'placeholder' => esc_html__( 'Your App tagline', 'appica' ),
            'required'    => array(
                array( 'footer_is_app', '=', 1 ),
                array( 'footer_is_app_tagline', '=', 1 )
            )
        ),
        array(
            'id'       => 'footer_is_app_rating',
            'type'     => 'switch',
            'title'    => esc_html__( 'Display App Rating', 'appica' ),
            'subtitle' => esc_html__( 'Show / Hide your app rating', 'appica' ),
            'on'       => $text_show,
            'off'      => $text_hide,
            'default'  => false,
            'required' => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'            => 'footer_app_rating',
            'type'          => 'slider',
            'title'         => esc_html__( 'App Rating', 'appica' ),
            'desc'          => '<p class="description">' . esc_html__( 'Min: 0, Max: 5, Step: 0.1. Also, you can edit field.', 'appica' ) . '</p>',
            'min'           => 0,
            'max'           => 5,
            'step'          => 0.1,
            'default'       => 0,
            'resolution'    => 0.1,
            'display_value' => 'text',
            'required'      => array(
                array( 'footer_is_app', '=', 1 ),
                array( 'footer_is_app_rating', '=', 1 )
            )
        ),
        array(
            'id'       => 'footer_is_app_ratings_counter',
            'type'     => 'switch',
            'title'    => esc_html__( 'Display ratings counter', 'appica' ),
            'subtitle' => esc_html__( 'Show / Hide your app ratings counter', 'appica' ),
            'on'       => $text_show,
            'off'      => $text_hide,
            'default'  => false,
            'required' => array( 'footer_is_app', '=', 1 )
        ),
        array(
            'id'          => 'footer_app_ratings_counter',
            'type'        => 'text',
            'title'       => esc_html__( 'Your App ratings counter', 'appica' ),
            'desc'        => esc_html__( 'Any number of votes, in any format, e.g. number or "10K" for shorter form.', 'appica' ),
            'placeholder' => esc_html__( 'Your App ratings counter', 'appica' ),
            'required'    => array(
                array( 'footer_is_app', '=', 1 ),
                array( 'footer_is_app_ratings_counter', '=', 1 )
            )
        )
    )
) );

/**
 * Flush cached options and delete compiled CSS file
 *
 * @see zurapp_option_get
 */
function appica_flush_cached_options() {
	wp_cache_delete( 'appica_options' );
}

add_action( "redux/options/{$appica_options}/reset", 'appica_flush_cached_options', 1 );
add_action( "redux/options/{$appica_options}/section/reset", 'appica_flush_cached_options', 1 );
add_action( "redux/options/{$appica_options}/saved", 'appica_flush_cached_options', 1 );

/**
 * Add custom field "icon" to Redux fields
 *
 * @deprecated
 *
 * @return string
 */
function appica_redux_field_icon() {
	return get_template_directory() . '/redux/icon/class-reduxframework-icon.php';
}

add_filter( "redux/{$appica_options}/field/class/icon", 'appica_redux_field_icon' );

/**
 * Add custom field "socials" to Redux fields
 *
 * @return string
 */
function appica_redux_field_socials() {
	return get_template_directory() . '/redux/socials/class-reduxframework-socials.php';
}

add_filter( "redux/{$appica_options}/field/class/socials", 'appica_redux_field_socials' );

/**
 * Modify the "socials" field before saving to database
 *
 * @param array  $field   Field declaration array
 * @param mixed  $value   Field value
 * @param string $default Field defaults
 *
 * @return array Clean and ready socials list
 */
function appica_redux_validate_socials( $field, $value, $default ) {
	/**
	 * Redux framework requires this structure
	 *
	 * @see /ReduxCore/framework.php :: 3052
	 */
	$output = array();

	if ( empty( $value ) ) {
		$output['value'] = array();
	}

	// Return empty if networks or url not provided.
	if ( empty( $value['networks'] ) || empty( $value['urls'] ) ) {
		$output['value'] = array();
	} else {
		$result = array();
		// Network is network slug / options group from social-networks.ini
		array_map( function ( $network, $url ) use ( &$result ) {

			// Just skip iteration if network or url not set
			if ( '' === $network || '' === $url ) {
				return;
			}

			switch ( $network ) {
				case 'email':
					if ( false !== strpos( $url, 'mailto:' ) ) {
						$url = ltrim( $url, 'mailto:' );
					}
					$url = sanitize_email( $url );
					$url = "mailto:{$url}";

					$result[ $network ] = $url;
					break;

				default:
					$result[ $network ] = esc_url_raw( $url );
					break;
			}
		}, $value['networks'], $value['urls'] );

		$output['value'] = $result;
	}

	return $output;
}

/**
 * Remove Redux annoying sample config
 */
function appica_redux_remove_sample() {
	if ( class_exists( 'ReduxFrameworkPlugin', false ) ) {
		remove_filter( 'plugin_row_meta', array( ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks' ), null );
		remove_action( 'admin_notices', array( ReduxFrameworkPlugin::get_instance(), 'admin_notices' ) );
	}
}

add_action( 'init', 'appica_redux_remove_sample' );

/**
 * Return compiled css for <head>
 * Contains styles of Typography, Global Colors and other styles
 *
 * @since 1.0.0
 *
 * @return string
 */
function appica_get_head_css() {
	$css = array();

	/*
	 * Body typography
	 */
	$body_font_family = appica_option_get( 'typography_font_family' );
	$body_font_size   = appica_option_get( 'typography_body_font_size', 16 );

	$css[] = appica_get_css_rules( 'body', array(
		'font-family' => stripslashes( $body_font_family ),
		'font-size'   => "{$body_font_size}px"
	) );

	/*
	 * .text-smaller Font size
	 */
	$smaller_font_size = appica_option_get( 'typography_smaller_font_size', 14 );

	$css[] = appica_get_css_rules( '.text-smaller', array( 'font-size' => "{$smaller_font_size}px" ) );

	/*
	 * <h1> typography
	 */
	$h1_font_size   = appica_option_get( 'typography_h1_font_size', 48 );
	$h1_font_weight = appica_option_get( 'typography_h1_font_weight', 300 );
	$h1_text_transf = appica_option_get( 'typography_h1_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h1, .h1', array(
		'font-size'      => "{$h1_font_size}px",
		'font-weight'    => $h1_font_weight,
		'text-transform' => $h1_text_transf
	) );

	/*
	 * <h2> typography
	 */
	$h2_font_size   = appica_option_get( 'typography_h2_font_size', 36 );
	$h2_font_weight = appica_option_get( 'typography_h2_font_weight', 300 );
	$h2_text_transf = appica_option_get( 'typography_h2_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h2, .h2', array(
		'font-size'      => "{$h2_font_size}px",
		'font-weight'    => $h2_font_weight,
		'text-transform' => $h2_text_transf
	) );

	/*
	 * <h3> typography
	 */
	$h3_font_size   = appica_option_get( 'typography_h3_font_size', 24 );
	$h3_font_weight = appica_option_get( 'typography_h3_font_weight', 300 );
	$h3_text_transf = appica_option_get( 'typography_h3_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h3, .h3', array(
		'font-size'      => "{$h3_font_size}px",
		'font-weight'    => $h3_font_weight,
		'text-transform' => $h3_text_transf
	) );

	/*
	 * <h4> typography
	 */
	$h4_font_size   = appica_option_get( 'typography_h4_font_size', 18 );
	$h4_font_weight = appica_option_get( 'typography_h4_font_weight', 400 );
	$h4_text_transf = appica_option_get( 'typography_h4_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h4, .h4', array(
		'font-size'      => "{$h4_font_size}px",
		'font-weight'    => $h4_font_weight,
		'text-transform' => $h4_text_transf
	) );

	/*
	 * <h5> typography
	 */
	$h5_font_size   = appica_option_get( 'typography_h5_font_size', 16 );
	$h5_font_weight = appica_option_get( 'typography_h5_font_weight', 600 );
	$h5_text_transf = appica_option_get( 'typography_h5_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h5, .h5', array(
		'font-size'      => "{$h5_font_size}px",
		'font-weight'    => $h5_font_weight,
		'text-transform' => $h5_text_transf
	) );

	/*
	 * <h6> typography
	 */
	$h6_font_size   = appica_option_get( 'typography_h6_font_size', 14 );
	$h6_font_weight = appica_option_get( 'typography_h6_font_weight', 700 );
	$h6_text_transf = appica_option_get( 'typography_h6_text_transform', 'none' );

	$css[] = appica_get_css_rules( 'h6, .h6', array(
		'font-size'      => "{$h6_font_size}px",
		'font-weight'    => $h6_font_weight,
		'text-transform' => $h6_text_transf
	) );

	/*
	 * .badge typography
	 */
	$badge_font_size   = appica_option_get( 'typography_badge_font_size', 14 );
	$badge_font_weight = appica_option_get( 'typography_badge_font_weight', 400 );
	$badge_text_transf = appica_option_get( 'typography_badge_text_transform', 'none' );

	$css[] = appica_get_css_rules( '.badge', array(
		'font-size'      => "{$badge_font_size}px",
		'font-weight'    => $badge_font_weight,
		'text-transform' => $badge_text_transf
	) );

	/*
	 * Body font color
	 */
	$body_font_color = appica_option_get( 'color_body_font', '#757575' );

	$css[] = appica_get_css_rules( array(
		'body',
		'a.downloadable',
		'div.downloadable',
		'.downloadable:hover',
		'.offcanvas-posts .post',
		'.offcanvas-posts .post:hover',
		'.icon-block .text',
		'.appica-versions .icon-block',
		'.gallery-item figcaption p',
		'.pricing-plan.pricing-plan-float',
		'.post-title span',
		'.post-title p',
		'.footer-head',
		'.footer-head:hover',
		'.footer-head:focus',
		'.copyright a'
	), array( 'color' => $body_font_color ) );

	$css[] = appica_get_css_rules( array(
		'.nav-tabs.alt-tabs li a',
		'.nav-tabs.alt-tabs li.active a',
		'.modal-dialog .btn-default',
		'.modal-dialog .btn-default:hover',
		'.modal-dialog .btn-default:focus'
	), array( 'color' => "{$body_font_color} !important" ) );

	/*
	 * Primary color
	 */
	$primary_color = appica_option_get( 'color_primary', '#007aff' );

	$css[] = appica_get_css_rules( array(
		'a',
		'a:hover',
		'a:focus',
		'.form-control.active label',
		'.search-field.active button[type=submit]',
		'.search-field button[type=submit]:hover',
		'a.downloadable p, div.downloadable p',
		'.nav-tabs > li > a:hover',
		'.nav-tabs > li > a:focus',
		'.nav-tabs > li.active > a',
		'.nav-tabs > li.active > a:hover',
		'.nav-tabs > li.active > a:focus',
		'.nav-filters > li > a:hover',
		'.nav-filters > li > a:focus',
		'.nav-filters > li.active > a',
		'.nav-filters > li.active > a:hover',
		'.nav-filters > li.active > a:focus',
		'.download-item figure i',
		'.download-item footer i',
		'.download-item footer i',
		'.twitter-feed .tweet a:hover',
		'.twitter-feed .tweet-float:before',
		'.pricing-plan .icon',
		'.pricing-plan.pricing-plan-float .icon',
		'.post-meta .comment-count:hover',
		'.post-meta .comment-count:hover i',
		'.copyright a:hover',
		'.copyright a:focus',
		'.offcanvas-nav .search-box .search-btn'
	), array( 'color' => $primary_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-primary',
		'.btn-flat.btn-primary',
		'.btn-flat.btn-primary:hover'
	), array( 'color' => "{$primary_color} !important" ) );

	$css[] = appica_get_css_rules( array(
		'.form-control input:focus',
		'.form-control textarea:focus'
	), array(
		'border-bottom-color' => $primary_color,
		'-webkit-box-shadow'  => "0 1px 0 0 {$primary_color}",
		'box-shadow'          => "0 1px 0 0 {$primary_color}"
	) );

	$css[] = appica_get_css_rules( array(
		'.btn-primary',
		'.badge.badge-primary .icon',
		'.intro .nav-toggle span',
		'.navbar .container',
		'.offcanvas-nav .nav-head',
		'.nav-tabs > li > a:after',
		'.nav-filters > li > a:after',
		'.app-gallery .item a',
		'.pricing-plan.pricing-plan-primary',
		'.featured-post.bg-primary',
		'.timeline .date:before',
		'.bar-charts .chart.chart-primary .bar',
		'.mCS-dark.mCSB_scrollTools .mCSB_dragger .mCSB_dragger_bar'
	), array( 'background-color' => $primary_color ) );

	$css[] = appica_get_css_rules( array(
		'.grid-btn span:before',
		'.grid-btn span:after'
	), array( 'border-color' => $primary_color ) );

	/*
	 * Success color
	 */
	$success_color = appica_option_get( 'color_success', '#96cb4b' );

	$css[] = appica_get_css_rules( array(
		'.badge.badge-success .icon',
		'.iradio:before',
		'.btn-success',
		'.featured-post.bg-success',
		'.pricing-plan.pricing-plan-success',
		'.bar-charts .chart.chart-success .bar'
	), array( 'background-color' => $success_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-success',
		'.btn-flat.btn-success',
		'.btn-flat.btn-success:hover'
	), array( 'color' => "{$success_color} !important" ) );

	$css[] = appica_get_css_rules( array(
		'.icheckbox.checked',
		'.radio-alt .iradio.checked'
	), array(
		'border-right-color'  => $success_color,
		'border-bottom-color' => $success_color
	) );

	$css[] = appica_get_css_rules( '.iradio.checked', array( 'border-color' => $success_color ) );
	$css[] = appica_get_css_rules( array(
		'.feature-tabs .nav-tabs > li > a > i',
		'.icon-block .icon',
		'.gallery-item figcaption h3',
		'.download-item footer:before',
		'.download-item footer h3',
		'.news-block a h3',
		'.post-title a',
		'.post-title h2',
		'.post-title h3',
		'.footer-head:before'
	), array( 'color' => $success_color ) );

	$css[] = appica_get_css_rules( '.news-block', array( 'border-right-color' => $success_color ) );
	$css[] = appica_get_css_rules( '.pace .pace-progress', array( 'background' => $success_color ) );

	/*
	 * Info color
	 */
	$info_color = appica_option_get( 'color_info', '#4b62ff' );

	$css[] = appica_get_css_rules( array(
		'.badge.badge-info .icon',
		'.btn-info',
		'.pricing-plan.pricing-plan-info',
		'.featured-post.bg-info',
		'.bar-charts .chart.chart-info .bar'
	), array( 'background-color' => $info_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-info',
		'.btn-flat.btn-info',
		'.btn-flat.btn-info:hover'
	), array( 'color' => "{$info_color} !important" ) );

	/*
	 * Warning color
	 */
	$warning_color = appica_option_get( 'color_warning', '#ff6d00' );

	$css[] = appica_get_css_rules( array(
		'.badge.badge-warning .icon',
		'.btn-warning',
		'.pricing-plan.pricing-plan-warning',
		'.featured-post.bg-warning',
		'.bar-charts .chart.chart-warning .bar'
	), array( 'background-color' => $warning_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-warning',
		'.btn-flat.btn-warning',
		'.btn-flat.btn-warning:hover'
	), array( 'color' => "{$warning_color} !important" ) );

	/*
	 * Danger color
	 */
	$danger_color = appica_option_get( 'color_danger', '#e91e63' );

	$css[] = appica_get_css_rules( array(
		'.badge.badge-danger .icon',
		'.btn-danger',
		'.pricing-plan.pricing-plan-danger',
		'.featured-post.bg-danger',
		'.bar-charts .chart.chart-danger .bar'
	), array( 'background-color' => $danger_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-danger',
		'.btn-flat.btn-danger',
		'.btn-flat.btn-danger:hover'
	), array( 'color' => "{$danger_color} !important" ) );

	/*
	 * Light color
	 */
	$light_color = appica_option_get( 'color_text_light', '#c4c4c4' );

	$css[] = appica_get_css_rules( array(
		'.text-muted',
		'.twitter-feed .tweet .author'
	), array( 'color' => "{$light_color} !important" ) );

	$css[] = appica_get_css_rules( '.unordered-list li:before', array( 'background-color' => $light_color ) );
	$css[] = appica_get_css_rules( array(
		'.pagination .page-numbers.current',
		'.timeline .date',
		'.contact-info .nav-tabs li.active a',
		'.widget_recent_comments li',
		'.widget_archive li'
	), array( 'color' => $light_color ) );

	/*
	 * Dark color
	 */
	$dark_color = appica_option_get( 'color_text_dark', '#999' );

	$css[] = appica_get_css_rules( array(
		'dl dt span',
		'figure figcaption',
		'.form-control label',
		'.checkbox',
		'.radio',
		'.checkbox-inline',
		'.radio-inline',
		'.btn-google-play span',
		'div.downloadable p',
		'.twitter-feed .tweet p',
		'.team-member span',
		'.team-grid .item span',
		'.post-meta',
		'.comment .comment-meta span',
		'.footer .rating span',
		'.copyright p',
		'#preloader .logo span',
		'.icons-demo'
	), array( 'color' => $dark_color ) );

	$css[] = appica_get_css_rules( array(
		'.text-gray',
		'.twitter-feed .tweet a'
	), array( 'color' => "{$dark_color} !important" ) );

	/*
	 * Page Heading Color
	 */
	$css[] = appica_get_css_rules( array(
		'.page-heading h1',
		'.page-heading h2',
		'.page-heading h3',
		'.page-heading h4'
	), array( 'color' => $body_font_color ) );

	/*
	 * Badge Font Color
	 */
	$css[] = appica_get_css_rules( '.badge', array( 'color' => $body_font_color ) );

	/*
	 * Navbar mobile @media
	 */
	$css[] = appica_get_navbar_mobile_css();

	/*
	 * Waves css
	 */
	$css[] = appica_get_waves_css( array(
		'primary' => $primary_color,
		'success' => $success_color,
		'info'    => $info_color,
		'warning' => $warning_color,
		'danger'  => $danger_color
	) );

	return implode( PHP_EOL, array_filter( $css ) );
}

/**
 * Generate CSS rules for Navbar mobile
 *
 * @since 1.0.0
 *
 * @return string
 */
function appica_get_navbar_mobile_css() {
	$width = appica_option_get( 'navbar_width', 991 );
	if ( '' === $width || 0 === $width ) {
		return '';
	}

	$css = '.navbar {padding: 0;height: 80px;}';
	$css .= '.navbar .container {width: 100%;height: 80px;}';
	$css .= '.navbar.navbar-fixed-top + * {padding-top: 80px;}';
	$css .= '.offset-top .navbar + *, .offset-top .sticky-wrapper + *, .navbar + .page-heading, .sticky-wrapper + .page-heading {padding-top: 120px;}';
	$css .= '.navbar .logo {line-height: 76px;}';
	$css .= '.navbar .social-buttons {display: none;}';
	$css .= '.navbar .toolbar {padding-top: 7px;}';
	$css .= '.navbar .toolbar .action-btn {margin-right: 0;}';
	$css .= '.navbar  + .page > .vc_row {padding-top: 105px !important;}';
	$css .= '.navbar.navbar-fixed-top + .page > .vc_row, .navbar.navbar-sticky.stuck  + .page > .vc_row {padding-top: 50px !important;}';

	$media = appica_get_css_rules( "@media screen and (max-width: {$width}px)", $css );

	return $media;
}

/**
 * Get waves CSS with new colors
 *
 * @since 1.0.0
 *
 * @param array $colors Colors values, like "primary", "success", etc
 *
 * @return string Waves color CSS
 */
function appica_get_waves_css( $colors ) {
	$css = array();

	$defaults = array(
		'primary' => '#008fed',
		'success' => '#96cb4b',
		'info'    => '#4b62ff',
		'warning' => '#ff6d00',
		'danger'  => '#e91e63'
	);
	$colors = wp_parse_args( $colors, $defaults );

	/*
	 * Primary
	 */
	$primary = $colors['primary'];
	$css[] = appica_get_css_rules( '.waves-effect.waves-primary .waves-ripple', array(
		'background' => array(
			appica_hex2rgba( $primary, '0.5' ),
			sprintf(
				'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $primary ), appica_hex2rgba( $primary, '0.4' ), appica_hex2rgba( $primary, '0.5' ),
				appica_hex2rgba( $primary, '0.6' ), appica_hex2rgba( $primary, '0' )
			),
			sprintf(
				'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $primary ), appica_hex2rgba( $primary, '0.4' ), appica_hex2rgba( $primary, '0.5' ),
				appica_hex2rgba( $primary, '0.6' ), appica_hex2rgba( $primary, '0' )
			),
			sprintf(
				'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $primary ), appica_hex2rgba( $primary, '0.4' ), appica_hex2rgba( $primary, '0.5' ),
				appica_hex2rgba( $primary, '0.6' ), appica_hex2rgba( $primary, '0' )
			),
			sprintf(
				'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $primary ), appica_hex2rgba( $primary, '0.4' ), appica_hex2rgba( $primary, '0.5' ),
				appica_hex2rgba( $primary, '0.6' ), appica_hex2rgba( $primary, '0' )
			)
		)
	) );

	$css[] = appica_get_css_rules( array(
		'.nav-tabs > li > a.waves-effect.waves-primary .waves-ripple',
		'.nav-filters > li > a.waves-effect.waves-primary .waves-ripple'
	), array( 'background' => array(
		appica_hex2rgba( $primary, '0.4' ),
		sprintf(
			'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
			appica_hex2rgba( $primary, '0.2' ), appica_hex2rgba( $primary, '0.3' ), appica_hex2rgba( $primary, '0.4' ),
			appica_hex2rgba( $primary, '0.5' ), appica_hex2rgba( $primary, '0' )
		),
		sprintf(
			'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
			appica_hex2rgba( $primary, '0.2' ), appica_hex2rgba( $primary, '0.3' ), appica_hex2rgba( $primary, '0.4' ),
			appica_hex2rgba( $primary, '0.5' ), appica_hex2rgba( $primary, '0' )
		),
		sprintf(
			'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
			appica_hex2rgba( $primary, '0.2' ), appica_hex2rgba( $primary, '0.3' ), appica_hex2rgba( $primary, '0.4' ),
			appica_hex2rgba( $primary, '0.5' ), appica_hex2rgba( $primary, '0' )
		),
		sprintf(
			'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
			appica_hex2rgba( $primary, '0.2' ), appica_hex2rgba( $primary, '0.3' ), appica_hex2rgba( $primary, '0.4' ),
			appica_hex2rgba( $primary, '0.5' ), appica_hex2rgba( $primary, '0' )
		)
	) ) );

	/*
	 * Success
	 */
	$success = $colors['success'];
	$css[] = appica_get_css_rules( '.waves-effect.waves-success .waves-ripple', array(
		'background' => array(
			appica_hex2rgba( $success, '0.6' ),
			sprintf(
				'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $success, '0.4' ), appica_hex2rgba( $success, '0.5' ),
				appica_hex2rgba( $success, '0.6' ),
				appica_hex2rgba( $success, '0.7' ), appica_hex2rgba( $success, '0' )
			),
			sprintf(
				'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $success, '0.4' ), appica_hex2rgba( $success, '0.5' ),
				appica_hex2rgba( $success, '0.6' ),
				appica_hex2rgba( $success, '0.7' ), appica_hex2rgba( $success, '0' )
			),
			sprintf(
				'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $success, '0.4' ), appica_hex2rgba( $success, '0.5' ),
				appica_hex2rgba( $success, '0.6' ),
				appica_hex2rgba( $success, '0.7' ), appica_hex2rgba( $success, '0' )
			),
			sprintf(
				'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $success, '0.4' ), appica_hex2rgba( $success, '0.5' ),
				appica_hex2rgba( $success, '0.6' ),
				appica_hex2rgba( $success, '0.7' ), appica_hex2rgba( $success, '0' )
			)
		)
	) );

	/*
	 * Info
	 */
	$info = $colors['info'];
	$css[] = appica_get_css_rules( '.waves-effect.waves-info .waves-ripple', array(
		'background' => array(
			appica_hex2rgba( $info, '0.6' ),
			sprintf(
				'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $info, '0.4' ), appica_hex2rgba( $info, '0.5' ), appica_hex2rgba( $info, '0.6' ),
				appica_hex2rgba( $info, '0.7' ), appica_hex2rgba( $info, '0' )
			),
			sprintf(
				'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $info, '0.4' ), appica_hex2rgba( $info, '0.5' ), appica_hex2rgba( $info, '0.6' ),
				appica_hex2rgba( $info, '0.7' ), appica_hex2rgba( $info, '0' )
			),
			sprintf(
				'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $info, '0.4' ), appica_hex2rgba( $info, '0.5' ), appica_hex2rgba( $info, '0.6' ),
				appica_hex2rgba( $info, '0.7' ), appica_hex2rgba( $info, '0' )
			),
			sprintf(
				'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $info, '0.4' ), appica_hex2rgba( $info, '0.5' ), appica_hex2rgba( $info, '0.6' ),
				appica_hex2rgba( $info, '0.7' ), appica_hex2rgba( $info, '0' )
			)
		)
	) );

	/*
	 * Warning
	 */
	$warning = $colors['warning'];
	$css[] = appica_get_css_rules( '.waves-effect.waves-warning .waves-ripple', array(
		'background' => array(
			appica_hex2rgba( $warning, '0.5' ),
			sprintf(
				'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $warning, '0.3' ), appica_hex2rgba( $warning, '0.4' ), appica_hex2rgba( $warning, '0.5' ),
				appica_hex2rgba( $warning, '0.6' ), appica_hex2rgba( $warning, '0' )
			),
			sprintf(
				'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $warning, '0.3' ), appica_hex2rgba( $warning, '0.4' ), appica_hex2rgba( $warning, '0.5' ),
				appica_hex2rgba( $warning, '0.6' ), appica_hex2rgba( $warning, '0' )
			),
			sprintf(
				'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $warning, '0.3' ), appica_hex2rgba( $warning, '0.4' ), appica_hex2rgba( $warning, '0.5' ),
				appica_hex2rgba( $warning, '0.6' ), appica_hex2rgba( $warning, '0' )
			),
			sprintf(
				'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $warning, '0.3' ), appica_hex2rgba( $warning, '0.4' ), appica_hex2rgba( $warning, '0.5' ),
				appica_hex2rgba( $warning, '0.6' ), appica_hex2rgba( $warning, '0' )
			)
		)
	) );

	/*
	 * Danger
	 */
	$danger = $colors['warning'];
	$css[] = appica_get_css_rules( '.waves-effect.waves-danger .waves-ripple', array(
		'background' => array(
			appica_hex2rgba( $danger, '0.5' ),
			sprintf(
				'-webkit-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $danger, '0.3' ), appica_hex2rgba( $danger, '0.4' ), appica_hex2rgba( $danger, '0.5' ),
				appica_hex2rgba( $danger, '0.6' ), appica_hex2rgba( $danger, '0' )
			),
			sprintf(
				'-o-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $danger, '0.3' ), appica_hex2rgba( $danger, '0.4' ), appica_hex2rgba( $danger, '0.5' ),
				appica_hex2rgba( $danger, '0.6' ), appica_hex2rgba( $danger, '0' )
			),
			sprintf(
				'-moz-radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $danger, '0.3' ), appica_hex2rgba( $danger, '0.4' ), appica_hex2rgba( $danger, '0.5' ),
				appica_hex2rgba( $danger, '0.6' ), appica_hex2rgba( $danger, '0' )
			),
			sprintf(
				'radial-gradient(%1$s 0, %2$s 40%%, %3$s 50%%, %4$s 60%%, %5$s 70%%);',
				appica_hex2rgba( $danger, '0.3' ), appica_hex2rgba( $danger, '0.4' ), appica_hex2rgba( $danger, '0.5' ),
				appica_hex2rgba( $danger, '0.6' ), appica_hex2rgba( $danger, '0' )
			)
		)
	) );

	return implode( PHP_EOL, $css );
}


/**
 * Fix missing redux "action" during the ajax response
 *
 * @param array $response
 *
 * @return array
 */
function appica_redux_ajax_response_fields( $response ) {
	if ( ! array_key_exists( 'action', $response ) ) {
		$response['action'] = '';
	}

	return $response;
}

add_filter( 'redux/options/appica_options/ajax_save/response', 'appica_redux_ajax_response_fields' );