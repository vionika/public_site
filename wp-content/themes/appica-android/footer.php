<?php
/**
 * The template for displaying the footer.
 *
 * @package Appica
 */

/**
 * Fires right before the <footer>
 */
do_action( 'appica_footer_before' );

?>

<footer class="<?php appica_footer_class(); ?>">
	<div class="container">

		<?php appica_footer_app(); ?>

		<div class="body padding-top-2x">
			<?php $appica_is_device = appica_is_footer_device(); ?>

			<div class="column copyright <?php if ( false === $appica_is_device ) : echo 'col-50'; endif; ?>">
				<?php appica_the_copyright(); ?>
			</div>

			<?php if ( $appica_is_device && appica_is_footer_device_screen() ) : ?>
			<div class="column hidden-sm hidden-xs">
				<div class="gadget">
					<?php appica_footer_device_screen(); ?>
				</div>
			</div>
			<?php endif; ?>

			<div class="column footer-nav <?php if ( false === $appica_is_device ) : echo 'col-50'; endif; ?>">
			<?php if ( appica_is_footer_nav() ) :
				wp_nav_menu( array(
					'theme_location' => 'footer',
					'container'      => false,
					'fallback_cb'    => false,
					'depth'          => -1,
					'items_wrap'     => '<ul>%3$s</ul>'
				) );
			endif; ?>
			</div>

			<?php unset( $appica_is_device ); ?>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
</body>
</html>
