<?php
/**
 * Template part for displaying single post with right sidebar
 *
 * @package Appica
 */
?>
<div class="col-lg-9 col-md-8 padding-bottom">
	<div class="single-post box-float">
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'inner' ); ?>>
			<?php
			the_title( '<h1>', '</h1>' );
			the_content();
			wp_link_pages( array(
				'before'           => '<div class="paginate-links">',
				'after'            => '</div>',
				'link_before'      => '<span>',
				'link_after'       => '</span>',
				'nextpagelink'     => esc_html__( 'Next', 'appica' ),
				'previouspagelink' => esc_html__( 'Previous', 'appica' ),
			) );
			?>
		</article>

		<?php appica_entry_footer(); ?>

	</div>

	<?php
	// If comments are open or we have at least one comment, load up the comment template
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>
</div>
<div class="col-lg-3 col-md-4">
	<?php get_sidebar(); ?>
</div>
