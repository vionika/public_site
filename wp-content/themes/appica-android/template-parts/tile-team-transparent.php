<?php
/**
 * A template part for displaying Team CPT in Grid
 *
 * @author 8guild
 */

$appica_teammate = appica_meta_box_get( get_the_ID(), '_appica_team_mate' );
if ( empty( $appica_teammate ) ) {
	// legacy
	$post_id = get_the_ID();

	$position = get_post_meta( $post_id, '_appica_team_subtitle', true );
	$socials  = get_post_meta( $post_id, '_appica_team_social', true );
} else {
	$appica_teammate = wp_parse_args( $appica_teammate, array(
		'position' => '',
		'socials'  => array(),
	) );

	$position = $appica_teammate['position'];
	$socials  = $appica_teammate['socials'];
}

?>
<div class="col-sm-4 team-member">

	<?php
	the_title( '<h3>', '</h3>' );
	appica_the_text( esc_html( $position ), '<span>', '</span>' );
	appica_the_socials( $socials );
	if ( has_post_thumbnail() ) {
		the_post_thumbnail( 'medium' );
	}
	?>

</div>
