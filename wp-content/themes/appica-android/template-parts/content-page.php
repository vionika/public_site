<?php
/**
 * For displaying page contents
 *
 * @package Appica
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php the_content(); ?>
</article>
