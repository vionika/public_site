<?php
/**
 * Single post template on Blog Home (Posts index) page
 *
 * @package Appica
 */

?>
<div id="post-<?php echo get_the_ID(); ?>" <?php post_class( 'item' ); ?>>
	<div class="post-tile">
		<?php if ( appica_has_featured_video() ) : ?>
			<div class="embed-responsive embed-responsive-16by9">
				<?php appica_the_featured_video(); ?>
			</div>
		<?php elseif ( has_post_thumbnail() ) : ?>
			<a href="<?php the_permalink(); ?>" class="post-thumb waves-effect">
				<?php
				$is_wide = absint( appica_meta_box_get( get_the_ID(), '_appica_post_settings', 'is_wide', 0 ) );
				the_post_thumbnail( (bool) $is_wide ? 'appica-home-thumbnail-double' : 'appica-home-thumbnail' );
				unset( $is_wide );
				?>
			</a>
		<?php endif; ?>
		<div class="post-body">
			<div class="post-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php the_title( '<h3>', '</h3>' ); ?>
				</a>
				<?php the_excerpt(); ?>
			</div>

			<?php appica_entry_footer_wo_social(); ?>

		</div>
	</div>
</div>
