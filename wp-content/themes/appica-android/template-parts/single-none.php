<?php
/**
 * Template part for displaying single post without sidebar
 *
 * @package Appica
 */
?>
<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 padding-bottom">
	<div class="single-post box-float">
		<article id="post-<?php the_ID(); ?>" <?php post_class( 'inner' ); ?>>
			<?php
			the_title( '<h1>', '</h1>' );
			the_content();
			wp_link_pages( array(
				'before'           => '<div class="paginate-links">',
				'after'            => '</div>',
				'link_before'      => '<span>',
				'link_after'       => '</span>',
				'nextpagelink'     => esc_html__( 'Next', 'appica' ),
				'previouspagelink' => esc_html__( 'Previous', 'appica' ),
			) );
			?>
		</article>

		<?php appica_entry_footer(); ?>

	</div>

	<?php
	// If comments are open or we have at least one comment, load up the comment template
	if ( comments_open() || get_comments_number() ) :
		comments_template();
	endif;
	?>
</div>
