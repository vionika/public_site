<?php
/**
 * Gallery single tile template
 *
 * @author 8guild
 */

if ( ! has_post_thumbnail() ) {
	return;
}

$appica_post_id = get_the_ID();

$appica_tile_classes   = array();
$appica_tile_classes[] = 'item';
$appica_tile_classes[] = implode( ' ', appica_get_post_terms( $appica_post_id, 'appica_gallery_category' ) );
$appica_tile_classes   = appica_get_class_set( $appica_tile_classes );

$appica_featured_video = appica_meta_box_get( $appica_post_id, '_appica_gallery_video' );
if ( empty( $appica_featured_video ) ) {
	$appica_tile_url   = appica_get_image_src( get_post_thumbnail_id(), 'full' );
	$appica_tile_class = appica_get_class_set( array( 'gallery-item', 'waves-effect', 'image-item', 'popup-image' ) );
} else {
	$appica_tile_url   = $appica_featured_video;
	$appica_tile_class = appica_get_class_set( array( 'gallery-item', 'waves-effect', 'video-item', 'popup-video' ) );
}
?>
<div class="<?php echo esc_attr( $appica_tile_classes ); ?>">
	<a href="<?php echo esc_url( $appica_tile_url ); ?>"
	   class="<?php echo esc_attr( $appica_tile_class ); ?>">
		<figure>
			<?php the_post_thumbnail( 'large' ); ?>
			<figcaption>
				<?php
				appica_the_text( esc_html( get_the_title() ), '<h3>', '</h3>' );
				appica_the_text( esc_html( get_the_excerpt() ), '<p>', '</p>' );
				?>
			</figcaption>
		</figure>
	</a>
</div>
