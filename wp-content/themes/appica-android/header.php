<?php
/**
 * The header for our theme
 *
 * @package Appica 2
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php appica_the_favicon(); ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php appica_preloader(); ?>

	<div class="fake-scrollbar"></div>

	<?php get_sidebar( 'off-canvas' ); ?>

	<?php appica_subscribe_modal_form(); ?>

	<?php
	/**
	 * Fires right before the <header>
	 */
	do_action( 'appica_header_before' );
	?>

	<header class="<?php appica_navbar_class(); ?>">
		<div class="container">

			<div class="nav-toggle waves-effect waves-light waves-circle" data-offcanvas="open"><i class="flaticon-menu55"></i></div>

			<?php appica_navbar_logo(); ?>

			<div class="toolbar">
				<?php appica_navbar_download_button(); ?>
				<?php appica_navbar_subscribe(); ?>
				<?php appica_navbar_socials(); ?>
			</div>

		</div>
	</header>
