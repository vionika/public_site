<?php
/**
 * Blog Posts Index template
 *
 * @package Appica
 */

get_header();
appica_page_title(); ?>

<section class="space-top padding-bottom">
	<div class="container">

		<?php if ( have_posts() ) : ?>

			<div class="masonry-grid">
				<div class="grid-sizer"></div>
				<div class="gutter-sizer"></div>

				<?php
				while ( have_posts() ) :
					the_post();
					get_template_part( 'template-parts/content', 'home' );
				endwhile;
				?>

			</div>

			<?php appica_posts_navigation(); ?>

		<?php else : ?>

			<?php get_template_part( 'template-parts/content', 'none' ); ?>

		<?php endif; ?>


	</div>
</section>

<?php
get_footer();
