<?php
/**
 * Redux custom field, called "social_networks".
 *
 * This field allow user to setup his social networks list.
 *
 * @author  8guild
 * @package Redux
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // exit if accessed directly
}

// Do not duplicate
if ( ! class_exists( 'ReduxFramework_socials', false ) ) :

	/**
	 * Custom field "socials" for Redux Framework
	 *
	 * @since 1.0.0
	 */
	class ReduxFramework_socials {
		/**
		 * Instance of Redux Framework Class
		 *
		 * @var ReduxFramework
		 */
		public $parent;
		/**
		 * Field attributes
		 *
		 * @var array
		 */
		public $field;
		/**
		 * Field value
		 *
		 * @var array|string
		 */
		public $value;

		/**
		 * Field Constructor.
		 *
		 * @since       1.0.0
		 * @access      public
		 *
		 * @param array  $field
		 * @param string $value
		 * @param array  $parent
		 */
		public function __construct( $field = array(), $value = '', $parent ) {
			$this->parent = $parent;
			$this->field  = $field;
			$this->value  = $value;
		}

		/**
		 * Field Render Function.
		 *
		 * Takes the vars and outputs the HTML for the field in the settings
		 */
		public function render() {
			$networks = appica_get_networks( get_template_directory() . '/misc/socials.ini' );
			$socials  = $this->value;
			if ( ! is_array( $socials ) || empty( $socials ) ) {
				$socials = array();
			}

			/*
			 * Options should be in format [network => url].
			 *
			 * When you reset options in redux the validate callback doesn't fire,
			 * so we have options in wrong format (multidimensional array)! Check
			 * this and maybe convert to required format.
			 */
			if ( is_array( $socials )
			     && count( $socials ) !== count( $socials, COUNT_RECURSIVE )
			) {
				$socials = $this->process( $socials );
			}

			$escaped = array();
			foreach ( $socials as $network => $url ) {
				$escaped[ $network ] = esc_url( $url );
			}

			$name = $this->field['name'] . $this->field['name_suffix'];
			$this->control( $name, $escaped, $networks );
		}

		private function process( $socials ) {
			if ( empty( $socials ) ) {
				return array();
			}

			// Return empty if networks or url not provided.
			if ( empty( $socials['networks'] ) || empty( $socials['urls'] ) ) {
				return array();
			}

			$result = array();
			// Network is network slug / options group from social-networks.ini
			array_map( function ( $network, $url ) use ( &$result ) {

				// Just skip iteration if network or url not set
				if ( '' === $network || '' === $url ) {
					return;
				}

				switch ( $network ) {
					case 'email':
						if ( false !== strpos( $url, 'mailto:' ) ) {
							$url = ltrim( $url, 'mailto:' );
						}
						$url = sanitize_email( $url );
						$url = "mailto:{$url}";

						$result[ $network ] = $url;
						break;

					default:
						$result[ $network ] = esc_url_raw( $url );
						break;
				}
			}, $socials['networks'], $socials['urls'] );

			return $result;
		}

		/**
		 * Render socials control. With networks or empty.
		 * Allow users to choose social networks from the list an fill in the links.
		 *
		 * @param string $name     Name of field, in context {$name}[networks][]
		 * @param array  $socials  List of networks from socials.ini
		 * @param array  $networks List of networks selected by user
		 */
		private function control( $name, $socials = array(), $networks = array() ) {
			if ( empty( $networks ) ) {
				return;
			}

			?>
			<style type="text/css">
				.redux-socials-group-remove {
					position: absolute;
					font-size: 18px;
					color: #fff;
					text-decoration: none;
					text-align: center;
					width: 20px;
					height: 20px;
					line-height: 20px;
					background-color: #f05252;
					top: 0;
					right: 0;
				}
				.redux-socials-group select {
					vertical-align: top;
				}
				.redux-socials-group-remove {
					position: relative;
					display: inline-block;
				}
				.redux-socials-group-remove:hover,
				.redux-socials-group-remove:focus,
				.redux-socials-group-remove:active { color: #fff; }
				.redux-socials-add {
					display: inline-block;
					margin-bottom: 15px;
				}
			</style>
			<div class="redux-socials-wrap">
				<?php

				if ( is_array( $socials ) && count( $socials ) > 0 ) :
					$this->not_empty_control( $name, $networks, $socials );
				else:
					$this->empty_control( $name, $networks );
				endif;

				?>
			</div>
			<br>
			<a href="#" class="redux-socials-add">
				<?php esc_html_e( 'Add one more social network', 'zurapp' ); ?>
			</a>
			<script type="text/javascript">
				(function ( $ ) {
					'use strict';

					$( document ).on( 'click', '.redux-socials-add', function ( e ) {
						e.preventDefault();

						var $this = $( this );
						// Detect the wrapper, based on clicked button
						// May be some buttons per one page
						var $wrapper = $this.siblings( '.redux-socials-wrap' );
						// Clone first element
						var $item = $wrapper.find( '.redux-socials-group' ).first().clone();

						// Append this item to container and clear the <input> field
						$item.appendTo( $wrapper ).children( 'input:text' ).val( '' );
					} );

					$( document ).on( 'click', '.redux-socials-group-remove', function ( e ) {
						e.preventDefault();

						var $this = $( this );
						// Find wrapper and groups to check if it possible to remove element
						var $wrapper = $this.parents( '.redux-socials-wrap' );
						var $groups = $wrapper.find( '.redux-socials-group' );
						// Find group which user want to remove..
						var $group = $this.parent( '.redux-socials-group' );
						if ( $groups.length > 1 ) {
							// ..and remove it
							$group.remove();
						} else {
							// Do not remove last element, just reset values
							var selectField = $group.find( 'select' );
							var firstOptionsValue = $group.find( 'select option:first' ).val();

							selectField.val( firstOptionsValue );
							$group.find( 'input' ).val( '' );
						}
					} );
				})( jQuery );
			</script>
			<?php
		}

		/**
		 * Render empty list of social networks (controls for single network)
		 *
		 * @access private
		 *
		 * @param string $name     Meta box name
		 * @param array  $networks List of networks from social-networks.ini
		 */
		private function empty_control( $name, $networks ) {
			$select_name = "{$name}[networks][]";
			$input_name  = "{$name}[urls][]";

			?>
			<div class="redux-socials-group">
			<select name="<?php echo esc_attr( $select_name ); ?>">
				<?php foreach ( $networks as $network => $data ) :
					printf( '<option value="%1$s">%2$s</option>',
						esc_attr( $network ),
						esc_html( $data['name'] )
					);
				endforeach;
				unset( $network, $data ); ?>
			</select>
			<input type="text"
			       name="<?php echo esc_attr( $input_name ); ?>"
			       placeholder="<?php esc_html_e( 'URL', 'zurapp' ); ?>">

			<a href="#" class="redux-socials-group-remove">&times;</a>
			</div><?php
		}

		/**
		 * Render filled list of social networks with controls
		 *
		 * @access private
		 *
		 * @param string $name     Meta box name
		 * @param array  $networks List of networks from social-networks.ini
		 * @param array  $socials  List of networks selected by user
		 */
		private function not_empty_control( $name, $networks, $socials = array() ) {
			$select_name = "{$name}[networks][]";
			$input_name  = "{$name}[urls][]";

			foreach ( $socials as $social => $url ) : ?>
				<div class="redux-socials-group">
				<select name="<?php echo esc_attr( $select_name ); ?>">
					<?php
					foreach ( $networks as $network => $data ) :
						$selected = ( $network === $social ) ? 'selected' : '';
						// 1 - network slug, 2 - network name, 3 - selected
						printf( '<option value="%1$s" %3$s>%2$s</option>',
							esc_attr( $network ),
							esc_html( $data['name'] ),
							$selected
						);
					endforeach;
					unset( $network, $data );
					?>
				</select>
				<input type="text"
				       name="<?php echo esc_attr( $input_name ); ?>"
				       value="<?php echo esc_url( $url ); ?>"
				       placeholder="<?php esc_html_e( 'URL', 'zurapp' ); ?>">

				<a href="#" class="redux-socials-group-remove">&times;</a>
				</div><?php
			endforeach;
			unset( $social, $url );
		}
	}
endif;