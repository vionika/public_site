<?php
/**
 * Shortcode "Download Button" output
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Android
 */

if ( ! function_exists( 'vc_build_link' ) ) {
	return;
}

$a = shortcode_atts( array(
	'text'        => esc_html__( 'Get it on', 'appica' ),
	'link'        => '',
	'extra_class' => ''
), $atts );

$text   = appica_get_text( esc_html( $a['text'] ), '<span>','</span>' );
$link   = vc_build_link( $a['link'] );
$href   = esc_url( $link['url'] );
$target = $link['target'];

$classes = appica_get_class_set( array(
	'btn',
	'btn-default',
	'btn-float',
	'waves-effect',
	'btn-google-play',
	$a['extra_class']
) );

// 1 - href, 2 - text, 3 - classes, 4 - target
printf( '<a href="%1$s" class="%3$s" target="%4$s"><img src="%5$s" alt="Google Play">%2$s</a>',
	$href,
	$text,
	esc_attr( $classes ),
	$target,
	appica_image_uri( 'img/google-play.png', false )
);