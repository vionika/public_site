<?php
/**
 * Shortcode "MailChimp Form" output
 *
 * @since      1.2.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Android
 */

$a = shortcode_atts( array(
	'title'       => '',
	'action'      => '',
	'label_name'  => '',
	'label_email' => '',
	'label_btn'   => '',
	'orientation' => ''
), $atts );

$action      = ( '' === $a['action'] ) ? '' : esc_url_raw( htmlspecialchars_decode( $a['action'] ) );
$orientation = esc_attr( $a['orientation'] );
$label_name  = ( '' === $a['label_name'] ) ? esc_html__( 'Name', 'appica' ) : esc_html( $a['label_name'] );
$label_email = ( '' === $a['label_email'] ) ? esc_html__( 'Email', 'appica' ) : esc_html( $a['label_email'] );
$label_btn   = ( '' === $a['label_btn'] ) ? esc_html__( 'Subscribe', 'appica' ) : esc_html( $a['label_btn'] );

// Build MC AntiSPAM
if ( '' === $action ) {
	$appica_options = (array) get_option( 'appica_options', array() );

	if ( count( $appica_options ) > 0
	     && array_key_exists( 'socials_mailchimp', $appica_options )
	     && '' !== $appica_options['socials_mailchimp']
	) {
		$action = $appica_options['socials_mailchimp'];
	}
}

// If action not set, just exit
if ( '' === $action ) {
	return;
}

$request_uri = parse_url( htmlspecialchars_decode( $action ), PHP_URL_QUERY );
parse_str( $request_uri , $c );
$mc_antispam = sprintf( 'b_%1$s_%2$s', $c['u'], $c['id'] );

unset( $request_uri, $c );

?><form method="post" action="<?php echo esc_attr( $action ); ?>" autocomplete="off" target="_blank">
	<?php
	appica_the_text( esc_html( $a['title'] ), '<h3 class="space-bottom-2x">', '</h3>' );
	?>
	<input type="hidden" name="<?php echo esc_attr( $mc_antispam ); ?>" tabindex="-1" value="">

	<?php if ( 'horizontal' === $orientation ) : ?>
		<div class="col-sm-6">
			<div class="form-control">
				<input type="text" name="NAME" id="si-name" required>
				<label for="si-name"><?php echo esc_html( $label_name ); ?></label>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="form-control">
				<input type="email" name="EMAIL" id="si_email" required>
				<label for="si_email"><?php echo esc_html($label_email); ?></label>
			</div>
		</div>
		<div class="col-sm-12 clearfix">
			<div class="pull-right">
				<button type="submit" class="btn btn-flat btn-primary waves-effect waves-primary"><?php echo esc_html($label_btn); ?></button>
			</div>
		</div>
	<?php else: ?>
		<div class="form-control">
			<input type="text" name="NAME" id="si-name" required>
			<label for="si-name"><?php echo esc_html($label_name); ?></label>
		</div>
		<div class="form-control">
			<input type="email" name="EMAIL" id="si_email" required>
			<label for="si_email"><?php echo esc_html($label_email); ?></label>
		</div>
		<div class="clearfix">
			<div class="pull-right">
				<button type="submit" class="btn btn-flat btn-primary waves-effect waves-primary"><?php echo esc_html($label_btn); ?></button>
			</div>
		</div>
	<?php endif; ?>
</form>