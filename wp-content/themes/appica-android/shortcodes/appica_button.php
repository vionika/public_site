<?php
/**
 * Shortcode "Button" output
 *
 * Mapped params are in {@path appica-core/inc/vc-map.php} {@see $appica_button}
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Android
 */

$a = shortcode_atts( array(
	'text'             => '',
	'link'             => '',
	'type'             => 'default', // default | round | flat
	'waves'            => 'dark', // dark | light
	'style'            => 'standard', // standard | outlined
	'size'             => 'nl', // small (sm) | normal (nl) | large (lg)
	'color'            => 'default', // default | primary | success etc
	'icon_lib'         => 'fontawesome', // fontawesome | openiconic | typicons | entypo | linecons | flaticons
	'icon_fontawesome' => '',
	'icon_openiconic'  => '',
	'icon_typicons'    => '',
	'icon_entypo'      => '',
	'icon_linecons'    => '',
	'icon_flaticons'   => '',
	'icon_pos'         => 'left', // left | right
	'is_full'          => 'no', // yes | no
	'extra_class'      => ''
), $atts );

// Build link
$link   = vc_build_link( $a['link'] );
$href   = ( '' === $link['url'] ) ? '#' : esc_url( $link['url'] );
$target = ( '' === $link['target'] ) ? '' : sprintf( 'target="%s"', trim( $link['target'] ) );
$title  = ( '' === $link['title'] ) ? '' : sprintf( 'title="%s"', esc_attr( $link['title'] ) );
$size   = esc_attr( $a['size'] );
$color  = esc_attr( $a['color'] );
/**
 * @var bool Check, if button is flat
 */
$is_flat = ( 'flat' === $a['type'] );
/**
 * @var bool Check, if is button type is default
 */
$is_default = ( 'default' === $a['type'] );
/**
 * @var bool Check, if button size is Normal
 */
$is_normal = ( 'nl' === $a['size'] );
/**
 * @var bool Check, if button type is round
 */
$is_round = ( 'round' === $a['type'] );

// Text on the button
$content = ( 'round' === $a['type'] ) ? '' : esc_attr( $a['text'] );

// Icon
$icon     = '';
$icon_pos = ( 'right' === $a['icon_pos'] ) ? 'icon-right' : 'icon-left';
$library  = $a['icon_lib'];
if ( '' !== $a["icon_{$library}"] ) {
	vc_icon_element_fonts_enqueue( $library );
	$icon = sprintf( '<i class="%s"></i>', $a["icon_{$library}"] );
}

// Global classes
$class = appica_get_class_set( array(
	'btn',
	( $is_flat ) ? 'btn-flat' : 'btn-float',
	( 'default' === $color ) ? 'btn-default' : "btn-{$color}",
	( $is_normal ) ? '' : "btn-{$size}",
	'waves-effect',
	( 'light' === $a['waves'] ) ? 'waves-light' : '',
	( $is_flat && 'default' !== $color ) ? "waves-{$color}" : '',
	( $is_default && 'yes' === $a['is_full'] ) ? 'btn-block' : '',
	( $is_default && '' !== $icon ) ? $icon_pos : '',
	$a['extra_class']
) );

// Template with icon position
$tpl = ( 'right' === $a['icon_pos'] )
	? '<a href="%1$s" class="%4$s" %5$s %6$s>%2$s %3$s</a>'
	: '<a href="%1$s" class="%4$s" %5$s %6$s>%3$s %2$s</a>';

// 1 - href, 2 - content, 3 - icon, 4 - class, 5 - target, 6 - title
printf( $tpl, $href, $content, $icon, $class, $target, $title );
