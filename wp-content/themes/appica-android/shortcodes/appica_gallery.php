<?php
/**
 * Shortcode "Gallery" output
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Android
 */

$a = shortcode_atts( array(
	'title'                => '',
	'subtitle'             => '',
	'source'               => 'categories', // categories | ids
	'query_post__in'       => '',
	'query_categories'     => '',
	'query_post__not_in'   => '',
	'query_posts_per_page' => 'all',
	'query_orderby'        => 'date',
	'query_order'          => 'DESC', // DESC | ASC
	'is_filters'           => 'yes', // yes | no
	'filters_exclude'      => '',
	'extra_class'          => '',
), $atts );

$gallery_type = 'appica_gallery';
$gallery_tax  = 'appica_gallery_category';

$is_filters = ( 'yes' === $a['is_filters'] );
$source     = sanitize_key( $a['source'] );

$query_default = array(
	'post_type'     => $gallery_type,
	'post_status'   => 'publish',
	'no_found_rows' => true,
);

$query_args = appica_parse_array( $a, 'query_' );
$query_args = array_merge( $query_default, $query_args );
$query_args = appica_query_build( $query_args, function( $query ) use ( $gallery_tax, $source ) {
	// build tax_query, @see WP_Query
	if ( 'categories' === $source && array_key_exists( 'categories', $query ) ) {
		$categories = $query['categories'];
		unset( $query['categories'] );

		$query['tax_query'] = appica_query_single_tax( $categories, $gallery_tax );
	}

	// fetch all posts if getting by IDs
	if ( 'ids' === $source ) {
		$query['posts_per_page'] = -1;
	}

	return $query;
} );

$query = new WP_Query( $query_args );
if ( $query->have_posts() ) {

	// block heading
	$heading = '';
	if ( '' !== $a['title'] || '' !== $a['subtitle'] ) {
		$heading .= '<div class="col-lg-4 col-lg-push-8 col-md-5 col-md-push-7 col-sm-6 col-sm-push-6">';
		$heading .= '<div class="block-heading">';
		$heading .= appica_get_text( esc_html( $a['title'] ), '<h2>', '</h2>' );
		$heading .= appica_get_text( esc_html( $a['subtitle'] ), '<p>', '</p>' );
		$heading .= '</div></div>';
	}

	// filters
	$filters = '';
	if ( $is_filters ) {
		$exclusion = array();
		if ( ! empty( $a['filters_exclude'] ) ) {
			$terms = appica_parse_slug_list( $a['filters_exclude'] );
			foreach( (array) $terms as $slug ) {
				$term = get_term_by( 'slug', $slug, $gallery_tax );
				if ( $term instanceof WP_Term ) {
					$exclusion[] = (int) $term->term_id;
				}

				continue;
			}
			unset( $terms, $term, $slug );
		}

		$categories = get_terms( $gallery_tax, array(
			'hierarchical' => false,
			'exclude'      => $exclusion,
		) );

		if ( ! is_wp_error( $categories )
		     && is_array( $categories )
		     && count( $categories ) > 0
		) {
			$filters .= '<div class="col-lg-8 col-lg-pull-4 col-md-7 col-md-pull-5 col-sm-6 col-sm-pull-6">';
			$filters .= '<ul class="nav-filters text-right space-top-3x">';
			$filters .= sprintf( '<li class="active"><a class="waves-effect waves-primary" data-filter="*" href="#">%s</a></li>', esc_html__( 'All', 'appica' ) );

			foreach ( (array) $categories as $category ) {
				$filters .= sprintf( '<li><a class="waves-effect waves-primary" data-filter=".%1$s" href="#">%2$s</a></li>',
					$category->slug,
					$category->name
				);
			}

			$filters .= '</ul></div>';
		}

		unset( $categories, $category );
	}

	if ( ! empty( $heading ) || ! empty( $filters ) ) {
		echo '<div class="row">', $heading, $filters, '</div>';
	}

	$grid_class = appica_get_class_set( array(
		'masonry-grid',
		'filter-grid',
		'space-top-2x',
		$a['extra_class'],
	) );

	echo '<div class="', $grid_class, '">';

	?>
	<div class="grid-sizer"></div>
	<div class="gutter-sizer"></div>
	<?php

	while( $query->have_posts() ) {
		$query->the_post();
		get_template_part( 'template-parts/tile', 'gallery' );
	}

	echo '</div>'; // .masonry-grid
}
wp_reset_postdata();
