<?php
/**
 * Shortcode "Gadgets Slideshow" output
 *
 * @since      1.0.0
 *
 * @author     8guild
 * @package    Appica
 * @subpackage Android
 */

$a = shortcode_atts( array(
	'title'                => '',
	'subtitle'             => '',
	'source'               => 'categories', // categories | ids
	'query_post__in'       => '',
	'query_categories'     => '',
	'query_post__not_in'   => '',
	'query_posts_per_page' => 'all',
	'query_orderby'        => 'date',
	'query_order'          => 'DESC', // DESC | ASC
	'is_sl'                => 'disable', // enable | disable
	'sl_interval'          => 3000,
	'extra_class'          => ''
), $atts );

$slideshow_type = 'appica_slideshow';
$slideshow_tax  = 'appica_slideshow_category';

$source      = sanitize_key( $a['source'] );
$is_sl       = ( 'enable' === $a['is_sl'] ) ? 'true' : 'false';
$sl_interval = absint( $a['sl_interval'] );

$query_defaults = array(
	'post_type'      => 'appica_slideshow',
	'post_status'    => 'publish',
	'no_found_rows'  => true,
);

$query_args = appica_parse_array( $a, 'query_' );
$query_args = array_merge( $query_defaults, $query_args );
$query_args = appica_query_build( $query_args, function( $query ) use ( $slideshow_tax, $source ) {
	// build a tax query if fetching by category
	// @see WP_Query
	if ( 'categories' === $source && array_key_exists( 'categories', $query ) ) {
		$categories = $query['categories'];
		unset( $query['categories'] );

		$query['tax_query'] = appica_query_single_tax( $categories, $slideshow_tax );
	}

	// fetch all posts if getting by IDs
	if ( 'ids' === $source ) {
		$query['posts_per_page'] = -1;
	}

	return $query;
} );

$visible_when_stack = '';
$hidden_when_stack  = '';
if ( ! empty( $a['title'] ) || ! empty( $a['subtitle'] ) ) {
	$title    = appica_get_text( esc_html( $a['title'] ), '<h2>', '</h2>' );
	$subtitle = appica_get_text( esc_html( $a['subtitle'] ), '<span>', '</span>' );
	$tpl      = '<div class="block-heading %1$s">%2$s%3$s</div>';

	$visible_when_stack = sprintf( $tpl, 'visible-when-stack', $title, $subtitle );
	$hidden_when_stack  = sprintf( $tpl, 'hidden-when-stack', $title, $subtitle );
	unset( $title, $subtitle, $tpl );
}

$query = new WP_Query( array(
	'post_type'           => 'appica_slideshow',
	'post_status'         => 'publish',
	'posts_per_page'      => -1,
	'ignore_sticky_posts' => true
) );

$query   = new WP_Query( $query_args );
$entries = array();
if ( $query->have_posts() ) {
	while ( $query->have_posts() ) {
		$query->the_post();

		$post_id  = get_the_ID();
		$settings = appica_meta_box_get( $post_id, '_appica_slideshow_settings' );
		if ( empty( $settings ) ) {
			// legacy meta boxes value
			$icon       = get_post_meta( $post_id, '_appica_slideshow_icon', true );
			$transition = get_post_meta( $post_id, '_appica_slideshow_transition', true );
			$media      = get_post_meta( $post_id, '_appica_slideshow_media', true );
			$media      = wp_parse_args( $media, array(
				'phone'  => 0,
				'tablet' => 0,
			) );
		} else {
			$icon       = sanitize_key( $settings['icon'] );
			$transition = esc_attr( $settings['transition'] );
			$media      = array(
				'phone'  => (int) $settings['phone'],
				'tablet' => (int) $settings['tablet'],
			);
		}

		$entries['posts'][ $post_id ] = array(
			'title'      => get_the_title(),
			'excerpt'    => get_the_excerpt(),
			'transition' => $transition,
		);

		$entries['icons'][ $post_id ]   = $icon;
		$entries['phones'][ $post_id ]  = $media['phone'];
		$entries['tablets'][ $post_id ] = $media['tablet'];

		unset( $post_id, $settings, $icon, $transition, $media );
	}
}
wp_reset_postdata();

// Start output
echo '<div class="', appica_get_class_set( array( 'feature-tabs', $a['extra_class'] ) ), '">';
echo $visible_when_stack;
?>
<div class="clearfix">
	<div class="devices">
		<div class="tablet">
			<?php
			printf( '<img src="%1$s" alt="%2$s">',
				appica_image_uri( 'img/features/tablet.png', false ),
				esc_html( 'Tablet' )
			);
			?>
			<img class="reflection" src="<?php appica_image_uri( 'img/features/tablet-reflection.png' ); ?>" alt="Reflection">
			<div class="mask">
				<ul class="screens"><?php
					/*
					 * Tablets
					 */
					$tablets = array_key_exists( 'tablets', $entries )
						? (array) $entries['tablets']
						: array();

					$first = null;
					if ( count( $tablets ) > 0 ) {
						reset( $tablets );
						$first = key( $tablets );
					}

					foreach ( $tablets as $post_id => $tablet ) :
						$tpl = '<li id="ts%1$s">%2$s</li>';
						if ( $first === $post_id ) {
							// add class .active.in for first element
							$tpl = '<li class="active in" id="ts%1$s">%2$s</li>';
						}

						printf( $tpl, $post_id, wp_get_attachment_image( $tablet, 'full' ) );
					endforeach;
					unset( $tablets, $tpl, $first, $post_id, $tablet );
					?></ul>
			</div>
		</div>
		<div class="phone">
			<?php
			printf( '<img src="%1$s" alt="%2$s">',
				appica_image_uri( 'img/features/iphone.png', false ),
				esc_html( 'Phone' )
			);
			?>
			<div class="mask">
				<ul class="screens"><?php
					/*
					 * Phones
					 */
					$phones = array_key_exists( 'phones', $entries )
						? (array) $entries['phones']
						: array();

					$first = null;
					if ( count( $phones ) > 0 ) {
						reset( $phones );
						$first = key( $phones );
					}

					foreach( $phones as $post_id => $phone ) :
						$tpl = '<li id="ps%1$s">%2$s</li>';
						if ( $first === $post_id ) {
							$tpl = '<li class="active in" id="ps%1$s">%2$s</li>';
						}

						printf( $tpl, $post_id, wp_get_attachment_image( $phone, 'full' ) );
					endforeach;
					unset( $phones, $first, $tpl, $post_id, $phone );
					?></ul>
			</div>
		</div>
	</div>
	<div class="tabs text-center">
		<?php
		// Title
		echo $hidden_when_stack;

		/*
		 * Start .nav-tabs
		 */
		printf( '<ul class="nav-tabs" data-autoswitch="%1$s" data-interval="%2$s">',
			$is_sl,
			$sl_interval
		);

		$icons = array_key_exists( 'icons', $entries )
			? (array) $entries['icons']
			: array();

		$first = null;
		if ( count( $icons ) > 0 ) {
			reset( $icons );
			$first = key( $icons );
		}

		foreach ( $icons as $post_id => $icon ) {
			// 1 - post_id, 2 - icon class
			$tpl = '<li><a href="#tab-%1$s" data-toggle="tab" data-tablet="#ts%1$s" data-phone="#ps%1$s" aria-expanded="false"><i class="%2$s"></i></a></li>';
			if ( $first === $post_id ) {
				$tpl = '<li class="active"><a href="#tab-%1$s" data-toggle="tab" data-tablet="#ts%1$s" data-phone="#ps%1$s" aria-expanded="false"><i class="%2$s"></i></a></li>';
			}

			printf( $tpl, $post_id, $icon );
		}
		unset( $icons, $first, $tpl, $post_id, $icon );

		echo '</ul>';

		/*
		 * Tabs content
		 */
		echo '<div class="tab-content">';
		$posts = array_key_exists( 'posts', $entries )
			? (array) $entries['posts']
			: array();

		$first = null;
		if ( count( $posts ) > 0 ) {
			reset( $posts );
			$first = key( $posts );
		}

		foreach ( $posts as $post_id => $post ) {
			// 1 - post_id, 2 - title, 3 - excerpt, 4 - transition
			$tpl = '<div class="tab-pane transition fade %4$s" id="tab-%1$s">%2$s%3$s</div>';
			if ( $first === $post_id ) {
				$tpl = '<div class="tab-pane transition fade %4$s active in" id="tab-%1$s">%2$s%3$s</div>';
			}

			printf( $tpl, $post_id, "<h3>{$post['title']}</h3>", "<p>{$post['excerpt']}</p>", $post['transition'] );
		}
		unset( $posts, $first, $tpl, $posts_keys, $post_id, $post );

		echo '</div>'; // end .tab-content
		?>
	</div>
</div>
<?php
echo '</div>'; // .feature-tabs
