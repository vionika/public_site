<?php
/**
 * App Gallery | appica_app_gallery
 *
 * @author  8guild
 * @package Appica\Android\Shortcode
 */

$a = shortcode_atts( array(
	'source'               => 'categories', // categories | ids
	'query_post__in'       => '',
	'query_categories'     => '',
	'query_post__not_in'   => '',
	'query_posts_per_page' => 'all',
	'query_orderby'        => 'date',
	'query_order'          => 'DESC', // DESC | ASC
	'extra_class'          => '',
), $atts );

$app_gallery_type = 'appica_app_gallery';
$app_gallery_tax  = 'appica_app_gallery_category';

$source         = sanitize_key( $a['source'] );
$query_defaults = array(
	'post_type'     => $app_gallery_type,
	'post_status'   => 'publish',
	'no_found_rows' => true,
);

$query_args = appica_parse_array( $a, 'query_' );
$query_args = array_merge( $query_defaults, $query_args );
$query_args = appica_query_build( $query_args, function( $query ) use ( $app_gallery_tax, $source ) {
	// tax query, @see WP_Query
	if ( 'categories' === $source && array_key_exists( 'categories', $query ) ) {
		$categories = $query['categories'];
		unset( $query['categories'] );

		$query['tax_query'] = appica_query_single_tax( $categories, $app_gallery_tax );
	}

	// fetch all posts if getting by IDs
	if ( 'ids' === $source ) {
		$query['posts_per_page'] = -1;
	}

	return $query;
} );

$query = new WP_Query( $query_args );
if ( $query->have_posts() ) {

	$classes = appica_get_class_set( array(
		'scroller',
		'app-gallery',
		$a['extra_class'],
	) );

	$class = appica_get_class_set( array( 'scroller', 'app-gallery', $a['extra_class'] ) );

	echo '<div class="', esc_attr( $classes ), '">';

	$i = 1;
	while ( $query->have_posts() ) {
		$query->the_post();

		$image   = esc_url( appica_get_image_src( get_post_thumbnail_id() ) );
		$post_id = get_the_ID();
		$r       = $i % 3;

		/*
		 * Show large & small previews
		 *
		 * First item wrap to div.item, each second and third item
		 * combined and wrapped to div.item together
		 */
		if ( 1 === $r || 2 === $r ) {
			echo '<div class="item">';
		}

		if ( 1 === $r ) {
			// large preview
			printf( '<a href="%2$s" class="waves-effect">%1$s</a>',
				get_the_post_thumbnail( $post_id, 'appica-app-gallery-large' ),
				$image
			);
		} else {
			// small preview
			printf( '<a href="%2$s" class="waves-effect">%1$s</a>',
				get_the_post_thumbnail( $post_id, 'appica-app-gallery' ),
				$image
			);
		}

		if ( 0 === $r || 1 === $r || 0 === $i % $query->post_count ) {
			echo '</div>';
		}

		$i ++;
	}

	echo '</div>';
}
wp_reset_postdata();