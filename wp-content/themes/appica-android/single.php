<?php
/**
 * The template for displaying all single posts.
 *
 * @package Appica
 */

get_header();
appica_page_title();
?>

<section class="space-top padding-bottom-2x appica-single-post">
	<div class="container">
		<div class="row">
			<?php

			while ( have_posts() ) :
				the_post();
				get_template_part( 'template-parts/single', appica_get_post_settings( 'sidebar', 'left' ) );
			endwhile;

			?>
		</div>
	</div>
</section>

<?php
get_footer();
