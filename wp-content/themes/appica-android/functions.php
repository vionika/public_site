<?php
/**
 * Appica2 functions and definitions
 *
 * @package Appica2
 */

define( 'APPICA_TEMPLATE_DIR', get_template_directory() );

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 750; /* pixels */
}

/**
 * Theme helper functions
 */
require APPICA_TEMPLATE_DIR . '/inc/helpers-compat.php';

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function appica_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Appica2, use a find and replace
	 * to change 'appica' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'appica', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );

	/*
	 * Switch default core markup for search form to output valid HTML5.
	 */
	add_theme_support( 'html5', array( 'search-form' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'anchor'  => esc_html__( 'Anchored Menu', 'appica' ),
		'primary' => esc_html__( 'Primary Menu', 'appica' ),
		'footer'  => esc_html__( 'Footer Menu', 'appica' )
	) );

	/*
	 * Add some extra image sizes.
	 * This image sizes uses only for images preview on blog posts index page
	 */
	add_image_size( 'appica-home-thumbnail-double', 770, 240, true );
	add_image_size( 'appica-home-thumbnail', 440, 371, true );
}

add_action( 'after_setup_theme', 'appica_setup' );

/**
 * Enqueue scripts and styles.
 *
 * @since 1.0.0
 */
function appica_scripts() {
	$template_uri = get_template_directory_uri();

	/**
	 * @var array Theme CSS dependencies
	 */
	$style_deps = array();

	if ( appica_is_google_font() ) {
		$google_font = appica_option_get( 'typography_google_font', '//fonts.googleapis.com/css?family=Open+Sans:400,600,700,300,800' );
		wp_register_style( 'appica-google-font', appica_google_font_url( $google_font ), array(), null, 'screen' );
		$style_deps[] = 'appica-google-font';
		unset( $google_font );
	}

	wp_register_style( 'appica-theme', $template_uri . '/css/style.css', array(), null, 'screen' );
	$style_deps[] = 'appica-theme';

	// enqueue theme main style.css file
	wp_enqueue_style( 'appica', get_stylesheet_uri(), $style_deps, null );
	wp_add_inline_style( 'appica', appica_get_head_css() );

	// scripts in <head>
	wp_enqueue_script( 'pace', $template_uri . '/js/plugins/pace.min.js', array(), null );
	wp_enqueue_script( 'modernizr', $template_uri . '/js/libs/modernizr.custom.js', array(), null );
	wp_enqueue_script( 'detectizr', $template_uri . '/js/libs/detectizr.min.js', array(), null );

	// scripts in footer
	$scripts_deps = array();
	$scripts_deps[] = 'jquery';

	wp_register_script( 'easing', $template_uri . '/js/libs/jquery.easing.1.3.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'easing';

	wp_register_script( 'velocity', $template_uri . '/js/plugins/velocity.min.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'velocity';

	wp_register_script( 'bootstrap', $template_uri . '/js/plugins/bootstrap.min.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'bootstrap';

	wp_register_script( 'waves', $template_uri . '/js/plugins/waves.min.js', array(), null, true );
	$scripts_deps[] = 'waves';

	wp_register_script( 'form', $template_uri . '/js/plugins/form-plugins.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'form';

	wp_register_script( 'm-custom-scrollbar', $template_uri . '/js/plugins/jquery.mCustomScrollbar.min.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'm-custom-scrollbar';

	if ( wp_script_is( 'isotope', 'registered' ) ) {
		wp_deregister_script( 'isotope' );
	}
	wp_register_script( 'isotope', $template_uri . '/js/plugins/isotope.pkgd.min.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'isotope';

	if ( wp_script_is( 'waypoints', 'registered' ) ) {
		wp_deregister_script( 'waypoints' );
	}
	wp_register_script( 'waypoints', $template_uri . '/js/plugins/jquery.waypoints.min.js', array( 'jquery' ), null, true );
	$scripts_deps[] = 'waypoints';

	wp_enqueue_script( 'appica', $template_uri . '/js/scripts.js', $scripts_deps, null, true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// Nonce and ajaxurl for AJAX calls
	wp_localize_script( 'appica', 'appica', array(
		'ajaxurl'   => admin_url( 'admin-ajax.php' ),
		'nonce'     => wp_create_nonce( 'appica-ajax' ),
		'masonry'   => '', // for masonry container
		'portfolio' => '', // for portfolio masonry container
	) );
}

add_action( 'wp_enqueue_scripts', 'appica_scripts', 11 );

/**
 * Enqueue scripts and styles on admin pages
 *
 * @since 1.0.0
 */
function appica_admin_scripts() {
	$template_directory_uri = get_template_directory_uri();

	wp_enqueue_style( 'flaticons', $template_directory_uri . '/css/vendor/flaticon.css', array(), null );
	wp_enqueue_script( 'appica', $template_directory_uri . '/js/admin-custom.js', array( 'jquery' ), null, true );
}

add_action( 'admin_enqueue_scripts', 'appica_admin_scripts' );

/**
 * Register the required plugins for this theme.
 *
 * @author 8guild
 */
function appica_tgm_init() {
	$plugins_subdir = '/plugins/';
	$plugins_dir    = wp_normalize_path( get_template_directory() . $plugins_subdir );

	/**
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 *
	 * @var array
	 */
	$plugins = array();

	$plugins[] = array(
		'name'             => esc_html__( 'Appica Core', 'appica' ),
		'slug'             => 'appica-core',
		'source'           => $plugins_dir . '/appica-core.zip',
		'required'         => true,
		'version'          => '2.0.0',
		'force_activation' => true,
	);

	$plugins[] = array(
		'name'             => esc_html__( 'Equip', 'appica' ),
		'slug'             => 'equip',
		'source'           => $plugins_dir . '/equip.zip',
		'required'         => true,
		'force_activation' => true,
	);

	$plugins[] = array(
		'name'             => esc_html__( 'Visual Composer', 'appica' ),
		'slug'             => 'js_composer',
		'source'           => $plugins_dir . '/js_composer.zip',
		'required'         => true,
		'force_activation' => true,
		'version'          => '4.9',
	);

	$plugins[] = array(
		'name'             => esc_html__( 'Redux Framework', 'appica' ),
		'slug'             => 'redux-framework',
		'required'         => true,
		'force_activation' => true,
	);

	$plugins[] = array(
		'name'     => esc_html__( 'Contact Form 7', 'appica' ),
		'slug'     => 'contact-form-7',
		'required' => false,
	);

	$plugins[] = array(
		'name'     => esc_html__( 'Revolution Slider', 'appica' ),
		'slug'     => 'revslider',
		'source'   => $plugins_dir . '/revslider.zip',
		'required' => false,
	);

	/**
	 * Array of configuration settings.
	 */
	$config = array(
		'id'           => 'appica-tgm',
		'menu'         => 'appica-plugins',
		'has_notices'  => true,
		'dismissable'  => true,
		'is_automatic' => true,
	);

	tgmpa( $plugins, $config );
}

add_action( 'tgmpa_register', 'appica_tgm_init' );

/**
 * Include the TGM_Plugin_Activation class.
 */
require_once APPICA_TEMPLATE_DIR . '/vendor/tgm/class-tgm-plugin-activation.php';

/**
 * Theme custom Template Tags
 */
require APPICA_TEMPLATE_DIR . '/inc/template-tags.php';

/**
 * Filters and actions that act independently of the theme templates.
 */
require APPICA_TEMPLATE_DIR . '/inc/extras.php';

/**
 * AJAX Handlers
 */
require APPICA_TEMPLATE_DIR . '/inc/ajax.php';

/**
 * Customizer additions.
 */
require APPICA_TEMPLATE_DIR . '/inc/customizer.php';

/**
 * Comments functions
 */
require APPICA_TEMPLATE_DIR . '/inc/comments.php';

/**
 * Theme custom widgets
 */
require APPICA_TEMPLATE_DIR . '/inc/widgets.php';

/**
 * Menus staff & walkers
 */
require APPICA_TEMPLATE_DIR . '/inc/menus.php';

/**
 * Theme custom meta boxes
 */
require APPICA_TEMPLATE_DIR . '/inc/meta-boxes.php';

/**
 * Theme Options via Redux Framework
 */
require APPICA_TEMPLATE_DIR . '/inc/options.php';

/**
 * Visual Composer actions & filters
 */
require APPICA_TEMPLATE_DIR . '/inc/vc.php';