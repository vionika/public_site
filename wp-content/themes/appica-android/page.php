<?php
/**
 * The template for displaying all pages.
 *
 * @package Appica
 */

get_header();

while ( have_posts() ) :
	the_post();
	appica_page_title();
	get_template_part( 'template-parts/content', 'page' );
endwhile; // end of the loop.

get_footer();
