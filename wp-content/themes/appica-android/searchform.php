<?php
/**
 * Search form template
 *
 * @package Appica
 */
?>
<form role="search" method="get" class="search-field form-control" action="<?php echo esc_url( home_url( '/' ) ); ?>">
	<button type="submit" class="search-btn"><i class="flaticon-search100"></i></button>
	<input type="hidden" name="post_type" value="post">
	<input type="text" name="s" id="search-input" value="<?php the_search_query(); ?>">
	<label for="search-input"><?php esc_html_e( 'Search', 'appica' ); ?></label>
</form>