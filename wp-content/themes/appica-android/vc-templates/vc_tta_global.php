<?php
/**
 * Wrapper shortcode for vc_tta_tabs & vc_tta_accordion
 *
 * @var array $atts    Shortcode attributes
 * @var mixed $content Shortcode content
 *
 * @var WPBakeryShortCode_VC_Tta_Accordion|WPBakeryShortCode_VC_Tta_Tabs $this
 */

$a = shortcode_atts( array(
	'title'       => '',
	'subtitle'    => '',
	'description' => '',
	'alignment'   => 'left',
	'position'    => 'right',
), $atts );

$is_tour = ( 'vc_tta_tour' === $this->shortcode );
$is_tabs = ( 'vc_tta_tabs' === $this->shortcode );
$is_left = ( 'left' === $a['position'] );

$this->resetVariables( $a, $content );
$this->setGlobalTtaInfo();

// without this tabs & accordions won't work!
$content = $this->getTemplateVariable( 'content' );

$tabs_class = appica_get_class_set( array(
	'nav-tabs',
	$is_tour ? 'alt-tabs' : '',
	$is_tour ? 'nav-vertical' : '',
	$is_tour ? 'space-top-2x' : '',
) );

$tabs_column_classes = appica_get_class_set( array(
	$is_tour ? 'col-md-3' : '',
	$is_tour ? 'col-sm-4' : '',

	// if content has left position, we have to add offset to tabs column
	$is_tour && $is_left ? 'col-lg-offset-1' : '',

	$is_tabs ? 'col-sm-6' : '',
	'text-' . sanitize_key( $a['alignment'] ),
) );

$content_column_classes = array(
	$is_tabs ? 'col-sm-6' : '',
	$is_tour ? 'col-lg-8' : '',
	$is_tour ? 'col-md-9' : '',
	$is_tour ? 'col-sm-8' : '',
);

if ( $is_tour ) {
	// add offset for "right" content position
	$content_column_classes[] = $is_left ? '' : 'col-lg-offset-1';
}

$content_column_classes = appica_get_class_set( $content_column_classes );

if ( $is_left ) : ?>
	<div class="<?php echo esc_attr( $content_column_classes ); ?>">
		<div class="tab-content">
			<?php echo $content; // already escaped and passed to do_shortcode ?>
		</div>
	</div>
	<div class="<?php echo esc_attr( $tabs_column_classes ); ?>">
		<?php if ( ! empty( $a['title'] ) || ! empty( $a['subtitle'] ) ) : ?>
			<div class="block-heading">
				<?php
				appica_the_text( esc_html( $a['title'] ), '<h2>', '</h2>' );
				appica_the_text( esc_html( $a['subtitle'] ), '<span>', '</span>' );
				?>
			</div>
		<?php endif;
		appica_the_text( esc_textarea( $a['description'] ), '<p class="space-bottom">', '</p>' );
		?>
		<ul class="<?php echo esc_attr( $tabs_class ); ?>" role="tablist">
			<?php
			foreach ( WPBakeryShortCode_VC_Tta_Section::$section_info as $nth => $section ) {
				$is_active = ( $nth === 0 );
				$tab_attr  = appica_get_html_attributes( array(
					'href'        => sprintf( '#%s', esc_attr( $section['tab_id'] ) ),
					'class'       => 'waves-effect waves-primary',
					'role'        => 'tab',
					'data-toggle' => 'tab',
				) );

				echo '<li', $is_active ? ' class="active"' : '', '>';
				echo '<a ', $tab_attr, '>';
				echo esc_html( $section['title'] );
				echo '</a>';
				echo '</li>';
				unset( $is_active, $tab_attr );
			}
			unset( $nth, $section );
			?>
		</ul>
	</div>
<?php else : ?>
	<div class="<?php echo esc_attr( $tabs_column_classes ); ?>">
		<?php if ( ! empty( $a['title'] ) || ! empty( $a['subtitle'] ) ) : ?>
			<div class="block-heading">
				<?php
				appica_the_text( esc_html( $a['title'] ), '<h2>', '</h2>' );
				appica_the_text( esc_html( $a['subtitle'] ), '<span>', '</span>' );
				?>
			</div>
		<?php
		endif;
		appica_the_text( esc_textarea( $a['description'] ), '<p class="space-bottom">', '</p>' );
		?>
		<ul class="<?php echo esc_attr( $tabs_class ); ?>" role="tablist">
			<?php
			foreach ( WPBakeryShortCode_VC_Tta_Section::$section_info as $nth => $section ) {
				$is_active = ( $nth === 0 );
				$tab_attr  = appica_get_html_attributes( array(
					'href'        => sprintf( '#%s', esc_attr( $section['tab_id'] ) ),
					'class'       => 'waves-effect waves-primary',
					'role'        => 'tab',
					'data-toggle' => 'tab',
				) );

				echo '<li', $is_active ? ' class="active"' : '', '>';
				echo '<a ', $tab_attr, '>';
				echo esc_html( $section['title'] );
				echo '</a>';
				echo '</li>';
				unset( $is_active, $tab_attr );
			}
			unset( $nth, $section );
			?>
		</ul>
	</div>
	<div class="<?php echo esc_attr( $content_column_classes ); ?>">
		<div class="tab-content">
			<?php echo $content; // already escaped and passed to do_shortcode ?>
		</div>
	</div>
<?php
endif;
