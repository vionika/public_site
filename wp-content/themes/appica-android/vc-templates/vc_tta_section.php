<?php
/**
 * Section | vc_tta_section
 *
 * Supports vc_tta_tabs & vc_tta_accordion
 *
 * @var array                            $atts    Shortcode attributes
 * @var mixed                            $content Shortcode content
 * @var WPBakeryShortCode_VC_Tta_Section $this
 */

$a = shortcode_atts( array(
	'title'     => '',
	'tab_id'    => '',
	'animation' => 'fade',
), $atts );

$this->resetVariables( $a, $content );
WPBakeryShortCode_VC_Tta_Section::$self_count ++;
WPBakeryShortCode_VC_Tta_Section::$section_info[] = $a;

// check the parent shortcode
$parent       = WPBakeryShortCode_VC_Tta_Section::$tta_base_shortcode;
$is_active    = ( WPBakeryShortCode_VC_Tta_Section::$self_count <= 1 );
$is_animation = ( 'fade' !== $a['animation'] );

$wrap_class = array(
	'tab-pane',
	'transition',
	'fade',
	$is_animation ? sanitize_key( $a['animation'] ) : '',
	$is_active ? 'in active' : '',
);

$panel_attr = array(
	'role'  => 'tabpanel',
	'class' => esc_attr( appica_get_class_set( $wrap_class ) ),
	'id'    => esc_attr( $this->getTemplateVariable( 'tab_id' ) ),
);

?>
	<div <?php echo appica_get_html_attributes( $panel_attr ); ?>>
		<?php echo $this->getTemplateVariable( 'content' ); ?>
	</div>
<?php
