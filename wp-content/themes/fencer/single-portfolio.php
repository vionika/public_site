<?php
/**
 * The Template for displaying Portfolio single posts.
 *
 * @package Fencer
 */

global $theme_options;

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

<?php 
	$comments_loaded = false;

  $portfolio_small = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'portfolio-small' );
  $portfolio_large = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'portfolio-large' );
  $portfolio_source = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'source' );

  $portfolio_brand = get_post_meta( get_the_ID(), '_portfolio_brand_value', true );
  $portfolio_brand_url = get_post_meta( get_the_ID(), '_portfolio_brand_url_value', true );
  $portfolio_url = get_post_meta( get_the_ID(), '_portfolio_url_value', true );

  $portfolio_fullwidthslider = get_post_meta( get_the_ID(), '_portfolio_fullwidthslider_value', true );
  $portfolio_socialshare_disable = get_post_meta( get_the_ID(), '_portfolio_socialshare_disable_value', true );
  $portfolio_original_image_sizes = get_post_meta( get_the_ID(), '_portfolio_original_image_sizes_value', true );

  $portfolio_hide_1st_image_from_slider = get_post_meta( get_the_ID(), '_portfolio_hide_1st_image_from_slider_value', true );

  $portfolio_images = array();
  $portfolio_images_sources = array();

  $portfolio_images_sources = get_images_src('source', false, get_the_ID());

  $portfolio_layout = get_post_meta( get_the_ID(), '_portfolio_display_type_value', true );
  $portfolio_disableslider = get_post_meta( get_the_ID(), '_portfolio_disableslider_value', true );

  $portfolio_sidebarposition = get_post_meta( $post->ID, '_portfolio_sidebarposition_value', true );

  if(!isset($portfolio_sidebarposition)) {
    $portfolio_sidebarposition = 0;
  }

  if($portfolio_sidebarposition == "0") {
    $portfolio_sidebarposition = $theme_options['portfolio_sidebar_position'];
  }

  if($portfolio_layout == 0) {

  	if(is_active_sidebar( 'main-sidebar' ) && ($portfolio_sidebarposition <> 'disable')) {
  		$span_class = 'col-md-4';
  		$span_class2 = 'col-md-5';
  	}
  	else {
  		$span_class = 'col-md-6';
  		$span_class2 = 'col-md-6';
  	}

  	$portfolio_main_image = $portfolio_small;
  	$portfolio_images = get_images_src('portfolio-small',false, get_the_ID());
  }
  else {
  	if(is_active_sidebar( 'main-sidebar' ) && ($portfolio_sidebarposition <> 'disable') ) {
  		$span_class = 'col-md-9';
  	}
  	else {
  		$span_class = 'col-md-12';
  		$span_class2 = 'col-md-12';
  	}

  	$portfolio_main_image = $portfolio_large;
  	$portfolio_images = get_images_src('portfolio-large',false, get_the_ID());
  }

  // full image size for fullwidth slider
  if($portfolio_fullwidthslider) { 
  	$portfolio_images = get_images_src('source',false, get_the_ID());
  	$portfolio_main_image = $portfolio_source;

    $span_class = 'col-md-12';
  }

  if(!isset($portfolio_original_image_sizes)) {
    $portfolio_original_image_sizes = false;
  }

  if($portfolio_original_image_sizes) {
      $portfolio_images = get_images_src('source',false, get_the_ID());
      $portfolio_main_image = $portfolio_source;
  }
  
  $terms = get_the_terms( $post->ID , 'portfolio_filter' );

  $terms_count = count($terms);

  $terms_counter = 0;

  $terms_slug = '';

  $categories_html = '';

  $parent_link = get_post_type_archive_link('portfolio');

  $related_filter = '';

  foreach ( $terms ? $terms: array() as $term ) {

    if($terms_counter  < $terms_count - 1) {
      $categories_html .= '<a href="'.$parent_link.'">'.$term->name.'</a> ';
      $related_filter .= $term->slug.', ';
    }
    else
    {
      $categories_html .= '<a href="'.$parent_link.'">'.$term->name.'</a>';
      $related_filter .= $term->slug;
    }
    
    $terms_slug .= ' '.$term->slug;

    $terms_counter++;
  }

?>	
	
	<div class="container-fluid page-item-title text-center">
	<h1><?php the_title(); ?></h1>
	</div>
	<div class="loop-item-nav"><?php fencer_content_nav( 'nav-below' ); ?></div>
	<div class="container portfolio-item-details">
		<div class="row">
		<?php if ( is_active_sidebar( 'main-sidebar' ) && ( $portfolio_sidebarposition == 'left') &&(!$portfolio_fullwidthslider)) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		  <ul id="main-sidebar">
		    <?php dynamic_sidebar( 'main-sidebar' ); ?>
		  </ul>
		</div>
		<?php endif; ?>
		<div class="<?php echo $span_class; ?>">
			<div class="portfolio-item-image<?php if($portfolio_fullwidthslider) { echo ' fullwidth'; } ?>">
				<div class="porftolio-slider<?php if((!$portfolio_disableslider)&&(count($portfolio_images) > 0)) { echo ' flexslider'; } ?>">
          <ul class="slides">
            <?php if(!$portfolio_hide_1st_image_from_slider): ?>
            <li><a href="<?php echo $portfolio_source[0]; ?>" rel="prettyPhoto[]"><img src="<?php echo $portfolio_main_image[0]; ?>" alt="<?php the_title(); ?>"/></a></li>
            <?php endif; ?>
            <?php
            $i = 1;
            foreach ( $portfolio_images as $portfolio_image ) {
              echo '<li><a href="'.$portfolio_images_sources['image'.$i][0].'" rel="prettyPhoto[]"><img src="'.$portfolio_image[0].'" alt=""/></a></li>';
              $i++;
            }

            ?>
          </ul>
        </div>
      </div>
      <div class="portfolio-items-categories"><?php echo $categories_html; ?></div>
    <?php if ( !is_active_sidebar( 'main-sidebar' ) || ($portfolio_sidebarposition == 'disable') || ($portfolio_layout == 0)) : ?>    
		</div>
		<div class="<?php echo $span_class2; ?>">
		<?php endif; ?>
		<div class="portfolio-item-data clearfix">
              <?php if(!isset($portfolio_socialshare_disable) || !$portfolio_socialshare_disable): ?>
              <div class="post-social">
                <a title="Share this" href="#" class="facebook-share"> <i class="fa fa-facebook"></i> <span class="count">0</span></a>
                <a title="Tweet this" href="#" class="twitter-share"> <i class="fa fa-twitter"></i> <span class="count">0</span></a>
                <a title="Pin this" href="#" class="pinterest-share"> <i class="fa fa-pinterest"></i> <span class="count">0</span></a>
              </div>
              <?php endif; ?>
              <h2 class="project-description-title"><?php _e('Project description', 'fencer'); ?></h2>
              <div class="project-client">
              <p><span><?php _e('Added', 'fencer');?></span>: <?php the_time(get_option('date_format')); ?></p>
              <?php if(isset($portfolio_brand) && $portfolio_brand <> ''): ?>
              <p><span><?php _e('Client', 'fencer');?></span>: <a href="<?php echo $portfolio_brand_url; ?>" target="_blank"><?php echo $portfolio_brand; ?></a></p>
              <?php endif; ?>
              <?php if(isset($portfolio_url) && $portfolio_url <> ''): ?>
              <p><span><?php _e('Project url', 'fencer');?></span>: <a href="<?php echo $portfolio_url; ?>" target="_blank"><?php echo $portfolio_url; ?></a></p>
              <?php endif; ?>
              </div>
              <?php the_content(); ?>
              
        </div>
        <?php
            if($portfolio_layout <> 0)
            {
              // If comments are open or we have at least one comment, load up the comment template
              if ( comments_open() || '0' != get_comments_number() ) 
                
                comments_template();

                $comments_loaded = true;
            }
            
        ?>
        <?php

            if($portfolio_layout <> 0)
            {
              if(!$comments_loaded) {
                   if ( comments_open() || '0' != get_comments_number() ) 
                
                      comments_template();
              }
              // If comments are open or we have at least one comment, load up the comment template
            }
        ?>
		</div>
		
		<?php if ( is_active_sidebar( 'main-sidebar' ) &&( $portfolio_sidebarposition == 'right') &&(!$portfolio_fullwidthslider) ) : ?>
		<div class="col-md-3 main-sidebar sidebar">
		  <ul id="main-sidebar">
		    <?php dynamic_sidebar( 'main-sidebar' ); ?>
		  </ul>
		</div>
		<?php endif; ?>
    
    <?php
        if($portfolio_layout == 0)
        {
          echo '<div class="col-md-12">';
          if(!$comments_loaded) {
               if ( comments_open() || '0' != get_comments_number() ) 
            
                  comments_template();
          }
          echo '</div>';
          // If comments are open or we have at least one comment, load up the comment template
        }
    ?>

		</div>
	</div>

<?php if(isset($theme_options['portfolio_related_works']) && ($theme_options['portfolio_related_works'])): ?>
<div class="related-works">
<div class="container-fluid page-item-title text-center">
  <h1><?php _e('Related works', 'fencer'); ?></h1>
</div>
<?php get_template_part( 'page-portfolio' ); ?>
</div>
<?php endif; ?>

<?php endwhile; // end of the loop. ?>



<?php if(!$portfolio_disableslider): ?>

<?php 
if(isset($theme_options['portfolio_show_slider_navigation']) && $theme_options['portfolio_show_slider_navigation']) {
  $portfolio_show_slider_navigation = 'true';
} else {
  $portfolio_show_slider_navigation = 'false';
}

if(isset($theme_options['portfolio_slider_autoplay']) && $theme_options['portfolio_slider_autoplay']) {
  $portfolio_slider_autoplay = 'true';
} else {
  $portfolio_slider_autoplay = 'false';
}
?>
<?php if(count($portfolio_images) > 0): ?>
<script>
(function($){
$(document).ready(function() {

	$('.porftolio-slider').flexslider({
		directionNav: <?php echo $portfolio_show_slider_navigation; ?>,
		slideshow: <?php echo $portfolio_slider_autoplay; ?>,
        slideshowSpeed: <?php echo $theme_options['portfolio_slider_speed']; ?>,
		controlNav: false,
		animation: "<?php echo $theme_options['portfolio_slider_animation']; ?>",
		prevText: "",
		nextText: "",
		smoothHeight: true
	});

});
})(jQuery);
</script>
<?php endif; ?>
<?php endif; ?>

<?php get_footer(); ?>