<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Fencer
 */
?>
<?php 
global $theme_options;

if(isset($theme_options['footer_show_twitter_navigation']) && $theme_options['footer_show_twitter_navigation']) {
  $footer_show_twitter_navigation = 'true';
} else {
  $footer_show_twitter_navigation = 'false';
}

if($theme_options['footer_twitter_autoplay']) {
  $footer_twitter_autoplay = 'true';
} else {
  $footer_twitter_autoplay = 'false';
}

?>
<?php 
$show_twitter = false;

if($theme_options['footer_show_twitter_homepage_only'] && (is_page_template('page-onepage.php'))) { 
  $show_twitter = true; 
}

if(!$theme_options['footer_show_twitter_homepage_only']) { 
  $show_twitter = true; 
}
?>
<?php if(isset($theme_options['footer_show_twitter']) && $theme_options['footer_show_twitter'] && $show_twitter): ?>

<div class="content-block twitter">
  <div class="container">
    <div class="row">
      <div class="col-md-12 twitter-item flexslider bottom">
        <script type="text/javascript">
        (function($){
        $(document).ready(function() {
          
          // Show twitter after 1 second to not freeze the other page scripts
          setTimeout(initTwitter, 1000);

          function initTwitter()
          {
            $('.twitter-item').tweet({
             modpath: '<?php echo get_template_directory_uri();?>/js/twitter/index.php',
             count: <?php echo $theme_options['footer_twitter_tweets_count']; ?>,
             username: "<?php echo $theme_options['footer_twitter_username']; ?>",
             template: '<div class="twitter-icon"><i class="fa fa-twitter"></i></div><p class="text-center"> {text} </p><p class="twitter-meta text-center"> <a href="http://twitter.com/<?php echo $theme_options["footer_twitter_username"]; ?>" target="_blank">@<?php echo $theme_options["footer_twitter_username"]; ?></a><span class="time">, {time}</span> </p> </div>',
             loading_text: '<div class="twitter-icon"><i class="fa fa-twitter"></i></div>',

            });
            
            $('.twitter-item').flexslider({
              selector: ".tweet_list > li",
              directionNav: <?php echo $footer_show_twitter_navigation; ?>,
              slideshow: <?php echo $footer_twitter_autoplay; ?>,
              controlNav: false,
              slideshowSpeed: <?php echo $theme_options['footer_twitter_speed']; ?>,
              animation: "<?php echo $theme_options['footer_twitter_animation']; ?>",
              prevText: "",
              nextText: ""
            });
          }
          
        });
        })(jQuery);
        </script>
      </div>
    </div>
  </div>
</div>
<?php endif; ?>
<?php if ( is_active_sidebar( 'footer-sidebar' ) ) : ?>
<div class="footer-sidebar sidebar container">
  <ul id="sidebar">
    <?php dynamic_sidebar( 'footer-sidebar' ); ?>
  </ul>
</div>
<?php endif; ?>
<footer>
<div class="container">
<div class="row">
    <div class="col-md-6">
    <p class="copyright"><?php echo $theme_options['footer_copyright_editor']; ?></p>
    </div>
    <div class="col-md-6">
  <ul class="social footer-social unstyled">
<?php

$social_services_arr = Array("facebook", "vk","twitter", "google-plus", "linkedin", "dribbble", "instagram", "tumblr", "pinterest", "vimeo-square", "youtube", "skype");

foreach( $social_services_arr as $ss_data ){
  if(isset($theme_options[$ss_data]) && (trim($theme_options[$ss_data])) <> '') {
    $social_service_url = $theme_options[$ss_data];
    $social_service = $ss_data;
    echo '<li><a href="'.$social_service_url.'" target="_blank"><i class="fa fa-'.$social_service.'"></i></a></li>';
  }
}

?>
  </ul>
  </div>
</div>
</div>
<a id="top-link" href="#top"></a>
</footer>
<?php wp_footer(); ?>

</body>
</html>