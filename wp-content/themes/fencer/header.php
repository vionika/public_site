<?php
/**
 * WP Theme Header
 *
 * Displays all of the <head> section
 *
 * @package Fencer
 */

global $theme_options;

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php bloginfo('name'); ?> <?php is_home() ? bloginfo('description') : wp_title( '|', true, 'left' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php wp_head(); ?>
</head>

<body data-spy="scroll" data-target=".navbar" data-offset="150" <?php echo body_class(); ?>>
  <!-- Preloader -->
  <div class="mask"><div id="preloader">
  <?php
  if(isset($theme_options['preloader_image']) && $theme_options['preloader_image']['url'] <> ''):
  ?>
  <style>
  #preloader {
      height: auto;
      left: auto;
      margin: 0;
      position: inherit;
      text-align: center;
      width: 100%;
  }
  </style>
  <div class="preloader-image"><img src="<?php echo $theme_options['preloader_image']['url']; ?>" alt="Preloader"/></div>
  <?php else: ?>
    <div class="bubblingG">
        <span id="bubblingG_1">
        </span>
        <span id="bubblingG_2">
        </span>
        <span id="bubblingG_3">
        </span>
    </div>
  <?php endif; ?>
  
  </div></div>
  <!--/Preloader -->
  <?php do_action( 'before' ); ?>
    <div id="navbar" class="navbar navbar-default clearfix">
    <div class="navbar-inner">
      <div class="container"> 
      <div class="navbar-toggle-container"><a class="brand" href="<?php echo site_url(); ?>"><img src="<?php echo get_header_image(); ?>" alt=""></a> 
        <div class="navbar-toggle" data-toggle="collapse" data-target=".collapse"><div class="pull-left"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></div>
        <?php _e( 'Menu', 'fencer' ); ?>
        </div>
         
        </div>
        <?php
          wp_nav_menu(array(
            'theme_location'  => 'primary',
            'container_class' => 'navbar-collapse collapse',
            'menu_class'      => 'nav',
            'walker'          => new description_walker
            ));
        ?>
      </div>
    </div>
  </div>

  
  