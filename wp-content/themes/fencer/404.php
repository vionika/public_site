<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Fencer
 */

get_header(); ?>
<header>
<div class="container-fluid page-item-title text-center">
	<h1><?php _e( 'Oops! That page can&rsquo;t be found.', 'fencer' ); ?></h1>
</div>
</header>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="page-404">
			<h1><?php _e("<span>Error</span> 404", 'fencer'); ?></h1>
			<h2><?php _e("Page not found", 'fencer'); ?></h2>
			<p><?php _e("You can try from", 'fencer'); ?></p>
			<a class="btn" href="<?php echo get_site_url(); ?>"><?php _e("Home page", 'fencer'); ?></a>
			</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>