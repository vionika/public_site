<?php
/**
 * Fencer functions
 *
 * @package Fencer
 */

/*
 *	@@@ iPanel Path Constant @@@
*/
define( 'IPANEL_PATH' , get_template_directory() . '/iPanel/' ); 


/*
 *	@@@ iPanel URI Constant @@@
*/
define( 'IPANEL_URI' , get_template_directory_uri() . '/iPanel/' );


/*
 *	@@@ Usage Constant @@@
*/
define( 'IPANEL_PLUGIN_USAGE' , false );


/*
 *	@@@ Include iPanel Main File @@@
*/
include_once IPANEL_PATH . 'iPanel.php';

global $theme_options;
$theme_options = unserialize( base64_decode( get_option('FENCER_PANEL') ) );


if (!isset($content_width))
	$content_width = 640; /* pixels */

if (!function_exists('fencer_setup')) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function fencer_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on Fencer, use a find and replace
	 * to change 'fencer' to the name of your theme in all the template files
	 */
	load_theme_textdomain('fencer', get_template_directory() . '/languages');

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support('automatic-feed-links');

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support('post-thumbnails');

	/**
	 * Enable support for Logo
	 */
	add_theme_support( 'custom-header', array(
	    'default-image' =>  get_template_directory_uri() . '/img/logo.png',
            'width'         => 166,
            'flex-width'    => true,
            'flex-height'   => false,
            'header-text'   => false,
	));

	/**
	 *	Woocommerce support
	 */
	add_theme_support( 'woocommerce' );
	
	/**
	 * Enable custom background support
	 */
	add_theme_support( 'custom-background' );
	/**
	 * Theme resize image.
	 */
	add_image_size( 'team-member', 150, 150, true);
	add_image_size( 'portfolio-small', 462, 291, true);
    add_image_size( 'portfolio-large', 956, 653, true);
    add_image_size( 'latest-post', 258, 145, true);
    add_image_size( 'blogpost', 840, 570, true);
	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
            'primary' => __('Primary Menu', 'fencer'),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support('post-formats', array('aside', 'image', 'gallery', 'video', 'audio', 'quote', 'link', 'status', 'chat'));
}
endif;
add_action('after_setup_theme', 'fencer_setup');

/**
 * Enqueue scripts and styles
 */
function fencer_scripts() {
	global $theme_options;

	wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
	wp_enqueue_style( 'bootstrap' );

	wp_register_style('stylesheet', get_stylesheet_uri(), array(), '1.0', 'all');
	wp_enqueue_style( 'stylesheet' );

	wp_register_style('responsive', get_template_directory_uri() . '/responsive.css', '1.0', 'all');
	wp_enqueue_style( 'responsive' );
	
	wp_register_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.css');
	wp_register_style('flexslider', get_template_directory_uri() . '/css/flexslider.css');
	wp_enqueue_style( 'font-awesome' );
	wp_enqueue_style( 'flexslider' );	
	
	wp_register_script('fencer-bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '2.0', true);
	wp_register_script('fencer-appear', get_template_directory_uri() . '/js/jquery.appear.js', array(), '1.0', true);

	wp_register_script('fencer-easing', get_template_directory_uri() . '/js/easing.js', array(), '1.3', true);
	wp_register_script('fencer-flexslider', get_template_directory_uri() . '/js/jquery.flexslider-min.js', array(), '1.0', true);
	wp_register_script('fencer-template', get_template_directory_uri() . '/js/template.js', array(), '1.0', true);
	wp_register_script('fencer-twitter', get_template_directory_uri() . '/js/twitter/jquery.tweet.min.js', array(), '1.0', true);
	wp_register_script('fencer-countto', get_template_directory_uri() . '/js/jquery.countTo.js', array(), '1.0', true);
	wp_register_script('fencer-parallax', get_template_directory_uri() . '/js/jquery.parallax.js', array(), '1.1.3', true);
	wp_register_script('fencer-mixitup', get_template_directory_uri() . '/js/jquery.mixitup.min.js', array(), '1.5.5', true);
	
	wp_enqueue_script('fencer-script', get_template_directory_uri() . '/js/template.js', array('jquery', 'fencer-bootstrap', 'fencer-appear', 'fencer-easing', 'fencer-flexslider', 'fencer-twitter', 'fencer-countto', 'fencer-parallax', 'fencer-mixitup'), '1.0', true);
	
	if (is_singular() && comments_open() && get_option('thread_comments')) {
		wp_enqueue_script('comment-reply');
	}
}
add_action('wp_enqueue_scripts', 'fencer_scripts');

function fencer_html5_shim() {
    global $is_IE;
    if ( $is_IE ) {
        echo '<!--[if lt IE 9]>';
        echo '<script src="' . get_template_directory_uri() . '/js/html5shiv.js" type="text/javascript"></script>';
        echo '<![endif]-->';
    }
}
add_action( 'wp_head', 'fencer_html5_shim' );

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/theme-tags.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Load theme functions.
 */
require get_template_directory() . '/inc/theme-functions.php';

/**
 * Load theme metaboxes.
 */
require get_template_directory() . '/inc/theme-metaboxes.php';
