(function($){
$(document).ready(function() {

/**
*	Init
*/	
	//'use strict';
	
	//$('#navbar').scrollspy();
	if($('#wpadminbar').length > 0) {
		$('.navbar').css('margin-top', $('#wpadminbar').height() + 'px');
	}

	// Revolution slider fix for video stretch
	$('.rev_slider').on("revolution.slide.onvideoplay",function (e,data) {
		$(this).revredraw();
	});
	
	// Hide related works if no works exists
	if(!$('.related-works .portfolio-list .portfolio-item-block').length > 0) {
		$('.related-works').hide();
	}

	$('.skip-intro').appendTo($(".rev_slider_wrapper.fullscreen-container"));

	// Jumping top menu fix
    $('.navbar-inner').appear(function() {
      $('.navbar-inner').css("transition","all .6s");
      $('.navbar-inner').css("-webkit-transition","all .6s");
    });

	// WooCommerce fix
	$('.page-item-title h1').after($('.woocommerce-breadcrumb'));

	$('.products-slider .woocommerce').addClass("flexslider").removeClass("WooCommerce");

	$('.products-slider > div > div > div li').wrapInner( "<div class='product-item'></div>" );

	$('.products-slider .flexslider').flexslider({
              selector: "ul > li",
              directionNav: true,
              slideshow: true,
              controlNav: false,
              slideshowSpeed: 7000,
              animation: "slide",
              prevText: "",
              nextText: "",
              smoothHeight: true,
              itemWidth: 239,//239 or 159,
              minItems: 1
            });

/**
*	Scroll functions
*/
	$(window).scroll(function () {

		var scrollonscreen = $(window).scrollTop() + $(window).height();
		
		if(scrollonscreen - $(".rev_slider_wrapper.fullscreen-container").height()/2 > $(".rev_slider_wrapper.fullscreen-container").height()) {
			$(".navbar-inner").css("padding", "10px 0 0 0");
		}
		else
		{
			$(".navbar-inner").css("padding", "25px 0 15px");
		}
	
		// Scroll to top function
		if(scrollonscreen > $(window).height() + 350){
			$('#top-link').css("bottom", "25px");
		}
		else {
			$('#top-link').css("bottom", "-60px");
		}
	
	});

//scroll up event
$('#top-link').click(function(){
	$('body,html').stop().animate({
		scrollTop:0
	},800,'easeOutCubic')
	return false;
});

/**
*	Resize events
*/

	$(window).resize(function () {
		if($('#wpadminbar').length > 0) {
			$('.navbar').css('margin-top', $('#wpadminbar').height() + 'px');
		}
		
		updateFullwidthSections();
		$(".fullwidthbanner-container").css("height", $(window).height());
		
	});

/**
*	Portfolio
*/
	
	$(".portfolio-item-block").click(function() {
		
		var current_portfolio_item = this;
		
		window.location.href = $(this).attr('data-url');
	});

/**
*	Fullwidth sections
*/
updateFullwidthSections();

function updateFullwidthSections() {

	if($(window).width() > 979) {
		$('.fullwidth-section').each(function(){

			var $sidebar_width = 0;

			if($('.main-sidebar').length > 0) {
				$sidebar_width = $('.main-sidebar').width() + 30;
			}

			var $justOutOfSight = ((parseInt($('body').width()) - parseInt($('.container').css('width'))) / 2);
			var $marginleft = (-1)*$justOutOfSight - $sidebar_width;

			
			$(this).css({
				'margin-left': $marginleft,
				'padding-left': $justOutOfSight,
				'padding-right': $justOutOfSight
			});	


			$(this).width($(window).width() - $justOutOfSight * 2);

		});	

		$('.portfolio-item-image.fullwidth, .fullwidth-slider').each(function(){
				var $sidebar_width = 0;

				if($('.main-sidebar').length > 0) {
					$sidebar_width = $('.main-sidebar').width() + 30;
				}

				var $justOutOfSight = ((parseInt($('body').width()) - parseInt($('.container').css('width'))) / 2);
				var $marginleft = (-1)*$justOutOfSight - $sidebar_width;
		

				$(this).css({
					'margin-left': $marginleft
				});	

				$(this).width($(window).width());
		});
	
	} else {
		$('.fullwidth-section, .portfolio-item-image.fullwidth, .fullwidth-slider').width("auto");

		$('.fullwidth-section, .portfolio-item-image.fullwidth, .fullwidth-slider').css({
				'margin-left': 0,
				'padding-left': 10,
				'padding-right': 10
			});	
	}
}

/**
*	Other scripts
*/

	$(".intro-block").css("height", $(window).height());
	$(".fullwidthbanner-container").css("height", $(window).height());
	
	$(".navbar li:not(.blank-link, .menu-item-type-taxonomy) a").click(function() {
		
		if($(window).width() > 1023) {

			if($($(this).attr("href")).position())
			{
				$("html, body").animate({scrollTop: $($(this).attr("href")).position().top - $("#navbar").height()}, 500);
				$(".nav-collapse").css("height", 0);
				$(".nav-collapse").removeClass("in");
				return false;
			}
		}
		else {
			$(".nav-collapse").css("height", 0);
			$(".nav-collapse").removeClass("in");
		}
		
	});

	$('a.scroll-to, .scroll-to a').click(function(){
		if($($(this).attr("href")).position())
		{
			$("html, body").animate({scrollTop: $($(this).attr("href")).position().top - $("#navbar").height()}, 500);
			$(".nav-collapse").css("height", 0);
			$(".nav-collapse").removeClass("in");
			return false;
		}
	});
	
	$(".skip-intro").click(function() {
		$("html, body").animate({scrollTop: $(".rev_slider_wrapper.fullscreen-container").height() }, 500);
		return false;
	});
		 
/**
* Social share for portfolio items
*/
if( $('a.facebook-share').length > 0 || $('a.twitter-share').length > 0 || $('a.pinterest-share').length > 0) {
	
		// Facebook
		
		$.getJSON("http://graph.facebook.com/?id="+ window.location +'&callback=?', function(data) {
			if((data.shares != 0) && (data.shares != undefined) && (data.shares != null)) { 
				$('.facebook-share span.count').html( data.shares );	
			}
			else {
				$('.facebook-share span.count').html( 0 );	
			}
			
		});
	 
		function facebookShare(){
			window.open( 'https://www.facebook.com/sharer/sharer.php?u='+window.location, "facebookWindow", "height=380,width=660,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
			return false;
		}
		
		$('.facebook-share').click(facebookShare);
		
		// Twitter
		
		$.getJSON('http://urls.api.twitter.com/1/urls/count.json?url='+window.location+'&callback=?', function(data) {
			if((data.count != 0) && (data.count != undefined) && (data.count != null)) { 
				$('.twitter-share span.count').html( data.count );
			}
			else {
				$('.twitter-share span.count').html( 0 );
			}

		});
		
		function twitterShare(){
			if($(".section-title h1").length > 0) {
				var $page_title = encodeURIComponent($(".portfolio-item-title h1").text());
			} else {
				var $page_title = encodeURIComponent($(document).find("title").text());
			}
			window.open( 'http://twitter.com/intent/tweet?text='+$page_title +' '+window.location, "twitterWindow", "height=370,width=600,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
			return false;
		}
		
		$('.twitter-share').click(twitterShare);

		// Pinterest
		
		$.getJSON('http://api.pinterest.com/v1/urls/count.json?url='+window.location+'&callback=?', function(data) {
			if((data.count != 0) && (data.count != undefined) && (data.count != null)) { 
				$('.pinterest-share span.count').html( data.count );
			}
			else {
				$('.pinterest-share span.count').html( 0 );
			}
	
		});
		
		function pinterestShare(){
			var $sharingImg = $('.porftolio-slider li img').first().attr('src'); 
			
			if($(".section-title h1").length > 0) {
				var $page_title = encodeURIComponent($(".portfolio-item-title h1").text());
			} else {
				var $page_title = encodeURIComponent($(document).find("title").text());
			}
			
			window.open( 'http://pinterest.com/pin/create/button/?url='+window.location+'&media='+$sharingImg+'&description='+$page_title, "pinterestWindow", "height=620,width=600,resizable=0,toolbar=0,menubar=0,status=0,location=0,scrollbars=0" ) 
			return false;
		}

		$('.pinterest-share').click(pinterestShare);
		
	}
	
});
})(jQuery);