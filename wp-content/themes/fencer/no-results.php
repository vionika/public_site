<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Fencer
 */
?>

<div class="content-block">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="page-header-title"><?php _e( 'Nothing Found', 'fencer' ); ?></h1>
      </div>
    </div>
    <div class="row">
    	<article id="post-0" class="post no-results not-found">
			<div class="entry-content text-center">
				<?php if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

					<p><?php printf( __( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'fencer' ), esc_url( admin_url( 'post-new.php' ) ) ); ?></p>

				<?php elseif ( is_search() ) : ?>

					<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'fencer' ); ?></p>
					<?php get_search_form(); ?>

				<?php else : ?>

					<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'fencer' ); ?></p>
					<?php get_search_form(); ?>

				<?php endif; ?>
			</div><!-- .entry-content -->
		</article><!-- #post-0 .post .no-results .not-found -->
    </div>
    
  </div>
</div>