<?php

/**
 * SETTINGS TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Main Settings',
	'id' => 'main_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "main_settings"
);

$ipanel_fencer_option[] = array(
	"name" => "Show site preloader",
	"id" => "enable_theme_preloader",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If turned on you will see loading animation before site will show",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"name" => "GIF preloader image",
	"id" => "preloader_image",
	"field_options" => array(
		"std" => ''
	),
	"desc" => "You can upload your animated GIF image to use in site preloader instead of default CSS3 preloader animation. Remove image to use default preloader.",
	"type" => "qup",
);
$ipanel_fencer_option[] = array(
	"name" => "Enable Parallax effects for pages backgrounds and parallax blocks",
	"id" => "enable_parallax",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "You can turn on/off parallax effects for scrolling here",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"name" => "Enable fixed top menu",
	"id" => "enable_fixed_menu",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If turned on top menu will always on screen top in page scrolling",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Enable semi transparent top menu",
	"id" => "enable_transparent_menu",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If turned on top menu will have semi transparent background",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Semi transparent top menu background color",
	"id" => "transparent_menu_bg",
	"std" => "black",
	"options" => array(
		"white" => "White",
		"black" => "Black"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Top menu transparency level",
	"id" => "transparent_menu_level",
	"std" => 8,
	"field_options" => array(
		"min" => 0,
		"step" => 1,
		"max" => 10,
		"animation" => true,
	),
	"desc" => "10 - Most transparent, 0 - No transparent (will work only if semi transparent menu Enabled above)",
	"type" => "slider",
);

$ipanel_fencer_option[] = array(
	"name" => "Retina logo",
	"id" => "retina_logo",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Enable this option if you uploaded logo image in retina size (2x larger than you want to have your logo size to be).",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Logo width for retina image (px)",
	"id" => "retina_logo_width",
	"std" => "166",
	"desc" => "Specify retina logo image width in pixels if you use retina logo feature above",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Center logo position horizontally",
	"id" => "center_logo_position",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Move logo to new line before menu items and display it horizontally. Use if you have large logo or many menu items (more then 1 line)",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Animate template items",
	"id" => "enable_theme_animations",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If turned on all page items will slide to screen when you scroll page",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"name" => "Upload Favicon",
	"id" => "favicon_image",
	"field_options" => array(
		"std" => get_template_directory_uri().'/img/favicon.png'
	),
	"desc" => "Upload your site Favicon in PNG format (32x32px)",
	"type" => "qup",
);
$ipanel_fencer_option[] = array(
	"name" => "Show author info and avatar after blog posts",
	"id" => "enable_author_info",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * SLIDER TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Home Slider',
	'id' => 'header_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "header_settings"
);

$ipanel_fencer_option[] = array(
	"name" => "Disable Slider Screen",
	"id" => "disable_slider",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Check if you want to see site immidiately without slider screen",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * SIDEBAR TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Sidebar',
	'id' => 'sidebar_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "sidebar_settings"
);
$ipanel_fencer_option[] = array(
	"name" => "Pages sidebar position",
	"id" => "page_sidebar_position",
	"std" => "disable",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Blog page sidebar position",
	"id" => "blog_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Blog Archive page sidebar position",
	"id" => "archive_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Blog Search page sidebar position",
	"id" => "search_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Blog post sidebar position",
	"id" => "post_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio item page sidebar position",
	"id" => "portfolio_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "WooCommerce listing pages sidebar position",
	"id" => "shop_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "WooCommerce product page sidebar position",
	"id" => "product_sidebar_position",
	"std" => "right",
	"options" => array(
		"left" => "Left",
		"right" => "Right",
		"disable" => "Disable sidebar"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * TEAM TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Team',
	'id' => 'team_settings'
);
$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "team_settings"
);
$ipanel_fencer_option[] = array(
	"name" => "Enable team slider",
	"id" => "team_slider_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Team slider autoplay",
	"id" => "team_slider_autoplay",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Team slider changing speed",
	"id" => "team_slider_speed",
	"std" => 7000,
	"field_options" => array(
		"min" => 100,
		"step" => 100,
		"max" => 30000,
		"animation" => true,
	),
	"desc" => "Time between slides changes in miliseconds",
	"type" => "slider",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * PORTFOLIO TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Portfolio',
	'id' => 'portfolio_settings'
);
$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "portfolio_settings"
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio grid sort animation effect 1",
	"id" => "portfolio_posts_animation_1",
	"std" => "fade",
	"options" => array(
		"fade" => "Fade",
		"scale" => "Scale",
		"rotateZ" => "RotateZ",
		"rotateX" => "RotateX",
		"rotateY" => "RotateY",
		"blur" => "Blur",
		"grayscale" => "Grayscale"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio grid sort animation effect 2",
	"id" => "portfolio_posts_animation_2",
	"std" => "scale",
	"options" => array(
		"fade" => "Fade",
		"scale" => "Scale",
		"rotateZ" => "RotateZ",
		"rotateX" => "RotateX",
		"rotateY" => "RotateY",
		"blur" => "Blur",
		"grayscale" => "Grayscale"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio latest items limit (for onepage display)",
	"id" => "portfolio_posts_limit",
	"std" => "8",
	"desc" => "Recommended values: 4, 8, 12, 16, etc",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio items per row",
	"id" => "portfolio_element_width",
	"std" => "25",
	"options" => array(
		"20" => "5 per row",
		"25" => "4 per row",
		"33.3" => "3 per row"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio item images Slider animation",
	"id" => "portfolio_slider_animation",
	"std" => "fade",
	"options" => array(
		"fade" => "Fade",
		"slide" => "Slide"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Display portfolio item images slider prev/next navigation buttons",
	"id" => "portfolio_show_slider_navigation",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio item images slider autoplay",
	"id" => "portfolio_slider_autoplay",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio item images slider changing speed",
	"id" => "portfolio_slider_speed",
	"std" => 7000,
	"field_options" => array(
		"min" => 100,
		"step" => 100,
		"max" => 30000,
		"animation" => true,
	),
	"desc" => "Time between slides changes in miliseconds",
	"type" => "slider",
);
$ipanel_fencer_option[] = array(
	"name" => "Show related works on portfolio items page",
	"id" => "portfolio_related_works",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "This will show works from the same categories",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio item page related works limit",
	"id" => "portfolio_related_limit",
	"std" => "8",
	"desc" => "Recommended values: 4, 8, 12, 16, etc",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * LATEST POSTS TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Latest posts',
	'id' => 'latestposts_settings'
);
$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "latestposts_settings"
);
$ipanel_fencer_option[] = array(
	"name" => "Latest posts items limit (for onepage display)",
	"id" => "latestposts_posts_limit",
	"std" => "9",
	"desc" => "Recommended values: 3, 6, 9, etc",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Enable latest posts slider",
	"id" => "latestposts_slider_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Latest posts slider autoplay",
	"id" => "latestposts_slider_autoplay",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Latest posts slider changing speed",
	"id" => "latestposts_slider_speed",
	"std" => 7000,
	"field_options" => array(
		"min" => 100,
		"step" => 100,
		"max" => 30000,
		"animation" => true,
	),
	"desc" => "Time between slides changes in miliseconds",
	"type" => "slider",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * WOOCOMMERCE TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Woocommerce',
	'id' => 'shop_settings'
);
$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "shop_settings"
);
$ipanel_fencer_option[] = array(
	"name" => "Show social share buttons and counters on product pages",
	"id" => "shop_social_buttons_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * FOOTER TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Footer',
	'id' => 'footer_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "footer_settings"
);

$ipanel_fencer_option[] = array(
	
	"name" => "Social icons",   
	"type" => "StartSection",
	"field_options" => array(
		"show" => true // Set true to show items by default.
	)
);
$ipanel_fencer_option[] = array(
	"type" => "info",
	"name" => "Leave URL fields blank to hide this social icons",
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_fencer_option[] = array(
	"name" => "Facebook Page url",
	"id" => "facebook",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Vkontakte page url",
	"id" => "vk",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Twitter Page url",
	"id" => "twitter",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Google+ Page url",
	"id" => "google-plus",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "LinkedIn Page url",
	"id" => "linkedin",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Instagram Page url",
	"id" => "instagram",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Dribbble page url",
	"id" => "dribbble",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Tumblr page url",
	"id" => "tumblr",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Pinterest page url",
	"id" => "pinterest",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Vimeo page url",
	"id" => "vimeo-square",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "YouTube page url",
	"id" => "youtube",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
	"name" => "Skype url",
	"id" => "skype",
	"std" => "",
	"desc" => "",
	"type" => "text",
);
$ipanel_fencer_option[] = array(
		"type" => "EndSection"
);

$ipanel_fencer_option[] = array(
	"name" => "Footer copyright text",
	"id" => "footer_copyright_editor",
	"std" => "Powered by <a href='http://themeforest.net/user/dedalx/' target='_blank'>Fencer - Premium Wordpress Theme</a>",
	"desc" => "This is a sample of wordpress wysiwyg editor.",
	"field_options" => array(
		'media_buttons' => false
	),
	"type" => "wp_editor",
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * TWITTER TAB
 **/
$ipanel_fencer_tabs[] = array(
	'name' => 'Twitter',
	'id' => 'twitter_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "twitter_settings"
);
$ipanel_fencer_option[] = array(
	"type" => "info",
	"name" => "IMPORTANT: You must put your Twitter API credentials to /wp-content/themes/fencer/js/twitter/index.php file by theme documentation to turn on twitter feeds display.",
	"field_options" => array(
		"style" => 'alert'
	)
);
$ipanel_fencer_option[] = array(
	"name" => "Display Twitter tweets slider in footer",
	"id" => "footer_show_twitter",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If you don't want to see twitter slider in footer disable this option",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Display Twitter tweets slider only on homepage (onepage template)",
	"id" => "footer_show_twitter_homepage_only",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "If you don't want to see twitter slider in other pages",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"name" => "Twitter username",
	"id" => "footer_twitter_username",
	"std" => "",
	"desc" => "Input your twitter username without @",
	"type" => "text",
);

$ipanel_fencer_option[] = array(
	"name" => "Twitter tweets count",
	"id" => "footer_twitter_tweets_count",
	"std" => 10,
	"field_options" => array(
		"min" => 1,
		"step" => 1,
		"max" => 50,
		"animation" => true,
	),
	"desc" => "Limit for tweets in Twitter feed",
	"type" => "slider",
);

$ipanel_fencer_option[] = array(
	"name" => "Twitter slider autoplay",
	"id" => "footer_twitter_autoplay",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);

$ipanel_fencer_option[] = array(
	"name" => "Twitter slider changing speed",
	"id" => "footer_twitter_speed",
	"std" => 7000,
	"field_options" => array(
		"min" => 100,
		"step" => 100,
		"max" => 30000,
		"animation" => true,
	),
	"desc" => "Time between slides changes in miliseconds",
	"type" => "slider",
);

$ipanel_fencer_option[] = array(
	"name" => "Twitter Slider animation",
	"id" => "footer_twitter_animation",
	"std" => "fade",
	"options" => array(
		"fade" => "Fade",
		"slide" => "Slide"
	),
	"desc" => "",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Display Twitter slider prev/next navigation buttons",
	"id" => "footer_show_twitter_navigation",
	"std" => false,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);
/**
 * FONTS TAB
 **/

$ipanel_fencer_tabs[] = array(
	'name' => 'Fonts',
	'id' => 'font_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "font_settings"
);

$ipanel_fencer_option[] = array(
	"name" => "Headers font",
	"id" => "header_font",
	"desc" => "Font used in headers",
	"options" => array(
		"font-sizes" => array(
			" " => "Font Size",
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px',
			'28' => '28px',
			'29' => '29px',
			'30' => '30px',
			'31' => '31px',
			'32' => '32px',
			'33' => '33px',
			'34' => '34px',
			'35' => '35px',
			'36' => '36px',
			'37' => '37px',
			'38' => '38px',
			'39' => '39px',
			'40' => '40px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '30',
		"font-family" => 'PT+Sans'
	),
	"type" => "typography"
);

$ipanel_fencer_option[] = array(
	"name" => "Body font",
	"id" => "body_font",
	"desc" => "Font used in other elements",
	"options" => array(
		"font-sizes" => array(
			" " => "Font Size",
			'11' => '11px',
			'12' => '12px',
			'13' => '13px',
			'14' => '14px',
			'15' => '15px',
			'16' => '16px',
			'17' => '17px',
			'18' => '18px',
			'19' => '19px',
			'20' => '20px',
			'21' => '21px',
			'22' => '22px',
			'23' => '23px',
			'24' => '24px',
			'25' => '25px',
			'26' => '26px',
			'27' => '27px'
		),
		"color" => false,
		"font-families" => iPanel::getGoogleFonts(),
		"font-styles" => false
	),
	"std" => array(
		"font-size" => '14',
		"font-family" => 'Ubuntu'
	),
	"type" => "typography"
);
$ipanel_fencer_option[] = array(
	"name" => "Enable cyrillic support for Google Fonts",
	"id" => "font_cyrillic_enable",
	"std" => true,
	"field_options" => array(
		"box_label" => "Check Me!"
	),
	"desc" => "Use this if you use Russian Language on your site. Please note that not all Google Fonts support cyrilic.",
	"type" => "checkbox",
);
$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * COLORS TAB
 **/

$ipanel_fencer_tabs[] = array(
	'name' => 'Colors',
	'id' => 'color_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "color_settings",
);
$ipanel_fencer_option[] = array(
	"name" => "Predefined color skins",
	"id" => "color_skin_name",
	"std" => "none",
	"options" => array(
		"none" => "Use colors specified below",
		"default" => "Fencer (Default)",
		"green" => "Green Grass",
		"blue" => "Blue Ocean",
		"red" => "Red Sun",
		"black" => "Black and White",
		"pink" => "Pinky",
		"orange" => "Orange Juice",
		"simplegreat" => "SimpleGreat",
		"perfectum" => "Perfectum",
		"metro" => "Metro"
	),
	"desc" => "Select one of predefined skins",
	"type" => "select",
);
$ipanel_fencer_option[] = array(
	"name" => "Body background color",
	"id" => "theme_body_color",
	"std" => "#F1F3F4",
	"desc" => "Body background color, default: #F1F3F4",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Theme preloader background color",
	"id" => "theme_preloader_bg_color",
	"std" => "#FFFFFF",
	"desc" => "Preloader animation background color, default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Main theme color",
	"id" => "theme_main_color",
	"std" => "#26CDB3",
	"desc" => "Used in many theme places, default: #26CDB3",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_fencer_option[] = array(
	"name" => "Buttons hover background color",
	"id" => "theme_button_hover_color",
	"std" => "#000000",
	"desc" => "Used for some of theme buttons hover, default: #000000",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_fencer_option[] = array(
	"name" => "Top menu background color",
	"id" => "theme_menu_color",
	"std" => "#000000",
	"desc" => "Default: #000000",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_fencer_option[] = array(
	"name" => "Top menu links color",
	"id" => "theme_menu_link_color",
	"std" => "#717171",
	"desc" => "Default: #717171",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_fencer_option[] = array(
	"name" => "Top menu submenu links color",
	"id" => "theme_menu_submenu_link_color",
	"std" => "#FFFFFF",
	"desc" => "Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Footer background color",
	"id" => "theme_footer_color",
	"std" => "#FFFFFF",
	"desc" => "Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Pages title color",
	"id" => "theme_title_color",
	"std" => "#000000",
	"desc" => "Default: #000000",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Pages title background color",
	"id" => "theme_title_bg_color",
	"std" => "#FFFFFF",
	"desc" => "Default: #FFFFFF",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Portfolio grid item hover background color",
	"id" => "theme_portfolio_hover_color",
	"std" => "#313a3d",
	"desc" => "Default: #313A3D",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Dark onepage pages background color",
	"id" => "theme_invert_bg_color",
	"std" => "#1D262A",
	"desc" => "Default: #1D262A",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);
$ipanel_fencer_option[] = array(
	"name" => "Widgets title color",
	"id" => "theme_widget_title_color",
	"std" => "#A3A3A4",
	"desc" => "Default: #A3A3A4",
	"field_options" => array(
		//'desc_in_tooltip' => true
	),
	"type" => "color",
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * CUSTOM CODE TAB
 **/

$ipanel_fencer_tabs[] = array(
	'name' => 'Custom code',
	'id' => 'custom_code'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "custom_code",
);

$ipanel_fencer_option[] = array(
	"name" => "Custom JavaScript code",
	"id" => "custom_js_code",
	"std" => '',
	"field_options" => array(
		"language" => "javascript",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => "This code will run in header",
	"type" => "code",
);

$ipanel_fencer_option[] = array(
	"name" => "Custom CSS styles",
	"id" => "custom_css_code",
	"std" => '',
	"field_options" => array(
		"language" => "json",
		"line_numbers" => true,
		"autoCloseBrackets" => true,
		"autoCloseTags" => true
	),
	"desc" => "This CSS code will be included in header",
	"type" => "code",
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * EXPORT TAB
 **/

$ipanel_fencer_tabs[] = array(
	'name' => 'Export',
	'id' => 'export_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "export_settings"
);
	
$ipanel_fencer_option[] = array(
	"name" => "Export with Download Possibility",
	"type" => "export",
	"desc" => "This is a sample of export field."
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * IMPORT TAB
 **/

$ipanel_fencer_tabs[] = array(
	'name' => 'Import',
	'id' => 'import_settings'
);

$ipanel_fencer_option[] = array(
	"type" => "StartTab",
	"id" => "import_settings"
);

$ipanel_fencer_option[] = array(
	"name" => "Import",
	"type" => "import",
	"desc" => "This is a sample of import field."
);

$ipanel_fencer_option[] = array(
	"type" => "EndTab"
);

/**
 * CONFIGURATION
 **/

$ipanel_configs = array(
	'ID'=> 'FENCER_PANEL', 
	'menu'=> 
		array(
			'submenu' => false,
			'page_title' => __('Fencer Control Panel', 'fencer'),
			'menu_title' => __('Fencer Control Panel', 'fencer'),
			'capability' => 'manage_options',
			'menu_slug' => 'manage_theme_options',
			'icon_url' => IPANEL_URI . 'assets/img/panel-icon.png',
			'position' => 58
		),
	'rtl' => ( function_exists('is_rtl') && is_rtl() ),
	'tabs' => $ipanel_fencer_tabs,
	'fields' => $ipanel_fencer_option,
	'download_capability' => 'manage_options',
	'live_preview' => site_url()
);

$ipanel_theme_usage = new IPANEL( $ipanel_configs );
	