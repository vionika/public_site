<?php
/*
Template Name: Portfolio
*/
if(!is_page_template('page-onepage.php')&&(!isset($related_filter))) {
get_header(); 

} ?>
<?php
global $theme_options;
global $portfolio_posts_limit;
global $related_filter;

$show_view_all_button = false;

if(!isset($theme_options['portfolio_posts_limit'])||($theme_options['portfolio_posts_limit'])=='') {
  $theme_options['portfolio_posts_limit'] = 8;
}

// Onepage display
if(!isset($portfolio_posts_limit)) {
  $portfolio_posts_limit = $theme_options['portfolio_posts_limit'];
  $show_view_all_button = true;
}

// Single item page related works
if(isset($related_filter)) {
  $portfolio_posts_limit = $theme_options['portfolio_related_limit'];
}

$all_terms = get_terms( 'portfolio_filter' );

?>
<?php if((count($all_terms) > 1) && (!isset($related_filter))): ?>
<div class="row portfolio-filter left">
<div class="col-md-12 text-center">
<a class="filter" data-filter="all">
<?php 
if($show_view_all_button) {
  _e('Recent', 'fencer'); 
}
else {
  _e('All', 'fencer'); 
}
?></a>
<?php 
foreach ( $all_terms as $term ) {
  echo '<a class="filter" data-filter="'.$term->slug.'">'.$term->name.'</a> ';
}
?> 
<?php if($show_view_all_button):?>
  <a class="filter view-all" href="<?php echo get_post_type_archive_link('portfolio'); ?>"><?php _e('View all', 'fencer'); ?></a>
<?php endif; ?>
</div>
</div>
<?php endif; ?>

<div class="portfolio-list bottom clearfix" id="portfolio-list">

<?php

$temp = $wp_query;
$wp_query = null;

$data_item = 0;

if(!isset($related_filter))
{
  $wp_query = new WP_Query(array(
    'post_type' => 'portfolio',
    'posts_per_page' => $portfolio_posts_limit,
    'orderby'=> 'date'
  ));
}
else
{
  $exclude_ids = array( get_the_ID() );

  $wp_query = new WP_Query(array(
    'post_type' => 'portfolio',
    'portfolio_filter' => $related_filter,
    'posts_per_page' => $portfolio_posts_limit,
    'orderby'=> 'date',
    'post__not_in' => $exclude_ids
  ));
}

$page_slug = get_site_url().'/#'.$post->post_name;

while ($wp_query->have_posts()) : $wp_query->the_post();
  
  $data_item++;
  
  $portfolio_small = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'portfolio-small' );
  $portfolio_large = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'portfolio-large' );
  $portfolio_brand = get_post_meta( $post->ID, '_portfolio_brand_value', true );
  $portfolio_brand_url = get_post_meta( $post->ID, '_portfolio_brand_url_value', true );
  $portfolio_url = get_post_meta( $post->ID, '_portfolio_url_value', true );

  $terms = get_the_terms( $post->ID , 'portfolio_filter' );

  $terms_count = count($terms);

  $terms_counter = 0;

  $terms_slug = '';

  $categories_html = '';

  foreach ( $terms ? $terms: array() as $term ) {

    if($terms_counter  < $terms_count - 1) {
      $categories_html.= $term->name.' / ';
    }
    else
    {
      $categories_html .= $term->name;
    }
    
    $terms_slug .= ' '.$term->slug;

    $terms_counter++;
  }
  
  $portfolio_images = array();
  $portfolio_images = get_images_src('portfolio-large',false, $post->ID);

?>
<div class="portfolio-item-block mix<?php echo $terms_slug; ?>" data-item="<?php echo $data_item; ?>" data-name="<?php the_title(); ?>" data-url="<?php echo get_permalink( $post->ID ); ?>">
  <div class="info"><img src="<?php echo $portfolio_small[0]; ?>" alt="">
    <h4 class="title"><?php the_title(); ?></h4>
    <span class="sub-title"><?php echo $categories_html; ?></span>
  </div>

</div>

<?php 
if($data_item == 3){
  $data_item = 0;
}

endwhile; // end of the loop. 
?>
         
<?php $wp_query = null; $wp_query = $temp;?>

</div>


<?php if(!is_page_template('page-onepage.php')&&(!isset($related_filter))) get_footer(); ?>