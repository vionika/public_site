<?php
/*
Template Name: Onepage
*/
?>

<?php get_header(); ?>

<?php
    global $theme_options;

	$locations = get_nav_menu_locations();
    $args = array(
        'order'                  => 'ASC',
        'orderby'                => 'menu_order',
        'post_type'              => 'nav_menu_item',
        'post_status'            => 'publish',
        'output'                 => ARRAY_A,
        'output_key'             => 'menu_order',
        'nopaging'               => true,
        'update_post_term_cache' => false );

    if(is_array($locations) && isset($locations["primary"])) {
        $items = wp_get_nav_menu_items( $locations["primary"], $args );
    } else {
        $items = Array();
    }

    if ($items) {
        ob_start();
        $i = 0;
        foreach ($items as $item) {

            // Don't show custom links as pages
            //if($item->object <> 'custom')

            $post = get_post($item->object_id);
            if ($i !== 0) {
                if($item->type <> 'taxonomy') {
                    $page_openinnewwindow = get_post_meta( $post->ID, '_page_openinnewwindow_value', true );
                    $page_notdisplaytitle = get_post_meta( $post->ID, '_page_notdisplaytitle_value', true );
                    $page_fullwidth = get_post_meta( $post->ID, '_page_fullwidth_value', true );
                    $page_blackbg = get_post_meta( $post->ID, '_page_blackbg_value', true );
                }
                else {
                    $page_openinnewwindow = true;
                    $page_notdisplayinmenu = false;
                    $page_fullwidth = false;
                    $page_blackbg = false;
               }

                if($page_fullwidth) {
                    $containerclass = 'container-fluid';
                }
                else {
                    $containerclass = 'container';
                }

                if($page_blackbg) {
                    $contentclass = 'invert';
                }
                else {
                    $contentclass = '';
                }

                if (($item->object !== 'custom' && $item->object !== 'post' && $item->object !== 'category')&&(!$page_openinnewwindow)) {
                    $sub_title = get_post_meta( $post->ID, '_page_subtitle_value', true );
                    $page_class = get_post_meta( $post->ID, '_page_class_value', true );
                    $page_image = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full');

                    $page_bgcolor = get_post_meta( $post->ID, '_page_bgcolor_value', true );
                
                    $page_bgcolor_css = '';

                    if(isset($page_bgcolor)&&($page_bgcolor<>'')) {
                        $page_bgcolor_css = 'background-color: '.$page_bgcolor;
                    }
                    else
                    {
                        $page_bgcolor_css = '';
                    }
                    
                    $parallax = '';

                    if(is_array($page_image) && isset($page_image[0])) { 

                        $parallax = 'parallax ';

                        if($page_bgcolor_css <> '')
                        {
                            $page_bgcolor_css .= ';background-image: url('.$page_image[0].')'; 
                        }
                        else
                        {
                            $page_bgcolor_css = 'background-image: url('.$page_image[0].')'; 
                        }
                        
                    }
                ?>

                <div id="<?php echo $post->post_name; ?>" class="<?php echo $parallax; ?>content-block <?php echo $page_class; ?> <?php echo $contentclass; ?>" <?php if($page_bgcolor_css<>'') { echo 'style="'.$page_bgcolor_css.'"'; }; ?>>
                    <div class="<?php echo $containerclass; ?>">
                    <?php if(!$page_notdisplaytitle): ?>
                        <div class="row-fluid clearfix">
                            <div class="col-md-12">
                                <h1 class="page-header-title left"><?php echo $post->post_title; ?></h1>
                                
                            </div>
                        </div>
                    <?php endif; ?>
                                <?php echo str_replace(']]>', ']]&gt;', apply_filters('the_content', $post->post_content)); ?>
                            
                        <?php 
                            
                        	$current_template = basename(get_page_template(),".php");
                        	if ( 'page-onepage' !== $current_template )
                        	{
                        		get_template_part( $current_template );
                        	}
		                ?>
                    </div>
                </div>
                
                <?php
                }
            } else {
                if ($item->classes[0] !== 'page') {
                ?>
                <?php
                if(isset($theme_options['disable_slider']) && !$theme_options['disable_slider']):
                ?>
                <div id="<?php echo $post->post_name; ?>" >
                <?php putRevSlider( "homepageslider" ) ?>
                <a href="#" class="skip-intro"><div class="floater"></div></a>
                </div>
                <?php endif; ?>
                <?php
                }
            }
        $i++;
        }
    
        $page_template = ob_get_contents();
        ob_end_clean();
        echo $page_template;
    }
?>

<?php get_footer(); ?>