<?php
/*
Template Name: Latest Posts
*/
?>
<?php 

global $theme_options;
if(!isset($theme_options['latestposts_posts_limit'])) {
  $theme_options['latestposts_posts_limit'] = 6;
}
?>
<div class="row bottom latest-posts-list bottom">
<ul class="slides">
<?php
$temp = $wp_query;
$wp_query = null;

$wp_query = new WP_Query(array(
  'post_type' => 'post',
  'tax_query' => array(
        array(                
            'taxonomy' => 'post_format',
            'field' => 'slug',
            'terms' => array( 
                'post-format-aside',
                'post-format-audio',
                'post-format-chat',
                'post-format-gallery',
                'post-format-image',
                'post-format-link',
                'post-format-quote',
                'post-format-status',
                'post-format-video'
            ),
            'operator' => 'NOT IN'
        )
  ),
  'posts_per_page' => $theme_options['latestposts_posts_limit'],
  'orderby'=> 'date'
));

$post_count = 0;

while ($wp_query->have_posts()) : $wp_query->the_post();
  $post_count ++;
  $related_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'latest-post' );

?>
<li>
<div class="post-item">
  <div class="post-item-content">
    <?php if($related_thumb[0]): ?>
    <div class="image">
      <a href="<?php echo get_permalink( $post->ID ); ?>"><img src="<?php echo $related_thumb[0]; ?>" alt="<?php the_title(); ?>" /></a>
    </div>
    <?php endif; ?>
    <div class="post-item-info">
      <a href="<?php echo get_permalink( $post->ID ); ?>"><h4 class="title"><?php the_title(); ?></h4></a>
      <div class="date"><?php the_time(get_option('date_format')); ?>
      <?php if(get_comments_number() > 0): ?>
       | <span><a href="<?php comments_link(); ?>"><?php comments_number('', '1 comment', '% comments' );?></a></span>
      <?php endif; ?>
      </div>
    </div>
    <div class="post-item-details">
      <div class="description"><?php the_excerpt(); ?></div>
    </div>
  </div>
</div>
</li>
<?php 

endwhile; 
// end of the loop. ?>

<?php if($post_count < 4): ?>
  <style>
  @media (min-width: 979px) {
    .latest-posts-list .flex-direction-nav a {
      display: none;
    }
  }
  </style>
<?php endif; ?>

<?php $wp_query = null; $wp_query = $temp;?>
</ul>
</div>
    