<?php
/*
Template Name: Team
*/
if(!is_page_template('page-onepage.php')) {
get_header(); 
?>
<header>
<div class="container-fluid page-item-title text-center">
  <h1><?php the_title(); ?></h1>
</div>
</header>
<?php } ?>
<div class="row bottom members-list bottom">
<ul class="slides">
<?php
$temp = $wp_query;
$wp_query = null;

$wp_query = new WP_Query(array(
  'post_type' => 'team',
  'posts_per_page' => 10000,
  'orderby'=> 'date'
));

$post_count = 0;

while ($wp_query->have_posts()) : $wp_query->the_post();
  $post_count ++;
  $related_thumb = wp_get_attachment_image_src( get_post_thumbnail_id(get_the_ID()), 'team-member' );
  $sub_title = get_post_meta( $post->ID, '_team_subtitle_value', true );
?>
<li>
<div class="members-item">
  <div class="members-item-content">
    <div class="image">
      <img src="<?php echo $related_thumb[0]; ?>" alt="<?php the_title(); ?>" />
    </div>
    <div class="members-item-info">
      <h4 class="title"><?php the_title(); ?></h4>
      <span class="sub-title"><?php echo $sub_title; ?></span>
    </div>
    <div class="members-item-details">
      <div class="description"><?php the_content(); ?></div>
      <ul class="social unstyled">
        <?php 
        $social_links_arr = array( 'facebook', 'twitter', 'google-plus', 'linkedin', 'tumblr', 'dribbble' );

        foreach ( $social_links_arr as $social_link ) {
          if ( get_post_meta($post->ID, '_team_social_'.$social_link.'_value', true) ) {
            echo '<li><a href="'.get_post_meta( $post->ID, '_team_social_'.$social_link.'_value', true ).'" target="_blank"><i class="fa fa-'.$social_link.'"></i></a></li>';
          }
        }
        ?>
      </ul>
      <div class="clearfix"></div>
    </div>

  </div>
</div>
</li>
<?php 

endwhile; 
// end of the loop. ?>

<?php if($post_count < 4): ?>
  <style>
  @media (min-width: 979px) {
    .members-list .flex-direction-nav a {
      display: none;
    }
  }
  </style>
<?php endif; ?>

<?php $wp_query = null; $wp_query = $temp;?>
</ul>
</div>
<?php if(!is_page_template('page-onepage.php')) get_footer(); ?>
