<?php
/*
	Portfolio page archive
*/
get_header(); 

?>
<div class="container-fluid page-item-title text-center">
	<h1><?php _e('Portfolio', 'fencer'); ?></h1>
</div>
<?php 

$portfolio_posts_limit = 100000;

get_template_part( 'page-portfolio' ); 

?>

<?php get_footer(); ?>