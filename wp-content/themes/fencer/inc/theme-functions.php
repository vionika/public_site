<?php
/** 
 * Plugin recomendations
 **/

require_once ('class-tgm-plugin-activation.php');

add_action( 'tgmpa_register', 'fencer_register_required_plugins' );

function fencer_register_required_plugins() {

    /**
     * Array of plugin arrays. Required keys are name and slug.
     * If the source is NOT from the .org repo, then source is also required.
     */
    $plugins = array(
        
        array(
            'name'                  => 'FENCER Custom Post Types', // The plugin name
            'slug'                  => 'fencer-post-types', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/fencer-post-types.zip', // The plugin source
            'required'              => true, // If false, the plugin is only 'recommended' instead of required
            'version'               => '2.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'FENCER Multi Image Uploader', // The plugin name
            'slug'                  => 'multi-image-metabox', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/multi-image-metabox.zip', // The plugin source
            'required'              => true, // If false, the plugin is only 'recommended' instead of required
            'version'               => '1.3.4', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'FENCER Visual Page Builder', // The plugin name
            'slug'                  => 'js_composer', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/js_composer.zip', // The plugin source
            'required'              => true, // If false, the plugin is only 'recommended' instead of required
            'version'               => '4.7.4', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'FENCER Translation Manager', // The plugin name
            'slug'                  => 'loco-translate', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/loco-translate.1.4.5.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '1.4.5', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'Revolution Slider', // The plugin name
            'slug'                  => 'revslider', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/revslider.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '4.6', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),

        array(
            'name'                  => 'WooCommerce', // The plugin name
            'slug'                  => 'woocommerce', // The plugin slug (typically the folder name)
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
        ),

        array(
            'name'                  => 'PrettyPhoto Lightbox', // The plugin name
            'slug'                  => 'prettyphoto', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/prettyphoto.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '1.2', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),


        array(
            'name'                  => 'Contact Form 7', // The plugin name
            'slug'                  => 'contact-form-7', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/contact-form-7.3.8.1.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '3.8.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),
        
        array(
            'name'                  => 'Regenerate Thumbnails', // The plugin name
            'slug'                  => 'regenerate-thumbnails', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/regenerate-thumbnails.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '2.2.4', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        ),
        
        array(
            'name'                  => 'Simple WP Retina', // The plugin name
            'slug'                  => 'simple-wp-retina', // The plugin slug (typically the folder name)
            'source'                => get_stylesheet_directory() . '/inc/plugins/simple-wp-retina.1.1.1.zip', // The plugin source
            'required'              => false, // If false, the plugin is only 'recommended' instead of required
            'version'               => '1.1.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
            'force_activation'      => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
            'force_deactivation'    => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
            'external_url'          => '', // If set, overrides default API URL and points to an external URL
        )

    );

    /**
     * Array of configuration settings. Amend each line as needed.
     * If you want the default strings to be available under your own theme domain,
     * leave the strings uncommented.
     * Some of the strings are added into a sprintf, so see the comments at the
     * end of each line for what each argument will be.
     */
    $config = array(
        'domain'            => 'fencer',           // Text domain - likely want to be the same as your theme.
        'default_path'      => '',                          // Default absolute path to pre-packaged plugins
        'parent_menu_slug'  => 'themes.php',                // Default parent menu slug
        'parent_url_slug'   => 'themes.php',                // Default parent URL slug
        'menu'              => 'install-required-plugins',  // Menu slug
        'has_notices'       => true,                        // Show admin notices or not
        'is_automatic'      => false,                       // Automatically activate plugins after installation or not
        'message'           => '',                          // Message to output right before the plugins table
        'strings'           => array(
            'page_title'                                => __( 'Install Required Plugins', 'fencer' ),
            'menu_title'                                => __( 'Install Plugins', 'fencer' ),
            'installing'                                => __( 'Installing Plugin: %s', 'fencer' ), // %1$s = plugin name
            'oops'                                      => __( 'Something went wrong with the plugin API.', 'fencer' ),
            'notice_can_install_required'               => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_install_recommended'            => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_install'                     => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.' ), // %1$s = plugin name(s)
            'notice_can_activate_required'              => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_can_activate_recommended'           => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_activate'                    => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.' ), // %1$s = plugin name(s)
            'notice_ask_to_update'                      => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.' ), // %1$s = plugin name(s)
            'notice_cannot_update'                      => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.' ), // %1$s = plugin name(s)
            'install_link'                              => _n_noop( 'Begin installing plugin', 'Begin installing plugins' ),
            'activate_link'                             => _n_noop( 'Activate installed plugin', 'Activate installed plugins' ),
            'return'                                    => __( 'Return to Required Plugins Installer', 'fencer' ),
            'plugin_activated'                          => __( 'Plugin activated successfully.', 'fencer' ),
            'complete'                                  => __( 'All plugins installed and activated successfully. %s', 'fencer' ), // %1$s = dashboard link
            'nag_type'                                  => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );

    tgmpa( $plugins, $config );

}

register_sidebar(
  array(
    'name' => __( 'Left/Right sidebar', 'fencer' ),
    'id' => 'main-sidebar',
    'description' => __( 'Widgets in this area will be shown in the left or right site column.', 'fencer' )
  )
);

register_sidebar(
  array(
    'name' => __( 'Woocommerce sidebar', 'fencer' ),
    'id' => 'shop-sidebar',
    'description' => __( 'Widgets in this area will be shown in the left or right column on shop pages.', 'fencer' )
  )
);

register_sidebar(
  array(
    'name' => __( 'Footer sidebar', 'fencer' ),
    'id' => 'footer-sidebar',
    'description' => __( 'Widgets in this area will be shown in site footer.', 'fencer' )
  )
);

// Customisation Menu Link
class description_walker extends Walker_Nav_Menu{
      function start_el(&$output, $item, $depth = 0, $args = Array(), $current_object_id = 0 ){
           global $wp_query;
           $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';
           $class_names = $value = '';
           $classes = empty( $item->classes ) ? array() : (array) $item->classes;
           $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
          
           $add_class = '';
           
           $post = get_post($item->object_id);

           if($item->type <> 'taxonomy') {
               $page_openinnewwindow = get_post_meta( $post->ID, '_page_openinnewwindow_value', true );

               if($page_openinnewwindow) {
                    $add_class = 'blank-link';
               }
                
               $page_notdisplayinmenu = get_post_meta( $post->ID, '_page_notdisplayinmenu_value', true );
           }
           else {
               $page_openinnewwindow = true;
               $page_notdisplayinmenu = false;
           }
          

           // Don't display page in menu
           if(!$page_notdisplayinmenu)
           {
               $class_names = ' class="'.$add_class.' '. esc_attr( $class_names ) . '"';
               $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';
               $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
               $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
               $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';

               if($item->object == 'page' && $item->classes[0] !== 'page')
               {
                    $varpost = get_post($item->object_id);

                    if($page_openinnewwindow) {
                        $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
                    } else {
                        if(is_front_page()){
                          $attributes .= ' href="#' . $varpost->post_name . '"';
                        }else{

                            if (preg_replace('/^[^:]+:\/\/[^\/]+\/?|^\/|\/$/', '', get_home_url()) == preg_replace('/^\/|\/$/', '', $_SERVER['REQUEST_URI'])) {
                                $attributes .= ' href="#' . $varpost->post_name . '"';
                            } else {
                                $attributes .= ' href="'.home_url().'/#' . $varpost->post_name . '"';
                            }
                        }
                    }

                    
               }
               else
                    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

                if (is_object($args)) {
                    $item_output = $args->before;
                    $item_output .= '<a'. $attributes .'>';
                    $item_output .= $args->link_before . apply_filters( 'the_title', $item->title, $item->ID );
                    $item_output .= $args->link_after;
                    $item_output .= '</a>';
                    $item_output .= $args->after;
                    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );

                    
                }
                
           }
      // LI fix
                $output = str_replace("</li>
</li>", "</li>", $output);
     
     }
}

/** 
* Favicon, Google Fonts and custom styles
**/
function fencer_js_settings() {
    global $theme_options;

    if(!isset($theme_options['portfolio_posts_animation_1'])) {
      $theme_options['portfolio_posts_animation_1'] = 'fade';
    }

    if(!isset($theme_options['portfolio_posts_animation_2'])) {
      $theme_options['portfolio_posts_animation_2'] = 'scale';
    }
    // team slider vats
    if(isset($theme_options['team_slider_autoplay']) && ($theme_options['team_slider_autoplay'])) {
      $team_slider_autoplay = 'true';
    } else {
      $team_slider_autoplay = 'false';
    }

    if(!isset($theme_options['team_slider_speed'])) {
      $team_slider_speed = 7000;
    }
    else {
      $team_slider_speed = $theme_options['team_slider_speed'];
    }
    // recent posts slider vars
    if(isset($theme_options['latestposts_slider_autoplay']) && ($theme_options['latestposts_slider_autoplay'])) {
      $latestposts_slider_autoplay = 'true';
    } else {
      $latestposts_slider_autoplay = 'false';
    }

    if(!isset($theme_options['latestposts_slider_speed'])) {
      $latestposts_slider_speed = 7000;
    }
    else {
      $latestposts_slider_speed = $theme_options['latestposts_slider_speed'];
    }
    ?>
    <script>
    (function($){
    $(document).ready(function() {

        $('#portfolio-list').mixitup({effects:['<?php echo $theme_options['portfolio_posts_animation_1'] ;?>','<?php echo $theme_options['portfolio_posts_animation_2'] ;?>'],easing:'snap'});

        <?php if(isset($theme_options['team_slider_enable']) && $theme_options['team_slider_enable']): ?>
        $('.members-list').flexslider({
          animation: "slide",
          animationLoop: true,
          itemWidth: 300,
          slideshowSpeed: <?php echo $team_slider_speed; ?>,
          slideshow: <?php echo $team_slider_autoplay; ?>,
          controlNav: false,
          prevText: "",
          nextText: ""
        });
        <?php endif; ?>

        <?php if(isset($theme_options['latestposts_slider_enable']) && $theme_options['latestposts_slider_enable']): ?>
        $('.latest-posts-list').flexslider({
          animation: "slide",
          animationLoop: true,
          itemWidth: 300,
          slideshowSpeed: <?php echo $latestposts_slider_speed; ?>,
          slideshow: <?php echo $latestposts_slider_autoplay; ?>,
          controlNav: false,
          prevText: "",
          nextText: ""
        });
        <?php endif; ?>

        <?php if(isset($theme_options['enable_fixed_menu']) && $theme_options['enable_fixed_menu']): ?>
        // Scrolling menu
        $(".navbar").css({position: "fixed", top: "0px"});
        
        $navheight = $(".navbar").height() - 30;

        if(!$(".rev_slider_wrapper.fullscreen-container")[0]) {
          $(".navbar-inner").css("padding", "10px 0 0");
          $("body").css("padding-top", $navheight);
          $(".mask").css("margin-top", (-1)*$navheight);
        }
      
        <?php endif; ?>

      

        <?php if(isset($theme_options['enable_theme_preloader']) && $theme_options['enable_theme_preloader']): ?>
        //Page Preloader
        $(window).load(function() { 
            $("#preloader").delay(500).fadeOut(); 
            $(".mask").delay(1000).fadeOut("slow");
        });
        <?php endif; ?>

        <?php if(isset($theme_options['enable_theme_animations']) && $theme_options['enable_theme_animations']): ?>
        $(window).load(function() { 
          setTimeout(function(){ $('.page-item-title.container-fluid').css('height','100px'); }, 1000);
        });

        if($('.left').length>0)
        $('.left').appear(function() {
          $(this).css({left: 0});
        });

        if($('.right').length>0)
        $('.right').appear(function() {
          $(this).css({right: 0});
        });

        if($('.bottom').length>0)
        $('.bottom').appear(function() {
          $(this).css({bottom: 0});
        });

        if($('.counter-number').length>0)
        $('.counter-number').appear(function() {
          $(this).countTo();
        });
        <?php else: ?>
        $(window).load(function() { 
          $('.page-item-title.container').css('height','100px');
          $('.page-item-title.container').css('-webkit-transition','none');
          $('.page-item-title.container').css('transition','none');
          
        });
        <?php endif; ?>

        <?php if(isset($theme_options['enable_parallax']) && $theme_options['enable_parallax']): ?>
  
        $('.parallax').each(function(){
           $(this).parallax("50%", 0.1);
        });

        <?php endif; ?>
        

    });
    })(jQuery);
    </script>
    <?php 

    if(isset($theme_options['retina_logo']) && $theme_options['retina_logo']): ?>
    <?php 
    if(!isset($theme_options['retina_logo_width'])) {
        $retina_logo_width = 166;
    } else {
        $retina_logo_width = $theme_options['retina_logo_width'];
    }
    ?>
    <style>
    .brand img {
        width: <?php echo $retina_logo_width / 2; ?>px;
    }
    </style>
    <?php endif; ?>
    <?php if(isset($theme_options['center_logo_position']) && $theme_options['center_logo_position']): ?>
        <style>
        @media (min-width: 1025px) {
            .navbar-toggle-container {
                float: none;
                text-align: center;
                margin: 0 auto;
                display: table;
                margin-bottom: 10px;
            }
            .navbar .nav {
                display: table;
                float: none;
                margin: 0 auto;
            }
        }
        </style>
    <?php endif; ?>
    <?php if(!isset($theme_options['team_slider_enable']) || !$theme_options['team_slider_enable']): ?>
        <style>
        .members-item {
          float: left;
        }
        </style>
    <?php endif; ?>
    <?php if(isset($theme_options['enable_theme_preloader']) && !$theme_options['enable_theme_preloader']): ?>
        <style>
        .mask {
            display: none;
        }
        </style>
    <?php endif; ?>
    <?php if(isset($theme_options['enable_parallax']) && !$theme_options['enable_parallax']): ?>
        <style>
        .content-block, #main-slider.flexslider li {
            background-attachment: inherit!important;
        }
        </style>
    <?php endif; ?>
    <?php if(isset($theme_options['enable_theme_animations']) && $theme_options['enable_theme_animations']): ?>
        <style>
        .left {
            position: relative;
            left: -200px;
            -webkit-transition: all .6s;
            transition: all .6s;
        }
        .right {
            position: relative;
            right: -200px;
            -webkit-transition: all .6s;
            transition: all .6s;
        }
        .bottom {
            position: relative;
            bottom: -200px;
            -webkit-transition: all .6s;
            transition: all .6s;
        }
        </style>
    <?php endif; ?>
    <?php
   
}
add_action( 'wp_head' , 'fencer_js_settings' );

function fencer_custom_favicon() {
    global $theme_options;

    if(isset($theme_options['favicon_image']) && $theme_options['favicon_image']['url'] <> '') {
      echo '<link rel="icon" type="image/png" href="'.$theme_options['favicon_image']['url'].'" />';
    }

}
add_action( 'wp_head' , 'fencer_custom_favicon' );

function fencer_google_fonts() {
    global $theme_options;
    ?>
    <link href='//fonts.googleapis.com/css?family=<?php if(isset($theme_options['header_font'])) { echo $theme_options['header_font']['font-family']; } ?>:400,400italic,300italic,300,500,500italic,700,700italic<?php if(isset($theme_options['font_cyrillic_enable']) && ($theme_options['font_cyrillic_enable'])) { echo '&subset=cyrillic'; } ?>' rel='stylesheet' type='text/css'>
    <?php if($theme_options['header_font']['font-family'] <> $theme_options['body_font']['font-family']): ?>
    <link href='//fonts.googleapis.com/css?family=<?php echo $theme_options['body_font']['font-family']; ?><?php if(isset($theme_options['font_cyrillic_enable']) && ($theme_options['font_cyrillic_enable'])) { echo '&subset=cyrillic'; } ?>' rel='stylesheet' type='text/css'>
    <?php endif; ?>
    <?php
}
add_action( 'wp_head' , 'fencer_google_fonts' );


function fencer_custom_styles() {
    global $theme_options;
    ?>
    
    <style type="text/css">
    /**
    * Custom CSS
    **/
    <?php if(isset($theme_options['custom_css_code'])) { echo $theme_options['custom_css_code']; } ?>
    
    /**
    * Portfolio elements width
    **/
    .portfolio-item-block {
      width: <?php echo $theme_options['portfolio_element_width']; ?>%;
    }
    
    /** 
    * Theme Google Font
    **/
    
    h1, h2, h3, h4, h5, h6 {
        font-family: '<?php echo str_replace("+"," ", $theme_options['header_font']['font-family']); ?>';
    }
    h1 {
        font-size: <?php echo $theme_options['header_font']['font-size']; ?>px;
    }
    body {
        font-family: '<?php echo str_replace("+"," ", $theme_options['body_font']['font-family']); ?>';
        font-size: <?php echo $theme_options['body_font']['font-size']; ?>px;
    }
    /**
    * Colors and color skins
    */
    <?php
    if(!isset($theme_options['color_skin_name'])) {
        $color_skin_name = 'none';
    }
    else {
        $color_skin_name = $theme_options['color_skin_name'];
    }
    // Use panel settings
    if($color_skin_name == 'none') {

        $theme_body_color = $theme_options['theme_body_color'];
        $theme_main_color = $theme_options['theme_main_color'];
        $theme_button_hover_color = $theme_options['theme_button_hover_color'];
        $theme_menu_color = $theme_options['theme_menu_color'];
        $theme_menu_link_color = $theme_options['theme_menu_link_color'];
        $theme_menu_submenu_link_color = $theme_options['theme_menu_submenu_link_color'];
        $theme_footer_color = $theme_options['theme_footer_color'];
        $theme_title_color = $theme_options['theme_title_color'];
        $theme_title_bg_color = $theme_options['theme_title_bg_color'];
        $theme_portfolio_hover_color = $theme_options['theme_portfolio_hover_color'];
        $theme_invert_bg_color = $theme_options['theme_invert_bg_color'];
        $theme_widget_title_color = $theme_options['theme_widget_title_color'];
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Default skin
    if($color_skin_name == 'default') {

        $theme_body_color = '#F1F3F4';
        $theme_main_color = '#26CDB3';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#000000';
        $theme_menu_link_color = '#717171';
        $theme_menu_submenu_link_color = '#FFFFFF';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#000000';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#313A3D';
        $theme_invert_bg_color = '#1D262A';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Green skin
    if($color_skin_name == 'green') {

        $theme_body_color = '#F1F3F4';
        $theme_main_color = '#83d465';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#000000';
        $theme_menu_link_color = '#83d465';
        $theme_menu_submenu_link_color = '#83d465';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#83d465';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#83d465';
        $theme_invert_bg_color = '#19211a';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Blue skin
    if($color_skin_name == 'blue') {

        $theme_body_color = '#e6f3fa';
        $theme_main_color = '#65abd4';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#0a75a3';
        $theme_menu_link_color = '#d0dfe8';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#0a75a3';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#0a75a3';
        $theme_invert_bg_color = '#18191f';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Red skin
    if($color_skin_name == 'red') {

        $theme_body_color = '#F1F3F4';
        $theme_main_color = '#db2c2c';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#c23838';
        $theme_menu_link_color = '#e6cfcf';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#c23838';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#c23838';
        $theme_invert_bg_color = '#1f1818';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Black skin
    if($color_skin_name == 'black') {

        $theme_body_color = '#f5f5f5';
        $theme_main_color = '#787878';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#000000';
        $theme_menu_link_color = '#b8b8b8';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#000000';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#000000';
        $theme_invert_bg_color = '#292929';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }

    // Pink skin
    if($color_skin_name == 'pink') {

        $theme_body_color = '#f5f5f5';
        $theme_main_color = '#fa5ffa';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#a639a6';
        $theme_menu_link_color = '#faaafa';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#fa5ffa';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#fa5ffa';
        $theme_invert_bg_color = '#3b083b';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }

    // Orange skin
    if($color_skin_name == 'orange') {

        $theme_body_color = '#f5f5f5';
        $theme_main_color = '#facc5f';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#f7b825';
        $theme_menu_link_color = '#faf6d5';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#facc5f';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#facc5f';
        $theme_invert_bg_color = '#303030';
        $theme_widget_title_color = '#A3A3A4';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Simplegreat skin
    if($color_skin_name == 'simplegreat') {

        $theme_body_color = '#F6F7F8';
        $theme_main_color = '#C2A26F';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#4A5456';
        $theme_menu_link_color = '#BABAB8';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#818181';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#C2A26F';
        $theme_invert_bg_color = '#3D4445';
        $theme_widget_title_color = '#818181';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Perfectum skin
    if($color_skin_name == 'perfectum') {

        $theme_body_color = '#f5f5f5';
        $theme_main_color = '#F2532F';
        $theme_button_hover_color = '#000000';
        $theme_menu_color = '#000000';
        $theme_menu_link_color = '#F2532F';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#F2532F';
        $theme_title_bg_color = '#FFFFFF';
        $theme_portfolio_hover_color = '#F2532F';
        $theme_invert_bg_color = '#3D4445';
        $theme_widget_title_color = '#F2532F';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    // Metro skin
    if($color_skin_name == 'metro') {

        $theme_body_color = '#F7F7F9';
        $theme_main_color = '#008C8D';
        $theme_button_hover_color = '#58BAE9';
        $theme_menu_color = '#414E5B';
        $theme_menu_link_color = '#e3e3e3';
        $theme_menu_submenu_link_color = '#ffffff';
        $theme_footer_color = '#FFFFFF';
        $theme_title_color = '#ffffff';
        $theme_title_bg_color = '#6CBE42';
        $theme_portfolio_hover_color = '#FFAA31';
        $theme_invert_bg_color = '#414E5B';
        $theme_widget_title_color = '#008C8D';
        $theme_preloader_bg_color = $theme_options['theme_preloader_bg_color'];

    }
    ?>
    a,
    a:hover, 
    a:focus,
    .page-header-title a:hover,
    .navbar .btn-navbar:hover,
    .navbar .btn-navbar:focus,
    .navbar .btn-navbar:active,
    .navbar .btn-navbar.active,
    .navbar .btn-navbar.disabled,
    .navbar .btn-navbar[disabled],
    .members-list .flex-direction-nav a,
    .latest-posts-list .post-item .date,
    .collapsible-page .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header:hover a,
    .collapsible-page .wpb_accordion .wpb_accordion_wrapper .wpb_accordion_header:hover .ui-icon,
    .loop-item-nav a,
    .portfolio-items-categories a:hover,
    .post-social a:hover,
    .latest-posts-list .flex-direction-nav a,
    .navigation-paging a,
    .woocommerce .star-rating,
    .container .products-slider .flexslider .flex-direction-nav .flex-prev:before,
    .container .products-slider .flexslider .flex-direction-nav .flex-next:before,
    .container .wpb_toggle:before,
    .container #content h4.wpb_toggle:before,
    .content-block .wpb_row h3,
    .page-404 h1 span {
        color: <?php echo $theme_main_color; ?>;
    }
    a.btn,
    .btn,
    .btn:focus, 
    input[type="submit"],
    .btn-primary,
    .btn-primary:focus,
    .navbar .btn-navbar .icon-bar,
    .members-item .social a:hover,
    .members-list .flex-direction-nav a:hover,
    .latest-posts-list .flex-direction-nav a:hover,
    .portfolio-filter a:hover, 
    .portfolio-filter a.active,
    .portfolio-more-button,
    .pricing-table .price-table-head,
    .pricing-table .wpb_button:hover,
    .portfolio-items-categories a,
    .blog-post .post-date,
    .blog-post .post-format,
    .blog-post .format-quote blockquote,
    .footer-social a:hover,
    #top-link,
    .latest-posts-list .flex-direction-nav a:hover,
    .navigation-paging a:hover,
    .sidebar .widget_calendar th,
    .sidebar .widget_calendar tfoot td,
    .woocommerce span.onsale, 
    .woocommerce-page span.onsale,
    .woocommerce .widget_layered_nav ul li.chosen a, 
    .woocommerce-page .widget_layered_nav ul li.chosen a,
    .woocommerce .widget_layered_nav_filters ul li a, 
    .woocommerce-page .widget_layered_nav_filters ul li a,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-handle, 
    .woocommerce-page .widget_price_filter .ui-slider .ui-slider-handle,
    .woocommerce .widget_price_filter .ui-slider .ui-slider-range,
    .woocommerce-page .widget_price_filter .ui-slider .ui-slider-range,
    .woocommerce #content input.button, 
    .woocommerce #respond input#submit, 
    .woocommerce a.button, 
    .woocommerce button.button, 
    .woocommerce input.button, 
    .woocommerce-page #content input.button, 
    .woocommerce-page #respond input#submit, 
    .woocommerce-page a.button, 
    .woocommerce-page button.button, 
    .woocommerce-page input.button,
    .woocommerce #content input.button.alt:hover, 
    .woocommerce #respond input#submit.alt:hover, 
    .woocommerce a.button.alt:hover, 
    .woocommerce button.button.alt:hover, 
    .woocommerce input.button.alt:hover, 
    .woocommerce-page #content input.button.alt:hover, 
    .woocommerce-page #respond input#submit.alt:hover, 
    .woocommerce-page a.button.alt:hover, 
    .woocommerce-page button.button.alt:hover, 
    .woocommerce-page input.button.alt:hover,
    .woocommerce div.product .woocommerce-tabs ul.tabs li, 
    .woocommerce #content div.product .woocommerce-tabs ul.tabs li, 
    .woocommerce-page div.product .woocommerce-tabs ul.tabs li, 
    .woocommerce-page #content div.product .woocommerce-tabs ul.tabs li,
    .wpb_button,
    .comment-meta .reply a,
    .nav .sub-menu li a:hover,
    .nav .children li a:hover,
    .bubblingG span {
        background-color: <?php echo $theme_main_color; ?>;
    }
    .latest-posts-list .flex-direction-nav a,
    .pricing-table .wpb_button:hover,
    .loop-item-nav a:hover,
    .latest-posts-list .flex-direction-nav a,
    .navigation-paging a,
    .sidebar .widget_calendar tbody td a,
    .woocommerce .widget_layered_nav ul li.chosen a, 
    .woocommerce-page .widget_layered_nav ul li.chosen a,
    .woocommerce .widget_layered_nav_filters ul li a, 
    .woocommerce-page .widget_layered_nav_filters ul li a,
    blockquote,
    .nav .sub-menu li:first-child a,
    .nav .children li:first-child a {
        border-color: <?php echo $theme_main_color; ?>;
    }
    .navbar-inner,
    .nav .sub-menu li a, 
    .nav .children li a {
        background-color: <?php echo $theme_menu_color; ?>;
    }
    .navbar .nav > li > a {
        color: <?php echo $theme_menu_link_color; ?>;
    }
    .nav .sub-menu li a, 
    .nav .children li a {
        color: <?php echo $theme_menu_submenu_link_color; ?>;
    }
    footer {
        background-color: <?php echo $theme_footer_color; ?>;
    }
    body {
        background-color: <?php echo $theme_body_color; ?>;
    }
    .page-item-title.container {
        background-color: <?php echo $theme_title_bg_color; ?>;
    }
    .page-item-title h1 {
        color: <?php echo $theme_title_color; ?>;
    }
    .portfolio-item-block:hover {
        background-color: <?php echo $theme_portfolio_hover_color; ?>;
    }
    .content-block.invert {
        background-color: <?php echo $theme_invert_bg_color; ?>;
    }
    .sidebar .widgettitle {
        color: <?php echo $theme_widget_title_color; ?>;
    }
    input[type="submit"]:hover,
    .woocommerce #content input.button:hover, 
    .woocommerce #respond input#submit:hover, 
    .woocommerce a.button:hover, 
    .woocommerce button.button:hover, 
    .woocommerce input.button:hover,
    .woocommerce-page #content input.button:hover, 
    .woocommerce-page #respond input#submit:hover, 
    .woocommerce-page a.button:hover, 
    .woocommerce-page button.button:hover, 
    .woocommerce-page input.button:hover,
    .woocommerce #content input.button.alt, 
    .woocommerce #respond input#submit.alt, 
    .woocommerce a.button.alt, 
    .woocommerce button.button.alt, 
    .woocommerce input.button.alt, 
    .woocommerce-page #content input.button.alt, 
    .woocommerce-page #respond input#submit.alt,
    .woocommerce-page a.button.alt, 
    .woocommerce-page button.button.alt, 
    .woocommerce-page input.button.alt {
        background-color: <?php echo $theme_button_hover_color; ?>;
    }
    .mask {
        background-color: <?php echo $theme_preloader_bg_color; ?>;
    }
    <?php 
    if(isset($theme_options['enable_transparent_menu']) && ($theme_options['enable_transparent_menu'])):
    ?>
    .navbar-inner {
        <?php if($theme_options['transparent_menu_bg'] == 'white'): ?>
        background-color: rgba(255, 255, 255, <?php echo $theme_options['transparent_menu_level']/10; ?>);
        <?php endif; ?>

        <?php if($theme_options['transparent_menu_bg'] == 'black'): ?>
        background-color: rgba(0, 0, 0, <?php echo $theme_options['transparent_menu_level']/10; ?>);
        <?php endif; ?>
    }
    <?php endif; ?>
    <?php 
    // Google Chrome Fix
    $chrome = strpos($_SERVER["HTTP_USER_AGENT"], 'Chrome') ? true : false;
    if($chrome):
    ?>
        .intro-block #main-slider.flexslider li {
            background-attachment: inherit;!important;
        } 
    <?php else: ?>
        .intro-block #main-slider.flexslider li {
            background-attachment: fixed!important;
        } 
    <?php endif; ?>
    </style>
    <script>
    <?php if(isset($theme_options['custom_js_code'])) { echo $theme_options['custom_js_code']; } ?>
    </script>
    <?php
}
add_action( 'wp_head' , 'fencer_custom_styles' );
