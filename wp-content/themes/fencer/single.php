<?php
/**
 * The Template for displaying all single posts.
 *
 * @package Fencer
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( 'content', 'single' ); ?>
	<div class="container">
	<?php fencer_content_nav( 'nav-below' ); ?>
	</div>


<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>